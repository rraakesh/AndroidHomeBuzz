package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 12/12/2015.
 */
public class GetAverageEstimate extends UseCase{

    private final UserRepository userRepository;
    private WeakHashMap<String, String> estimateAverageData;
    @Inject
    public GetAverageEstimate(UserRepository userRepository,
                         ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override protected Observable buildUseCaseObservable() {
        return this.userRepository.userEstimateAverage(this.estimateAverageData);
    }
    public void setEstimateAverage(WeakHashMap<String, String> estimateAverageData) {
        this.estimateAverageData = estimateAverageData;
    }
}
