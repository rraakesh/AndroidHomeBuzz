/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.domain.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a User in the domain layer.
 */
public class User_ {
  private String token;
  private Integer id;

  private String email;
  private String name;

  private String zipCode;

  private String role;

  private Object facebookId;

  private String createdAt;

  private String updatedAt;

  private Integer estimatesCount;

  private Double accurate;

  private Integer sashId;

  private Integer level;

  private String verificationCode;

  private String phone;

  private List<String> estimateZips = new ArrayList<String>();

  private Object teamId;

  private Object teamRequest;

  private Object profilePic;

  private Object officeId;

  private Object officeRequest;

  /**
   *
   * @return
   * The id
   */
  public Integer getId() {
    return id;
  }

  /**
   *
   * @param id
   * The id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   *
   * @return
   * The email
   */
  public String getEmail() {
    return email;
  }

  /**
   *
   * @param email
   * The email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   *
   * @return
   * The name
   */
  public String getName() {
    return name;
  }

  /**
   *
   * @param name
   * The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   *
   * @return
   * The zipCode
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   *
   * @param zipCode
   * The zip_code
   */
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   *
   * @return
   * The role
   */
  public String getRole() {
    return role;
  }

  /**
   *
   * @param role
   * The role
   */
  public void setRole(String role) {
    this.role = role;
  }

  /**
   *
   * @return
   * The facebookId
   */
  public Object getFacebookId() {
    return facebookId;
  }

  /**
   *
   * @param facebookId
   * The facebook_id
   */
  public void setFacebookId(Object facebookId) {
    this.facebookId = facebookId;
  }

  /**
   *
   * @return
   * The createdAt
   */
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   *
   * @param createdAt
   * The created_at
   */
  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  /**
   *
   * @return
   * The updatedAt
   */
  public String getUpdatedAt() {
    return updatedAt;
  }

  /**
   *
   * @param updatedAt
   * The updated_at
   */
  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  /**
   *
   * @return
   * The estimatesCount
   */
  public Integer getEstimatesCount() {
    return estimatesCount;
  }

  /**
   *
   * @param estimatesCount
   * The estimates_count
   */
  public void setEstimatesCount(Integer estimatesCount) {
    this.estimatesCount = estimatesCount;
  }

  /**
   *
   * @return
   * The accurate
   */
  public Double getAccurate() {
    return accurate;
  }

  /**
   *
   * @param accurate
   * The accurate
   */
  public void setAccurate(Double accurate) {
    this.accurate = accurate;
  }

  /**
   *
   * @return
   * The sashId
   */
  public Integer getSashId() {
    return sashId;
  }

  /**
   *
   * @param sashId
   * The sash_id
   */
  public void setSashId(Integer sashId) {
    this.sashId = sashId;
  }

  /**
   *
   * @return
   * The level
   */
  public Integer getLevel() {
    return level;
  }

  /**
   *
   * @param level
   * The level
   */
  public void setLevel(Integer level) {
    this.level = level;
  }

  /**
   *
   * @return
   * The verificationCode
   */
  public String getVerificationCode() {
    return verificationCode;
  }

  /**
   *
   * @param verificationCode
   * The verification_code
   */
  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }
  /**
   *
   * @return
   * The token
   */
  public String getToken() {
    return token;
  }

  /**
   *
   * @param token
   * The token
   */
  public void setToken(String token) {
    this.token = token;
  }
  /**
   *
   * @return
   * The phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   *
   * @param phone
   * The phone
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   *
   * @return
   * The estimateZips
   */
  public List<String> getEstimateZips() {
    return estimateZips;
  }

  /**
   *
   * @param estimateZips
   * The estimate_zips
   */
  public void setEstimateZips(List<String> estimateZips) {
    this.estimateZips = estimateZips;
  }

  /**
   *
   * @return
   * The teamId
   */
  public Object getTeamId() {
    return teamId;
  }

  /**
   *
   * @param teamId
   * The team_id
   */
  public void setTeamId(Object teamId) {
    this.teamId = teamId;
  }

  /**
   *
   * @return
   * The teamRequest
   */
  public Object getTeamRequest() {
    return teamRequest;
  }

  /**
   *
   * @param teamRequest
   * The team_request
   */
  public void setTeamRequest(Object teamRequest) {
    this.teamRequest = teamRequest;
  }

  /**
   *
   * @return
   * The profilePic
   */
  public Object getProfilePic() {
    return profilePic;
  }

  /**
   *
   * @param profilePic
   * The profile_pic
   */
  public void setProfilePic(Object profilePic) {
    this.profilePic = profilePic;
  }

  /**
   *
   * @return
   * The officeId
   */
  public Object getOfficeId() {
    return officeId;
  }

  /**
   *
   * @param officeId
   * The office_id
   */
  public void setOfficeId(Object officeId) {
    this.officeId = officeId;
  }

  /**
   *
   * @return
   * The officeRequest
   */
  public Object getOfficeRequest() {
    return officeRequest;
  }

  /**
   *
   * @param officeRequest
   * The office_request
   */
  public void setOfficeRequest(Object officeRequest) {
    this.officeRequest = officeRequest;
  }

//  @Override public String toString() {
//    StringBuilder stringBuilder = new StringBuilder();
//
//    stringBuilder.append("***** User Details *****\n");
//    stringBuilder.append("id=" + this.getUserId() + "\n");
//    stringBuilder.append("cover url=" + this.getCoverUrl() + "\n");
//    stringBuilder.append("fullname=" + this.getFullName() + "\n");
//    stringBuilder.append("email=" + this.getEmail() + "\n");
//    stringBuilder.append("description=" + this.getDescription() + "\n");
//    stringBuilder.append("followers=" + this.getFollowers() + "\n");
//    stringBuilder.append("*******************************");
//
//    return stringBuilder.toString();
//  }
}
