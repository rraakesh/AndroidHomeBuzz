package us.homebuzz.homebuzz.domain.interactor;

import java.util.SortedMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/13/2015.
 */
public class GetNearbyListing extends UseCase {
    private SortedMap<String,String> nearbyData;
    private final UserRepository userRepository;
    int  user_id;
    @Inject
    public GetNearbyListing( UserRepository userRepository,
                               ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override protected Observable buildUseCaseObservable() {
        return this.userRepository.userNearby(/*this.lat, this.lon, this.radius*/this.user_id,this.nearbyData);
    }

  //TODO Later TO BE used For the Filter MODULE AS WELL
    public void setNearbyData(int user_id,SortedMap<String,String> nearbyData){
        this.user_id=user_id;
      this.nearbyData=nearbyData;
    }
}
