package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by rohitkumar on 6/11/15.
 */
public class GetUpdateChecking extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> updateCheck_in;
    private String listing_id;
    @Inject
    public GetUpdateChecking( UserRepository userRepository,
                             ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userUpdateCheck_in(this.listing_id,this.updateCheck_in);
    }
    public void setUpdateChecking(String listing_id,WeakHashMap<String, String> updateCheck_in){
        this.listing_id=listing_id;
        this.updateCheck_in = updateCheck_in;
    }
}
