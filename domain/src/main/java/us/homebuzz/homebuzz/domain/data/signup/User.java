package us.homebuzz.homebuzz.domain.data.signup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 10/31/2015.
 */
public class User {

    private Integer id;

    private String email;

    private Object name;

    private String zipCode;

    private String role;

    private Object facebookId;

    private String createdAt;

    private String updatedAt;

    private Integer estimatesCount;

    private Double accurate;

    private Object sashId;

    private Integer level;

    private Object verificationCode;

    private String phone;

    private List<Object> estimateZips = new ArrayList<Object>();

    private Object teamId;

    private Object teamRequest;

    private Object profilePic;

    private Object officeId;

    private Object officeRequest;

    private Object firstName;

    private Object lastName;

    private Object invitationToken;

    private Object invitationCreatedAt;

    private Object invitationSentAt;

    private Object invitationAcceptedAt;

    private Object invitationLimit;

    private Object invitedById;

    private Object invitedByType;

    private Integer invitationsCount;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The name
     */
    public Object getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(Object name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public Object getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The sashId
     */
    public Object getSashId() {
        return sashId;
    }

    /**
     *
     * @param sashId
     * The sash_id
     */
    public void setSashId(Object sashId) {
        this.sashId = sashId;
    }

    /**
     *
     * @return
     * The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     *
     * @param level
     * The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     *
     * @return
     * The verificationCode
     */
    public Object getVerificationCode() {
        return verificationCode;
    }

    /**
     *
     * @param verificationCode
     * The verification_code
     */
    public void setVerificationCode(Object verificationCode) {
        this.verificationCode = verificationCode;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The estimateZips
     */
    public List<Object> getEstimateZips() {
        return estimateZips;
    }

    /**
     *
     * @param estimateZips
     * The estimate_zips
     */
    public void setEstimateZips(List<Object> estimateZips) {
        this.estimateZips = estimateZips;
    }

    /**
     *
     * @return
     * The teamId
     */
    public Object getTeamId() {
        return teamId;
    }

    /**
     *
     * @param teamId
     * The team_id
     */
    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    /**
     *
     * @return
     * The teamRequest
     */
    public Object getTeamRequest() {
        return teamRequest;
    }

    /**
     *
     * @param teamRequest
     * The team_request
     */
    public void setTeamRequest(Object teamRequest) {
        this.teamRequest = teamRequest;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public Object getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The officeId
     */
    public Object getOfficeId() {
        return officeId;
    }

    /**
     *
     * @param officeId
     * The office_id
     */
    public void setOfficeId(Object officeId) {
        this.officeId = officeId;
    }

    /**
     *
     * @return
     * The officeRequest
     */
    public Object getOfficeRequest() {
        return officeRequest;
    }

    /**
     *
     * @param officeRequest
     * The office_request
     */
    public void setOfficeRequest(Object officeRequest) {
        this.officeRequest = officeRequest;
    }

    /**
     *
     * @return
     * The firstName
     */
    public Object getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public Object getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The invitationToken
     */
    public Object getInvitationToken() {
        return invitationToken;
    }

    /**
     *
     * @param invitationToken
     * The invitation_token
     */
    public void setInvitationToken(Object invitationToken) {
        this.invitationToken = invitationToken;
    }

    /**
     *
     * @return
     * The invitationCreatedAt
     */
    public Object getInvitationCreatedAt() {
        return invitationCreatedAt;
    }

    /**
     *
     * @param invitationCreatedAt
     * The invitation_created_at
     */
    public void setInvitationCreatedAt(Object invitationCreatedAt) {
        this.invitationCreatedAt = invitationCreatedAt;
    }

    /**
     *
     * @return
     * The invitationSentAt
     */
    public Object getInvitationSentAt() {
        return invitationSentAt;
    }

    /**
     *
     * @param invitationSentAt
     * The invitation_sent_at
     */
    public void setInvitationSentAt(Object invitationSentAt) {
        this.invitationSentAt = invitationSentAt;
    }

    /**
     *
     * @return
     * The invitationAcceptedAt
     */
    public Object getInvitationAcceptedAt() {
        return invitationAcceptedAt;
    }

    /**
     *
     * @param invitationAcceptedAt
     * The invitation_accepted_at
     */
    public void setInvitationAcceptedAt(Object invitationAcceptedAt) {
        this.invitationAcceptedAt = invitationAcceptedAt;
    }

    /**
     *
     * @return
     * The invitationLimit
     */
    public Object getInvitationLimit() {
        return invitationLimit;
    }

    /**
     *
     * @param invitationLimit
     * The invitation_limit
     */
    public void setInvitationLimit(Object invitationLimit) {
        this.invitationLimit = invitationLimit;
    }

    /**
     *
     * @return
     * The invitedById
     */
    public Object getInvitedById() {
        return invitedById;
    }

    /**
     *
     * @param invitedById
     * The invited_by_id
     */
    public void setInvitedById(Object invitedById) {
        this.invitedById = invitedById;
    }

    /**
     *
     * @return
     * The invitedByType
     */
    public Object getInvitedByType() {
        return invitedByType;
    }

    /**
     *
     * @param invitedByType
     * The invited_by_type
     */
    public void setInvitedByType(Object invitedByType) {
        this.invitedByType = invitedByType;
    }

    /**
     *
     * @return
     * The invitationsCount
     */
    public Integer getInvitationsCount() {
        return invitationsCount;
    }

    /**
     *
     * @param invitationsCount
     * The invitations_count
     */
    public void setInvitationsCount(Integer invitationsCount) {
        this.invitationsCount = invitationsCount;
    }

}
