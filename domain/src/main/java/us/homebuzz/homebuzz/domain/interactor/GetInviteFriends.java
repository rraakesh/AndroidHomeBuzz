package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class GetInviteFriends extends UseCase{
    private final UserRepository userRepository;
    private WeakHashMap<String, String> inviteFriendsCredential;

    public GetInviteFriends(UserRepository userRepository,
                            ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }


    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userInviteFriends(this.inviteFriendsCredential);
    }
    public void setInviteFriends(WeakHashMap<String, String> inviteFriendsCredential) {
        this.inviteFriendsCredential = inviteFriendsCredential;
    }
}
