package us.homebuzz.homebuzz.domain.data;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
public class FollowUnfollowData {

    private Boolean followed;

    /**
     *
     * @return
     * The followed
     */
    public Boolean getFollowed() {
        return followed;
    }

    /**
     *
     * @param followed
     * The followed
     */
    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

}
