package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 12/1/2015.
 */
public class EditUserOtherProfile extends UseCase {
    private final UserRepository userRepository;

    WeakHashMap<String, String> userData;
    WeakHashMap<String, String> userProfilePic;
    @Inject
    public EditUserOtherProfile(UserRepository userRepository, ThreadExecutor threadExecutor,
                           PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.userRepository.userOtherEditProfile(this.userProfilePic,this.userData);
    }

    public void setUserData(WeakHashMap<String, String> ProfileImage, WeakHashMap<String, String> userData) {
        this.userProfilePic=ProfileImage;
        this.userData = userData;
    }
}