package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/13/2015.
 */
public class EditUserProfile extends UseCase {

    private final UserRepository userRepository;

    WeakHashMap<String,String> userData ;
    WeakHashMap<String,String> userProfilePic ;

    @Inject
    public EditUserProfile(UserRepository userRepository, ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.userRepository.userEditProfile(this.userProfilePic,this.userData);
    }

    public void setUserData(WeakHashMap<String,String> userProfilePic,WeakHashMap<String,String> userData) {
        this.userData = userData;
        this.userProfilePic = userProfilePic;
    }
}
