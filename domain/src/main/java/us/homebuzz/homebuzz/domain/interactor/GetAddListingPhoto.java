package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/5/2015.
 */
public class GetAddListingPhoto extends UseCase {

    private WeakHashMap<String, String> photoImages;
    private WeakHashMap<String, String> AddListingData;
    private String listing_id;
    private final UserRepository userRepository;

    @Inject
    public GetAddListingPhoto(UserRepository userRepository,
                         ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userAddListingPhoto(this.listing_id,this.photoImages,this.AddListingData);
    }

    public void setUserAddListingPhoto(String listing_id,WeakHashMap<String, String> photoImages,WeakHashMap<String, String> AddListingData) {
       this.listing_id=listing_id;
        this.AddListingData = AddListingData;
        this.photoImages = photoImages;
    }
}
