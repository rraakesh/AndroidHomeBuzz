package us.homebuzz.homebuzz.domain.data.zipcode;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipCodePurchaseDomain {

    private Integer id;

    private Integer userId;

    private String zipCode;

    private Boolean expired;

    private String expiredAt;
    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The expired
     */
    public Boolean getExpired() {
        return expired;
    }

    /**
     *
     * @param expired
     * The expired
     */
    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    /**
     *
     * @return
     * The expiredAt
     */
    public String getExpiredAt() {
        return expiredAt;
    }

    /**
     *
     * @param expiredAt
     * The expired_at
     */
    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }
}
