package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 11/5/2015.
 */
public class GetMessageReply extends UseCase {
    private final UserRepository userRepository;
    private WeakHashMap<String, String> replyMessageCredential;

    @Inject
    public GetMessageReply(UserRepository userRepository,
                           ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userReplyMessages(this.replyMessageCredential);
    }

    public void setMessageReply(WeakHashMap<String, String> replyMessageCredential) {
        this.replyMessageCredential = replyMessageCredential;
    }

}