package us.homebuzz.homebuzz.domain.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class SubscriptionDomain {
    private Integer id;
    private String email;
    private String name;
    private String zipCode;
    private String role;
    private Object facebookId;
    private String createdAt;
    private String updatedAt;
    private Integer estimatesCount;
    private Double accurate;
    private Integer sashId;
    private Integer level;
    private String verificationCode;
    private String phone;
    private List<String> estimateZips = new ArrayList<String>();
    private Object teamId;
    private Object teamRequest;
    private String profilePic;
    private Object officeId;
    private Object officeRequest;
    private String firstName;
    private String lastName;
    private Object invitationToken;
    private Object invitationCreatedAt;
    private Object invitationSentAt;
    private Object invitationAcceptedAt;
    private Object invitationLimit;
    private Object invitedById;
    private Object invitedByType;
    private Integer invitationsCount;
    private String iapReceipt;
    private Boolean subscriptionActive;
    private Boolean notifyJustListed;
    private Boolean notifyJustSold;
    private Boolean notifyCheckins;
    private Boolean notifyOffers;
    private String androidReceipt;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public Object getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The sashId
     */
    public Integer getSashId() {
        return sashId;
    }

    /**
     *
     * @param sashId
     * The sash_id
     */
    public void setSashId(Integer sashId) {
        this.sashId = sashId;
    }

    /**
     *
     * @return
     * The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     *
     * @param level
     * The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     *
     * @return
     * The verificationCode
     */
    public String getVerificationCode() {
        return verificationCode;
    }

    /**
     *
     * @param verificationCode
     * The verification_code
     */
    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The estimateZips
     */
    public List<String> getEstimateZips() {
        return estimateZips;
    }

    /**
     *
     * @param estimateZips
     * The estimate_zips
     */
    public void setEstimateZips(List<String> estimateZips) {
        this.estimateZips = estimateZips;
    }

    /**
     *
     * @return
     * The teamId
     */
    public Object getTeamId() {
        return teamId;
    }

    /**
     *
     * @param teamId
     * The team_id
     */
    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    /**
     *
     * @return
     * The teamRequest
     */
    public Object getTeamRequest() {
        return teamRequest;
    }

    /**
     *
     * @param teamRequest
     * The team_request
     */
    public void setTeamRequest(Object teamRequest) {
        this.teamRequest = teamRequest;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The officeId
     */
    public Object getOfficeId() {
        return officeId;
    }

    /**
     *
     * @param officeId
     * The office_id
     */
    public void setOfficeId(Object officeId) {
        this.officeId = officeId;
    }

    /**
     *
     * @return
     * The officeRequest
     */
    public Object getOfficeRequest() {
        return officeRequest;
    }

    /**
     *
     * @param officeRequest
     * The office_request
     */
    public void setOfficeRequest(Object officeRequest) {
        this.officeRequest = officeRequest;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The invitationToken
     */
    public Object getInvitationToken() {
        return invitationToken;
    }

    /**
     *
     * @param invitationToken
     * The invitation_token
     */
    public void setInvitationToken(Object invitationToken) {
        this.invitationToken = invitationToken;
    }

    /**
     *
     * @return
     * The invitationCreatedAt
     */
    public Object getInvitationCreatedAt() {
        return invitationCreatedAt;
    }

    /**
     *
     * @param invitationCreatedAt
     * The invitation_created_at
     */
    public void setInvitationCreatedAt(Object invitationCreatedAt) {
        this.invitationCreatedAt = invitationCreatedAt;
    }

    /**
     *
     * @return
     * The invitationSentAt
     */
    public Object getInvitationSentAt() {
        return invitationSentAt;
    }

    /**
     *
     * @param invitationSentAt
     * The invitation_sent_at
     */
    public void setInvitationSentAt(Object invitationSentAt) {
        this.invitationSentAt = invitationSentAt;
    }

    /**
     *
     * @return
     * The invitationAcceptedAt
     */
    public Object getInvitationAcceptedAt() {
        return invitationAcceptedAt;
    }

    /**
     *
     * @param invitationAcceptedAt
     * The invitation_accepted_at
     */
    public void setInvitationAcceptedAt(Object invitationAcceptedAt) {
        this.invitationAcceptedAt = invitationAcceptedAt;
    }

    /**
     *
     * @return
     * The invitationLimit
     */
    public Object getInvitationLimit() {
        return invitationLimit;
    }

    /**
     *
     * @param invitationLimit
     * The invitation_limit
     */
    public void setInvitationLimit(Object invitationLimit) {
        this.invitationLimit = invitationLimit;
    }

    /**
     *
     * @return
     * The invitedById
     */
    public Object getInvitedById() {
        return invitedById;
    }

    /**
     *
     * @param invitedById
     * The invited_by_id
     */
    public void setInvitedById(Object invitedById) {
        this.invitedById = invitedById;
    }

    /**
     *
     * @return
     * The invitedByType
     */
    public Object getInvitedByType() {
        return invitedByType;
    }

    /**
     *
     * @param invitedByType
     * The invited_by_type
     */
    public void setInvitedByType(Object invitedByType) {
        this.invitedByType = invitedByType;
    }

    /**
     *
     * @return
     * The invitationsCount
     */
    public Integer getInvitationsCount() {
        return invitationsCount;
    }

    /**
     *
     * @param invitationsCount
     * The invitations_count
     */
    public void setInvitationsCount(Integer invitationsCount) {
        this.invitationsCount = invitationsCount;
    }

    /**
     *
     * @return
     * The iapReceipt
     */
    public String getIapReceipt() {
        return iapReceipt;
    }

    /**
     *
     * @param iapReceipt
     * The iap_receipt
     */
    public void setIapReceipt(String iapReceipt) {
        this.iapReceipt = iapReceipt;
    }

    /**
     *
     * @return
     * The subscriptionActive
     */
    public Boolean getSubscriptionActive() {
        return subscriptionActive;
    }

    /**
     *
     * @param subscriptionActive
     * The subscription_active
     */
    public void setSubscriptionActive(Boolean subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    /**
     *
     * @return
     * The notifyJustListed
     */
    public Boolean getNotifyJustListed() {
        return notifyJustListed;
    }

    /**
     *
     * @param notifyJustListed
     * The notify_just_listed
     */
    public void setNotifyJustListed(Boolean notifyJustListed) {
        this.notifyJustListed = notifyJustListed;
    }

    /**
     *
     * @return
     * The notifyJustSold
     */
    public Boolean getNotifyJustSold() {
        return notifyJustSold;
    }

    /**
     *
     * @param notifyJustSold
     * The notify_just_sold
     */
    public void setNotifyJustSold(Boolean notifyJustSold) {
        this.notifyJustSold = notifyJustSold;
    }

    /**
     *
     * @return
     * The notifyCheckins
     */
    public Boolean getNotifyCheckins() {
        return notifyCheckins;
    }

    /**
     *
     * @param notifyCheckins
     * The notify_checkins
     */
    public void setNotifyCheckins(Boolean notifyCheckins) {
        this.notifyCheckins = notifyCheckins;
    }

    /**
     *
     * @return
     * The notifyOffers
     */
    public Boolean getNotifyOffers() {
        return notifyOffers;
    }

    /**
     *
     * @param notifyOffers
     * The notify_offers
     */
    public void setNotifyOffers(Boolean notifyOffers) {
        this.notifyOffers = notifyOffers;
    }

    /**
     *
     * @return
     * The androidReceipt
     */
    public String getAndroidReceipt() {
        return androidReceipt;
    }

    /**
     *
     * @param androidReceipt
     * The android_receipt
     */
    public void setAndroidReceipt(String androidReceipt) {
        this.androidReceipt = androidReceipt;
    }
}
