package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class GetSubscriptionInteractor extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> subscriptionPlanData;

    @Inject
    public GetSubscriptionInteractor(UserRepository userRepository,
                             ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userSubscription(this.subscriptionPlanData);
    }
    public void setSubPlan(WeakHashMap<String, String> subscriptionPlanData){
        this.subscriptionPlanData=subscriptionPlanData;

    }
}