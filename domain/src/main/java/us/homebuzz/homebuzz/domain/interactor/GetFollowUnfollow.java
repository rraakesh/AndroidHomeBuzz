package us.homebuzz.homebuzz.domain.interactor;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
public class GetFollowUnfollow extends UseCase {
    private final UserRepository userRepository;
    private int user_id;
    @Inject
    public GetFollowUnfollow(/*int user_id,*/UserRepository userRepository,
                                     ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        // System.out.println("UseCase UserID"+user_id);
        this.userRepository = userRepository;
        /*this.user_id=String.valueOf(user_id);*/
    }

    @Override protected Observable buildUseCaseObservable() {
        return this.userRepository.userFollowUnfollow(String.valueOf(this.user_id));
    }

    public void setUser_id(int user_id){
        this.user_id=user_id;
    }
}