package us.homebuzz.homebuzz.domain.data.profile;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class ProfileDomain {

    private Integer userId;

    private Integer id;

    private String email;
    private Integer signInCount;
    private String currentSignInAt;
    private String lastSignInAt;
    private Object confirmedAt;
    private String name;
    private String zipCode;
    private String role;
    private Object facebookId;
    private Double accurate;
    private Integer points;
    private Object teamId;
    private Object officeId;
    private String firstName;
    private String lastName;
    private Object subscriptionActive;
    private Boolean confirmed;
    private Boolean followed;
    private Integer soldListings;
    private Integer followers;
    private Integer followings;
    private Integer photos;
    private Integer estimatesCount;
    private Integer checkinCount;
    private Integer reviewsCount;
    private String educations;

    private String bio;

    private String degree;

    private Integer yrGraduated;

    private String college;

    private String otherSchool;

    private String designations;

    private String linkedIn;

    private String website;

    private String profilePic;

    private Object verified;

    private Integer agentId;

    private String createdAt;

    private String updatedAt;

    private String phone;

    private Object mlsName;

    private Object licenseNum;

    private Object napNum;

    private Object street;

    private Object city;

    private Object state;

    private Object jobTitle;

    private Object company;

    private Object facebookUrl;

    private Object twitterUrl;
    private Integer profile_id;
    /**
     * @return The userId
     */
    public Integer getProfileId() {
        return profile_id;
    }

    /**
     * @param profile_id The user_id
     */
    public void setProfileId(Integer profile_id) {
        this.profile_id = profile_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSignInCount() {
        return signInCount;
    }

    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    public String getLastSignInAt() {
        return lastSignInAt;
    }

    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

    public Object getConfirmedAt() {
        return confirmedAt;
    }

    public void setConfirmedAt(Object confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Double getAccurate() {
        return accurate;
    }

    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Object getTeamId() {
        return teamId;
    }

    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    public Object getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Object officeId) {
        this.officeId = officeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getSubscriptionActive() {
        return subscriptionActive;
    }

    public void setSubscriptionActive(Object subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Boolean getFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public Integer getSoldListings() {
        return soldListings;
    }

    public void setSoldListings(Integer soldListings) {
        this.soldListings = soldListings;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getFollowings() {
        return followings;
    }

    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    public Integer getPhotos() {
        return photos;
    }

    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }
    public Integer getCheckinCount() {
        return checkinCount;
    }

    public void setCheckinCount(Integer checkinCount) {
        this.checkinCount = checkinCount;
    }
    public Integer getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(Integer reviewsCount) {
        this.reviewsCount = reviewsCount;
    }


    public String getEducations() {
        return educations;
    }

    public void setEducations(String educations) {
        this.educations = educations;
    }


    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * @return The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     * @param degree The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * @return The yrGraduated
     */
    public Integer getYrGraduated() {
        return yrGraduated;
    }

    /**
     * @param yrGraduated The yr_graduated
     */
    public void setYrGraduated(Integer yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    /**
     * @return The college
     */
    public String getCollege() {
        return college;
    }

    /**
     * @param college The college
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     * @return The otherSchool
     */
    public String getOtherSchool() {
        return otherSchool;
    }

    /**
     * @param otherSchool The other_school
     */
    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    /**
     * @return The designations
     */
    public String getDesignations() {
        return designations;
    }

    /**
     * @param designations The designations
     */
    public void setDesignations(String designations) {
        this.designations = designations;
    }

    /**
     * @return The linkedIn
     */
    public String getLinkedIn() {
        return linkedIn;
    }

    /**
     * @param linkedIn The linked_in
     */
    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * @param profilePic The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * @return The verified
     */
    public Object getVerified() {
        return verified;
    }

    /**
     * @param verified The verified
     */
    public void setVerified(Object verified) {
        this.verified = verified;
    }

    /**
     * @return The agentId
     */
    public Integer getAgentId() {
        return agentId;
    }

    /**
     * @param agentId The agent_id
     */
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The mlsName
     */
    public Object getMlsName() {
        return mlsName;
    }

    /**
     * @param mlsName The mls_name
     */
    public void setMlsName(Object mlsName) {
        this.mlsName = mlsName;
    }

    /**
     * @return The licenseNum
     */
    public Object getLicenseNum() {
        return licenseNum;
    }

    /**
     * @param licenseNum The license_num
     */
    public void setLicenseNum(Object licenseNum) {
        this.licenseNum = licenseNum;
    }

    /**
     * @return The napNum
     */
    public Object getNapNum() {
        return napNum;
    }

    /**
     * @param napNum The nap_num
     */
    public void setNapNum(Object napNum) {
        this.napNum = napNum;
    }

    /**
     * @return The street
     */
    public Object getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(Object street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public Object getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(Object city) {
        this.city = city;
    }

    /**
     * @return The state
     */
    public Object getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(Object state) {
        this.state = state;
    }

    /**
     * @return The jobTitle
     */
    public Object getJobTitle() {
        return jobTitle;
    }

    /**
     * @param jobTitle The job_title
     */
    public void setJobTitle(Object jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * @return The company
     */
    public Object getCompany() {
        return company;
    }

    /**
     * @param company The company
     */
    public void setCompany(Object company) {
        this.company = company;
    }

    /**
     * @return The facebookUrl
     */
    public Object getFacebookUrl() {
        return facebookUrl;
    }

    /**
     * @param facebookUrl The facebook_url
     */
    public void setFacebookUrl(Object facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    /**
     * @return The twitterUrl
     */
    public Object getTwitterUrl() {
        return twitterUrl;
    }

    /**
     * @param twitterUrl The twitter_url
     */
    public void setTwitterUrl(Object twitterUrl) {
        this.twitterUrl = twitterUrl;
    }
}
