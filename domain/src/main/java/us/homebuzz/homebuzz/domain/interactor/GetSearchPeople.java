package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 12/8/2015.
 */
public class GetSearchPeople  extends UseCase{

    private final UserRepository userRepository;
    private WeakHashMap<String, String> justSold;
    @Inject
    public GetSearchPeople(UserRepository userRepository,
                          ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userSearchPeople(this.justSold);
    }

    public void setUserSearch(WeakHashMap<String, String> justSold) {
        this.justSold = justSold;
    }
}
