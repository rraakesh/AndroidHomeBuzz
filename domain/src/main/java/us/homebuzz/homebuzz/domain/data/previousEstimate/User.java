package us.homebuzz.homebuzz.domain.data.previousEstimate;

/**
 * Created by amit.singh on 10/27/2015.
 */
public class User {

    private Integer id;

    private String email;

    private Integer signInCount;

    private String currentSignInAt;

    private String lastSignInAt;

    private Object confirmedAt;

    private String name;

    private String zipCode;

    private String role;

    private Object facebookId;

    private Integer estimatesCount;

    private Double accurate;

    private Boolean confirmed;

    private Boolean followed;

    private String bio;

    private String degree;

    private Integer yrGraduated;

    private String college;

    private String otherSchool;

    private String designations;

    private String linkedIn;

    private String website;

    private String profilePic;

    private Object verified;

    private Integer agentId;

    private String phone;

    private String educations;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The signInCount
     */
    public Integer getSignInCount() {
        return signInCount;
    }

    /**
     *
     * @param signInCount
     * The sign_in_count
     */
    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    /**
     *
     * @return
     * The currentSignInAt
     */
    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    /**
     *
     * @param currentSignInAt
     * The current_sign_in_at
     */
    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    /**
     *
     * @return
     * The lastSignInAt
     */
    public String getLastSignInAt() {
        return lastSignInAt;
    }

    /**
     *
     * @param lastSignInAt
     * The last_sign_in_at
     */
    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

  /*  *//**
     *
     * @return
     * The currentSignInIp
     *//*
    public CurrentSignInIp getCurrentSignInIp() {
        return currentSignInIp;
    }

    *//**
     *
     * @param currentSignInIp
     * The current_sign_in_ip
     *//*
    public void setCurrentSignInIp(CurrentSignInIp currentSignInIp) {
        this.currentSignInIp = currentSignInIp;
    }

    *//**
     *
     * @return
     * The lastSignInIp
     *//*
    public LastSignInIp getLastSignInIp() {
        return lastSignInIp;
    }

    *//**
     *
     * @param lastSignInIp
     * The last_sign_in_ip
     *//*
    public void setLastSignInIp(LastSignInIp lastSignInIp) {
        this.lastSignInIp = lastSignInIp;
    }*/

    /**
     *
     * @return
     * The confirmedAt
     */
    public Object getConfirmedAt() {
        return confirmedAt;
    }

    /**
     *
     * @param confirmedAt
     * The confirmed_at
     */
    public void setConfirmedAt(Object confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public Object getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     *
     * @param confirmed
     * The confirmed
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     *
     * @return
     * The followed
     */
    public Boolean getFollowed() {
        return followed;
    }

    /**
     *
     * @param followed
     * The followed
     */
    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    /**
     *
     * @return
     * The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     *
     * @param bio
     * The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     *
     * @return
     * The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     *
     * @param degree
     * The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     *
     * @return
     * The yrGraduated
     */
    public Integer getYrGraduated() {
        return yrGraduated;
    }

    /**
     *
     * @param yrGraduated
     * The yr_graduated
     */
    public void setYrGraduated(Integer yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    /**
     *
     * @return
     * The college
     */
    public String getCollege() {
        return college;
    }

    /**
     *
     * @param college
     * The college
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     *
     * @return
     * The otherSchool
     */
    public String getOtherSchool() {
        return otherSchool;
    }

    /**
     *
     * @param otherSchool
     * The other_school
     */
    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    /**
     *
     * @return
     * The designations
     */
    public String getDesignations() {
        return designations;
    }

    /**
     *
     * @param designations
     * The designations
     */
    public void setDesignations(String designations) {
        this.designations = designations;
    }

    /**
     *
     * @return
     * The linkedIn
     */
    public String getLinkedIn() {
        return linkedIn;
    }

    /**
     *
     * @param linkedIn
     * The linked_in
     */
    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The verified
     */
    public Object getVerified() {
        return verified;
    }

    /**
     *
     * @param verified
     * The verified
     */
    public void setVerified(Object verified) {
        this.verified = verified;
    }

    /**
     *
     * @return
     * The agentId
     */
    public Integer getAgentId() {
        return agentId;
    }

    /**
     *
     * @param agentId
     * The agent_id
     */
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The educations
     */
    public String getEducations() {
        return educations;
    }

    /**
     *
     * @param educations
     * The educations
     */
    public void setEducations(String educations) {
        this.educations = educations;
    }

}
