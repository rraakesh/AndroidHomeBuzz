package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 12/4/2015.
 */
public class MarkMessageRead extends UseCase {
    private final UserRepository userRepository;
    private WeakHashMap<String, String> replyMessageCredential;

    @Inject
    public MarkMessageRead(UserRepository userRepository,
                           ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userReadMessages(this.replyMessageCredential);
    }

    public void setMessageRead(WeakHashMap<String, String> replyMessageCredential) {
        this.replyMessageCredential = replyMessageCredential;
    }
}
