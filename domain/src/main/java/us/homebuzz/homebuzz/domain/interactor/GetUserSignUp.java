package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/31/2015.
 */
public class GetUserSignUp extends UseCase {

    private WeakHashMap<String, String> userRegData;
    private final UserRepository userRepository;

    @Inject
    public GetUserSignUp(UserRepository userRepository,
                         ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userRegistration(this.userRegData);
    }

    public void setUserRegDetails(WeakHashMap<String, String> userRegistartionData, boolean isFbLogin) {
        this.userRegData = userRegistartionData;
    }
}
