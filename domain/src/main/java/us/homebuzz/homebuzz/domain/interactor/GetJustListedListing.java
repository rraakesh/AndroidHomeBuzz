package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class GetJustListedListing extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> justListed;

    @Inject
    public GetJustListedListing(UserRepository userRepository,
                                ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.useJustListedListing(this.justListed);
    }

    public void setUserJustListed(WeakHashMap<String, String> justListed) {
        this.justListed = justListed;
    }
}
