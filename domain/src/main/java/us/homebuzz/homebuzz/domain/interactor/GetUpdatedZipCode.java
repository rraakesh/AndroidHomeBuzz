package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 12/15/2015.
 */
public class GetUpdatedZipCode extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> zipCode;

    @Inject
    public GetUpdatedZipCode(UserRepository userRepository,
                             ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userUpdateZipCode(this.zipCode);
    }

    public void setUpdateZipCode(WeakHashMap<String, String> zipCode) {
        this.zipCode = zipCode;

    }
}
