package us.homebuzz.homebuzz.domain.interactor;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/3/2015.
 */
public class GetUserFriendfollow extends UseCase {

    private final UserRepository userRepository;

    @Inject
    public GetUserFriendfollow(UserRepository userRepository,
                               ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userFollowed();
    }


}