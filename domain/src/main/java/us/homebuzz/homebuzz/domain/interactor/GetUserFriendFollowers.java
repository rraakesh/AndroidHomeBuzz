package us.homebuzz.homebuzz.domain.interactor;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;
import rx.Observable;
/**
 * Created by abhishek.singh on 11/23/2015.
 */
public class GetUserFriendFollowers  extends UseCase {
    private final UserRepository userRepository;

    @Inject
    public GetUserFriendFollowers(UserRepository userRepository,
                                  ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userFollowers();
    }
}
