package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/21/2015.
 */
public class GetLeaderByAccuracy extends UseCase{

    private final UserRepository userRepository;
    private WeakHashMap<String,String> filterdata;
    @Inject
    public GetLeaderByAccuracy( UserRepository userRepository,
                             ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override protected Observable buildUseCaseObservable() {
        return this.userRepository.userLederboardAccuracy(this.filterdata);
    }
    public void setFilterData(WeakHashMap<String,String> filterdata){
        this.filterdata=filterdata;
    }
}
