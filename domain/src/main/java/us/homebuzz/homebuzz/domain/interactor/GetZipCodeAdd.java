package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class GetZipCodeAdd extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> zipAdd;

    @Inject
    public GetZipCodeAdd(UserRepository userRepository,
                         ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userZipCodeAddEntity(this.zipAdd);
    }

    public void setZipAdd(WeakHashMap<String, String> zipAdd) {
        this.zipAdd = zipAdd;
    }
}
