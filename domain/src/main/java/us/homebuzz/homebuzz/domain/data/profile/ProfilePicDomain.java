package us.homebuzz.homebuzz.domain.data.profile;

/**
 * Created by amit.singh on 12/3/2015.
 */
public class ProfilePicDomain {

    private String imageUrl;

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
