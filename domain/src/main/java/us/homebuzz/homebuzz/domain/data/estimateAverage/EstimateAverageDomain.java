package us.homebuzz.homebuzz.domain.data.estimateAverage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 12/12/2015.
 */
public class EstimateAverageDomain {

    private Object id;

    private Object userId;

    private Object listingId;

    private Object locationRating;

    private Object conditionRating;

    private String price;

    private Object reason;

    private Object accurate;

    private Object createdAt;

    private Object updatedAt;

    private List<String> imageUrl = new ArrayList<String>();

    private Object name;

    /**
     *
     * @return
     * The id
     */
    public Object getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Object id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Object getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Object userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The listingId
     */
    public Object getListingId() {
        return listingId;
    }

    /**
     *
     * @param listingId
     * The listing_id
     */
    public void setListingId(Object listingId) {
        this.listingId = listingId;
    }

    /**
     *
     * @return
     * The locationRating
     */
    public Object getLocationRating() {
        return locationRating;
    }

    /**
     *
     * @param locationRating
     * The location_rating
     */
    public void setLocationRating(Object locationRating) {
        this.locationRating = locationRating;
    }

    /**
     *
     * @return
     * The conditionRating
     */
    public Object getConditionRating() {
        return conditionRating;
    }

    /**
     *
     * @param conditionRating
     * The condition_rating
     */
    public void setConditionRating(Object conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     *
     * @return
     * The price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The reason
     */
    public Object getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(Object reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Object getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Object accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public Object getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public Object getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public List<String> getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The name
     */
    public Object getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(Object name) {
        this.name = name;
    }

}
