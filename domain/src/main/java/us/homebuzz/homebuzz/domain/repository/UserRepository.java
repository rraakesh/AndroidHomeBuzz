/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.domain.repository;

import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import rx.Observable;
import us.homebuzz.homebuzz.domain.data.AccuracyLeaderboardDomain;
import us.homebuzz.homebuzz.domain.data.AddCheckInPhotoData;
import us.homebuzz.homebuzz.domain.data.EstimatesDomain;
import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.NearbyDomain;
import us.homebuzz.homebuzz.domain.data.SubscriptionDomain;
import us.homebuzz.homebuzz.domain.data.UpdateChecking;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.domain.data.ZipCodeDeleteDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingPhoto;
import us.homebuzz.homebuzz.domain.data.addListing.SearchAddressDomain;
import us.homebuzz.homebuzz.domain.data.estimateAverage.EstimateAverageDomain;
import us.homebuzz.homebuzz.domain.data.friends.followed.FriendFollowedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustListedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustSoldDomain;
import us.homebuzz.homebuzz.domain.data.message.AllMessages;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.data.message.unread.UnreadMessageDomain;
import us.homebuzz.homebuzz.domain.data.previousEstimate.PreviouseEstimate;
import us.homebuzz.homebuzz.domain.data.profile.InviteFriendsDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfileDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfilePicDomain;
import us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain;
import us.homebuzz.homebuzz.domain.data.searchPeople.SearchPeopleDomain;
import us.homebuzz.homebuzz.domain.data.signup.User_Signup_Domain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodeAddDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.UpdateZipCodeDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodePurchaseDomain;

/**
 * Interface that represents a Repository for getting {@link User} related data.
 */
public interface UserRepository {
    /**
     * Get an {@link Observable} which will emit a List of {@link User}.
     */
    Observable<List<User>> users();

    /**
     * Get an {@link Observable} which will emit a {@link User}.
     *
     * @param userId The user id used to retrieve user data.
     */
    Observable<User> user(final int userId);

    /**
     * Get an {@link Observable} which will emit a {@link User}.
     *
     * @param loginCredential The Contian the user specific login Details @Link LoginFragment.
     *  @param isFbLogin use to Switch Between the fblogin end point and login endpoint
     */
    Observable<User_> user(/*String username,String password,String device_id*/ WeakHashMap<String, String> loginCredential, boolean isFbLogin);

    Observable<List<NearbyDomain>> userNearby(int user_id,SortedMap<String, String> nearbyData);

    Observable<List<AccuracyLeaderboardDomain>> userLederboardAccuracy(WeakHashMap<String, String> filterData);

    Observable<List<EstimatesDomain>> userLederboardEstimates(WeakHashMap<String,String> filterdata);

    Observable<List<AllMessages>> userAllMessages();

    Observable<MessageReply> userReplyMessages(WeakHashMap<String, String> replyMessageCredential);
    Observable<InviteFriendsDomain> userInviteFriends(WeakHashMap<String, String> inviteFriendsCredential);

    Observable<List<PreviouseEstimate>> userPreviousEstimate(String user_id);

    Observable<UpdateChecking> userUpdateCheck_in(String listing_id,WeakHashMap<String, String> updateCheckin);

    /**
     * Get the User Profile Based on the User_id
     * @param user_id
     * @return ProfileDomain class Object Which Represent the profile in domain
     */
    Observable<ProfileDomain> userprofile(String user_id);

    Observable<User_Signup_Domain> userRegistration(WeakHashMap<String, String> registationData);

    Observable<List<FriendFollowedDomain>> userFollowed();
    Observable<AddListingDomain> userAddListing(WeakHashMap<String,String> listingData);
    Observable<AddListingPhoto> userAddListingPhoto(String listing_id,WeakHashMap<String,String> photo,WeakHashMap<String,String> listingData);
    Observable<AddCheckInPhotoData> userAddCheckInPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData);
    Observable<UpdateProfileDomain> userEditProfile(WeakHashMap<String,String> userProfilePic,WeakHashMap<String,String> listingData);
    Observable<List<UnreadMessageDomain>> userUnreadMessages();
    Observable<List<JustSoldDomain>> useJustSoldListing(WeakHashMap<String,String> soldData);
    Observable<List<JustListedDomain>>  useJustListedListing(WeakHashMap<String,String> listedData);
    Observable<FollowUnfollowData> userFollowUnfollow(String user_id);
    Observable<List<FriendFollowedDomain>> userFollowers();
    Observable<List<ZipCodePurchaseDomain>> userZipCodePurchase();
    Observable<SearchAddressDomain> useOnBoardProp(WeakHashMap<String,String> soldData);
    Observable<UpdateProfileDomain> userOtherEditProfile(WeakHashMap<String,String> profilePic,WeakHashMap<String,String> listingData);
    Observable<ProfilePicDomain> userProfilePic(WeakHashMap<String,String> profilePic);
    Observable<MessageReply> userReadMessages(WeakHashMap<String, String> replyMessageCredential);
    Observable<List<SearchPeopleDomain>> userSearchPeople(WeakHashMap<String, String> replyMessageCredential);
    Observable<EstimateAverageDomain> userEstimateAverage(WeakHashMap<String, String> estimateAverageData);
    Observable<ZipCodeAddDomain> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd);
    Observable<UpdateZipCodeDomain> userUpdateZipCode(WeakHashMap<String, String> estimateAverageData);
    Observable<SubscriptionDomain> userSubscription(WeakHashMap<String, String> userSubscription);
    Observable<ZipCodeDeleteDomain> zipDelete(WeakHashMap<String, String> zipDelete);
}
