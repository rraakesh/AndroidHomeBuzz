package us.homebuzz.homebuzz.domain.data;

/**
 * Created by abhishek.singh on 12/2/2015.
 */
public class AddCheckInPhotoData {

    private String url;

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
