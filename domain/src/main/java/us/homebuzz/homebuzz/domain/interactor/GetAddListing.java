package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/5/2015.
 */
public class GetAddListing extends UseCase {

    private WeakHashMap<String, String> AddListingData;
    private final UserRepository userRepository;

    @Inject
    public GetAddListing(UserRepository userRepository,
                         ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.userAddListing(this.AddListingData);
    }

    public void setUserAddListing(WeakHashMap<String, String> AddListingData) {
        this.AddListingData = AddListingData;
    }
}
