package us.homebuzz.homebuzz.domain.interactor;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class GetUserProfile extends UseCase {

    private final UserRepository userRepository;

    int user_id = -1;

    @Inject
    public GetUserProfile(UserRepository userRepository, ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.userRepository.userprofile(String.valueOf(this.user_id));
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }
}
