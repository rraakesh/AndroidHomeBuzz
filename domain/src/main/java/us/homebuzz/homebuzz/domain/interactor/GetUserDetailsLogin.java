package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/8/2015.
 */
public class GetUserDetailsLogin extends UseCase {

  /*  private String userName;
    private String password;
    private String device_id;*/
    private boolean isFbLogin;
    private WeakHashMap<String,String> loginCredential;
    private final UserRepository userRepository;
    @Inject
    public GetUserDetailsLogin( UserRepository userRepository,
                          ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override protected Observable buildUseCaseObservable() {
        return this.userRepository.user(/*this.userName,this.password,this.device_id*/this.loginCredential,isFbLogin);
    }

    public void setUserLoginDetails(/*String userName, String password,String device_id*/WeakHashMap<String,String> loginCredential,boolean isFbLogin){
       /* this.userName = userName;
        this.password = password;
        this.device_id = device_id;*/
        this.loginCredential=loginCredential;
        this.isFbLogin=isFbLogin;
    }
}