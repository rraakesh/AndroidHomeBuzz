package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class GetZipDeleteInteractor extends UseCase {

    private final UserRepository userRepository;
    private WeakHashMap<String, String> zipDelete;

    @Inject
    public GetZipDeleteInteractor(UserRepository userRepository,
                                     ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.userRepository.zipDelete(this.zipDelete);
    }
    public void setZipDelete(WeakHashMap<String, String> zipDelete){
        this.zipDelete=zipDelete;

    }
}