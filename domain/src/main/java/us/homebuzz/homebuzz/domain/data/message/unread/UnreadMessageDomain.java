package us.homebuzz.homebuzz.domain.data.message.unread;

import us.homebuzz.homebuzz.domain.data.message.Recipient;
import us.homebuzz.homebuzz.domain.data.message.Sender;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class UnreadMessageDomain {

    private Integer id;

    private String body;

    private Integer listingId;

    private String status;

    private String createdAt;

    private String updatedAt;

    private Sender sender;

    private Recipient recipient;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The listingId
     */
    public Integer getListingId() {
        return listingId;
    }

    /**
     * @param listingId The listing_id
     */
    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The sender
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * @param sender The sender
     */
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    /**
     * @return The recipient
     */
    public Recipient getRecipient() {
        return recipient;
    }

    /**
     * @param recipient The recipient
     */
    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }
}
