package us.homebuzz.homebuzz.domain.data.addListing;

/**
 * Created by amit.singh on 11/5/2015.
 */
public class AddListingPhoto {

    private String url;

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
