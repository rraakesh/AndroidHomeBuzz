package us.homebuzz.homebuzz.domain.data.profile;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class InviteFriendsDomain {
    String message;
    String phone;
    private Boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
