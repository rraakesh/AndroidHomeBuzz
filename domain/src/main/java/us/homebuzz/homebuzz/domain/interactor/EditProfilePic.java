package us.homebuzz.homebuzz.domain.interactor;

import java.util.WeakHashMap;

import javax.inject.Inject;

import rx.Observable;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 12/2/2015.
 */
public class EditProfilePic extends UseCase {
    private final UserRepository userRepository;

    WeakHashMap<String, String> userProfilePic;
    @Inject
    public EditProfilePic(UserRepository userRepository, ThreadExecutor threadExecutor,
                                PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.userRepository.userProfilePic(this.userProfilePic);
    }

    public void setUserProfileData(WeakHashMap<String, String> ProfileImage) {
        this.userProfilePic=ProfileImage;

    }
}
