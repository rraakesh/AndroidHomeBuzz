package us.homebuzz.homebuzz.domain.data;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipCodeDeleteDomain {
    private Boolean delete;

    /**
     *
     * @return
     * The delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     *
     * @param delete
     * The delete
     */
    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

}
