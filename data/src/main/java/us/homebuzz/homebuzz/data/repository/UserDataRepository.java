/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.repository;

import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import us.homebuzz.homebuzz.data.entity.mapper.UserEntityDataMapper;
import us.homebuzz.homebuzz.data.repository.datasource.UserDataStore;
import us.homebuzz.homebuzz.data.repository.datasource.UserDataStoreFactory;
import us.homebuzz.homebuzz.domain.data.AccuracyLeaderboardDomain;
import us.homebuzz.homebuzz.domain.data.AddCheckInPhotoData;
import us.homebuzz.homebuzz.domain.data.EstimatesDomain;
import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.NearbyDomain;
import us.homebuzz.homebuzz.domain.data.SubscriptionDomain;
import us.homebuzz.homebuzz.domain.data.UpdateChecking;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.domain.data.ZipCodeDeleteDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingPhoto;
import us.homebuzz.homebuzz.domain.data.addListing.SearchAddressDomain;
import us.homebuzz.homebuzz.domain.data.estimateAverage.EstimateAverageDomain;
import us.homebuzz.homebuzz.domain.data.friends.followed.FriendFollowedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustListedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustSoldDomain;
import us.homebuzz.homebuzz.domain.data.message.AllMessages;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.data.message.unread.UnreadMessageDomain;
import us.homebuzz.homebuzz.domain.data.previousEstimate.PreviouseEstimate;
import us.homebuzz.homebuzz.domain.data.profile.InviteFriendsDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfileDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfilePicDomain;
import us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain;
import us.homebuzz.homebuzz.domain.data.searchPeople.SearchPeopleDomain;
import us.homebuzz.homebuzz.domain.data.signup.User_Signup_Domain;
import us.homebuzz.homebuzz.domain.data.zipcode.UpdateZipCodeDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodeAddDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodePurchaseDomain;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * {@link UserRepository} for retrieving user data.
 */
@Singleton
public class UserDataRepository implements UserRepository {

    private final UserDataStoreFactory userDataStoreFactory;
    private final UserEntityDataMapper userEntityDataMapper;

    /**
     * Constructs a {@link UserRepository}.
     *
     * @param dataStoreFactory     A factory to construct different data source implementations.
     * @param userEntityDataMapper {@link UserEntityDataMapper}.
     */
    @Inject
    public UserDataRepository(UserDataStoreFactory dataStoreFactory,
                              UserEntityDataMapper userEntityDataMapper) {
        this.userDataStoreFactory = dataStoreFactory;
        this.userEntityDataMapper = userEntityDataMapper;
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<List<User>> users() {
        //we always get all users from the cloud
        final UserDataStore userDataStore = this.userDataStoreFactory.createCloudDataStore();
        return userDataStore.userEntityList()
                .map(userEntities -> this.userEntityDataMapper.transform(userEntities));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<User> user(int userId) {
        final UserDataStore userDataStore = this.userDataStoreFactory.create(userId);
        return userDataStore.userEntityDetails(userId)
                .map(userEntity -> this.userEntityDataMapper.transform(userEntity));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<User_> user(WeakHashMap<String, String> loginCredential, boolean isFbLogin) {
        //we always get  users details from the cloud
        final UserDataStore userDataStore = this.userDataStoreFactory.createLogin();
        return userDataStore.userEntityDetailsLogin(loginCredential, isFbLogin)
                .map(userEntities -> this.userEntityDataMapper.transform(userEntities));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<List<NearbyDomain>> userNearby(/*String lat, String lon, String radius*/int user_id, SortedMap<String, String> nearbyData) {
        //we always get  users nearby Listing From Cloud
        final UserDataStore userDataStore = this.userDataStoreFactory.createNearby();
        return userDataStore.userEntityListing(nearbyData)
                .map(userEntities -> this.userEntityDataMapper.transformListing(user_id, userEntities));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<List<AccuracyLeaderboardDomain>> userLederboardAccuracy(WeakHashMap<String, String> filterData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createAccuracy();
        return userDataStore.userEntityAccuracy(filterData)
                .map(userEntities -> this.userEntityDataMapper.transformAccuracy(userEntities));

    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Observable<List<EstimatesDomain>> userLederboardEstimates(WeakHashMap<String,String>filterData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createEstimate();
        return userDataStore.userEntityEstimate(filterData)
                .map(userEntities -> this.userEntityDataMapper.transformEstimates(userEntities));

    }

    @Override
    public Observable<List<AllMessages>> userAllMessages() {
        final UserDataStore userDataStore = this.userDataStoreFactory.createAllMessages();
        return userDataStore.userEntityAllMessages()
                .map(userEntities -> this.userEntityDataMapper.transformAllMessages(userEntities));

    }

    @Override
    public Observable<List<PreviouseEstimate>> userPreviousEstimate(String user_id) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createPreviousEstimate();
        return userDataStore.userEntityPreviousEstimate(user_id)
                .map(userEntities -> this.userEntityDataMapper.transformPreviouseEstimate(userEntities));
    }

    @Override
    public Observable<ProfileDomain> userprofile(String user_id) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserProfile();
        return userDataStore.userProfileEntity(user_id)
                .map(userEntities -> this.userEntityDataMapper.transformProfile(userEntities));
    }

    @Override
    public Observable<User_Signup_Domain> userRegistration(WeakHashMap<String, String> registationData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserProfile();
        return userDataStore.userRegistration(registationData)
                .map(userEntities -> this.userEntityDataMapper.transformSignUp(userEntities));
    }

    @Override
    public Observable<List<FriendFollowedDomain>> userFollowed() {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserFollowed();
        return userDataStore.userFollowed()
                .map(userEntities -> this.userEntityDataMapper.transformFriendsFollowed(userEntities));
    }

    @Override
    public Observable<AddListingDomain> userAddListing(WeakHashMap<String, String> listingData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createAddListing();
        return userDataStore.userAddListing(listingData)
                .map(userEntities -> this.userEntityDataMapper.transform(userEntities));
    }

    @Override
    public Observable<AddListingPhoto> userAddListingPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createAddListing();
        return userDataStore.userAddListingPhoto(listing_id, photo, listingData)
                .map(userEntities -> this.userEntityDataMapper.transformPhoto(userEntities));
    }

    @Override
    public Observable<AddCheckInPhotoData> userAddCheckInPhoto(String id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createAddCheckIn();
        return userDataStore.userAddCheckInPhoto(id, photo, listingData)
                .map(userEntities -> this.userEntityDataMapper.transformPhoto(userEntities));
    }

    @Override
    public Observable<MessageReply> userReplyMessages(WeakHashMap<String, String> replyMessageCredential) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userReplyMessages(replyMessageCredential)
                .map(userEntities -> this.userEntityDataMapper.transformMessageReply(userEntities));
    }

    @Override
    public Observable<InviteFriendsDomain> userInviteFriends(WeakHashMap<String, String> inviteFriendsCredential) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createInviteFreinds();
        return userDataStore.userInviteFriends(inviteFriendsCredential)
                .map(userEntities -> this.userEntityDataMapper.transformInviteFriends(userEntities));
    }

    @Override
    public Observable<UpdateChecking> userUpdateCheck_in(String listing_id, WeakHashMap<String, String> updateCheckin) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUpdateCheckIn();
        return userDataStore.userUpdateCheckInEntity(listing_id, updateCheckin)
                .map(userEntities -> this.userEntityDataMapper.transformUpdateCheckinEstimate(userEntities));
    }

    @Override
    public Observable<UpdateProfileDomain> userEditProfile(WeakHashMap<String, String> userProfilePic,WeakHashMap<String, String> listingData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserProfile();
        return userDataStore.userProfileEntity(userProfilePic, listingData)
                .map(userEntities -> this.userEntityDataMapper.transformUpdateProfile(userEntities));
    }
    @Override
    public Observable<UpdateProfileDomain> userOtherEditProfile(WeakHashMap<String, String> profilePic,WeakHashMap<String, String> listingData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserProfile();
        return userDataStore.userOtherProfileEntity(profilePic,listingData)
                .map(userEntities -> this.userEntityDataMapper.transformUpdateProfile(userEntities));
    }
    @Override
    public Observable<List<UnreadMessageDomain>> userUnreadMessages() {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUnreadMessage();
        return userDataStore.userUnreadMessages()
                .map(userEntities -> this.userEntityDataMapper.transformUnreadMessages(userEntities));
    }

    @Override
    public Observable<List<JustListedDomain>> useJustListedListing(WeakHashMap<String, String> listedData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUnreadMessage();
        return userDataStore.useJustListedListingEntity(listedData)
                .map(userEntities -> this.userEntityDataMapper.transformJustListed(userEntities));
    }


    @Override
    public Observable<List<JustSoldDomain>> useJustSoldListing(WeakHashMap<String, String> soldData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUnreadMessage();
        return userDataStore.useJustSoldListingEntity(soldData)
                .map(userEntities -> this.userEntityDataMapper.transformJustSold(userEntities));
    }

    @Override
    public Observable<FollowUnfollowData> userFollowUnfollow(String user_id) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createFollowUnfollow();
        return userDataStore.userFollowUnfollow(user_id)
                .map(userEntities -> this.userEntityDataMapper.transformFollowUnfollow(userEntities));
    }


    @Override
    public Observable<List<FriendFollowedDomain>> userFollowers() {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserFollowed();
        return userDataStore.userFollowers()
                .map(userEntities -> this.userEntityDataMapper.transformFriendsFollowed(userEntities));
    }

    @Override
    public Observable<List<ZipCodePurchaseDomain>> userZipCodePurchase() {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserFollowed();
        return userDataStore.userZipPurchaseEntity()
                .map(userEntities -> this.userEntityDataMapper.transformZipPurchase(userEntities));
    }

    @Override
    public Observable<SearchAddressDomain> useOnBoardProp(WeakHashMap<String, String> searchData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserFollowed();
        return userDataStore.userOnBoardPropEntity(searchData)
                .map(userEntities -> this.userEntityDataMapper.transformOnBoardProp(userEntities));
    }

    @Override
    public Observable<ProfilePicDomain> userProfilePic(WeakHashMap<String, String> profilePic) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUserFollowed();
        return userDataStore.userProfilePicEntity(profilePic)
                .map(userEntities -> this.userEntityDataMapper.transformProfilePic(userEntities));
    }

    @Override
    public Observable<MessageReply> userReadMessages(WeakHashMap<String, String> replyMessageCredential) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userReadMessages(replyMessageCredential)
                .map(userEntities -> this.userEntityDataMapper.transformMessageReply(userEntities));
    }

    @Override
    public Observable<List<SearchPeopleDomain>> userSearchPeople(WeakHashMap<String, String> replyMessageCredential) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userSearchPeople(replyMessageCredential)
                .map(userEntities -> this.userEntityDataMapper.transformSearchPeople(userEntities));
    }

    @Override
    public Observable<EstimateAverageDomain> userEstimateAverage(WeakHashMap<String, String> estimateAverageData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userEstimateAverage(estimateAverageData)
                .map(userEntities -> this.userEntityDataMapper.transformAverageEstimate(userEntities));
    }

    @Override
    public Observable<UpdateZipCodeDomain> userUpdateZipCode(WeakHashMap<String, String> estimateAverageData) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createUpdateZipCode();
        return userDataStore.userUpdateZip(estimateAverageData)
                .map(userEntities -> this.userEntityDataMapper.transformUpdtateZipCode(userEntities));
    }

    @Override
    public Observable<ZipCodeAddDomain> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userZipCodeAddEntity(zipCodeAdd)
                .map(userEntities -> this.userEntityDataMapper.transformZipCodeAdd(userEntities));
    }
    @Override
    public Observable<SubscriptionDomain> userSubscription(WeakHashMap<String, String> userSubscription) {
        final UserDataStore userDataStore = this.userDataStoreFactory.createReplyMessages();
        return userDataStore.userSubscription(userSubscription)
                .map(userEntities -> this.userEntityDataMapper.transformSubscription(userEntities));
    }

    @Override
    public Observable<ZipCodeDeleteDomain> zipDelete(WeakHashMap<String, String> zipDelete) {
        final UserDataStore userDataStore = this.userDataStoreFactory.deleteZipCode();
        return userDataStore.zipDelete(zipDelete)
                .map(userEntities -> this.userEntityDataMapper.transformZipDelete(userEntities));
    }
}
