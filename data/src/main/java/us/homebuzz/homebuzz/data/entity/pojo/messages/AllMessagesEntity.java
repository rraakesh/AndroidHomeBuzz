package us.homebuzz.homebuzz.data.entity.pojo.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 10/22/2015.
 */
public class AllMessagesEntity {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("listing_id")
    @Expose
    private Integer listingId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sender")
    @Expose
    private Sender sender;
    @SerializedName("recipient")
    @Expose
    private Recipient recipient;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The listingId
     */
    public Integer getListingId() {
        return listingId;
    }

    /**
     * @param listingId The listing_id
     */
    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The sender
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * @param sender The sender
     */
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    /**
     * @return The recipient
     */
    public Recipient getRecipient() {
        return recipient;
    }

    /**
     * @param recipient The recipient
     */
    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }
}
