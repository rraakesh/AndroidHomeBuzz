package us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abhishek.singh on 12/2/2015.
 */
public class AddCheckInPhotoEntity {
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
