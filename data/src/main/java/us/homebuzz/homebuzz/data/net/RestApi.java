/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.net;

import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import rx.Observable;
import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;

/**
 * RestApi for retrieving data from the network.
 */
public interface RestApi {
    String API_BASE_URL = "https://realbuzzapp.herokuapp.com/api/v1/";

    String API_URL_POST_USER_CHECKIN_ESTIMATE = API_BASE_URL + "listings/";

    String API_URL_POST_USER_LOGIN = API_BASE_URL + "auth/login";
    String API_URL_POST_USER_LOGIN_FB = API_BASE_URL + "auth/fblogin";
    /**
     * Api url for getting all users
     */
    String API_URL_GET_USER_LIST = API_BASE_URL + "users.json";
    /**
     * Api url for getting a user profile: Remember to concatenate id + 'json'
     */
    String API_URL_GET_USER_DETAILS = API_BASE_URL + "user_";
    String API_URL_GET_USER_NEARBY = API_BASE_URL + "listings/nearests?";
    String API_URL_GET_USER_ACCURACY = API_BASE_URL + "estimates/leaders";
    String API_URL_GET_USER_ESTIMATE = API_BASE_URL + "estimates/leaders_by_amounts";
    String API_URL_GET_USER_ALL_MESSAGES = API_BASE_URL + "messages";
    String API_URL_POST_USER_MESSAGE_REPLY = API_BASE_URL + "messages";
    String API_URL_GET_USER_PREVIOUS_ESTIMATE = API_BASE_URL + "users/";
    String API_URL_GET_USER_PROFILE = API_BASE_URL + "users/";
    String API_URL_POST_USER_MESSAGE = API_BASE_URL + "message?";
    String API_URL_POST_USER_REGISTRATION = API_BASE_URL + "auth/register";
    String API_URL_GET_FRIEND_FOLLOWED = API_BASE_URL + "users/following";
    String API_URL_POST_ADD_LISTING = API_BASE_URL + "listings";
    String API_URL_PUT_USER_OTHER_PROFILE = API_BASE_URL + "profiles";
    String API_URL_PUT_USER_PROFILE = API_BASE_URL + "users";
    String API_URL_GET_UNREAD_MESSAGE = API_BASE_URL + "messages/unread";
    String API_URL_GET_JUST_SOLDLISTING = API_BASE_URL + "listings/just_sold";
    String API_URL_GET_JUST_LISTEDLISTING = API_BASE_URL + "listings/just_listed";
    String API_URL_GET_FOLLOW_UNFOLLOW = API_BASE_URL + "users/";
    String API_URL_GET_FRIEND_FOLLOWERS = API_BASE_URL + "users/followers";
    String API_URL_GET_ZIP_PURCHASE = API_BASE_URL + "extra_zips";
    String API_URL_GET_ON_BOARD_PROP = API_BASE_URL + "listings/get_onboard_prop";
    String API_URL_POST_SEND_INVITATION = API_BASE_URL + "texts";
    String API_URL_POST_ADD_CHECKIN_PHOTO = API_BASE_URL + "estimates";
    String API_URL_POST_SEARCH_PEOPLE = API_BASE_URL + "users";
    String API_URL_GET_ESTIMATE_AVERAGE = API_BASE_URL + "estimate_averages";
    String API_URL_POST_ZIP_CODE = API_BASE_URL + "extra_zips";
    String API_URL_SUBSCRIPTION_PLAN = API_BASE_URL + "users/";
    String API_URL_ZIP_CODE_DELETE = API_BASE_URL + "extra_zips";


    /**
     * Retrieves an {@link rx.Observable} which will emit a List of {@link UserEntity}.
     */
    Observable<List<UserEntity>> userEntityList();

    /**
     * Retrieves an {@link rx.Observable} which will emit a {@link UserEntity}.
     *
     * @param userId The user id used to get user data.
     */
    Observable<UserEntity> userEntityById(final int userId);

    Observable<User_Entity> userEntityByLogin(/*String username,String password,String device_id*/ WeakHashMap<String, String> loginCredential, boolean isFbLogin);

    Observable<List<NearbyEntity>> userNearbyEntity(/*String lat,String lon,String radius*/SortedMap<String, String> nearbyData);

    Observable<List<AccuracyEntity>> userAccuracyEntity(WeakHashMap<String, String> filterData);

    Observable<List<EstimatesEntity>> userEstimateEntity(WeakHashMap<String, String> filterdata);

    Observable<List<AllMessagesEntity>> userEstimateAllMessages();

    Observable<List<PreviousEstimate>> userPreviousEstimate(String user_id);

    Observable<ProfileEntity> userProfileEntity(String user_id);

    Observable<User_Signup> userSignupEntity(WeakHashMap<String, String> userRegData);

    Observable<List<FriendFollowed>> userFollowedEntity();

    Observable<AddListingEntity> userAddListing(WeakHashMap<String, String> listing);

    Observable<AddListingPhotoEntity> userAddListingPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listing);

    Observable<AddCheckInPhotoEntity> userAddCheckInPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listing);

    Observable<ReplyMessageEntity> userReplyMessageEntity(WeakHashMap<String, String> replyData);

    Observable<InviteFriends> userInviteFriends(WeakHashMap<String, String> sendInvitation);

    Observable<UpdateCheckinEntity> usereUpdateCheckinEntity(String listing_id, WeakHashMap<String, String> updateCheck_in);

    Observable<UpdateProfile> userProfileEntity(WeakHashMap<String, String> userProfilePic, WeakHashMap<String, String> listing);

    Observable<UpdateProfile> userOtherProfileEntity(WeakHashMap<String, String> profilePic, WeakHashMap<String, String> listing);

    Observable<List<MessageEntity>> userUnReadMessages();

    Observable<JustSoldEntity> useJustSoldListing(WeakHashMap<String, String> soldData);

    Observable<JustListedEntity> useJustListedListing(WeakHashMap<String, String> listedData);

    Observable<FollowUnfollowEntity> userFollowUnfollow(String user_id);

    Observable<List<FriendFollowed>> userFollowersEntity();

    Observable<List<ZipPurchaseEntity>> userZipPurchaseEntity();

    Observable<SearchAddressEntity> userOnBoardProp(WeakHashMap<String, String> searchData);

    Observable<ProfilePicEntity> userProfilePicUpdate(WeakHashMap<String, String> profileData);

    //Todo Change That Later
    Observable<ReplyMessageEntity> userReadMessageEntity(WeakHashMap<String, String> replyDatamessage);

    Observable<List<SearchPeopleEntity>> userSearchPeopleEntity(WeakHashMap<String, String> searchPeople);

    Observable<EstimateAverage> userEstimateAverageEntity(WeakHashMap<String, String> estimateAverage);

    Observable<ZipAddEntity> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd);

    Observable<UpdateZipCodeEntity> userUpdateZipEntity(WeakHashMap<String, String> replyData);
    Observable<SubscriptionEntity> userSubscription(WeakHashMap<String, String> userSubscription);
    Observable<ZipCodeDeleteEntity> zipDelete(WeakHashMap<String, String> zipDelete);
}
