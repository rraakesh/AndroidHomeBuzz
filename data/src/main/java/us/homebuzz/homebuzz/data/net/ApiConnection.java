/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.net;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

/**
 * Api Connection class used to retrieve data from the cloud.
 * Implements {@link java.util.concurrent.Callable} so when executed asynchronously can
 * return a value.
 */
@Singleton
public class ApiConnection implements Callable<String> {
    private static final String TAG = ApiConnection.class.getSimpleName();
    // private static final MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
    private static final String CONTENT_TYPE_LABEL = "Content-Type";
    private static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";
    //Cant Access the Presenter Module
    private static final String KEY_EMAIL = "email";
    public static final String KEY_TOKEN = "token";
    //ReFactor this to Find a alternative Solution because it is sending the null
    public static String USER_TOKEN = "", USER_EMAIL = "";
    private static final String USER_TOKEN_LABEL = "X-Api-Token";
    private static final String USER_EMAIL_LABEL = "X-Api-Email";
    public final static int GET = 1;
    public final static int POST = 2;
    public final static int MULTIPART = 3;
    public final static int PUT = 4;
    public final static int DELETE = 5;
    public int method;
    private URL url;
    private String response;
    private WeakHashMap<String, String> nameValue, fileValue;

    private ApiConnection(String url, int method, SharedPreferences sharedPreferences) throws MalformedURLException {
        this.url = new URL(url);
        this.method = method;
        //Adding the Header Info If Available
        if (!(sharedPreferences.getString(KEY_EMAIL, "").isEmpty())) {
            USER_TOKEN = (sharedPreferences.getString(KEY_TOKEN, ""));
        }
        if (!(sharedPreferences.getString(KEY_TOKEN, "").isEmpty())) {
            USER_EMAIL = (sharedPreferences.getString(KEY_EMAIL, ""));
        }
        System.out.println("inside the constructor GET" + url);
        Log.d(TAG, "Check for the Authorization in ApiConnection GET" + USER_TOKEN + "EMAIL" + USER_EMAIL);
    }

    private ApiConnection(String url, int method, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        this.url = new URL(url);
        this.nameValue = nameValue;
        this.method = method;
        //Log.d(TAG, "Check for the SharedPreference in ApiConnection POST" + sharedPreferences);
        //Adding the Header Info If Available
        if (!(sharedPreferences.getString(KEY_EMAIL, "").isEmpty())) {
            USER_TOKEN = (sharedPreferences.getString(KEY_TOKEN, ""));
        }
        if (!(sharedPreferences.getString(KEY_TOKEN, "").isEmpty())) {
            USER_EMAIL = (sharedPreferences.getString(KEY_EMAIL, ""));
        }
        System.out.println("inside the constructor" + nameValue.get("username"));
    }

    //For Multipart with Value Or without Value
    private ApiConnection(String url, int method, WeakHashMap<String, String> fileValue, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        this.url = new URL(url);
        this.nameValue = nameValue;
        this.method = method;
        this.fileValue = fileValue;
        //Log.d(TAG, "Check for the SharedPreference in ApiConnection POST" + sharedPreferences);
        //Adding the Header Info If Available
        if (!(sharedPreferences.getString(KEY_EMAIL, "").isEmpty())) {
            USER_TOKEN = (sharedPreferences.getString(KEY_TOKEN, ""));
        }
        if (!(sharedPreferences.getString(KEY_TOKEN, "").isEmpty())) {
            USER_EMAIL = (sharedPreferences.getString(KEY_EMAIL, ""));
        }
//        System.out.println("inside the constructor" + nameValue.get("username"));
    }

    public static ApiConnection createGET(String url, SharedPreferences sharedPreferences) throws MalformedURLException {
        System.out.println("inside the  GET" + url);
        return new ApiConnection(url, GET, sharedPreferences);
    }

    public static ApiConnection createPUT(String url, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        return new ApiConnection(url, PUT, nameValue, sharedPreferences);
    }

    public static ApiConnection createPOST(String url, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        return new ApiConnection(url, POST, nameValue, sharedPreferences);
    }

    public static ApiConnection createPOSTWithMultipart(String url, WeakHashMap<String, String> fileValue, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        return new ApiConnection(url, MULTIPART, fileValue, nameValue, sharedPreferences);
    }

    public static ApiConnection createDELETE(String url, WeakHashMap<String, String> nameValue, SharedPreferences sharedPreferences) throws MalformedURLException {
        return new ApiConnection(url, DELETE, nameValue, sharedPreferences);
    }

    /**
     * Do a request to an api synchronously.
     * It should not be executed in the main thread of the application.
     *
     * @return A string response
     */
    @Nullable
    public String requestSyncCall() {
        connectToApi();
        return response;
    }

    private void connectToApi() {
        OkHttpClient okHttpClient = this.createClient();
        System.out.println("Response " + "URLLLL IN API Connection" + this.url);
        switch (method) {
            case POST:
                //Add the Name Value to the RequestBody
                FormEncodingBuilder formBody = new FormEncodingBuilder();
                for (WeakHashMap.Entry<String, String> entry : nameValue.entrySet()) {
                    System.out.println("Key--" + entry.getKey() + "Value--" + entry.getValue());
                    formBody.add(entry.getKey(), entry.getValue());
                }
                final Request requestPost = new Request.Builder()
                        .url(this.url)
                        .addHeader(USER_EMAIL_LABEL, USER_EMAIL)
                        .addHeader(USER_TOKEN_LABEL, USER_TOKEN)
                        .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                        .post(formBody.build())
                        .build();
                try {
                    this.response = okHttpClient.newCall(requestPost).execute().body().string();
                    System.out.println("Response POST" + this.response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case MULTIPART:
                MultipartBuilder multipartBuilder = new MultipartBuilder();

                for (WeakHashMap.Entry<String, String> entry : fileValue.entrySet()) {
                    System.out.println("Key-- Image" + entry.getKey() + "Value-- Image" + entry.getValue());
                   /* multipartBuilder
                            .addPart(
                                    Headers.of("Content-Disposition", "form-data; name=\"photo" + entry.getKey() + "\""),
                                    RequestBody.create(MEDIA_TYPE_JPEG, new File(entry.getValue())));*/
                    /**
                     * For adding the Dynamic File NAme And its Type use
                     *   Log.d(TAG, "File Path" + photoUrl.substring(photoUrl.lastIndexOf("/")+1)+photoUrl.substring(photoUrl.lastIndexOf(".")+1));
                     */
                    multipartBuilder.addFormDataPart(entry.getKey(), entry.getValue().substring(entry.getValue().lastIndexOf("/") + 1), RequestBody.create(MediaType.parse("image/" + entry.getValue().substring(entry.getValue().lastIndexOf(".") + 1)), new File(entry.getValue())));
                }
                //Check to See If the Value is Not Null Then Add it to Form body
                if (null != nameValue && nameValue.size() > 0) {
                    for (WeakHashMap.Entry<String, String> entry : nameValue.entrySet()) {
                        System.out.println("Key--" + entry.getKey() + "Value--" + entry.getValue());
                        multipartBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                    }
                }
                final Request request = new Request.Builder()
                        .url(this.url)
                        .addHeader(USER_EMAIL_LABEL, USER_EMAIL)
                        .addHeader(USER_TOKEN_LABEL, USER_TOKEN)
                        .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                        .post(multipartBuilder.type(MultipartBuilder.FORM).build())
                        .build();
                try {
                    this.response = okHttpClient.newCall(request).execute().body().string();
                    System.out.println("Response POST Image" + this.response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case PUT:
                //Add the Name Value to the RequestBody
                FormEncodingBuilder formBodyPut = new FormEncodingBuilder();
                for (WeakHashMap.Entry<String, String> entry : nameValue.entrySet()) {
                    System.out.println("Key--" + entry.getKey() + "Value--" + entry.getValue());
                    formBodyPut.add(entry.getKey(), entry.getValue());
                }
                final Request requestPut = new Request.Builder()
                        .url(this.url)
                        .addHeader(USER_EMAIL_LABEL, USER_EMAIL)
                        .addHeader(USER_TOKEN_LABEL, USER_TOKEN)
                        .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                        .put(formBodyPut.build())
                        .build();
                try {
                    this.response = okHttpClient.newCall(requestPut).execute().body().string();
                    System.out.println("Response POST" + this.response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case DELETE:
                FormEncodingBuilder formBodyDelete = new FormEncodingBuilder();
                for (WeakHashMap.Entry<String, String> entry : nameValue.entrySet()) {
                    System.out.println("Key--" + entry.getKey() + "Value--" + entry.getValue());
                    formBodyDelete.add(entry.getKey(), entry.getValue());
                }
                final Request requestDelete = new Request.Builder()
                        .url(this.url)
                        .addHeader(USER_EMAIL_LABEL, USER_EMAIL)
                        .addHeader(USER_TOKEN_LABEL, USER_TOKEN)
                        .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                        .delete(formBodyDelete.build())
                        .build();
                try {
                    this.response = okHttpClient.newCall(requestDelete).execute().body().string();
                    System.out.println("Response delete" + this.response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case GET:
            default:
                //Get Method Call
                final Request requestGet = new Request.Builder()
                        .url(this.url)
                        .addHeader(USER_EMAIL_LABEL, USER_EMAIL)
                        .addHeader(USER_TOKEN_LABEL, USER_TOKEN)
                        .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                        .get()
                        .build();
                try {
                    this.response = okHttpClient.newCall(requestGet).execute().body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }


    }

    private OkHttpClient createClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(20000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(35000, TimeUnit.MILLISECONDS);
        return okHttpClient;
    }

    @Override
    public String call() throws Exception {
        return requestSyncCall();
    }


}
