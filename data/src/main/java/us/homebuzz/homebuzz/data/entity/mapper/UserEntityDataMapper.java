/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.entity.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.Estimate;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;
import us.homebuzz.homebuzz.domain.data.AccuracyLeaderboardDomain;
import us.homebuzz.homebuzz.domain.data.AddCheckInPhotoData;
import us.homebuzz.homebuzz.domain.data.EstimatesDomain;
import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.NearbyDomain;
import us.homebuzz.homebuzz.domain.data.SubscriptionDomain;
import us.homebuzz.homebuzz.domain.data.UpdateChecking;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.domain.data.ZipCodeDeleteDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingPhoto;
import us.homebuzz.homebuzz.domain.data.addListing.SearchAddressDomain;
import us.homebuzz.homebuzz.domain.data.estimateAverage.EstimateAverageDomain;
import us.homebuzz.homebuzz.domain.data.friends.followed.FriendFollowedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustListedDomain;
import us.homebuzz.homebuzz.domain.data.listing.JustSoldDomain;
import us.homebuzz.homebuzz.domain.data.message.AllMessages;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.data.message.Recipient;
import us.homebuzz.homebuzz.domain.data.message.Sender;
import us.homebuzz.homebuzz.domain.data.message.unread.UnreadMessageDomain;
import us.homebuzz.homebuzz.domain.data.previousEstimate.Listing;
import us.homebuzz.homebuzz.domain.data.previousEstimate.PreviouseEstimate;
import us.homebuzz.homebuzz.domain.data.profile.InviteFriendsDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfileDomain;
import us.homebuzz.homebuzz.domain.data.profile.ProfilePicDomain;
import us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain;
import us.homebuzz.homebuzz.domain.data.searchPeople.SearchPeopleDomain;
import us.homebuzz.homebuzz.domain.data.signup.User_Signup_Domain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodeAddDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.UpdateZipCodeDomain;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodePurchaseDomain;

/**
 * Mapper class used to transform {@link UserEntity} (in the data layer) to {@link User} in the
 * domain layer.
 */
@Singleton
public class UserEntityDataMapper {

    @Inject
    public UserEntityDataMapper() {
    }

    /**
     * Transform a {@link UserEntity} into an {@link User}.
     *
     * @param userEntity Object to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public User transform(UserEntity userEntity) {
        User user = null;
        if (userEntity != null) {
            user = new User(userEntity.getUserId());
            user.setCoverUrl(userEntity.getCoverUrl());
            user.setFullName(userEntity.getFullname());
            user.setDescription(userEntity.getDescription());
            user.setFollowers(userEntity.getFollowers());
            user.setEmail(userEntity.getEmail());
        }

        return user;
    }

    /**
     * Transform a List of {@link User_Entity} into a Collection of {@link User_}.
     *
     * @param userEntity Object  to be transformed.
     * @return {@link User_} if valid {@link User_Entity} otherwise null.
     */
    public User_ transform(User_Entity userEntity) {
        User_ user = null;
        if (userEntity != null) {
            //Make A Class Consisting oF User_ object for
            user = new User_();
            user.setEmail(userEntity.getUser().getEmail());
            user.setId(userEntity.getUser().getId());
            user.setProfilePic(userEntity.getUser().getProfilePic());
            user.setToken(userEntity.getToken());
            user.setToken(userEntity.getToken());
            user.setAccurate(userEntity.getUser().getAccurate());
            user.setEstimatesCount(userEntity.getUser().getEstimatesCount());
            user.setAccurate(userEntity.getUser().getAccurate());
            user.setEstimateZips(userEntity.getUser().getEstimateZips());
            user.setName(userEntity.getUser().getName());
            user.setLevel(userEntity.getUser().getLevel());
            user.setRole(userEntity.getUser().getRole());
            user.setTeamId(userEntity.getUser().getTeamId());
            user.setTeamId(userEntity.getUser().getTeamId());
            user.setRole(userEntity.getUser().getRole());
            user.setZipCode(userEntity.getUser().getZipCode());
        }

        return user;
    }


    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<User> transform(Collection<UserEntity> userEntityCollection) {
        List<User> userList = new ArrayList<>(20);
        User user;
        for (UserEntity userEntity : userEntityCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userNearbyEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<NearbyDomain> transformListing(int user_id, Collection<NearbyEntity> userNearbyEntityCollection) {
        List<NearbyDomain> userList = new ArrayList<>(25);
        NearbyDomain user;
        for (NearbyEntity userEntity : userNearbyEntityCollection) {
            user = transform(user_id, userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public NearbyDomain transform(int user_id, NearbyEntity nearbyEntity) {
        if (nearbyEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        NearbyDomain userNearbyModel = new NearbyDomain();
        userNearbyModel.setId(nearbyEntity.getId());
        userNearbyModel.setForSale(nearbyEntity.getForSale());
        userNearbyModel.setPhotos(nearbyEntity.getPhotos());
        userNearbyModel.setZip(nearbyEntity.getZip());
        userNearbyModel.setAddress(nearbyEntity.getAddress());
        userNearbyModel.setUserId(nearbyEntity.getUserId());
        userNearbyModel.setAgentId(nearbyEntity.getAgentId());
        userNearbyModel.setBaths(nearbyEntity.getBaths());
        userNearbyModel.setBedrooms(nearbyEntity.getBedrooms());
        userNearbyModel.setCity(nearbyEntity.getCity());
        userNearbyModel.setLat(nearbyEntity.getLat());
        userNearbyModel.setLon(nearbyEntity.getLon());
        userNearbyModel.setLocationRating(nearbyEntity.getLocationRating());
        userNearbyModel.setConditionRating(nearbyEntity.getConditionRating());
        userNearbyModel.setLastSoldPrice(nearbyEntity.getLastSoldPrice());
        userNearbyModel.setYearBuilt(nearbyEntity.getYearBuilt());
        userNearbyModel.setStatus(nearbyEntity.getStatus());
        userNearbyModel.setRemarks(nearbyEntity.getRemarks());
        userNearbyModel.setFavorited(nearbyEntity.getFavorited());
        userNearbyModel.setPrice(nearbyEntity.getPrice());
        userNearbyModel.setEstimatedSquareFeet(nearbyEntity.getEstimatedSquareFeet());
        userNearbyModel.setAcres(nearbyEntity.getAcres());
        userNearbyModel.setState(nearbyEntity.getState());
        userNearbyModel.setFireplaces(nearbyEntity.getFireplaces());
        userNearbyModel.setLotSqftApprox(nearbyEntity.getLotSqftApprox());
        userNearbyModel.setGarage(nearbyEntity.getGarage());
        userNearbyModel.setEstimates_count(nearbyEntity.getEstimates_count());
        userNearbyModel.setReviews_count(nearbyEntity.getReviews_count());
        userNearbyModel.setStreet(nearbyEntity.getStreet());
        userNearbyModel.setCheckin_count(nearbyEntity.getCheckin_count());
        userNearbyModel.setOptionalBedrooms(nearbyEntity.getOptionalBedrooms());
        if (nearbyEntity.getEstimates() != null && nearbyEntity.getEstimates().size() > 0) {
            ArrayList<NearbyDomain.Estimate> data = new ArrayList<>();
            //Loop through the Value and Set IF IsEstimated
            // Log.e("ValueinEntitydata mappe", "Near by domain");
            for (Estimate estimate : nearbyEntity.getEstimates()) {
                NearbyDomain.Estimate estimateData = userNearbyModel.new Estimate();
                estimateData.setId(estimate.getId());
                estimateData.setUserId(estimate.getUserId());
                estimateData.setPrice(estimate.getPrice());
                estimateData.setLocationRating(estimate.getLocationRating());
                estimateData.setConditionRating(estimate.getConditionRating());
                estimateData.setImageUrl(estimate.getImage_url());
                //Log.e("List size entity", String.valueOf(estimate.getImage_url().size()));
                estimateData.setName(estimate.getName());
                estimateData.setReason(estimate.getReason());
                // Log.e("ValueinEntitydata value", estimate.getPrice().toString());
                data.add(estimateData);
            }
            userNearbyModel.setEstimates(data);
        }

        return userNearbyModel;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userAccuracyEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<AccuracyLeaderboardDomain> transformAccuracy(Collection<AccuracyEntity> userAccuracyEntityCollection) {
        List<AccuracyLeaderboardDomain> userList = new ArrayList<>(25);
        AccuracyLeaderboardDomain user;
        for (AccuracyEntity userEntity : userAccuracyEntityCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public AccuracyLeaderboardDomain transform(AccuracyEntity accuracyEntity) {
        if (accuracyEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AccuracyLeaderboardDomain userAccuracyDomain = new AccuracyLeaderboardDomain();
        userAccuracyDomain.setId(accuracyEntity.getId());
        userAccuracyDomain.setEstimatesCount(accuracyEntity.getEstimatesCount());
        userAccuracyDomain.setLevel(accuracyEntity.getLevel());
        userAccuracyDomain.setTeamId(accuracyEntity.getTeamId());
        userAccuracyDomain.setEmail(accuracyEntity.getEmail());
        userAccuracyDomain.setEstimateZips(accuracyEntity.getEstimateZips());
        userAccuracyDomain.setRole(accuracyEntity.getRole());
        userAccuracyDomain.setZipCode(accuracyEntity.getZipCode());
        userAccuracyDomain.setProfilePic(accuracyEntity.getProfilePic());
        userAccuracyDomain.setAccurate(accuracyEntity.getAccurate());
        userAccuracyDomain.setName(accuracyEntity.getName());
        userAccuracyDomain.setPhone(accuracyEntity.getPhone());
        userAccuracyDomain.setFirstName(accuracyEntity.getFirstName());
        userAccuracyDomain.setFollowed(accuracyEntity.getFollowed());
        /*userAccuracyDomain.setId(accuracyEntity.getId());
        userAccuracyDomain.setId(accuracyEntity.getId());*/
        return userAccuracyDomain;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userEstimatesEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<EstimatesDomain> transformEstimates(Collection<EstimatesEntity> userEstimatesEntityCollection) {
        List<EstimatesDomain> userList = new ArrayList<>(25);
        EstimatesDomain user;
        for (EstimatesEntity userEntity : userEstimatesEntityCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public EstimatesDomain transform(EstimatesEntity estimatesEntity) {
        if (estimatesEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        EstimatesDomain estimatesDomain = new EstimatesDomain();
        estimatesDomain.setId(estimatesEntity.getId());
        estimatesDomain.setEstimatesCount(estimatesEntity.getEstimatesCount());
        estimatesDomain.setLevel(estimatesEntity.getLevel());
        estimatesDomain.setTeamId(estimatesEntity.getTeamId());
        estimatesDomain.setEmail(estimatesEntity.getEmail());
        estimatesDomain.setEstimateZips(estimatesEntity.getEstimateZips());
        estimatesDomain.setRole(estimatesEntity.getRole());
        estimatesDomain.setZipCode(estimatesEntity.getZipCode());
        estimatesDomain.setProfilePic(estimatesEntity.getProfilePic());
        estimatesDomain.setAccurate(estimatesEntity.getAccurate());
        estimatesDomain.setName(estimatesEntity.getName());
        estimatesDomain.setPhone(estimatesEntity.getPhone());
        estimatesDomain.setFirstName(estimatesEntity.getFirstName());
        return estimatesDomain;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userEstimatesEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<AllMessages> transformAllMessages(Collection<AllMessagesEntity> userEstimatesEntityCollection) {
        List<AllMessages> userList = new ArrayList<>(25);
        AllMessages user;
        for (AllMessagesEntity userEntity : userEstimatesEntityCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public AllMessages transform(AllMessagesEntity allMessagesEntity) {
        if (allMessagesEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AllMessages allMessages = new AllMessages();
        allMessages.setId(allMessagesEntity.getId());
        allMessages.setListingId(allMessagesEntity.getListingId());
        allMessages.setStatus(allMessagesEntity.getStatus());
        allMessages.setBody(allMessagesEntity.getBody());
        allMessages.setCreatedAt(allMessagesEntity.getCreatedAt());
        System.out.println(" body" + allMessagesEntity.getBody());
        // System.out.println("For All Messages Sender" + allMessagesEntity.getSender());
        if (allMessagesEntity.getSender() != null) {
            Sender sender = new Sender();
            sender.setId((allMessagesEntity.getSender().getId() == null) ? 0 : allMessagesEntity.getSender().getId());
            sender.setEmail(allMessagesEntity.getSender().getEmail());
            sender.setName(allMessagesEntity.getSender().getName());
            sender.setProfile_pic(allMessagesEntity.getSender().getProfilePic());
            allMessages.setSender(sender);
        }
        if (allMessagesEntity.getRecipient() != null) {
            Recipient recipient = new Recipient();
            recipient.setId(allMessagesEntity.getRecipient().getId());
            recipient.setEmail(allMessagesEntity.getRecipient().getEmail());
            recipient.setName(allMessagesEntity.getRecipient().getName());
            allMessages.setRecipient(recipient);
        }
        return allMessages;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userPreviousEstimateCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<PreviouseEstimate> transformPreviouseEstimate(Collection<PreviousEstimate> userPreviousEstimateCollection) {
        List<PreviouseEstimate> userList = new ArrayList<>(25);
        PreviouseEstimate user;
        for (PreviousEstimate userEntity : userPreviousEstimateCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public PreviouseEstimate transform(PreviousEstimate previousEstimateEntity) {
        if (previousEstimateEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        PreviouseEstimate previouseEstimate = new PreviouseEstimate();
        previouseEstimate.setId(previousEstimateEntity.getId());
        previouseEstimate.setListingId(previousEstimateEntity.getListingId());
        previouseEstimate.setLocationRating(previousEstimateEntity.getLocationRating());
        previouseEstimate.setConditionRating(previousEstimateEntity.getConditionRating());
        previouseEstimate.setPrice(previousEstimateEntity.getPrice());
        previouseEstimate.setReason(previousEstimateEntity.getReason());
        previouseEstimate.setPrice(previousEstimateEntity.getPrice());
        if (previousEstimateEntity.getListing() != null) {
            Listing listing = new Listing();
            listing.setAcres(previousEstimateEntity.getListing().getAcres());
            listing.setEstimatedSquareFeet(previousEstimateEntity.getListing().getEstimatedSquareFeet());
            listing.setId(previousEstimateEntity.getListing().getId());
            listing.setForSale(previousEstimateEntity.getListing().getForSale());
            listing.setPhotos(previousEstimateEntity.getListing().getPhotos());
            listing.setZip(previousEstimateEntity.getListing().getZip());
            listing.setAddress(previousEstimateEntity.getListing().getAddress());
            listing.setUserId(previousEstimateEntity.getListing().getUserId());
            listing.setAgentId(previousEstimateEntity.getListing().getAgentId());
            listing.setBaths(previousEstimateEntity.getListing().getBaths());
            listing.setBedrooms(previousEstimateEntity.getListing().getBedrooms());
            listing.setCity(previousEstimateEntity.getListing().getCity());
            listing.setLat(previousEstimateEntity.getListing().getLat());
            listing.setLon(previousEstimateEntity.getListing().getLon());
            listing.setLocationRating(previousEstimateEntity.getListing().getLocationRating());
            listing.setConditionRating(previousEstimateEntity.getListing().getConditionRating());
            listing.setLastSoldPrice(previousEstimateEntity.getListing().getLastSoldPrice());
            listing.setYearBuilt(previousEstimateEntity.getListing().getYearBuilt());
            listing.setStatus(previousEstimateEntity.getListing().getStatus());
            listing.setRemarks(previousEstimateEntity.getListing().getRemarks());
            listing.setFavorited(previousEstimateEntity.getListing().getFavorited());
            listing.setPrice(previousEstimateEntity.getListing().getPrice());
            listing.setState(previousEstimateEntity.getListing().getState());
            listing.setStreet(previousEstimateEntity.getListing().getStreet());
            previouseEstimate.setListing(listing);
        }
        if (previousEstimateEntity.getUser() != null) {
            us.homebuzz.homebuzz.domain.data.previousEstimate.User user = new us.homebuzz.homebuzz.domain.data.previousEstimate.User();
            user.setEmail(previousEstimateEntity.getUser().getEmail());
            user.setId(previousEstimateEntity.getUser().getId());
            user.setProfilePic(previousEstimateEntity.getUser().getProfilePic());
            user.setAccurate(previousEstimateEntity.getUser().getAccurate());
            user.setEstimatesCount(previousEstimateEntity.getUser().getEstimatesCount());
            user.setAccurate(previousEstimateEntity.getUser().getAccurate());
            user.setName(previousEstimateEntity.getUser().getName());
            user.setYrGraduated(previousEstimateEntity.getUser().getYrGraduated());
            user.setRole(previousEstimateEntity.getUser().getRole());
            user.setPhone(previousEstimateEntity.getUser().getPhone());
            user.setEducations(previousEstimateEntity.getUser().getEducations());
            user.setBio(previousEstimateEntity.getUser().getBio());
            user.setZipCode(previousEstimateEntity.getUser().getZipCode());
            user.setFollowed(previousEstimateEntity.getUser().getFollowed());
            previouseEstimate.setUser(user);
        }
        return previouseEstimate;
    }

    /**
     * TranForm  A ProfileEntity to ProfileDomain
     *
     * @param profileEntity
     * @return
     */
    public ProfileDomain transformProfile(ProfileEntity profileEntity) {
        if (profileEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ProfileDomain profileDomain = new ProfileDomain();
        profileDomain.setId(profileEntity.getId());
        profileDomain.setUserId(profileEntity.getUserId());


        profileDomain.setEmail(profileEntity.getEmail());
        profileDomain.setSignInCount(profileEntity.getSignInCount());
        profileDomain.setCurrentSignInAt(profileEntity.getCurrentSignInAt());
        profileDomain.setLastSignInAt(profileEntity.getLastSignInAt());
        profileDomain.setConfirmedAt(profileEntity.getConfirmedAt());
        profileDomain.setName(profileEntity.getName());
        profileDomain.setZipCode(profileEntity.getZipCode());
        profileDomain.setRole(profileEntity.getRole());
        profileDomain.setFacebookId(profileEntity.getFacebookId());
        profileDomain.setAccurate(profileEntity.getAccurate());
        profileDomain.setPoints(profileEntity.getPoints());
        profileDomain.setTeamId(profileEntity.getTeamId());
        profileDomain.setOfficeId(profileEntity.getOfficeId());
        profileDomain.setFirstName(profileEntity.getFirstName());
        profileDomain.setLastName(profileEntity.getLastName());
        profileDomain.setSubscriptionActive(profileEntity.getSubscriptionActive());
        profileDomain.setConfirmed(profileEntity.getConfirmed());
        profileDomain.setFollowed(profileEntity.getFollowed());
        profileDomain.setSoldListings(profileEntity.getSoldListings());
        profileDomain.setFollowers(profileEntity.getFollowers());
        profileDomain.setFollowings(profileEntity.getFollowings());
        profileDomain.setEstimatesCount(profileEntity.getEstimatesCount());
        profileDomain.setCheckinCount(profileEntity.getCheckinsCount());
        profileDomain.setReviewsCount(profileEntity.getReviesCount());
        profileDomain.setPhotos(profileEntity.getPhotos());
        profileDomain.setEducations(profileEntity.getEducations());
        profileDomain.setBio(profileEntity.getBio());
        profileDomain.setDegree(profileEntity.getDegree());
        profileDomain.setYrGraduated(profileEntity.getYrGraduated());
        profileDomain.setOtherSchool(profileEntity.getOtherSchool());
        profileDomain.setDesignations(profileEntity.getDesignations());
        profileDomain.setPhone(profileEntity.getPhone());
        profileDomain.setProfilePic(profileEntity.getProfilePic());
        profileDomain.setAgentId(profileEntity.getAgentId());
        profileDomain.setWebsite(profileEntity.getWebsite());
        profileDomain.setTwitterUrl(profileEntity.getTwitterUrl());
        profileDomain.setFacebookUrl(profileEntity.getFacebookUrl());
        profileDomain.setPhone(profileEntity.getPhone());
        profileDomain.setPhone((String) profileEntity.getPhone());
        profileDomain.setCity((String) profileEntity.getCity());
        profileDomain.setState((String) profileEntity.getState());
        profileDomain.setStreet((String) profileEntity.getStreet());
        profileDomain.setLinkedIn(profileEntity.getLinkedIn());
        profileDomain.setJobTitle(profileEntity.getJobTitle());
        profileDomain.setCompany(profileEntity.getCompany());
        profileDomain.setMlsName(profileEntity.getMlsName());
        profileDomain.setNapNum(profileEntity.getNapNum());
        profileDomain.setLicenseNum(profileEntity.getLicenseNum());
        profileDomain.setProfileId(profileEntity.getProfileId());
        profileDomain.setCollege(profileEntity.getCollege());
        return profileDomain;
    }

    public UpdateProfileDomain transformUpdateProfile(UpdateProfile profileEntity) {
        if (profileEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UpdateProfileDomain profileDomain = new UpdateProfileDomain();
     /*   profileDomain.setToken(profileEntity.getToken());
        profileDomain.setConfirmed(profileEntity.getConfirmed());*/
//OuterClass.InnerClass innerObject = outerObject.new InnerClass();

        us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain.User user = profileDomain.new User();
        user.setEmail(profileEntity.getEmail());
        user.setName(profileEntity.getName());
        user.setZipCode(profileEntity.getZipCode());
        user.setRole(profileEntity.getRole());
        user.setFacebookId(profileEntity.getFacebookId());
        user.setAccurate(profileEntity.getAccurate());
        user.setPhone(profileEntity.getPhone());
        user.setTeamId(profileEntity.getTeamId());
        user.setOfficeId(profileEntity.getOfficeId());
        user.setFirstName(profileEntity.getFirstName());
        user.setLastName(profileEntity.getLastName());
        user.setSubscriptionActive(profileEntity.getSubscriptionActive());
        user.setEstimateZips(profileEntity.getEstimateZips());
        user.setEstimatesCount(profileEntity.getEstimatesCount());
        profileDomain.setUser(user);

        return profileDomain;
    }

    /**
     * TranForm  A ProfileEntity to ProfileDomain
     *
     * @param SigUser_signup
     * @return
     */
    public User_Signup_Domain transformSignUp(User_Signup SigUser_signup) {
        if (SigUser_signup == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        User_Signup_Domain user_signup_domain = new User_Signup_Domain();
        user_signup_domain.setErrors(SigUser_signup.getErrors());
        if (SigUser_signup.getUser() != null) {
            us.homebuzz.homebuzz.domain.data.signup.User user = new us.homebuzz.homebuzz.domain.data.signup.User();
            user.setEmail(SigUser_signup.getUser().getEmail());
            user.setId(SigUser_signup.getUser().getId());
            user.setProfilePic(SigUser_signup.getUser().getProfilePic());
            user.setInvitationToken(SigUser_signup.getUser().getInvitationToken());
            user.setInvitedById(SigUser_signup.getUser().getInvitedById());
            user.setAccurate(SigUser_signup.getUser().getAccurate());
            user.setEstimatesCount(SigUser_signup.getUser().getEstimatesCount());
            user.setAccurate(SigUser_signup.getUser().getAccurate());
            user.setEstimateZips(SigUser_signup.getUser().getEstimateZips());
            user.setName(SigUser_signup.getUser().getName());
            user.setLevel(SigUser_signup.getUser().getLevel());
            user.setRole(SigUser_signup.getUser().getRole());
            user.setTeamId(SigUser_signup.getUser().getTeamId());
            user.setRole(SigUser_signup.getUser().getRole());
            user.setZipCode(SigUser_signup.getUser().getZipCode());
            user_signup_domain.setUser(user);
        }
        return user_signup_domain;
    }

    /**
     * Transform a List of {@link FriendFollowed} into a Collection of {@link FriendFollowedDomain}.
     *
     * @param userFriendFollowedCollection Object Collection to be transformed.
     * @return {@link FriendFollowedDomain} if valid {@link FriendFollowed} otherwise null.
     */
    public List<FriendFollowedDomain> transformFriendsFollowed(Collection<FriendFollowed> userFriendFollowedCollection) {
        List<FriendFollowedDomain> friendFollowedList = new ArrayList<>(25);
        FriendFollowedDomain friendFollowedDomain;
        for (FriendFollowed userEntity : userFriendFollowedCollection) {
            friendFollowedDomain = transform(userEntity);
            if (friendFollowedDomain != null) {
                friendFollowedList.add(friendFollowedDomain);
            }
        }

        return friendFollowedList;
    }

    public FriendFollowedDomain transform(FriendFollowed FriendFollowedDomainEntity) {
        if (FriendFollowedDomainEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        FriendFollowedDomain friendFollowedDomain = new FriendFollowedDomain();
        friendFollowedDomain.setId(FriendFollowedDomainEntity.getId());
        friendFollowedDomain.setEmail(FriendFollowedDomainEntity.getEmail());
        friendFollowedDomain.setName(FriendFollowedDomainEntity.getName());
        friendFollowedDomain.setZipCode(FriendFollowedDomainEntity.getZipCode());
        friendFollowedDomain.setRole(FriendFollowedDomainEntity.getRole());
        friendFollowedDomain.setEstimatesCount(FriendFollowedDomainEntity.getEstimatesCount());
        friendFollowedDomain.setAccurate(FriendFollowedDomainEntity.getAccurate());
        friendFollowedDomain.setProfilePic(FriendFollowedDomainEntity.getProfilePic());
        friendFollowedDomain.setFollowed(FriendFollowedDomainEntity.getFollowed());
        friendFollowedDomain.setFirstName(FriendFollowedDomainEntity.getFirstName());
        friendFollowedDomain.setLastName(FriendFollowedDomainEntity.getLastName());
        // friendFollowedDomain.setAccurate(FriendFollowedDomainEntity.getAccurate());

        return friendFollowedDomain;
    }

    public AddListingDomain transform(AddListingEntity addListingEntity) {
        if (addListingEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddListingDomain userNearbyModel = new AddListingDomain();
        userNearbyModel.setId(addListingEntity.getId());
        userNearbyModel.setZip(addListingEntity.getZip());
        userNearbyModel.setUserId(addListingEntity.getUserId());
        userNearbyModel.setBaths(addListingEntity.getBaths());
        userNearbyModel.setBedrooms(addListingEntity.getBedrooms());
        userNearbyModel.setLocationRating(addListingEntity.getLocationRating());
        userNearbyModel.setConditionRating(addListingEntity.getConditionRating());
        userNearbyModel.setLastSoldPrice(addListingEntity.getLastSoldPrice());
        userNearbyModel.setYearBuilt(addListingEntity.getYearBuilt());
        userNearbyModel.setRemarks(addListingEntity.getRemarks());
        userNearbyModel.setFavorited(addListingEntity.getFavorited());
        userNearbyModel.setPrice(addListingEntity.getPrice());
        userNearbyModel.setLat((Double) addListingEntity.getLat());
        userNearbyModel.setLon((Double) addListingEntity.getLon());
        userNearbyModel.setEstimatedSquareFeet(addListingEntity.getEstimatedSquareFeet());
        userNearbyModel.setAcres(addListingEntity.getAcres());
        return userNearbyModel;
    }

    public AddListingPhoto transformPhoto(AddListingPhotoEntity addListingEntity) {
        if (addListingEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddListingPhoto addListingPhoto = new AddListingPhoto();
        addListingPhoto.setUrl(addListingEntity.getUrl());
        System.out.println("Add photo Url in Entity" + addListingEntity.getUrl());
        return addListingPhoto;
    }

    public AddCheckInPhotoData transformPhoto(AddCheckInPhotoEntity addListingEntity) {
        if (addListingEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddCheckInPhotoData addCheckInPhoto = new AddCheckInPhotoData();
        addCheckInPhoto.setUrl(addListingEntity.getUrl());
        System.out.println("Add photo Url in Entity" + addListingEntity.getUrl());
        return addCheckInPhoto;
    }

    public MessageReply transformMessageReply(ReplyMessageEntity ReplyMessageEntity) {
        if (ReplyMessageEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        MessageReply messageReply = new MessageReply();
        messageReply.setBody(ReplyMessageEntity.getBody());
        messageReply.setCreated_at(ReplyMessageEntity.getCreatedAt());
        messageReply.setId(String.valueOf(ReplyMessageEntity.getId()));
        messageReply.setRecipient_id(String.valueOf(ReplyMessageEntity.getRecipientId()));
        messageReply.setSender_id(String.valueOf(ReplyMessageEntity.getSenderId()));
        messageReply.setStatus(ReplyMessageEntity.getStatus());
        messageReply.setUpdated_at(ReplyMessageEntity.getUpdatedAt());
        return messageReply;
    }

    public UpdateChecking transformUpdateCheckinEstimate(UpdateCheckinEntity updateCheckinEntity) {
        if (updateCheckinEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UpdateChecking updateChecking = new UpdateChecking();
        UpdateChecking.Listing listing = updateChecking.new Listing();
        listing.setUserId(updateCheckinEntity.getListing().getUserId());
        listing.setId(updateCheckinEntity.getListing().getId());
        if (updateCheckinEntity.getListing().getEstimates() != null && updateCheckinEntity.getListing().getEstimates().size() > 0) {
            ArrayList<UpdateChecking.Estimate> data = new ArrayList<>();
            for (us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.Estimate estimate : updateCheckinEntity.getListing().getEstimates()) {
                UpdateChecking.Estimate estimateData = updateChecking.new Estimate();
                estimateData.setId(estimate.getId());
                estimateData.setUserId(estimate.getUserId());
                estimateData.setListingId(estimate.getListingId());
                data.add(estimateData);
            }
            updateChecking.setListing(listing);
            updateChecking.getListing().setEstimates(data);
        }
        return updateChecking;
    }

    /**
     * Transform a List of {@link UserEntity} into a Collection of {@link User}.
     *
     * @param userEstimatesEntityCollection Object Collection to be transformed.
     * @return {@link User} if valid {@link UserEntity} otherwise null.
     */
    public List<UnreadMessageDomain> transformUnreadMessages(Collection<MessageEntity> userEstimatesEntityCollection) {
        List<UnreadMessageDomain> userList = new ArrayList<>(25);
        UnreadMessageDomain user;
        for (MessageEntity userEntity : userEstimatesEntityCollection) {
            user = transform(userEntity);
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    public UnreadMessageDomain transform(MessageEntity allMessagesEntity) {
        if (allMessagesEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UnreadMessageDomain allMessages = new UnreadMessageDomain();
        allMessages.setId(allMessagesEntity.getId());
        allMessages.setStatus(allMessagesEntity.getStatus());
        allMessages.setBody(allMessagesEntity.getBody());
        allMessages.setCreatedAt(allMessagesEntity.getCreatedAt());
        System.out.println(" body" + allMessagesEntity.getBody());
        // System.out.println("For All Messages Sender" + allMessagesEntity.getSender());
        if (allMessagesEntity.getSender() != null) {
            Sender sender = new Sender();
            sender.setId((allMessagesEntity.getSender().getId() == null) ? 0 : allMessagesEntity.getSender().getId());
            sender.setEmail(allMessagesEntity.getSender().getEmail());
            sender.setName(allMessagesEntity.getSender().getName());
            sender.setProfile_pic(allMessagesEntity.getSender().getProfilePic());
            allMessages.setSender(sender);
        }
        if (allMessagesEntity.getRecipient() != null) {
            Recipient recipient = new Recipient();
            recipient.setId(allMessagesEntity.getRecipient().getId());
            recipient.setEmail(allMessagesEntity.getRecipient().getEmail());
            recipient.setName(allMessagesEntity.getRecipient().getName());
            allMessages.setRecipient(recipient);
        }
        return allMessages;
    }

    public List<JustSoldDomain> transformJustSold(JustSoldEntity justSoldEntity) {
        if (justSoldEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        List<JustSoldDomain> justSoldDomainList = new ArrayList<>(25);
        for (JustSoldEntity.JustLisitng justLisitng : justSoldEntity.getListings()) {
            JustSoldDomain justSoldDomain = new JustSoldDomain();
            justSoldDomain.setId(justLisitng.getId());
            justSoldDomain.setForSale(justLisitng.getForSale());
            justSoldDomain.setPhotos(justLisitng.getPhotos());
            justSoldDomain.setZip(justLisitng.getZip());
            justSoldDomain.setAddress(justLisitng.getAddress());
            justSoldDomain.setUserId(justLisitng.getUserId());
            justSoldDomain.setAgentId(justLisitng.getAgentId());
            justSoldDomain.setBaths(justLisitng.getBaths());
            justSoldDomain.setBedrooms(justLisitng.getBedrooms());
            justSoldDomain.setCity(justLisitng.getCity());
            justSoldDomain.setLat(justLisitng.getLat());
            justSoldDomain.setLon(justLisitng.getLon());
            justSoldDomain.setLocationRating(justLisitng.getLocationRating());
            justSoldDomain.setConditionRating(justLisitng.getConditionRating());
            justSoldDomain.setLastSoldPrice(justLisitng.getLastSoldPrice());
            justSoldDomain.setYearBuilt(justLisitng.getYearBuilt());
            justSoldDomain.setStatus(justLisitng.getStatus());
            justSoldDomain.setRemarks(justLisitng.getRemarks());
            justSoldDomain.setPrice(justLisitng.getPrice());
            justSoldDomain.setEstimatedSquareFeet(justLisitng.getEstimatedSquareFeet());
            justSoldDomain.setAcres(justLisitng.getAcres());
            justSoldDomain.setState(justLisitng.getState());
            justSoldDomainList.add(justSoldDomain);
            justSoldDomain.setStreet(justLisitng.getStreet());
        }
        return justSoldDomainList;
    }

    public List<JustListedDomain> transformJustListed(JustListedEntity justListedEntity) {
        if (justListedEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        List<JustListedDomain> justSoldDomainList = new ArrayList<>(25);
        for (JustListedEntity.JustListing justLisitng : justListedEntity.getListings()) {
            JustListedDomain justSoldDomain = new JustListedDomain();
            justSoldDomain.setId(justLisitng.getId());
            justSoldDomain.setForSale(justLisitng.getForSale());
            justSoldDomain.setPhotos(justLisitng.getPhotos());
            justSoldDomain.setZip(justLisitng.getZip());
            justSoldDomain.setAddress(justLisitng.getAddress());
            justSoldDomain.setUserId(justLisitng.getUserId());
            justSoldDomain.setAgentId(justLisitng.getAgentId());
            justSoldDomain.setBaths(justLisitng.getBaths());
            justSoldDomain.setBedrooms(justLisitng.getBedrooms());
            justSoldDomain.setCity(justLisitng.getCity());
            justSoldDomain.setLat(justLisitng.getLat());
            justSoldDomain.setLon(justLisitng.getLon());
            justSoldDomain.setLocationRating(justLisitng.getLocationRating());
            justSoldDomain.setConditionRating(justLisitng.getConditionRating());
            justSoldDomain.setLastSoldPrice(justLisitng.getLastSoldPrice());
            justSoldDomain.setYearBuilt(justLisitng.getYearBuilt());
            justSoldDomain.setStatus(justLisitng.getStatus());
            justSoldDomain.setRemarks(justLisitng.getRemarks());
            justSoldDomain.setPrice(justLisitng.getPrice());
            justSoldDomain.setEstimatedSquareFeet(justLisitng.getEstimatedSquareFeet());
            justSoldDomain.setAcres(justLisitng.getAcres());
            justSoldDomain.setState(justLisitng.getState());
            justSoldDomain.setStreet(justLisitng.getStreet());
            justSoldDomainList.add(justSoldDomain);


        }
        return justSoldDomainList;
    }

    public FollowUnfollowData transformFollowUnfollow(FollowUnfollowEntity followUnfollowEntity) {
        if (followUnfollowEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        FollowUnfollowData followUnfollowData = new FollowUnfollowData();
        followUnfollowData.setFollowed(followUnfollowEntity.getFollowed());
        return followUnfollowData;
    }

    public InviteFriendsDomain transformInviteFriends(InviteFriends inviteFriends) {
        if (inviteFriends == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        InviteFriendsDomain inviteFriendsDomain = new InviteFriendsDomain();
        inviteFriendsDomain.setSuccess(inviteFriends.getSuccess());
        return inviteFriendsDomain;
    }

    /**
     * Transform a
     *
     * @param allMessagesCollection Object to be transformed.
     * @return {@link ZipCodePurchaseDomain}.
     */
    public List<ZipCodePurchaseDomain> transformZipPurchase(Collection<ZipPurchaseEntity> allMessagesCollection) {
        List<ZipCodePurchaseDomain> userNearbyModelCollection;

        if (allMessagesCollection != null && !allMessagesCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (ZipPurchaseEntity allMessages : allMessagesCollection) {
                userNearbyModelCollection.add(transformZip(allMessages));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }

    //Zip Code Purchase Transformation
    public ZipCodePurchaseDomain transformZip(ZipPurchaseEntity zipPurchaseEntity) {
        if (zipPurchaseEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipCodePurchaseDomain zipPurchaseDomain = new ZipCodePurchaseDomain();
        zipPurchaseDomain.setId(zipPurchaseEntity.getId());
        zipPurchaseDomain.setUserId(zipPurchaseEntity.getUserId());
        zipPurchaseDomain.setExpired(zipPurchaseEntity.getExpired());
        zipPurchaseDomain.setExpiredAt(zipPurchaseEntity.getExpiredAt());
        zipPurchaseDomain.setZipCode(zipPurchaseEntity.getZipCode());
        return zipPurchaseDomain;
    }

    //Search On Board Prop Transformation
    public SearchAddressDomain transformOnBoardProp(SearchAddressEntity searchAddressEntity) {
        if (searchAddressEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SearchAddressDomain searchAddressDomain = new SearchAddressDomain();
        searchAddressDomain.setId(searchAddressEntity.getId());
        searchAddressDomain.setForSale(searchAddressEntity.getForSale());
        searchAddressDomain.setPhotos(searchAddressEntity.getPhotos());
        searchAddressDomain.setZip(searchAddressEntity.getZip());
        searchAddressDomain.setAddress(searchAddressEntity.getAddress());
        searchAddressDomain.setUserId(searchAddressEntity.getUserId());
        searchAddressDomain.setAgentId(searchAddressEntity.getAgentId());
        searchAddressDomain.setBaths(searchAddressEntity.getBaths());
        searchAddressDomain.setBedrooms(searchAddressEntity.getBedrooms());
        searchAddressDomain.setCity(searchAddressEntity.getCity());
        searchAddressDomain.setLat(searchAddressEntity.getLat());
        searchAddressDomain.setLon(searchAddressEntity.getLon());
        searchAddressDomain.setLocationRating(searchAddressEntity.getLocationRating());
        searchAddressDomain.setConditionRating(searchAddressEntity.getConditionRating());
        searchAddressDomain.setLastSoldPrice(searchAddressEntity.getLastSoldPrice());
        searchAddressDomain.setYearBuilt(searchAddressEntity.getYearBuilt());
        searchAddressDomain.setStatus(searchAddressEntity.getStatus());
        searchAddressDomain.setRemarks(searchAddressEntity.getRemarks());
        searchAddressDomain.setPrice(searchAddressEntity.getPrice());
        searchAddressDomain.setEstimatedSquareFeet(searchAddressEntity.getEstimatedSquareFeet());
        searchAddressDomain.setAcres(searchAddressEntity.getAcres());
        searchAddressDomain.setState(searchAddressEntity.getState());
        searchAddressDomain.setStreet(searchAddressEntity.getStreet());
        searchAddressDomain.setParcelNum(searchAddressEntity.getParcelNum());
        return searchAddressDomain;
    }

    //User Profile Pic Update
    public ProfilePicDomain transformProfilePic(ProfilePicEntity profilePicEntity) {
        if (profilePicEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ProfilePicDomain searchAddressDomain = new ProfilePicDomain();
        searchAddressDomain.setImageUrl(profilePicEntity.getImageUrl());
        return searchAddressDomain;
    }

    public List<SearchPeopleDomain> transformSearchPeople(Collection<SearchPeopleEntity> searchPeopleEntity) {
        List<SearchPeopleDomain> SearchPeopleDomain = new ArrayList<>();
        if (searchPeopleEntity != null) {
            for (SearchPeopleEntity userEntity : searchPeopleEntity) {
                SearchPeopleDomain.add(transform(userEntity));

            }
        } else {
            SearchPeopleDomain = Collections.emptyList();
        }

        return SearchPeopleDomain;
    }

    public SearchPeopleDomain transform(SearchPeopleEntity searchPeopleEntity) {
        if (searchPeopleEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SearchPeopleDomain searchPeopleDomain = new SearchPeopleDomain();
        // searchPeopleModel.setImageUrl(searchPeopleDomain.getImageUrl());
        searchPeopleDomain.setAccurate(searchPeopleEntity.getAccurate());
        searchPeopleDomain.setProfilePic(searchPeopleEntity.getProfilePic());
        searchPeopleDomain.setEmail(searchPeopleEntity.getEmail());
        searchPeopleDomain.setName(searchPeopleEntity.getName());
        searchPeopleDomain.setEstimateZips(searchPeopleEntity.getEstimateZips());
        searchPeopleDomain.setEstimatesCount(searchPeopleEntity.getEstimatesCount());
        searchPeopleDomain.setFirstName(searchPeopleEntity.getFirstName());
        searchPeopleDomain.setZipCode(searchPeopleEntity.getZipCode());
        searchPeopleDomain.setLastName(searchPeopleEntity.getLastName());
        searchPeopleDomain.setId(searchPeopleEntity.getId());
        searchPeopleDomain.setPhone(searchPeopleEntity.getPhone());
        searchPeopleDomain.setRole(searchPeopleEntity.getRole());
        //  searchPeopleModel.setInvitaion(searchPeopleDomain.getEstimateZips());
        return searchPeopleDomain;
    }

    public EstimateAverageDomain transformAverageEstimate(EstimateAverage estimateAverage) {
        if (estimateAverage == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        EstimateAverageDomain estimateAverageDomain = new EstimateAverageDomain();
        estimateAverageDomain.setAccurate(estimateAverage.getAccurate());
        estimateAverageDomain.setImageUrl(estimateAverage.getImageUrl());
        estimateAverageDomain.setPrice(estimateAverage.getPrice());
        estimateAverageDomain.setLocationRating(estimateAverage.getLocationRating());
        estimateAverageDomain.setName(estimateAverage.getName());
        return estimateAverageDomain;
    }

    public ZipCodeAddDomain transformZipCodeAdd(ZipAddEntity zipAddEntity) {
        if (zipAddEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipCodeAddDomain zipCodeAddDomain = new ZipCodeAddDomain();
        zipCodeAddDomain.setId(zipAddEntity.getId());
        zipCodeAddDomain.setUserId(zipAddEntity.getUserId());
        zipCodeAddDomain.setZipCode(zipAddEntity.getZipCode());
        zipCodeAddDomain.setExpiredAt(zipAddEntity.getExpiredAt());
        zipCodeAddDomain.setCreatedAt(zipAddEntity.getCreatedAt());
        zipCodeAddDomain.setUpdatedAt(zipAddEntity.getUpdatedAt());
        return zipCodeAddDomain;
    }

    public UpdateZipCodeDomain transformUpdtateZipCode(UpdateZipCodeEntity updateZipCodeEntity) {
        if (updateZipCodeEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UpdateZipCodeDomain updateZipCodeDomain = new UpdateZipCodeDomain();
        updateZipCodeDomain.setId(updateZipCodeEntity.getZip().getId());
        updateZipCodeDomain.setUserId(updateZipCodeEntity.getZip().getUserId());
        updateZipCodeDomain.setUpdatedAt(updateZipCodeEntity.getZip().getUpdatedAt());
        updateZipCodeDomain.setExpiredAt(updateZipCodeEntity.getZip().getExpiredAt());
        updateZipCodeDomain.setZipCode(updateZipCodeEntity.getZip().getZipCode());
        return updateZipCodeDomain;
    }

    public SubscriptionDomain transformSubscription(SubscriptionEntity subscriptionEntity) {
        if (subscriptionEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SubscriptionDomain subscriptionDomain = new SubscriptionDomain();
        subscriptionDomain.setId(subscriptionEntity.getId());
        subscriptionDomain.setEmail(subscriptionEntity.getEmail());
        subscriptionDomain.setName(subscriptionEntity.getName());
        subscriptionDomain.setZipCode(subscriptionEntity.getZipCode());
        subscriptionDomain.setRole(subscriptionEntity.getRole());
        subscriptionDomain.setFacebookId(subscriptionEntity.getFacebookId());
        subscriptionDomain.setCreatedAt(subscriptionEntity.getCreatedAt());
        subscriptionDomain.setUpdatedAt(subscriptionEntity.getUpdatedAt());
        subscriptionDomain.setUpdatedAt(subscriptionEntity.getUpdatedAt());
        subscriptionDomain.setEstimatesCount(subscriptionEntity.getEstimatesCount());
        subscriptionDomain.setAccurate(subscriptionEntity.getAccurate());
        subscriptionDomain.setSashId(subscriptionEntity.getSashId());
        subscriptionDomain.setLevel(subscriptionEntity.getLevel());
        subscriptionDomain.setVerificationCode(subscriptionEntity.getVerificationCode());
        subscriptionDomain.setPhone(subscriptionEntity.getPhone());
        subscriptionDomain.setEstimateZips(subscriptionEntity.getEstimateZips());
        subscriptionDomain.setTeamId(subscriptionEntity.getTeamId());
        subscriptionDomain.setTeamRequest(subscriptionEntity.getTeamRequest());
        subscriptionDomain.setOfficeId(subscriptionEntity.getOfficeId());
        subscriptionDomain.setOfficeRequest(subscriptionEntity.getOfficeRequest());
        subscriptionDomain.setFirstName(subscriptionEntity.getFirstName());
        subscriptionDomain.setLastName(subscriptionEntity.getLastName());
        subscriptionDomain.setInvitationToken(subscriptionEntity.getInvitationToken());
        subscriptionDomain.setInvitationCreatedAt(subscriptionEntity.getInvitationCreatedAt());
        subscriptionDomain.setInvitationSentAt(subscriptionEntity.getInvitationSentAt());
        subscriptionDomain.setInvitationAcceptedAt(subscriptionEntity.getInvitationAcceptedAt());
        subscriptionDomain.setInvitationLimit(subscriptionEntity.getInvitationLimit());
        subscriptionDomain.setInvitedById(subscriptionEntity.getInvitedById());
        subscriptionDomain.setInvitedByType(subscriptionEntity.getInvitedByType());
        subscriptionDomain.setInvitationsCount(subscriptionEntity.getInvitationsCount());
        subscriptionDomain.setIapReceipt(subscriptionEntity.getIapReceipt());
        subscriptionDomain.setSubscriptionActive(subscriptionEntity.getSubscriptionActive());
        subscriptionDomain.setNotifyJustListed(subscriptionEntity.getNotifyJustListed());
        subscriptionDomain.setNotifyJustSold(subscriptionEntity.getNotifyJustSold());
        subscriptionDomain.setNotifyCheckins(subscriptionEntity.getNotifyCheckins());
        subscriptionDomain.setNotifyOffers(subscriptionEntity.getNotifyOffers());
        subscriptionDomain.setAndroidReceipt(subscriptionEntity.getAndroidReceipt());
        return subscriptionDomain;
    }
    public ZipCodeDeleteDomain transformZipDelete(ZipCodeDeleteEntity zipCodeDeleteEntity) {
        if (zipCodeDeleteEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipCodeDeleteDomain zipCodeDeleteDomain = new ZipCodeDeleteDomain();
        zipCodeDeleteDomain.setDelete(zipCodeDeleteEntity.getDelete());
        return zipCodeDeleteDomain;
    }
}