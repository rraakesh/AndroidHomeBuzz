/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.repository.datasource;

import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import rx.Observable;
import rx.functions.Action1;
import us.homebuzz.homebuzz.data.cache.UserCache;
import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;
import us.homebuzz.homebuzz.data.net.RestApi;

/**
 * {@link UserDataStore} implementation based on connections to the api (Cloud).
 */
public class CloudUserDataStore implements UserDataStore {

    private final RestApi restApi;
    private final UserCache userCache;

    private final Action1<UserEntity> saveToCacheAction =
            userEntity -> {
                if (userEntity != null) {
                    CloudUserDataStore.this.userCache.put(userEntity);
                }
            };

    /**
     * Construct a {@link UserDataStore} based on connections to the api (Cloud).
     *
     * @param restApi   The {@link RestApi} implementation to use.
     * @param userCache A {@link UserCache} to cache data retrieved from the api.
     */
    public CloudUserDataStore(RestApi restApi, UserCache userCache) {
        this.restApi = restApi;
        this.userCache = userCache;
    }

    @Override
    public Observable<List<UserEntity>> userEntityList() {
        return this.restApi.userEntityList();
    }

    @Override
    public Observable<UserEntity> userEntityDetails(final int userId) {
        return this.restApi.userEntityById(userId)
                .doOnNext(saveToCacheAction);
    }

    @Override
    public Observable<User_Entity> userEntityDetailsLogin(WeakHashMap<String, String> value, final boolean check) {
        return this.restApi.userEntityByLogin(value, check);
    }

    @Override
    public Observable<List<NearbyEntity>> userEntityListing(SortedMap<String, String> nearbyData) {
        return this.restApi.userNearbyEntity(nearbyData);
    }

    @Override
    public Observable<List<AccuracyEntity>> userEntityAccuracy(WeakHashMap<String, String> filterData) {
        return this.restApi.userAccuracyEntity(filterData);
    }

    @Override
    public Observable<List<EstimatesEntity>> userEntityEstimate(WeakHashMap<String, String> filterdata) {
        return this.restApi.userEstimateEntity(filterdata);
    }

    @Override
    public Observable<List<AllMessagesEntity>> userEntityAllMessages() {
        return this.restApi.userEstimateAllMessages();
    }

    @Override
    public Observable<List<PreviousEstimate>> userEntityPreviousEstimate(String User_id) {
        return this.restApi.userPreviousEstimate(User_id);
    }

    @Override
    public Observable<ProfileEntity> userProfileEntity(String user_id) {
        return this.restApi.userProfileEntity(user_id);
    }

    @Override
    public Observable<User_Signup> userRegistration(WeakHashMap<String, String> userData) {
        return this.restApi.userSignupEntity(userData);
    }

    @Override
    public Observable<List<FriendFollowed>> userFollowed() {
        return this.restApi.userFollowedEntity();
    }

    @Override
    public Observable<AddListingEntity> userAddListing(WeakHashMap<String, String> listing) {
        return restApi.userAddListing(listing);
    }

    @Override
    public Observable<AddListingPhotoEntity> userAddListingPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData) {
        return restApi.userAddListingPhoto(listing_id, photo, listingData);
    }
    @Override
    public Observable<AddCheckInPhotoEntity> userAddCheckInPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData) {
        return restApi.userAddCheckInPhoto(listing_id, photo, listingData);
    }
    @Override
    public Observable<ReplyMessageEntity> userReplyMessages(WeakHashMap<String, String> replyMessageCredential) {
        return this.restApi.userReplyMessageEntity(replyMessageCredential);
    }

    @Override
    public Observable<UpdateCheckinEntity> userUpdateCheckInEntity(String listing_id, WeakHashMap<String, String> updateCheckin) {
        return this.restApi.usereUpdateCheckinEntity(listing_id, updateCheckin);
    }

    @Override
    public Observable<UpdateProfile> userProfileEntity(WeakHashMap<String, String> userProfilePic, WeakHashMap<String, String> listing) {
        return this.restApi.userProfileEntity(userProfilePic, listing);
    }

    @Override
    public Observable<UpdateProfile> userOtherProfileEntity(WeakHashMap<String, String> profilePicData,WeakHashMap<String, String> listing) {
        return this.restApi.userOtherProfileEntity(profilePicData, listing);
    }

    @Override
    public Observable<List<MessageEntity>> userUnreadMessages() {
        return this.restApi.userUnReadMessages();
    }

    @Override
    public Observable<JustListedEntity> useJustListedListingEntity(WeakHashMap<String, String> listedData) {
        return this.restApi.useJustListedListing(listedData);
    }

    @Override
    public Observable<JustSoldEntity> useJustSoldListingEntity(WeakHashMap<String, String> soldData) {
        return this.restApi.useJustSoldListing(soldData);
    }

    @Override
    public Observable<FollowUnfollowEntity> userFollowUnfollow(String User_id) {
        return restApi.userFollowUnfollow(User_id);
    }

    @Override
    public Observable<InviteFriends> userInviteFriends(WeakHashMap<String, String> sendInvitation) {
        return restApi.userInviteFriends(sendInvitation);
    }

    @Override
    public Observable<List<FriendFollowed>> userFollowers() {
        return this.restApi.userFollowersEntity();
    }

    @Override
    public Observable<List<ZipPurchaseEntity>> userZipPurchaseEntity() {
        return this.restApi.userZipPurchaseEntity();
    }

    @Override
    public Observable<SearchAddressEntity> userOnBoardPropEntity(WeakHashMap<String, String> searchData) {
        return this.restApi.userOnBoardProp(searchData);
    }

    @Override
    public Observable<ProfilePicEntity> userProfilePicEntity(WeakHashMap<String, String> profilePicData) {
        return this.restApi.userProfilePicUpdate(profilePicData);
    }

    @Override
    public Observable<ReplyMessageEntity> userReadMessages(WeakHashMap<String, String> replyMessageCredential) {
        return this.restApi.userReadMessageEntity(replyMessageCredential);
    }

    @Override
    public Observable<List<SearchPeopleEntity>> userSearchPeople(WeakHashMap<String, String> replyMessageCredential) {
        return this.restApi.userSearchPeopleEntity(replyMessageCredential);
    }

    @Override
    public Observable<EstimateAverage> userEstimateAverage(WeakHashMap<String, String> replyMessageCredential) {
        return this.restApi.userEstimateAverageEntity(replyMessageCredential);
    }

    @Override
    public Observable<UpdateZipCodeEntity> userUpdateZip(WeakHashMap<String, String> replyMessageCredential) {
        return this.restApi.userUpdateZipEntity(replyMessageCredential);
    }

    @Override
    public Observable<ZipAddEntity> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd) {
        return this.restApi.userZipCodeAddEntity(zipCodeAdd);
    }
    @Override
    public Observable<SubscriptionEntity> userSubscription(WeakHashMap<String, String> userSubscription) {
        return this.restApi.userSubscription(userSubscription);
    } @Override
    public Observable<ZipCodeDeleteEntity> zipDelete(WeakHashMap<String, String> zipCodeDeleteEntity) {
        return this.restApi.zipDelete(zipCodeDeleteEntity);
    }
}
