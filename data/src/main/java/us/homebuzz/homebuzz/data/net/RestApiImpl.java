/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.MalformedURLException;
import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import rx.Observable;
import rx.Subscriber;
import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.mapper.UserEntityJsonMapper;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;
import us.homebuzz.homebuzz.data.exception.NetworkConnectionException;

/**
 * {@link RestApi} implementation for retrieving data from the network.
 */
public class RestApiImpl implements RestApi {
    public static String TAG = RestApiImpl.class.getClass().getSimpleName();
    private final Context context;
    private final UserEntityJsonMapper userEntityJsonMapper;
    private final SharedPreferences sharedPreferences;

    /**
     * Constructor of the class
     *
     * @param context              {@link android.content.Context}.
     * @param userEntityJsonMapper {@link UserEntityJsonMapper}.
     */
    public RestApiImpl(Context context, UserEntityJsonMapper userEntityJsonMapper, SharedPreferences sharedPreferences) {
        if (context == null || userEntityJsonMapper == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        this.sharedPreferences = sharedPreferences;
        this.context = context.getApplicationContext();
        this.userEntityJsonMapper = userEntityJsonMapper;
    }

    @Override
    public Observable<List<UserEntity>> userEntityList() {
        return Observable.create(new Observable.OnSubscribe<List<UserEntity>>() {
            @Override
            public void call(Subscriber<? super List<UserEntity>> subscriber) {

                if (isThereInternetConnection()) {
                    try {
                        String responseUserEntities = getUserEntitiesFromApi();
                        if (responseUserEntities != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserEntityCollection(
                                    responseUserEntities));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<UserEntity> userEntityById(final int userId) {
        return Observable.create(new Observable.OnSubscribe<UserEntity>() {
            @Override
            public void call(Subscriber<? super UserEntity> subscriber) {

                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserDetailsFromApi(userId);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<User_Entity> userEntityByLogin(/*String username,String password,String device_id*/ WeakHashMap<String, String> loginCredential, boolean isFbLogin) {
        return Observable.create(new Observable.OnSubscribe<User_Entity>() {
            @Override
            public void call(Subscriber<? super User_Entity> subscriber) {
                //  Log.d("rest api observer", "Email " + username + "Password is" + password + "Device Token" + device_id);
                if (isThereInternetConnection()) {
                    try {
                        //Switch Between Two Endpoint Based on this Flag ie:-Fb login  and Login Api
                        String responseUserDetails;
                        if (isFbLogin)
                            responseUserDetails = getUserDetailsLoginFbFromApi(loginCredential);
                        else
                            responseUserDetails = getUserDetailsLoginFromApi(loginCredential);

                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserEntityLogin(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<NearbyEntity>> userNearbyEntity(/*String lat,String lon,String radius*/SortedMap<String, String> nearbyData) {
        return Observable.create(new Observable.OnSubscribe<List<NearbyEntity>>() {
            @Override
            public void call(Subscriber<? super List<NearbyEntity>> subscriber) {
                Log.d(TAG, "LAT " + nearbyData.size());
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserNearbyFromApi(nearbyData);
                        //  Log.d(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserNearbyEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<AccuracyEntity>> userAccuracyEntity(WeakHashMap<String, String> filterData) {
        return Observable.create(new Observable.OnSubscribe<List<AccuracyEntity>>() {
            @Override
            public void call(Subscriber<? super List<AccuracyEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserAccuracyFromApi(filterData);
                        //  Log.d(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserAccuracyEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<EstimatesEntity>> userEstimateEntity(WeakHashMap<String, String> filterdata) {
        return Observable.create(new Observable.OnSubscribe<List<EstimatesEntity>>() {
            @Override
            public void call(Subscriber<? super List<EstimatesEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserEstimateFromApi(filterdata);
                        // Log.d(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserEstimatesEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<AllMessagesEntity>> userEstimateAllMessages() {
        return Observable.create(new Observable.OnSubscribe<List<AllMessagesEntity>>() {
            @Override
            public void call(Subscriber<? super List<AllMessagesEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {

                        String responseUserDetails = getUserAllMessagesFromApi();
                        // Log.d(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserAllMessagesEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<PreviousEstimate>> userPreviousEstimate(String user_id) {
        return Observable.create(new Observable.OnSubscribe<List<PreviousEstimate>>() {
            @Override
            public void call(Subscriber<? super List<PreviousEstimate>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserPreviousEstimateFromApi(user_id);
                        // Log.d(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformPreviousEstimateEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ProfileEntity> userProfileEntity(String user_id) {
        return Observable.create(new Observable.OnSubscribe<ProfileEntity>() {
            @Override
            public void call(Subscriber<? super ProfileEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserProfileFromApi(user_id);
                        // Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformProfileEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<User_Signup> userSignupEntity(WeakHashMap<String, String> userRegData) {
        return Observable.create(new Observable.OnSubscribe<User_Signup>() {
            @Override
            public void call(Subscriber<? super User_Signup> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserRegistrationFromApi(userRegData);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformSignupEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<FriendFollowed>> userFollowedEntity() {
        return Observable.create(new Observable.OnSubscribe<List<FriendFollowed>>() {
            @Override
            public void call(Subscriber<? super List<FriendFollowed>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserfollowedFromApi();
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformFriendsFollowedEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }


    @Override
    public Observable<AddListingEntity> userAddListing(WeakHashMap<String, String> listing) {
        return Observable.create(new Observable.OnSubscribe<AddListingEntity>() {
            @Override
            public void call(Subscriber<? super AddListingEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getAddListingFromApi(listing);
                        // Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformAddListingEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<AddListingPhotoEntity> userAddListingPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listing) {
        return Observable.create(new Observable.OnSubscribe<AddListingPhotoEntity>() {
            @Override
            public void call(Subscriber<? super AddListingPhotoEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getAddListingPhotoFromApi(listing_id, photo, listing);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformAddListinPhotoEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<AddCheckInPhotoEntity> userAddCheckInPhoto(String id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listing) {
        return Observable.create(new Observable.OnSubscribe<AddCheckInPhotoEntity>() {
            @Override
            public void call(Subscriber<? super AddCheckInPhotoEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getAddCheckInPhotoFromApi(id, photo, listing);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformAddCheckInPhotoEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ReplyMessageEntity> userReplyMessageEntity(WeakHashMap<String, String> replyData) {
        return Observable.create(new Observable.OnSubscribe<ReplyMessageEntity>() {
            @Override
            public void call(Subscriber<? super ReplyMessageEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getReplyFromApi(replyData);
                        // Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformReplypEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<InviteFriends> userInviteFriends(WeakHashMap<String, String> sendInvitation) {
        return Observable.create(new Observable.OnSubscribe<InviteFriends>() {
            @Override
            public void call(Subscriber<? super InviteFriends> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getInviteFromApi(sendInvitation);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformSendInvitationEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<UpdateCheckinEntity> usereUpdateCheckinEntity(String listing_id, WeakHashMap<String, String> updateCheck_in) {
        return Observable.create(new Observable.OnSubscribe<UpdateCheckinEntity>() {
            @Override
            public void call(Subscriber<? super UpdateCheckinEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUpdateCheck_inFromApi(listing_id, updateCheck_in);
                        Log.d(TAG, "response RESTAPI IMP Observer222 " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformCheck_InEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<UpdateProfile> userProfileEntity(WeakHashMap<String, String> userProfilePic, WeakHashMap<String, String> listing) {
        return Observable.create(new Observable.OnSubscribe<UpdateProfile>() {
            @Override
            public void call(Subscriber<? super UpdateProfile> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserEditOProfileFromApi(userProfilePic, listing);
                        Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUpdateProfileEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<MessageEntity>> userUnReadMessages() {
        return Observable.create(new Observable.OnSubscribe<List<MessageEntity>>() {
            @Override
            public void call(Subscriber<? super List<MessageEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {

                        String responseUserDetails = getUserUnreadMessagesFromApi();
                        // Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserUnreadMessagesEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }


    @Override
    public Observable<FollowUnfollowEntity> userFollowUnfollow(String user_id) {
        return Observable.create(new Observable.OnSubscribe<FollowUnfollowEntity>() {
            @Override
            public void call(Subscriber<? super FollowUnfollowEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserFollowUnfollow(user_id);
                        //   Log.e(TAG,"response RESTAPI IMP Observer "+responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformFollowUnfollow(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<JustListedEntity> useJustListedListing(WeakHashMap<String, String> listedData) {
        return Observable.create(new Observable.OnSubscribe<JustListedEntity>() {
            @Override
            public void call(Subscriber<? super JustListedEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {

                        String responseUserDetails = getUserJustListedFromApi(listedData);
                        // Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserJustListedEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<JustSoldEntity> useJustSoldListing(WeakHashMap<String, String> soldData) {
        return Observable.create(new Observable.OnSubscribe<JustSoldEntity>() {
            @Override
            public void call(Subscriber<? super JustSoldEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {

                        String responseUserDetails = getUserJustSoldFromApi(soldData);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUserJustSoldEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<FriendFollowed>> userFollowersEntity() {
        return Observable.create(new Observable.OnSubscribe<List<FriendFollowed>>() {
            @Override
            public void call(Subscriber<? super List<FriendFollowed>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserfollowersFromApi();
                        Log.d(TAG, "response RESTAPI IMP Observer  " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformFriendsFollowedEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<ZipPurchaseEntity>> userZipPurchaseEntity() {
        return Observable.create(new Observable.OnSubscribe<List<ZipPurchaseEntity>>() {
            @Override
            public void call(Subscriber<? super List<ZipPurchaseEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserZipFromApi();
                        Log.d(TAG, "response RESTAPI IMP Observer  " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformZipPurchase(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<SearchAddressEntity> userOnBoardProp(WeakHashMap<String, String> searchData) {
        return Observable.create(new Observable.OnSubscribe<SearchAddressEntity>() {
            @Override
            public void call(Subscriber<? super SearchAddressEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserOnBoardPropFromApi(searchData);
                        Log.d(TAG, "response RESTAPI IMP Observer  " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformGetOnBoardProp(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }


    @Override
    public Observable<UpdateProfile> userOtherProfileEntity(WeakHashMap<String, String> profilePic, WeakHashMap<String, String> listing) {
        return Observable.create(new Observable.OnSubscribe<UpdateProfile>() {
            @Override
            public void call(Subscriber<? super UpdateProfile> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUserEditOtherProfileFromApi(profilePic, listing);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUpdateProfileEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ProfilePicEntity> userProfilePicUpdate(WeakHashMap<String, String> profileData) {
        return Observable.create(new Observable.OnSubscribe<ProfilePicEntity>() {
            @Override
            public void call(Subscriber<? super ProfilePicEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = updateProiflePicFromApi(profileData);
                        //  Log.d(TAG, "response RESTAPI IMP Observer " + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformProfilePicUpdate(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ReplyMessageEntity> userReadMessageEntity(WeakHashMap<String, String> replyData) {
        return Observable.create(new Observable.OnSubscribe<ReplyMessageEntity>() {
            @Override
            public void call(Subscriber<? super ReplyMessageEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = markMessageRadFromApi(replyData);
                        Log.d(TAG, "response RESTAPI IMP Observer MESSAGE READ" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformReplypEntity(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<List<SearchPeopleEntity>> userSearchPeopleEntity(WeakHashMap<String, String> replyData) {
        return Observable.create(new Observable.OnSubscribe<List<SearchPeopleEntity>>() {
            @Override
            public void call(Subscriber<? super List<SearchPeopleEntity>> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = searchPeopleFromApi(replyData);
                        Log.d(TAG, "response RESTAPI IMP Observer MESSAGE READ" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformSearchPeople(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }
    @Override
    public Observable<EstimateAverage> userEstimateAverageEntity(WeakHashMap<String, String> replyData) {
        return Observable.create(new Observable.OnSubscribe<EstimateAverage>() {
            @Override
            public void call(Subscriber<? super EstimateAverage> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = egtEstimateAverageFromApi(replyData);
                        Log.d(TAG, "response RESTAPI IMP Observer" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformEstimateAverage(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<UpdateZipCodeEntity> userUpdateZipEntity(WeakHashMap<String, String> replyData) {
        return Observable.create(new Observable.OnSubscribe<UpdateZipCodeEntity>() {
            @Override
            public void call(Subscriber<? super UpdateZipCodeEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getUpdateZipFromApi(replyData);
                        Log.d(TAG, "response RESTAPI IMP Observer" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformUpdateZipCode(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ZipAddEntity> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd) {
        return Observable.create(new Observable.OnSubscribe<ZipAddEntity>() {
            @Override
            public void call(Subscriber<? super ZipAddEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = addZipCodeFromApi(zipCodeAdd);
                        Log.d(TAG, "response RESTAPI IMP Observer" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformZipAdd(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<SubscriptionEntity> userSubscription(WeakHashMap<String, String> userSubscription) {
        return Observable.create(new Observable.OnSubscribe<SubscriptionEntity>() {
            @Override
            public void call(Subscriber<? super SubscriptionEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getSubscriptionPlan(userSubscription);
                        Log.d(TAG, "response RESTAPI IMP Observer" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformSubscriptionPlanData(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    @Override
    public Observable<ZipCodeDeleteEntity> zipDelete(WeakHashMap<String, String> zipDelete) {
        return Observable.create(new Observable.OnSubscribe<ZipCodeDeleteEntity>() {
            @Override
            public void call(Subscriber<? super ZipCodeDeleteEntity> subscriber) {
                if (isThereInternetConnection()) {
                    try {
                        String responseUserDetails = getZipDelete(zipDelete);
                        Log.d(TAG, "response RESTAPI IMP Observer" + responseUserDetails);
                        if (responseUserDetails != null) {
                            subscriber.onNext(userEntityJsonMapper.transformZipCodeDelete(responseUserDetails));
                            subscriber.onCompleted();
                        } else {
                            subscriber.onError(new NetworkConnectionException());
                        }
                    } catch (Exception e) {
                        subscriber.onError(new NetworkConnectionException(e.getCause()));
                    }
                } else {
                    subscriber.onError(new NetworkConnectionException());
                }
            }
        });
    }

    private String getUserEntitiesFromApi() throws MalformedURLException {
        return ApiConnection.createGET(RestApi.API_URL_GET_USER_LIST, sharedPreferences).requestSyncCall();
    }

    private String getUserDetailsFromApi(int userId) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_DETAILS + userId + ".json";
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserNearbyFromApi(/*String lat,String lon,String radius*/SortedMap<String, String> nearbyData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_NEARBY;/*+ "lat="+lat + "&lon="+lon+"&radius="+radius;*/
        int size = nearbyData.size();
        //ToDO Make A Loop And add all the Value to the Url
        for (int i = 0; i < nearbyData.size(); i++) {
            System.out.println("Key--" + nearbyData.keySet().toArray()[i] + "Value--" + nearbyData.get(nearbyData.keySet().toArray()[i]));
            if (i == nearbyData.size() - 1) {
                apiUrl += nearbyData.keySet().toArray()[i] + "=" + nearbyData.get(nearbyData.keySet().toArray()[i]);
            } else {
                apiUrl += nearbyData.keySet().toArray()[i] + "=" + nearbyData.get(nearbyData.keySet().toArray()[i]) + "&";
            }
        }
        Log.d(TAG, "Nearby" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserDetailsLoginFromApi(/*String username,String password,String device_id*/ WeakHashMap<String, String> loginCredential) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_LOGIN;
        return ApiConnection.createPOST(apiUrl, loginCredential, sharedPreferences).requestSyncCall();
    }

    private String getUserDetailsLoginFbFromApi(/*String username,String password,String device_id*/ WeakHashMap<String, String> loginCredential) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_LOGIN_FB;
        return ApiConnection.createPOST(apiUrl, loginCredential, sharedPreferences).requestSyncCall();
    }

    private String getUserAccuracyFromApi(WeakHashMap<String, String> filterData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_ACCURACY;
        if (filterData != null) {
            apiUrl += "?";
            int i = 1;
            for (WeakHashMap.Entry<String, String> entry : filterData.entrySet()) {
                if (filterData.size() != i)
                    apiUrl += entry.getKey() + "=" + entry.getValue() + "&";
                else
                    apiUrl += entry.getKey() + "=" + entry.getValue();

                i++;
            }

        }
        Log.d(TAG, "Accuracy" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserEstimateFromApi(WeakHashMap<String, String> filterData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_ESTIMATE;
        if (filterData != null) {
            apiUrl += "?";
            int i = 1;
            for (WeakHashMap.Entry<String, String> entry : filterData.entrySet()) {
                if (filterData.size() != i)
                    apiUrl += entry.getKey() + "=" + entry.getValue() + "&";
                else
                    apiUrl += entry.getKey() + "=" + entry.getValue();

                i++;
            }

        }
        Log.d(TAG, "Estimate" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserAllMessagesFromApi() throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_ALL_MESSAGES;
        Log.d(TAG, "Accuracy" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    // Request for Review details listing  from user  edit profile page

    private String getUserPreviousEstimateFromApi(String user_id) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_PREVIOUS_ESTIMATE + user_id + "/estimates";
        Log.d(TAG, "PreviousEstimate" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserProfileFromApi(String user_id) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_USER_PROFILE + user_id;
        Log.d(TAG, "ProfileGet" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserRegistrationFromApi(WeakHashMap<String, String> userData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_REGISTRATION;
        return ApiConnection.createPOST(apiUrl, userData, sharedPreferences).requestSyncCall();
    }

    private String getUserfollowedFromApi() throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_FRIEND_FOLLOWED;
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getReplyFromApi(WeakHashMap<String, String> replyData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_MESSAGE_REPLY;
        return ApiConnection.createPOST(apiUrl, replyData, sharedPreferences).requestSyncCall();
    }

    private String getInviteFromApi(WeakHashMap<String, String> sendInvitation) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_SEND_INVITATION;
        return ApiConnection.createPOST(apiUrl, sendInvitation, sharedPreferences).requestSyncCall();
    }

    private String getAddListingFromApi(WeakHashMap<String, String> addListing) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_ADD_LISTING;
        return ApiConnection.createPOST(apiUrl, addListing, sharedPreferences).requestSyncCall();
    }

    private String getAddListingPhotoFromApi(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> addListingData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_ADD_LISTING + "/" + listing_id + "/upload";
        return ApiConnection.createPOSTWithMultipart(apiUrl, photo, addListingData, sharedPreferences).requestSyncCall();
    }

    private String getAddCheckInPhotoFromApi(String id, WeakHashMap<String, String> photo, WeakHashMap<String, String> addCheckinData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_ADD_CHECKIN_PHOTO + "/" + id + "/upload";
        return ApiConnection.createPOSTWithMultipart(apiUrl, photo, addCheckinData, sharedPreferences).requestSyncCall();
    }

    private String getUpdateCheck_inFromApi(String listing_id, WeakHashMap<String, String> updateCheckin) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_CHECKIN_ESTIMATE + listing_id + "/estimates";
        return ApiConnection.createPOST(apiUrl, updateCheckin, sharedPreferences).requestSyncCall();
    }

    private String getUserEditOProfileFromApi(WeakHashMap<String, String> userProfilePic, WeakHashMap<String, String> userData) throws MalformedURLException {
        //TODO Pass a Separate Parameters
        String apiUrl = RestApi.API_URL_PUT_USER_PROFILE + "/" + userData.get("users[user_id]");
        return ApiConnection.createPUT(apiUrl,/*userProfilePic,*/ userData, sharedPreferences).requestSyncCall();
    }

    private String getUserEditOtherProfileFromApi(WeakHashMap<String, String> profilePicData, WeakHashMap<String, String> userData) throws MalformedURLException {
        //TODO Pass a Separate Parameters
        String apiUrl = RestApi.API_URL_PUT_USER_OTHER_PROFILE;
        if (profilePicData != null && !(profilePicData.isEmpty())) {
            return ApiConnection.createPOSTWithMultipart(apiUrl, profilePicData, userData, sharedPreferences).requestSyncCall();
        } else {
            return ApiConnection.createPOST(apiUrl,/*userProfilePic,*/ userData, sharedPreferences).requestSyncCall();
        }

    }

    private String getUserUnreadMessagesFromApi() throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_UNREAD_MESSAGE;
        Log.d(TAG, "Accuracy" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserJustListedFromApi(WeakHashMap<String, String> justListed) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_JUST_LISTEDLISTING + "?";
        int i = 1;
        for (WeakHashMap.Entry<String, String> entry : justListed.entrySet()) {
            if (justListed.size() != i) {
                apiUrl += entry.getKey() + "=" + entry.getValue() + "&";
            } else {
                apiUrl += entry.getKey() + "=" + entry.getValue();

            }
            i++;
        }
        Log.d(TAG, "JUST LISTED" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserJustSoldFromApi(WeakHashMap<String, String> justSold) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_JUST_SOLDLISTING + "?";
        int i = 1;
        for (WeakHashMap.Entry<String, String> entry : justSold.entrySet()) {
            if (justSold.size() != i)
                apiUrl += entry.getKey() + "=" + entry.getValue() + "&";
            else
                apiUrl += entry.getKey() + "=" + entry.getValue();
            i++;
        }
        Log.d(TAG, "JUST SOLD" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserFollowUnfollow(String user_id) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_FOLLOW_UNFOLLOW + user_id + "/toggle_follow";
        Log.d(TAG, "Follow unfollow" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserfollowersFromApi() throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_FRIEND_FOLLOWERS;
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserZipFromApi() throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_ZIP_PURCHASE;
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String getUserOnBoardPropFromApi(WeakHashMap<String, String> searchData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_ON_BOARD_PROP;
        if (searchData != null) {
            apiUrl += "?";
            int i = 1;
            for (WeakHashMap.Entry<String, String> entry : searchData.entrySet()) {
                if (searchData.size() != i)
                    apiUrl += entry.getKey() + "=" + entry.getValue() + "&";
                else
                    apiUrl += entry.getKey() + "=" + entry.getValue();
                i++;
            }
        }
        Log.d(TAG, "On Board Prop" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String updateProiflePicFromApi(WeakHashMap<String, String> profileData) throws MalformedURLException {
        String profile_Id = profileData.get("profile[profile_id]");
        profileData.remove("profile[profile_id]");
        String apiUrl = RestApi.API_URL_PUT_USER_OTHER_PROFILE + "/" + profile_Id + "/upload";
        Log.d(TAG, "Profile PIC Update" + apiUrl);
        return ApiConnection.createPOSTWithMultipart(apiUrl, profileData, null, sharedPreferences).requestSyncCall();
    }

    private String markMessageRadFromApi(WeakHashMap<String, String> replyData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_USER_MESSAGE_REPLY + "/" + replyData.get("message_id") + "/read";
        Log.d(TAG, "Read Message" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String searchPeopleFromApi(WeakHashMap<String, String> replyData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_SEARCH_PEOPLE + "/" + replyData.get("user_id") + "/user_search";
        replyData.remove("user_id");
        Log.d(TAG, "Read Message" + apiUrl);
        return ApiConnection.createPOST(apiUrl, replyData, sharedPreferences).requestSyncCall();
    }

    private String egtEstimateAverageFromApi(WeakHashMap<String, String> replyData) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_ESTIMATE_AVERAGE + "?listing_id=" + replyData.get("listing_id");
        Log.d(TAG, "EstimateAverage" + apiUrl);
        return ApiConnection.createGET(apiUrl, sharedPreferences).requestSyncCall();
    }

    private String addZipCodeFromApi(WeakHashMap<String, String> zipCodeAdd) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_POST_ZIP_CODE;
        Log.d(TAG, "Zip code Add" + apiUrl);
        return ApiConnection.createPOST(apiUrl, zipCodeAdd, sharedPreferences).requestSyncCall();
    }

    private String getUpdateZipFromApi(WeakHashMap<String, String> zipdata) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_GET_ZIP_PURCHASE + "/" + zipdata.get("id");
        Log.d(TAG, "EstimateAverage" + apiUrl);
        zipdata.remove("id");
        return ApiConnection.createPUT(apiUrl, zipdata, sharedPreferences).requestSyncCall();
    }
    // check the put request url

    private String getSubscriptionPlan(WeakHashMap<String, String> zipdata) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_SUBSCRIPTION_PLAN +  zipdata.get("id")+"?"+zipdata.get("user[android_receipt]");
        Log.d(TAG, "SubscriptionPlan" + apiUrl);
        zipdata.remove("id");
        return ApiConnection.createPUT(apiUrl, zipdata, sharedPreferences).requestSyncCall();
    }
    private String getZipDelete(WeakHashMap<String, String> zipdata) throws MalformedURLException {
        String apiUrl = RestApi.API_URL_ZIP_CODE_DELETE+"/"+zipdata.get("id");
        Log.d(TAG, "Zip Delete" + apiUrl);
        zipdata.remove("id");
        return ApiConnection.createDELETE(apiUrl, zipdata, sharedPreferences).requestSyncCall();
    }

    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }
}
