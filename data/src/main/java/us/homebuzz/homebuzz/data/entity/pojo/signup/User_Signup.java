package us.homebuzz.homebuzz.data.entity.pojo.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 10/31/2015.
 */
public class User_Signup {
    @SerializedName("errors")
    @Expose
    private String errors;

    /**
     *
     * @return
     * The errors
     */
    public String getErrors() {
        return errors;
    }

    /**
     *
     * @param errors
     * The errors
     */
    public void setErrors(String errors) {
        this.errors = errors;
    }

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("confirmed")
    @Expose
    private Boolean confirmed;
    @SerializedName("token")
    @Expose
    private String token;

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     *
     * @param confirmed
     * The confirmed
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }
}
