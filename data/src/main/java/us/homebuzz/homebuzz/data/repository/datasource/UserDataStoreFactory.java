/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.repository.datasource;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import us.homebuzz.homebuzz.data.cache.UserCache;
import us.homebuzz.homebuzz.data.entity.mapper.UserEntityJsonMapper;
import us.homebuzz.homebuzz.data.net.RestApi;
import us.homebuzz.homebuzz.data.net.RestApiImpl;

/**
 * Factory that creates different implementations of {@link UserDataStore}.
 */
@Singleton
public class UserDataStoreFactory {

    private final Context context;
    private final UserCache userCache;
    private final SharedPreferences sharedPreferences;

    @Inject
    public UserDataStoreFactory(Context context, UserCache userCache, SharedPreferences sharedPreferences) {
        if (context == null || userCache == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
        }
        this.sharedPreferences = sharedPreferences;
        this.context = context.getApplicationContext();
        this.userCache = userCache;
    }

    /**
     * Create {@link UserDataStore} from a user id.
     */
    public UserDataStore create(int userId) {
        UserDataStore userDataStore;

        if (!this.userCache.isExpired() && this.userCache.isCached(userId)) {
            userDataStore = new DiskUserDataStore(this.userCache);
        } else {
            userDataStore = createCloudDataStore();
        }

        return userDataStore;
    }

    /**
     * Create {@link UserDataStore} to retrieve data from the Cloud.
     */
    public UserDataStore createCloudDataStore() {
        UserEntityJsonMapper userEntityJsonMapper = new UserEntityJsonMapper();
        RestApi restApi = new RestApiImpl(this.context, userEntityJsonMapper, sharedPreferences);
        return new CloudUserDataStore(restApi, this.userCache);
    }

    public UserDataStore createLogin() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createNearby() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createAccuracy() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createEstimate() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createAllMessages() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createPreviousEstimate() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createUserProfile() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createUserFollowed() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createReplyMessages() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createAddListing() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createUpdateCheckIn() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createUnreadMessage() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }
    public UserDataStore createFollowUnfollow() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

    public UserDataStore createUserFollowers() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }
    public UserDataStore createInviteFreinds(){
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }
    public UserDataStore createAddCheckIn() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }
    public UserDataStore createUpdateZipCode() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    } public UserDataStore deleteZipCode() {
        UserDataStore userDataStore = createCloudDataStore();
        return userDataStore;
    }

}
