package us.homebuzz.homebuzz.data.entity.pojo.nearby;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 10/15/2015.
 */
public class PreviousReview {

    @SerializedName("listing_id")
    @Expose
    private Integer listingId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("location_rating")
    @Expose
    private Double locationRating;
    @SerializedName("condition_rating")
    @Expose
    private Double conditionRating;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("accurate")
    @Expose
    private Object accurate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     *
     * @return
     * The listingId
     */
    public Integer getListingId() {
        return listingId;
    }

    /**
     *
     * @param listingId
     * The listing_id
     */
    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The locationRating
     */
    public Double getLocationRating() {
        return locationRating;
    }

    /**
     *
     * @param locationRating
     * The location_rating
     */
    public void setLocationRating(Double locationRating) {
        this.locationRating = locationRating;
    }

    /**
     *
     * @return
     * The conditionRating
     */
    public Double getConditionRating() {
        return conditionRating;
    }

    /**
     *
     * @param conditionRating
     * The condition_rating
     */
    public void setConditionRating(Double conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     *
     * @return
     * The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Object getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Object accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
