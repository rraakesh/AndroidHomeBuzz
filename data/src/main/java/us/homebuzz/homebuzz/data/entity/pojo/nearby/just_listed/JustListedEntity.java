package us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class JustListedEntity {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("listings")
    @Expose
    private List<JustListing> listings = new ArrayList<JustListing>();

    /**
     * @return The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * @return The listings
     */
    public List<JustListing> getListings() {
        return listings;
    }

    /**
     * @param listings The listings
     */
    public void setListings(List<JustListing> listings) {
        this.listings = listings;
    }

    public class JustListing {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Object userId;
        @SerializedName("mls_num")
        @Expose
        private String mlsNum;
        @SerializedName("listing_class")
        @Expose
        private String listingClass;
        @SerializedName("listing_class_type")
        @Expose
        private String listingClassType;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("for_sale")
        @Expose
        private Boolean forSale;
        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lon")
        @Expose
        private Double lon;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("zip")
        @Expose
        private String zip;
        @SerializedName("location_rating")
        @Expose
        private Double locationRating;
        @SerializedName("condition_rating")
        @Expose
        private Double conditionRating;
        @SerializedName("bathshalf")
        @Expose
        private Integer bathshalf;
        @SerializedName("fireplaces")
        @Expose
        private Integer fireplaces;
        @SerializedName("bedrooms")
        @Expose
        private Integer bedrooms;
        @SerializedName("optional_bedrooms")
        @Expose
        private Integer optionalBedrooms;
        @SerializedName("baths")
        @Expose
        private Integer baths;
        @SerializedName("acres")
        @Expose
        private String acres;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("list_on_market")
        @Expose
        private String listOnMarket;
        @SerializedName("last_sold_price")
        @Expose
        private String lastSoldPrice;
        @SerializedName("year_built")
        @Expose
        private Integer yearBuilt;
        @SerializedName("recorded_mortgage")
        @Expose
        private String recordedMortgage;
        @SerializedName("assesed_value")
        @Expose
        private String assesedValue;
        @SerializedName("current_taxes")
        @Expose
        private String currentTaxes;
        @SerializedName("remarks")
        @Expose
        private String remarks;
        @SerializedName("home_owner_fees")
        @Expose
        private String homeOwnerFees;
        @SerializedName("home_owner_total_fees")
        @Expose
        private String homeOwnerTotalFees;
        @SerializedName("other_fees")
        @Expose
        private String otherFees;
        @SerializedName("mello_roos_fee")
        @Expose
        private String melloRoosFee;
        @SerializedName("bathsfull")
        @Expose
        private String bathsfull;
        @SerializedName("parkng_non_garaged_spaces")
        @Expose
        private String parkngNonGaragedSpaces;
        @SerializedName("estimated_square_feet")
        @Expose
        private String estimatedSquareFeet;
        @SerializedName("lot_sqft_approx")
        @Expose
        private String lotSqftApprox;
        @SerializedName("garage")
        @Expose
        private String garage;
        @SerializedName("parking_spaces_total")
        @Expose
        private String parkingSpacesTotal;
        @SerializedName("monthly_total_fees")
        @Expose
        private String monthlyTotalFees;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("agent_phone")
        @Expose
        private String agentPhone;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("agent_id")
        @Expose
        private String agentId;
        @SerializedName("tour_link")
        @Expose
        private String tourLink;
        @SerializedName("mls_name")
        @Expose
        private String mlsName;
        @SerializedName("sys_id")
        @Expose
        private Object sysId;
        @SerializedName("upgrade1")
        @Expose
        private Object upgrade1;
        @SerializedName("upgrade2")
        @Expose
        private Object upgrade2;
        @SerializedName("upgrade3")
        @Expose
        private Object upgrade3;
        @SerializedName("upgrade4")
        @Expose
        private Object upgrade4;
        @SerializedName("green_feature1")
        @Expose
        private Object greenFeature1;
        @SerializedName("green_feature2")
        @Expose
        private Object greenFeature2;
        @SerializedName("green_feature3")
        @Expose
        private Object greenFeature3;
        @SerializedName("green_feature4")
        @Expose
        private Object greenFeature4;
        @SerializedName("upgrade1_pic")
        @Expose
        private Object upgrade1Pic;
        @SerializedName("upgrade2_pic")
        @Expose
        private Object upgrade2Pic;
        @SerializedName("upgrade3_pic")
        @Expose
        private Object upgrade3Pic;
        @SerializedName("upgrade4_pic")
        @Expose
        private Object upgrade4Pic;
        @SerializedName("green_feature1_pic")
        @Expose
        private Object greenFeature1Pic;
        @SerializedName("green_feature2_pic")
        @Expose
        private Object greenFeature2Pic;
        @SerializedName("green_feature3_pic")
        @Expose
        private Object greenFeature3Pic;
        @SerializedName("green_feature4_pic")
        @Expose
        private Object greenFeature4Pic;
        @SerializedName("photos")
        @Expose
        private List<String> photos = new ArrayList<String>();
        @SerializedName("thumbnails")
        @Expose
        private List<String> thumbnails = new ArrayList<String>();
        @SerializedName("parcel_num")
        @Expose
        private String parcelNum;
        @SerializedName("avm_low")
        @Expose
        private Object avmLow;
        @SerializedName("avm_high")
        @Expose
        private Object avmHigh;
        @SerializedName("sold_date")
        @Expose
        private Object soldDate;
        @SerializedName("distance")
        @Expose
        private Object distance;

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The userId
         */
        public Object getUserId() {
            return userId;
        }

        /**
         * @param userId The user_id
         */
        public void setUserId(Object userId) {
            this.userId = userId;
        }

        /**
         * @return The mlsNum
         */
        public String getMlsNum() {
            return mlsNum;
        }

        /**
         * @param mlsNum The mls_num
         */
        public void setMlsNum(String mlsNum) {
            this.mlsNum = mlsNum;
        }

        /**
         * @return The listingClass
         */
        public String getListingClass() {
            return listingClass;
        }

        /**
         * @param listingClass The listing_class
         */
        public void setListingClass(String listingClass) {
            this.listingClass = listingClass;
        }

        /**
         * @return The listingClassType
         */
        public String getListingClassType() {
            return listingClassType;
        }

        /**
         * @param listingClassType The listing_class_type
         */
        public void setListingClassType(String listingClassType) {
            this.listingClassType = listingClassType;
        }

        /**
         * @return The status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return The forSale
         */
        public Boolean getForSale() {
            return forSale;
        }

        /**
         * @param forSale The for_sale
         */
        public void setForSale(Boolean forSale) {
            this.forSale = forSale;
        }

        /**
         * @return The lat
         */
        public Double getLat() {
            return lat;
        }

        /**
         * @param lat The lat
         */
        public void setLat(Double lat) {
            this.lat = lat;
        }

        /**
         * @return The lon
         */
        public Double getLon() {
            return lon;
        }

        /**
         * @param lon The lon
         */
        public void setLon(Double lon) {
            this.lon = lon;
        }

        /**
         * @return The address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         * @return The zip
         */
        public String getZip() {
            return zip;
        }

        /**
         * @param zip The zip
         */
        public void setZip(String zip) {
            this.zip = zip;
        }

        /**
         * @return The locationRating
         */
        public Double getLocationRating() {
            return locationRating;
        }

        /**
         * @param locationRating The location_rating
         */
        public void setLocationRating(Double locationRating) {
            this.locationRating = locationRating;
        }

        /**
         * @return The conditionRating
         */
        public Double getConditionRating() {
            return conditionRating;
        }

        /**
         * @param conditionRating The condition_rating
         */
        public void setConditionRating(Double conditionRating) {
            this.conditionRating = conditionRating;
        }

        /**
         * @return The bathshalf
         */
        public Integer getBathshalf() {
            return bathshalf;
        }

        /**
         * @param bathshalf The bathshalf
         */
        public void setBathshalf(Integer bathshalf) {
            this.bathshalf = bathshalf;
        }

        /**
         * @return The fireplaces
         */
        public Integer getFireplaces() {
            return fireplaces;
        }

        /**
         * @param fireplaces The fireplaces
         */
        public void setFireplaces(Integer fireplaces) {
            this.fireplaces = fireplaces;
        }

        /**
         * @return The bedrooms
         */
        public Integer getBedrooms() {
            return bedrooms;
        }

        /**
         * @param bedrooms The bedrooms
         */
        public void setBedrooms(Integer bedrooms) {
            this.bedrooms = bedrooms;
        }

        /**
         * @return The optionalBedrooms
         */
        public Integer getOptionalBedrooms() {
            return optionalBedrooms;
        }

        /**
         * @param optionalBedrooms The optional_bedrooms
         */
        public void setOptionalBedrooms(Integer optionalBedrooms) {
            this.optionalBedrooms = optionalBedrooms;
        }

        /**
         * @return The baths
         */
        public Integer getBaths() {
            return baths;
        }

        /**
         * @param baths The baths
         */
        public void setBaths(Integer baths) {
            this.baths = baths;
        }

        /**
         * @return The acres
         */
        public String getAcres() {
            return acres;
        }

        /**
         * @param acres The acres
         */
        public void setAcres(String acres) {
            this.acres = acres;
        }

        /**
         * @return The price
         */
        public Integer getPrice() {
            return price;
        }

        /**
         * @param price The price
         */
        public void setPrice(Integer price) {
            this.price = price;
        }

        /**
         * @return The listOnMarket
         */
        public String getListOnMarket() {
            return listOnMarket;
        }

        /**
         * @param listOnMarket The list_on_market
         */
        public void setListOnMarket(String listOnMarket) {
            this.listOnMarket = listOnMarket;
        }

        /**
         * @return The lastSoldPrice
         */
        public String getLastSoldPrice() {
            return lastSoldPrice;
        }

        /**
         * @param lastSoldPrice The last_sold_price
         */
        public void setLastSoldPrice(String lastSoldPrice) {
            this.lastSoldPrice = lastSoldPrice;
        }

        /**
         * @return The yearBuilt
         */
        public Integer getYearBuilt() {
            return yearBuilt;
        }

        /**
         * @param yearBuilt The year_built
         */
        public void setYearBuilt(Integer yearBuilt) {
            this.yearBuilt = yearBuilt;
        }

        /**
         * @return The recordedMortgage
         */
        public String getRecordedMortgage() {
            return recordedMortgage;
        }

        /**
         * @param recordedMortgage The recorded_mortgage
         */
        public void setRecordedMortgage(String recordedMortgage) {
            this.recordedMortgage = recordedMortgage;
        }

        /**
         * @return The assesedValue
         */
        public String getAssesedValue() {
            return assesedValue;
        }

        /**
         * @param assesedValue The assesed_value
         */
        public void setAssesedValue(String assesedValue) {
            this.assesedValue = assesedValue;
        }

        /**
         * @return The currentTaxes
         */
        public String getCurrentTaxes() {
            return currentTaxes;
        }

        /**
         * @param currentTaxes The current_taxes
         */
        public void setCurrentTaxes(String currentTaxes) {
            this.currentTaxes = currentTaxes;
        }

        /**
         * @return The remarks
         */
        public String getRemarks() {
            return remarks;
        }

        /**
         * @param remarks The remarks
         */
        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        /**
         * @return The homeOwnerFees
         */
        public String getHomeOwnerFees() {
            return homeOwnerFees;
        }

        /**
         * @param homeOwnerFees The home_owner_fees
         */
        public void setHomeOwnerFees(String homeOwnerFees) {
            this.homeOwnerFees = homeOwnerFees;
        }

        /**
         * @return The homeOwnerTotalFees
         */
        public String getHomeOwnerTotalFees() {
            return homeOwnerTotalFees;
        }

        /**
         * @param homeOwnerTotalFees The home_owner_total_fees
         */
        public void setHomeOwnerTotalFees(String homeOwnerTotalFees) {
            this.homeOwnerTotalFees = homeOwnerTotalFees;
        }

        /**
         * @return The otherFees
         */
        public String getOtherFees() {
            return otherFees;
        }

        /**
         * @param otherFees The other_fees
         */
        public void setOtherFees(String otherFees) {
            this.otherFees = otherFees;
        }

        /**
         * @return The melloRoosFee
         */
        public String getMelloRoosFee() {
            return melloRoosFee;
        }

        /**
         * @param melloRoosFee The mello_roos_fee
         */
        public void setMelloRoosFee(String melloRoosFee) {
            this.melloRoosFee = melloRoosFee;
        }

        /**
         * @return The bathsfull
         */
        public String getBathsfull() {
            return bathsfull;
        }

        /**
         * @param bathsfull The bathsfull
         */
        public void setBathsfull(String bathsfull) {
            this.bathsfull = bathsfull;
        }

        /**
         * @return The parkngNonGaragedSpaces
         */
        public String getParkngNonGaragedSpaces() {
            return parkngNonGaragedSpaces;
        }

        /**
         * @param parkngNonGaragedSpaces The parkng_non_garaged_spaces
         */
        public void setParkngNonGaragedSpaces(String parkngNonGaragedSpaces) {
            this.parkngNonGaragedSpaces = parkngNonGaragedSpaces;
        }

        /**
         * @return The estimatedSquareFeet
         */
        public String getEstimatedSquareFeet() {
            return estimatedSquareFeet;
        }

        /**
         * @param estimatedSquareFeet The estimated_square_feet
         */
        public void setEstimatedSquareFeet(String estimatedSquareFeet) {
            this.estimatedSquareFeet = estimatedSquareFeet;
        }

        /**
         * @return The lotSqftApprox
         */
        public String getLotSqftApprox() {
            return lotSqftApprox;
        }

        /**
         * @param lotSqftApprox The lot_sqft_approx
         */
        public void setLotSqftApprox(String lotSqftApprox) {
            this.lotSqftApprox = lotSqftApprox;
        }

        /**
         * @return The garage
         */
        public String getGarage() {
            return garage;
        }

        /**
         * @param garage The garage
         */
        public void setGarage(String garage) {
            this.garage = garage;
        }

        /**
         * @return The parkingSpacesTotal
         */
        public String getParkingSpacesTotal() {
            return parkingSpacesTotal;
        }

        /**
         * @param parkingSpacesTotal The parking_spaces_total
         */
        public void setParkingSpacesTotal(String parkingSpacesTotal) {
            this.parkingSpacesTotal = parkingSpacesTotal;
        }

        /**
         * @return The monthlyTotalFees
         */
        public String getMonthlyTotalFees() {
            return monthlyTotalFees;
        }

        /**
         * @param monthlyTotalFees The monthly_total_fees
         */
        public void setMonthlyTotalFees(String monthlyTotalFees) {
            this.monthlyTotalFees = monthlyTotalFees;
        }

        /**
         * @return The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         * @param updatedAt The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         * @return The agentPhone
         */
        public String getAgentPhone() {
            return agentPhone;
        }

        /**
         * @param agentPhone The agent_phone
         */
        public void setAgentPhone(String agentPhone) {
            this.agentPhone = agentPhone;
        }

        /**
         * @return The street
         */
        public String getStreet() {
            return street;
        }

        /**
         * @param street The street
         */
        public void setStreet(String street) {
            this.street = street;
        }

        /**
         * @return The city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city The city
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return The state
         */
        public String getState() {
            return state;
        }

        /**
         * @param state The state
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         * @return The agentId
         */
        public String getAgentId() {
            return agentId;
        }

        /**
         * @param agentId The agent_id
         */
        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }

        /**
         * @return The tourLink
         */
        public String getTourLink() {
            return tourLink;
        }

        /**
         * @param tourLink The tour_link
         */
        public void setTourLink(String tourLink) {
            this.tourLink = tourLink;
        }

        /**
         * @return The mlsName
         */
        public String getMlsName() {
            return mlsName;
        }

        /**
         * @param mlsName The mls_name
         */
        public void setMlsName(String mlsName) {
            this.mlsName = mlsName;
        }

        /**
         * @return The sysId
         */
        public Object getSysId() {
            return sysId;
        }

        /**
         * @param sysId The sys_id
         */
        public void setSysId(Object sysId) {
            this.sysId = sysId;
        }

        /**
         * @return The upgrade1
         */
        public Object getUpgrade1() {
            return upgrade1;
        }

        /**
         * @param upgrade1 The upgrade1
         */
        public void setUpgrade1(Object upgrade1) {
            this.upgrade1 = upgrade1;
        }

        /**
         * @return The upgrade2
         */
        public Object getUpgrade2() {
            return upgrade2;
        }

        /**
         * @param upgrade2 The upgrade2
         */
        public void setUpgrade2(Object upgrade2) {
            this.upgrade2 = upgrade2;
        }

        /**
         * @return The upgrade3
         */
        public Object getUpgrade3() {
            return upgrade3;
        }

        /**
         * @param upgrade3 The upgrade3
         */
        public void setUpgrade3(Object upgrade3) {
            this.upgrade3 = upgrade3;
        }

        /**
         * @return The upgrade4
         */
        public Object getUpgrade4() {
            return upgrade4;
        }

        /**
         * @param upgrade4 The upgrade4
         */
        public void setUpgrade4(Object upgrade4) {
            this.upgrade4 = upgrade4;
        }

        /**
         * @return The greenFeature1
         */
        public Object getGreenFeature1() {
            return greenFeature1;
        }

        /**
         * @param greenFeature1 The green_feature1
         */
        public void setGreenFeature1(Object greenFeature1) {
            this.greenFeature1 = greenFeature1;
        }

        /**
         * @return The greenFeature2
         */
        public Object getGreenFeature2() {
            return greenFeature2;
        }

        /**
         * @param greenFeature2 The green_feature2
         */
        public void setGreenFeature2(Object greenFeature2) {
            this.greenFeature2 = greenFeature2;
        }

        /**
         * @return The greenFeature3
         */
        public Object getGreenFeature3() {
            return greenFeature3;
        }

        /**
         * @param greenFeature3 The green_feature3
         */
        public void setGreenFeature3(Object greenFeature3) {
            this.greenFeature3 = greenFeature3;
        }

        /**
         * @return The greenFeature4
         */
        public Object getGreenFeature4() {
            return greenFeature4;
        }

        /**
         * @param greenFeature4 The green_feature4
         */
        public void setGreenFeature4(Object greenFeature4) {
            this.greenFeature4 = greenFeature4;
        }

        /**
         * @return The upgrade1Pic
         */
        public Object getUpgrade1Pic() {
            return upgrade1Pic;
        }

        /**
         * @param upgrade1Pic The upgrade1_pic
         */
        public void setUpgrade1Pic(Object upgrade1Pic) {
            this.upgrade1Pic = upgrade1Pic;
        }

        /**
         * @return The upgrade2Pic
         */
        public Object getUpgrade2Pic() {
            return upgrade2Pic;
        }

        /**
         * @param upgrade2Pic The upgrade2_pic
         */
        public void setUpgrade2Pic(Object upgrade2Pic) {
            this.upgrade2Pic = upgrade2Pic;
        }

        /**
         * @return The upgrade3Pic
         */
        public Object getUpgrade3Pic() {
            return upgrade3Pic;
        }

        /**
         * @param upgrade3Pic The upgrade3_pic
         */
        public void setUpgrade3Pic(Object upgrade3Pic) {
            this.upgrade3Pic = upgrade3Pic;
        }

        /**
         * @return The upgrade4Pic
         */
        public Object getUpgrade4Pic() {
            return upgrade4Pic;
        }

        /**
         * @param upgrade4Pic The upgrade4_pic
         */
        public void setUpgrade4Pic(Object upgrade4Pic) {
            this.upgrade4Pic = upgrade4Pic;
        }

        /**
         * @return The greenFeature1Pic
         */
        public Object getGreenFeature1Pic() {
            return greenFeature1Pic;
        }

        /**
         * @param greenFeature1Pic The green_feature1_pic
         */
        public void setGreenFeature1Pic(Object greenFeature1Pic) {
            this.greenFeature1Pic = greenFeature1Pic;
        }

        /**
         * @return The greenFeature2Pic
         */
        public Object getGreenFeature2Pic() {
            return greenFeature2Pic;
        }

        /**
         * @param greenFeature2Pic The green_feature2_pic
         */
        public void setGreenFeature2Pic(Object greenFeature2Pic) {
            this.greenFeature2Pic = greenFeature2Pic;
        }

        /**
         * @return The greenFeature3Pic
         */
        public Object getGreenFeature3Pic() {
            return greenFeature3Pic;
        }

        /**
         * @param greenFeature3Pic The green_feature3_pic
         */
        public void setGreenFeature3Pic(Object greenFeature3Pic) {
            this.greenFeature3Pic = greenFeature3Pic;
        }

        /**
         * @return The greenFeature4Pic
         */
        public Object getGreenFeature4Pic() {
            return greenFeature4Pic;
        }

        /**
         * @param greenFeature4Pic The green_feature4_pic
         */
        public void setGreenFeature4Pic(Object greenFeature4Pic) {
            this.greenFeature4Pic = greenFeature4Pic;
        }

        /**
         * @return The photos
         */
        public List<String> getPhotos() {
            return photos;
        }

        /**
         * @param photos The photos
         */
        public void setPhotos(List<String> photos) {
            this.photos = photos;
        }

        /**
         * @return The thumbnails
         */
        public List<String> getThumbnails() {
            return thumbnails;
        }

        /**
         * @param thumbnails The thumbnails
         */
        public void setThumbnails(List<String> thumbnails) {
            this.thumbnails = thumbnails;
        }

        /**
         * @return The parcelNum
         */
        public String getParcelNum() {
            return parcelNum;
        }

        /**
         * @param parcelNum The parcel_num
         */
        public void setParcelNum(String parcelNum) {
            this.parcelNum = parcelNum;
        }

        /**
         * @return The avmLow
         */
        public Object getAvmLow() {
            return avmLow;
        }

        /**
         * @param avmLow The avm_low
         */
        public void setAvmLow(Object avmLow) {
            this.avmLow = avmLow;
        }

        /**
         * @return The avmHigh
         */
        public Object getAvmHigh() {
            return avmHigh;
        }

        /**
         * @param avmHigh The avm_high
         */
        public void setAvmHigh(Object avmHigh) {
            this.avmHigh = avmHigh;
        }

        /**
         * @return The soldDate
         */
        public Object getSoldDate() {
            return soldDate;
        }

        /**
         * @param soldDate The sold_date
         */
        public void setSoldDate(Object soldDate) {
            this.soldDate = soldDate;
        }

        /**
         * @return The distance
         */
        public Object getDistance() {
            return distance;
        }

        /**
         * @param distance The distance
         */
        public void setDistance(Object distance) {
            this.distance = distance;
        }
    }
}

