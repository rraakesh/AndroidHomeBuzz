/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.repository.datasource;

import java.util.List;
import java.util.SortedMap;
import java.util.WeakHashMap;

import rx.Observable;
import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface UserDataStore {
    /**
     * Get an {@link rx.Observable} which will emit a List of {@link UserEntity}.
     */
    Observable<List<UserEntity>> userEntityList();

    /**
     * Get an {@link rx.Observable} which will emit a {@link UserEntity} by its id.
     *
     * @param userId The id to retrieve user data.
     */
    Observable<UserEntity> userEntityDetails(final int userId);

    Observable<User_Entity> userEntityDetailsLogin(/*String username, String password, String device_id*/ WeakHashMap<String,String> loginCredential,boolean isFbLogin);

    Observable<List<NearbyEntity>> userEntityListing(/*String lat, String lon, String radius*/SortedMap<String,String> nearbyData);

    Observable<List<AccuracyEntity>> userEntityAccuracy(WeakHashMap<String, String> filterData);

    Observable<List<EstimatesEntity>> userEntityEstimate(WeakHashMap<String, String> filterData);

    Observable<List<AllMessagesEntity>> userEntityAllMessages();

    Observable<List<PreviousEstimate>> userEntityPreviousEstimate(String User_id);

    Observable<ProfileEntity> userProfileEntity(String user_id);
    Observable<User_Signup> userRegistration(WeakHashMap<String,String> userData);
    Observable<List<FriendFollowed>> userFollowed();
    Observable<AddListingEntity> userAddListing(WeakHashMap<String,String> listing);
    Observable<AddListingPhotoEntity> userAddListingPhoto(String listing_id,WeakHashMap<String,String> photo,WeakHashMap<String,String> listingData);
    Observable<AddCheckInPhotoEntity> userAddCheckInPhoto(String listing_id, WeakHashMap<String, String> photo, WeakHashMap<String, String> listingData);
    Observable<ReplyMessageEntity> userReplyMessages(WeakHashMap<String, String> replyMessageCredential);
    Observable<UpdateCheckinEntity> userUpdateCheckInEntity(String listing_id,WeakHashMap<String, String> updateCheckin);
    Observable<UpdateProfile> userProfileEntity(WeakHashMap<String,String> userProfilePic,WeakHashMap<String,String> listing);
    Observable<List<MessageEntity>> userUnreadMessages();
    Observable<JustSoldEntity> useJustSoldListingEntity(WeakHashMap<String,String> soldData);
    Observable<JustListedEntity>  useJustListedListingEntity(WeakHashMap<String,String> listedData);
    Observable<FollowUnfollowEntity> userFollowUnfollow(String User_id);
    Observable<InviteFriends> userInviteFriends(WeakHashMap<String, String> sendInvitation);
    Observable<List<FriendFollowed>> userFollowers();
    Observable<List<ZipPurchaseEntity>> userZipPurchaseEntity();
    Observable<SearchAddressEntity> userOnBoardPropEntity(WeakHashMap<String,String> listedData);
    Observable<UpdateProfile> userOtherProfileEntity(WeakHashMap<String,String> profilepic ,WeakHashMap<String,String> listing);
    Observable<ProfilePicEntity> userProfilePicEntity(WeakHashMap<String,String> profilePicData);
    Observable<ReplyMessageEntity> userReadMessages(WeakHashMap<String, String> replyMessageCredential);
    Observable<List<SearchPeopleEntity>> userSearchPeople(WeakHashMap<String, String> replyMessageCredential);

    Observable<EstimateAverage> userEstimateAverage(WeakHashMap<String, String> replyMessageCredential);
    Observable<ZipAddEntity> userZipCodeAddEntity(WeakHashMap<String, String> zipCodeAdd);
    Observable<UpdateZipCodeEntity> userUpdateZip(WeakHashMap<String, String> replyMessageCredential);
    Observable<SubscriptionEntity> userSubscription(WeakHashMap<String, String> userSubscription);
    Observable<ZipCodeDeleteEntity> zipDelete(WeakHashMap<String, String> zipDelete);
}
