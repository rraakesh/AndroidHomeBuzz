package us.homebuzz.homebuzz.data.Utility;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by amit.singh on 9/21/2015.
 */
public class NetworkStateManager {

    private final ConnectivityManager connectivityManager;

    public NetworkStateManager(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    public boolean isConnectedOrConnecting(){
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo.isConnectedOrConnecting();
    }
}