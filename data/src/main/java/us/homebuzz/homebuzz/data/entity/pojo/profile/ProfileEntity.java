package us.homebuzz.homebuzz.data.entity.pojo.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class ProfileEntity {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("sign_in_count")
    @Expose
    private Integer signInCount;
    @SerializedName("current_sign_in_at")
    @Expose
    private String currentSignInAt;
    @SerializedName("last_sign_in_at")
    @Expose
    private String lastSignInAt;
    @SerializedName("current_sign_in_ip")
    @Expose
    private CurrentSignInIp currentSignInIp;
    @SerializedName("last_sign_in_ip")
    @Expose
    private LastSignInIp lastSignInIp;
    @SerializedName("confirmed_at")
    @Expose
    private Object confirmedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("accurate")
    @Expose
    private Double accurate;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("team_id")
    @Expose
    private Object teamId;
    @SerializedName("office_id")
    @Expose
    private Object officeId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("subscription_active")
    @Expose
    private Object subscriptionActive;
    @SerializedName("confirmed")
    @Expose
    private Boolean confirmed;
    @SerializedName("followed")
    @Expose
    private Boolean followed;
    @SerializedName("sold_listings")
    @Expose
    private Integer soldListings;
    @SerializedName("followers")
    @Expose
    private Integer followers;
    @SerializedName("followings")
    @Expose
    private Integer followings;
    @SerializedName("photos")
    @Expose
    private Integer photos;
    @SerializedName("estimates_count")
    @Expose
    private Integer estimatesCount;
    @SerializedName("checkins_count")
    @Expose
    private Integer checkinsCount;
    @SerializedName("reviews_count")
    @Expose
    private Integer reviesCount ;

    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("yr_graduated")
    @Expose
    private Integer yrGraduated;
    @SerializedName("college")
    @Expose
    private String college;
    @SerializedName("other_school")
    @Expose
    private String otherSchool;
    @SerializedName("designations")
    @Expose
    private String designations;
    @SerializedName("linked_in")
    @Expose
    private String linkedIn;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("verified")
    @Expose
    private Object verified;
    @SerializedName("agent_id")
    @Expose
    private Integer agentId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("mls_name")
    @Expose
    private Object mlsName;
    @SerializedName("license_num")
    @Expose
    private Object licenseNum;
    @SerializedName("nap_num")
    @Expose
    private Object napNum;
    @SerializedName("street")
    @Expose
    private Object street;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("job_title")
    @Expose
    private Object jobTitle;
    @SerializedName("company")
    @Expose
    private Object company;
    @SerializedName("facebook_url")
    @Expose
    private Object facebookUrl;
    @SerializedName("twitter_url")
    @Expose
    private Object twitterUrl;
    @SerializedName("educations")
    @Expose
    private String educations;
    @SerializedName("profile_id")
    @Expose
    private Integer profile_id;
    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    /**
     * @return The userId
     */
    public Integer getProfileId() {
        return profile_id;
    }

    /**
     * @param profile_id The user_id
     */
    public void setProfileId(Integer profile_id) {
        this.profile_id = profile_id;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The signInCount
     */
    public Integer getSignInCount() {
        return signInCount;
    }

    /**
     *
     * @param signInCount
     * The sign_in_count
     */
    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    /**
     *
     * @return
     * The currentSignInAt
     */
    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    /**
     *
     * @param currentSignInAt
     * The current_sign_in_at
     */
    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    /**
     *
     * @return
     * The lastSignInAt
     */
    public String getLastSignInAt() {
        return lastSignInAt;
    }

    /**
     *
     * @param lastSignInAt
     * The last_sign_in_at
     */
    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

    /**
     *
     * @return
     * The currentSignInIp
     */
    public CurrentSignInIp getCurrentSignInIp() {
        return currentSignInIp;
    }

    /**
     *
     * @param currentSignInIp
     * The current_sign_in_ip
     */
    public void setCurrentSignInIp(CurrentSignInIp currentSignInIp) {
        this.currentSignInIp = currentSignInIp;
    }

    /**
     *
     * @return
     * The lastSignInIp
     */
    public LastSignInIp getLastSignInIp() {
        return lastSignInIp;
    }

    /**
     *
     * @param lastSignInIp
     * The last_sign_in_ip
     */
    public void setLastSignInIp(LastSignInIp lastSignInIp) {
        this.lastSignInIp = lastSignInIp;
    }

    /**
     *
     * @return
     * The confirmedAt
     */
    public Object getConfirmedAt() {
        return confirmedAt;
    }

    /**
     *
     * @param confirmedAt
     * The confirmed_at
     */
    public void setConfirmedAt(Object confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public Object getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     *
     * @param points
     * The points
     */
    public void setPoints(Integer points) {
        this.points = points;
    }

    /**
     *
     * @return
     * The teamId
     */
    public Object getTeamId() {
        return teamId;
    }

    /**
     *
     * @param teamId
     * The team_id
     */
    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    /**
     *
     * @return
     * The officeId
     */
    public Object getOfficeId() {
        return officeId;
    }

    /**
     *
     * @param officeId
     * The office_id
     */
    public void setOfficeId(Object officeId) {
        this.officeId = officeId;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The subscriptionActive
     */
    public Object getSubscriptionActive() {
        return subscriptionActive;
    }

    /**
     *
     * @param subscriptionActive
     * The subscription_active
     */
    public void setSubscriptionActive(Object subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    /**
     *
     * @return
     * The confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     *
     * @param confirmed
     * The confirmed
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     *
     * @return
     * The followed
     */
    public Boolean getFollowed() {
        return followed;
    }

    /**
     *
     * @param followed
     * The followed
     */
    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    /**
     *
     * @return
     * The soldListings
     */
    public Integer getSoldListings() {
        return soldListings;
    }

    /**
     *
     * @param soldListings
     * The sold_listings
     */
    public void setSoldListings(Integer soldListings) {
        this.soldListings = soldListings;
    }

    /**
     *
     * @return
     * The followers
     */
    public Integer getFollowers() {
        return followers;
    }

    /**
     *
     * @param followers
     * The followers
     */
    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    /**
     *
     * @return
     * The followings
     */
    public Integer getFollowings() {
        return followings;
    }

    /**
     *
     * @param followings
     * The followings
     */
    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    /**
     *
     * @return
     * The photos
     */
    public Integer getPhotos() {
        return photos;
    }

    /**
     *
     * @param photos
     * The photos
     */
    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }
    public Integer getCheckinsCount(){
        return checkinsCount;
    }

    /**
     *
     * @param checkinsCount
     * The estimates_count
     */
    public void setCheckinsCount(Integer checkinsCount) {
        this.checkinsCount = checkinsCount;
    }
    public Integer getReviesCount(){
        return reviesCount;
    }

    /**
     *
     * @param reviesCount
     * The estimates_count
     */
    public void setReviesCount(Integer reviesCount) {
        this.reviesCount = reviesCount;
    }



    /**
     * @return The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * @return The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     * @param degree The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * @return The yrGraduated
     */
    public Integer getYrGraduated() {
        return yrGraduated;
    }

    /**
     * @param yrGraduated The yr_graduated
     */
    public void setYrGraduated(Integer yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    /**
     * @return The college
     */
    public String getCollege() {
        return college;
    }

    /**
     * @param college The college
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     * @return The otherSchool
     */
    public String getOtherSchool() {
        return otherSchool;
    }

    /**
     * @param otherSchool The other_school
     */
    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    /**
     * @return The designations
     */
    public String getDesignations() {
        return designations;
    }

    /**
     * @param designations The designations
     */
    public void setDesignations(String designations) {
        this.designations = designations;
    }

    /**
     * @return The linkedIn
     */
    public String getLinkedIn() {
        return linkedIn;
    }

    /**
     * @param linkedIn The linked_in
     */
    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * @param profilePic The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * @return The verified
     */
    public Object getVerified() {
        return verified;
    }

    /**
     * @param verified The verified
     */
    public void setVerified(Object verified) {
        this.verified = verified;
    }

    /**
     * @return The agentId
     */
    public Integer getAgentId() {
        return agentId;
    }

    /**
     * @param agentId The agent_id
     */
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The mlsName
     */
    public Object getMlsName() {
        return mlsName;
    }

    /**
     * @param mlsName The mls_name
     */
    public void setMlsName(Object mlsName) {
        this.mlsName = mlsName;
    }

    /**
     * @return The licenseNum
     */
    public Object getLicenseNum() {
        return licenseNum;
    }

    /**
     * @param licenseNum The license_num
     */
    public void setLicenseNum(Object licenseNum) {
        this.licenseNum = licenseNum;
    }

    /**
     * @return The napNum
     */
    public Object getNapNum() {
        return napNum;
    }

    /**
     * @param napNum The nap_num
     */
    public void setNapNum(Object napNum) {
        this.napNum = napNum;
    }

    /**
     * @return The street
     */
    public Object getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(Object street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public Object getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(Object city) {
        this.city = city;
    }

    /**
     * @return The state
     */
    public Object getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(Object state) {
        this.state = state;
    }

    /**
     * @return The jobTitle
     */
    public Object getJobTitle() {
        return jobTitle;
    }

    /**
     * @param jobTitle The job_title
     */
    public void setJobTitle(Object jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * @return The company
     */
    public Object getCompany() {
        return company;
    }

    /**
     * @param company The company
     */
    public void setCompany(Object company) {
        this.company = company;
    }

    /**
     * @return The facebookUrl
     */
    public Object getFacebookUrl() {
        return facebookUrl;
    }

    /**
     * @param facebookUrl The facebook_url
     */
    public void setFacebookUrl(Object facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    /**
     * @return The twitterUrl
     */
    public Object getTwitterUrl() {
        return twitterUrl;
    }

    /**
     * @param twitterUrl The twitter_url
     */
    public void setTwitterUrl(Object twitterUrl) {
        this.twitterUrl = twitterUrl;
    }


    /**
     *
     * @return
     * The educations
     */
    public String getEducations() {
        return educations;
    }

    /**
     *
     * @param educations
     * The educations
     */
    public void setEducations(String educations) {
        this.educations = educations;
    }
}
