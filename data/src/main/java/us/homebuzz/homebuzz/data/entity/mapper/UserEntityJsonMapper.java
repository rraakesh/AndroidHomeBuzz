/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.data.entity.mapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import us.homebuzz.homebuzz.data.entity.UserEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.AddCheckInPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin.UpdateCheckinEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.AddListingPhotoEntity;
import us.homebuzz.homebuzz.data.entity.pojo.addListing.SearchAddressEntity;
import us.homebuzz.homebuzz.data.entity.pojo.estimateAvarage.EstimateAverage;
import us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow.FollowUnfollowEntity;
import us.homebuzz.homebuzz.data.entity.pojo.friends.followed.FriendFollowed;
import us.homebuzz.homebuzz.data.entity.pojo.home.User_Entity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.AccuracyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.leader.EstimatesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.AllMessagesEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.ReplyMessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.messages.unread.MessageEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.NearbyEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_listed.JustListedEntity;
import us.homebuzz.homebuzz.data.entity.pojo.nearby.just_sold.JustSoldEntity;
import us.homebuzz.homebuzz.data.entity.pojo.previousEstimate.PreviousEstimate;
import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfileEntity;
import us.homebuzz.homebuzz.data.entity.pojo.profile.ProfilePicEntity;
import us.homebuzz.homebuzz.data.entity.pojo.purchaseZipCode.SubscriptionEntity;
import us.homebuzz.homebuzz.data.entity.pojo.searchPeople.SearchPeopleEntity;
import us.homebuzz.homebuzz.data.entity.pojo.signup.User_Signup;
import us.homebuzz.homebuzz.data.entity.pojo.updateProfile.UpdateProfile;
import us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete.ZipCodeDeleteEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipAddEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.UpdateZipCodeEntity;
import us.homebuzz.homebuzz.data.entity.pojo.zipcode.ZipPurchaseEntity;

/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class UserEntityJsonMapper {

    private final Gson gson;

    @Inject
    public UserEntityJsonMapper() {
        this.gson = new GsonBuilder().serializeNulls().create();
     /* this.gson = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();*/
    }

    /**
     * Transform from valid json string to {@link UserEntity}.
     *
     * @param userJsonResponse A json representing a user profile.
     * @return {@link UserEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public UserEntity transformUserEntity(String userJsonResponse) throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<UserEntity>() {
            }.getType();
            UserEntity userEntity = this.gson.fromJson(userJsonResponse, userEntityType);

            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public User_Entity transformUserEntityLogin(String userJsonResponse) throws JsonSyntaxException {
        try {
            System.out.println("Json String in Class UserEntityJsonMapper" + userJsonResponse);
            Type userEntityType = new TypeToken<User_Entity>() {
            }.getType();
            User_Entity userEntity = this.gson.fromJson(userJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public List<NearbyEntity> transformUserNearbyEntity(String userJsonResponse) throws JsonSyntaxException {
        try {
            System.out.println("Json String in Class UserEntityJsonMapper" + userJsonResponse);
            Type userEntityType = new TypeToken<List<NearbyEntity>>() {
            }.getType();
            List<NearbyEntity> userEntity = this.gson.fromJson(userJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userListJsonResponse A json representing a collection of users.
     * @return List of {@link UserEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<UserEntity> transformUserEntityCollection(String userListJsonResponse)
            throws JsonSyntaxException {

        List<UserEntity> userEntityCollection;
        try {
            Type listOfUserEntityType = new TypeToken<List<UserEntity>>() {
            }.getType();
            userEntityCollection = this.gson.fromJson(userListJsonResponse, listOfUserEntityType);

            return userEntityCollection;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userAccuracyJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<AccuracyEntity> transformUserAccuracyEntity(String userAccuracyJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<List<AccuracyEntity>>() {
            }.getType();
            List<AccuracyEntity> userEntity = this.gson.fromJson(userAccuracyJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userEstimatesJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<EstimatesEntity> transformUserEstimatesEntity(String userEstimatesJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<List<EstimatesEntity>>() {
            }.getType();
            List<EstimatesEntity> userEntity = this.gson.fromJson(userEstimatesJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userEstimatesJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<AllMessagesEntity> transformUserAllMessagesEntity(String userEstimatesJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<List<AllMessagesEntity>>() {
            }.getType();
            List<AllMessagesEntity> userEntity = this.gson.fromJson(userEstimatesJsonResponse, userEntityType);
            //  System.out.println(" body JSon" + userEntity.get(0).getBody());
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userPreviousEstimatJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<PreviousEstimate> transformPreviousEstimateEntity(String userPreviousEstimatJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<List<PreviousEstimate>>() {
            }.getType();
            List<PreviousEstimate> userEntity = this.gson.fromJson(userPreviousEstimatJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userProfileJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public ProfileEntity transformProfileEntity(String userProfileJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<ProfileEntity>() {
            }.getType();
            ProfileEntity userEntity = this.gson.fromJson(userProfileJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userSignupJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public User_Signup transformSignupEntity(String userSignupJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<User_Signup>() {
            }.getType();
            User_Signup userEntity = this.gson.fromJson(userSignupJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userFrFollowJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<FriendFollowed> transformFriendsFollowedEntity(String userFrFollowJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<List<FriendFollowed>>() {
            }.getType();
            List<FriendFollowed> userEntity = this.gson.fromJson(userFrFollowJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userAddListinJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public AddListingEntity transformAddListingEntity(String userAddListinJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<AddListingEntity>() {
            }.getType();
            AddListingEntity userEntity = this.gson.fromJson(userAddListinJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public AddListingPhotoEntity transformAddListinPhotoEntity(String userAddListinJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<AddListingPhotoEntity>() {
            }.getType();
            AddListingPhotoEntity userEntity = this.gson.fromJson(userAddListinJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public AddCheckInPhotoEntity transformAddCheckInPhotoEntity(String userAddListinJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<AddCheckInPhotoEntity>() {
            }.getType();
            AddCheckInPhotoEntity userEntity = this.gson.fromJson(userAddListinJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userMessagepJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public ReplyMessageEntity transformReplypEntity(String userMessagepJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<ReplyMessageEntity>() {
            }.getType();
            ReplyMessageEntity userEntity = this.gson.fromJson(userMessagepJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public InviteFriends transformSendInvitationEntity(String userSendInvitationJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<InviteFriends>() {
            }.getType();
            InviteFriends userEntity = this.gson.fromJson(userSendInvitationJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userUpdateCheckinJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public UpdateCheckinEntity transformCheck_InEntity(String userUpdateCheckinJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<UpdateCheckinEntity>() {
            }.getType();
            UpdateCheckinEntity userEntity = this.gson.fromJson(userUpdateCheckinJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userProfileJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public UpdateProfile transformUpdateProfileEntity(String userProfileJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<UpdateProfile>() {
            }.getType();
            UpdateProfile userEntity = this.gson.fromJson(userProfileJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userEstimatesJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<MessageEntity> transformUserUnreadMessagesEntity(String userEstimatesJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<List<MessageEntity>>() {
            }.getType();
            List<MessageEntity> userEntity = this.gson.fromJson(userEstimatesJsonResponse, userEntityType);
//            System.out.println(" body JSon" + userEntity.get(0).getBody());
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userEstimatesJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public JustListedEntity transformUserJustListedEntity(String userEstimatesJsonResponse)
            throws JsonSyntaxException {

        try {
            Type userEntityType = new TypeToken<JustListedEntity>() {
            }.getType();
            JustListedEntity userEntity = this.gson.fromJson(userEstimatesJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userEstimatesJsonResponse A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public JustSoldEntity transformUserJustSoldEntity(String userEstimatesJsonResponse)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<JustSoldEntity>() {
            }.getType();
            JustSoldEntity userEntity = this.gson.fromJson(userEstimatesJsonResponse, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param userFollowUnfollow A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public FollowUnfollowEntity transformFollowUnfollow(String userFollowUnfollow)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<FollowUnfollowEntity>() {
            }.getType();
            FollowUnfollowEntity userEntity = this.gson.fromJson(userFollowUnfollow, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param zipPurchaseString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<ZipPurchaseEntity> transformZipPurchase(String zipPurchaseString)
            throws JsonSyntaxException {
        try {
            Type ZipPurchaseEntity = new TypeToken<List<ZipPurchaseEntity>>() {
            }.getType();
            List<ZipPurchaseEntity> userEntity = this.gson.fromJson(zipPurchaseString, ZipPurchaseEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param getOnBoardPropString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public SearchAddressEntity transformGetOnBoardProp(String getOnBoardPropString)
            throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<SearchAddressEntity>() {
            }.getType();
            SearchAddressEntity userEntity = this.gson.fromJson(getOnBoardPropString, userEntityType);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param getOnBoardPropString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public ProfilePicEntity transformProfilePicUpdate(String getOnBoardPropString)
            throws JsonSyntaxException {
        try {
            Type ProfilePicEntity = new TypeToken<ProfilePicEntity>() {
            }.getType();
            ProfilePicEntity userEntity = this.gson.fromJson(getOnBoardPropString, ProfilePicEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param getOnBoardPropString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<SearchPeopleEntity> transformSearchPeople(String getOnBoardPropString)
            throws JsonSyntaxException {
        try {
            Type SearchPeopleEntity = new TypeToken<List<SearchPeopleEntity>>() {
            }.getType();
            List<SearchPeopleEntity> userEntity = this.gson.fromJson(getOnBoardPropString, SearchPeopleEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param estimateAverageString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public EstimateAverage transformEstimateAverage(String estimateAverageString)
            throws JsonSyntaxException {
        try {
            Type EstimateAverage = new TypeToken<EstimateAverage>() {
            }.getType();
            EstimateAverage userEntity = this.gson.fromJson(estimateAverageString, EstimateAverage);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public ZipAddEntity transformZipAdd(String zipAddString)
            throws JsonSyntaxException {
        try {
            Type zipAddEntity = new TypeToken<ZipAddEntity>() {
            }.getType();
            ZipAddEntity userEntity = this.gson.fromJson(zipAddString, zipAddEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param updateZipcodeString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public UpdateZipCodeEntity transformUpdateZipCode(String updateZipcodeString)
            throws JsonSyntaxException {
        try {
            Type UpdateZipCodeEntity = new TypeToken<UpdateZipCodeEntity>() {}.getType();
            UpdateZipCodeEntity  userEntity = this.gson.fromJson(updateZipcodeString, UpdateZipCodeEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link UserEntity}.
     *
     * @param updateZipCodeString A json representing a collection of users.
     * @return List of {@link AccuracyEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public SubscriptionEntity transformSubscriptionPlanData(String updateZipCodeString)
            throws JsonSyntaxException {
        try {
            Type SubscriptionEntity = new TypeToken<SubscriptionEntity>() {}.getType();
            SubscriptionEntity  userEntity = this.gson.fromJson(updateZipCodeString, SubscriptionEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    public ZipCodeDeleteEntity transformZipCodeDelete(String updateZipcodeString)
            throws JsonSyntaxException {
        try {
            Type zipCodeDeleteEntity = new TypeToken<ZipCodeDeleteEntity>() {}.getType();
            ZipCodeDeleteEntity  userEntity = this.gson.fromJson(updateZipcodeString, zipCodeDeleteEntity);
            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

}
