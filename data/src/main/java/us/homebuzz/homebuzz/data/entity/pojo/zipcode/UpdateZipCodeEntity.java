package us.homebuzz.homebuzz.data.entity.pojo.zipcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 12/15/2015.
 */
public class UpdateZipCodeEntity {

    @SerializedName("zip")
    @Expose
    private Zip zip;

    /**
     *
     * @return
     * The zip
     */
    public Zip getZip() {
        return zip;
    }

    /**
     *
     * @param zip
     * The zip
     */
    public void setZip(Zip zip) {
        this.zip = zip;
    }
    public class Zip {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("zip_code")
        @Expose
        private String zipCode;
        @SerializedName("expired_at")
        @Expose
        private String expiredAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         *
         * @param userId
         * The user_id
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         *
         * @return
         * The zipCode
         */
        public String getZipCode() {
            return zipCode;
        }

        /**
         *
         * @param zipCode
         * The zip_code
         */
        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        /**
         *
         * @return
         * The expiredAt
         */
        public String getExpiredAt() {
            return expiredAt;
        }

        /**
         *
         * @param expiredAt
         * The expired_at
         */
        public void setExpiredAt(String expiredAt) {
            this.expiredAt = expiredAt;
        }

        /**
         *
         * @return
         * The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         * The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         *
         * @return
         * The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         *
         * @param updatedAt
         * The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
