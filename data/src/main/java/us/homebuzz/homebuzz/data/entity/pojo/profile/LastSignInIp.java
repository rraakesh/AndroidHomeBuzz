package us.homebuzz.homebuzz.data.entity.pojo.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abhishek.singh on 11/14/2015.
 */
public class LastSignInIp {
    @SerializedName("family")
    @Expose
    private Double family;
    @SerializedName("addr")
    @Expose
    private Double addr;
    @SerializedName("mask_addr")
    @Expose
    private Double maskAddr;

    /**
     * @return The family
     */
    public Double getFamily() {
        return family;
    }

    /**
     * @param family The family
     */
    public void setFamily(Double family) {
        this.family = family;
    }

    /**
     * @return The addr
     */
    public Double getAddr() {
        return addr;
    }

    /**
     * @param addr The addr
     */
    public void setAddr(Double addr) {
        this.addr = addr;
    }

    /**
     * @return The maskAddr
     */
    public Double getMaskAddr() {
        return maskAddr;
    }

    /**
     * @param maskAddr The mask_addr
     */
    public void setMaskAddr(Double maskAddr) {
        this.maskAddr = maskAddr;
    }

}