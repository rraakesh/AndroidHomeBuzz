package us.homebuzz.homebuzz.data.entity.pojo.zipCodeDelete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipCodeDeleteEntity {

    @SerializedName("delete")
    @Expose
    private Boolean delete;

    /**
     *
     * @return
     * The delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     *
     * @param delete
     * The delete
     */
    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

}
