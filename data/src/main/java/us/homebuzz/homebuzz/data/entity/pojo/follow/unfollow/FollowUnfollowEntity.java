package us.homebuzz.homebuzz.data.entity.pojo.follow.unfollow;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
public class FollowUnfollowEntity {

    @SerializedName("followed")
    @Expose
    private Boolean followed;

    /**
     *
     * @return
     * The followed
     */
    public Boolean getFollowed() {
        return followed;
    }

    /**
     *
     * @param followed
     * The followed
     */
    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

}