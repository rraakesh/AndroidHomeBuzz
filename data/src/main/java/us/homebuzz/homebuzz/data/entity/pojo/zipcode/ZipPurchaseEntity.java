package us.homebuzz.homebuzz.data.entity.pojo.zipcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipPurchaseEntity {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("expired")
    @Expose
    private Boolean expired;
    @SerializedName("expired_at")
    @Expose
    private String expiredAt;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The expired
     */
    public Boolean getExpired() {
        return expired;
    }

    /**
     *
     * @param expired
     * The expired
     */
    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    /**
     *
     * @return
     * The expiredAt
     */
    public String getExpiredAt() {
        return expiredAt;
    }

    /**
     *
     * @param expiredAt
     * The expired_at
     */
    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }
}
