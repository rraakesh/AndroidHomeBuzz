package us.homebuzz.homebuzz.data.entity.pojo.UpdateCheckin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import us.homebuzz.homebuzz.data.entity.pojo.nearby.PreviousReview;
import us.homebuzz.homebuzz.domain.data.User_;

/**
 * Created by amit.singh on 11/9/2015.
 */
public class Listing {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private User_ user;
    @SerializedName("mls_num")
    @Expose
    private Object mlsNum;
    @SerializedName("listing_class")
    @Expose
    private Object listingClass;
    @SerializedName("listing_class_type")
    @Expose
    private Object listingClassType;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("for_sale")
    @Expose
    private Object forSale;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("photos")
    @Expose
    private List<String> photos = new ArrayList<String>();
    @SerializedName("thumbnails")
    @Expose
    private List<String> thumbnails = new ArrayList<String>();
    @SerializedName("location_rating")
    @Expose
    private Double locationRating;
    @SerializedName("condition_rating")
    @Expose
    private Double conditionRating;
    @SerializedName("bathshalf")
    @Expose
    private Integer bathshalf;
    @SerializedName("fireplaces")
    @Expose
    private Integer fireplaces;
    @SerializedName("bedrooms")
    @Expose
    private Integer bedrooms;
    @SerializedName("optional_bedrooms")
    @Expose
    private Object optionalBedrooms;
    @SerializedName("baths")
    @Expose
    private Integer baths;
    @SerializedName("acres")
    @Expose
    private Double acres;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("list_on_market")
    @Expose
    private String listOnMarket;
    @SerializedName("last_sold_price")
    @Expose
    private Object lastSoldPrice;
    @SerializedName("year_built")
    @Expose
    private Integer yearBuilt;
    @SerializedName("recorded_mortgage")
    @Expose
    private Double recordedMortgage;
    @SerializedName("assesed_value")
    @Expose
    private Double assesedValue;
    @SerializedName("current_taxes")
    @Expose
    private Double currentTaxes;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("home_owner_fees")
    @Expose
    private Object homeOwnerFees;
    @SerializedName("home_owner_total_fees")
    @Expose
    private Object homeOwnerTotalFees;
    @SerializedName("other_fees")
    @Expose
    private Object otherFees;
    @SerializedName("mello_roos_fee")
    @Expose
    private Object melloRoosFee;
    @SerializedName("bathsfull")
    @Expose
    private Object bathsfull;
    @SerializedName("parkng_non_garaged_spaces")
    @Expose
    private Object parkngNonGaragedSpaces;
    @SerializedName("estimated_square_feet")
    @Expose
    private Object estimatedSquareFeet;
    @SerializedName("lot_sqft_approx")
    @Expose
    private Object lotSqftApprox;
    @SerializedName("garage")
    @Expose
    private Object garage;
    @SerializedName("agent_phone")
    @Expose
    private Object agentPhone;
    @SerializedName("parking_spaces_total")
    @Expose
    private Object parkingSpacesTotal;
    @SerializedName("monthly_total_fees")
    @Expose
    private Object monthlyTotalFees;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("street")
    @Expose
    private Object street;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("agent_id")
    @Expose
    private Object agentId;
    @SerializedName("tour_link")
    @Expose
    private Object tourLink;
    @SerializedName("mls_name")
    @Expose
    private Object mlsName;
    @SerializedName("sys_id")
    @Expose
    private Object sysId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("upgrade1")
    @Expose
    private Object upgrade1;
    @SerializedName("upgrade2")
    @Expose
    private Object upgrade2;
    @SerializedName("upgrade3")
    @Expose
    private Object upgrade3;
    @SerializedName("upgrade4")
    @Expose
    private Object upgrade4;
    @SerializedName("upgrade1_pic")
    @Expose
    private Object upgrade1Pic;
    @SerializedName("upgrade2_pic")
    @Expose
    private Object upgrade2Pic;
    @SerializedName("upgrade3_pic")
    @Expose
    private Object upgrade3Pic;
    @SerializedName("upgrade4_pic")
    @Expose
    private Object upgrade4Pic;
    @SerializedName("green_feature1")
    @Expose
    private Object greenFeature1;
    @SerializedName("green_feature2")
    @Expose
    private Object greenFeature2;
    @SerializedName("green_feature3")
    @Expose
    private Object greenFeature3;
    @SerializedName("green_feature4")
    @Expose
    private Object greenFeature4;
    @SerializedName("green_feature1_pic")
    @Expose
    private Object greenFeature1Pic;
    @SerializedName("green_feature2_pic")
    @Expose
    private Object greenFeature2Pic;
    @SerializedName("green_feature3_pic")
    @Expose
    private Object greenFeature3Pic;
    @SerializedName("green_feature4_pic")
    @Expose
    private Object greenFeature4Pic;
    @SerializedName("favorited")
    @Expose
    private Boolean favorited;
    @SerializedName("previous_reviews")
    @Expose
    private List<PreviousReview> previousReviews = new ArrayList<PreviousReview>();
    @SerializedName("estimates")
    @Expose
    private List<Estimate> estimates = new ArrayList<Estimate>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The user
     */
    public User_ getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User_ user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The mlsNum
     */
    public Object getMlsNum() {
        return mlsNum;
    }

    /**
     *
     * @param mlsNum
     * The mls_num
     */
    public void setMlsNum(Object mlsNum) {
        this.mlsNum = mlsNum;
    }

    /**
     *
     * @return
     * The listingClass
     */
    public Object getListingClass() {
        return listingClass;
    }

    /**
     *
     * @param listingClass
     * The listing_class
     */
    public void setListingClass(Object listingClass) {
        this.listingClass = listingClass;
    }

    /**
     *
     * @return
     * The listingClassType
     */
    public Object getListingClassType() {
        return listingClassType;
    }

    /**
     *
     * @param listingClassType
     * The listing_class_type
     */
    public void setListingClassType(Object listingClassType) {
        this.listingClassType = listingClassType;
    }

    /**
     *
     * @return
     * The status
     */
    public Object getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Object status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The forSale
     */
    public Object getForSale() {
        return forSale;
    }

    /**
     *
     * @param forSale
     * The for_sale
     */
    public void setForSale(Object forSale) {
        this.forSale = forSale;
    }

    /**
     *
     * @return
     * The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     *
     * @param lon
     * The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     *
     * @return
     * The address
     */
    public Object getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(Object address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The zip
     */
    public String getZip() {
        return zip;
    }

    /**
     *
     * @param zip
     * The zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     *
     * @return
     * The photos
     */
    public List<String> getPhotos() {
        return photos;
    }

    /**
     *
     * @param photos
     * The photos
     */
    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    /**
     *
     * @return
     * The thumbnails
     */
    public List<String> getThumbnails() {
        return thumbnails;
    }

    /**
     *
     * @param thumbnails
     * The thumbnails
     */
    public void setThumbnails(List<String> thumbnails) {
        this.thumbnails = thumbnails;
    }

    /**
     *
     * @return
     * The locationRating
     */
    public Double getLocationRating() {
        return locationRating;
    }

    /**
     *
     * @param locationRating
     * The location_rating
     */
    public void setLocationRating(Double locationRating) {
        this.locationRating = locationRating;
    }

    /**
     *
     * @return
     * The conditionRating
     */
    public Double getConditionRating() {
        return conditionRating;
    }

    /**
     *
     * @param conditionRating
     * The condition_rating
     */
    public void setConditionRating(Double conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     *
     * @return
     * The bathshalf
     */
    public Integer getBathshalf() {
        return bathshalf;
    }

    /**
     *
     * @param bathshalf
     * The bathshalf
     */
    public void setBathshalf(Integer bathshalf) {
        this.bathshalf = bathshalf;
    }

    /**
     *
     * @return
     * The fireplaces
     */
    public Integer getFireplaces() {
        return fireplaces;
    }

    /**
     *
     * @param fireplaces
     * The fireplaces
     */
    public void setFireplaces(Integer fireplaces) {
        this.fireplaces = fireplaces;
    }

    /**
     *
     * @return
     * The bedrooms
     */
    public Integer getBedrooms() {
        return bedrooms;
    }

    /**
     *
     * @param bedrooms
     * The bedrooms
     */
    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    /**
     *
     * @return
     * The optionalBedrooms
     */
    public Object getOptionalBedrooms() {
        return optionalBedrooms;
    }

    /**
     *
     * @param optionalBedrooms
     * The optional_bedrooms
     */
    public void setOptionalBedrooms(Object optionalBedrooms) {
        this.optionalBedrooms = optionalBedrooms;
    }

    /**
     *
     * @return
     * The baths
     */
    public Integer getBaths() {
        return baths;
    }

    /**
     *
     * @param baths
     * The baths
     */
    public void setBaths(Integer baths) {
        this.baths = baths;
    }

    /**
     *
     * @return
     * The acres
     */
    public Double getAcres() {
        return acres;
    }

    /**
     *
     * @param acres
     * The acres
     */
    public void setAcres(Double acres) {
        this.acres = acres;
    }

    /**
     *
     * @return
     * The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The listOnMarket
     */
    public String getListOnMarket() {
        return listOnMarket;
    }

    /**
     *
     * @param listOnMarket
     * The list_on_market
     */
    public void setListOnMarket(String listOnMarket) {
        this.listOnMarket = listOnMarket;
    }

    /**
     *
     * @return
     * The lastSoldPrice
     */
    public Object getLastSoldPrice() {
        return lastSoldPrice;
    }

    /**
     *
     * @param lastSoldPrice
     * The last_sold_price
     */
    public void setLastSoldPrice(Object lastSoldPrice) {
        this.lastSoldPrice = lastSoldPrice;
    }

    /**
     *
     * @return
     * The yearBuilt
     */
    public Integer getYearBuilt() {
        return yearBuilt;
    }

    /**
     *
     * @param yearBuilt
     * The year_built
     */
    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    /**
     *
     * @return
     * The recordedMortgage
     */
    public Double getRecordedMortgage() {
        return recordedMortgage;
    }

    /**
     *
     * @param recordedMortgage
     * The recorded_mortgage
     */
    public void setRecordedMortgage(Double recordedMortgage) {
        this.recordedMortgage = recordedMortgage;
    }

    /**
     *
     * @return
     * The assesedValue
     */
    public Double getAssesedValue() {
        return assesedValue;
    }

    /**
     *
     * @param assesedValue
     * The assesed_value
     */
    public void setAssesedValue(Double assesedValue) {
        this.assesedValue = assesedValue;
    }

    /**
     *
     * @return
     * The currentTaxes
     */
    public Double getCurrentTaxes() {
        return currentTaxes;
    }

    /**
     *
     * @param currentTaxes
     * The current_taxes
     */
    public void setCurrentTaxes(Double currentTaxes) {
        this.currentTaxes = currentTaxes;
    }

    /**
     *
     * @return
     * The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     *
     * @param remarks
     * The remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return
     * The homeOwnerFees
     */
    public Object getHomeOwnerFees() {
        return homeOwnerFees;
    }

    /**
     *
     * @param homeOwnerFees
     * The home_owner_fees
     */
    public void setHomeOwnerFees(Object homeOwnerFees) {
        this.homeOwnerFees = homeOwnerFees;
    }

    /**
     *
     * @return
     * The homeOwnerTotalFees
     */
    public Object getHomeOwnerTotalFees() {
        return homeOwnerTotalFees;
    }

    /**
     *
     * @param homeOwnerTotalFees
     * The home_owner_total_fees
     */
    public void setHomeOwnerTotalFees(Object homeOwnerTotalFees) {
        this.homeOwnerTotalFees = homeOwnerTotalFees;
    }

    /**
     *
     * @return
     * The otherFees
     */
    public Object getOtherFees() {
        return otherFees;
    }

    /**
     *
     * @param otherFees
     * The other_fees
     */
    public void setOtherFees(Object otherFees) {
        this.otherFees = otherFees;
    }

    /**
     *
     * @return
     * The melloRoosFee
     */
    public Object getMelloRoosFee() {
        return melloRoosFee;
    }

    /**
     *
     * @param melloRoosFee
     * The mello_roos_fee
     */
    public void setMelloRoosFee(Object melloRoosFee) {
        this.melloRoosFee = melloRoosFee;
    }

    /**
     *
     * @return
     * The bathsfull
     */
    public Object getBathsfull() {
        return bathsfull;
    }

    /**
     *
     * @param bathsfull
     * The bathsfull
     */
    public void setBathsfull(Object bathsfull) {
        this.bathsfull = bathsfull;
    }

    /**
     *
     * @return
     * The parkngNonGaragedSpaces
     */
    public Object getParkngNonGaragedSpaces() {
        return parkngNonGaragedSpaces;
    }

    /**
     *
     * @param parkngNonGaragedSpaces
     * The parkng_non_garaged_spaces
     */
    public void setParkngNonGaragedSpaces(Object parkngNonGaragedSpaces) {
        this.parkngNonGaragedSpaces = parkngNonGaragedSpaces;
    }

    /**
     *
     * @return
     * The estimatedSquareFeet
     */
    public Object getEstimatedSquareFeet() {
        return estimatedSquareFeet;
    }

    /**
     *
     * @param estimatedSquareFeet
     * The estimated_square_feet
     */
    public void setEstimatedSquareFeet(Object estimatedSquareFeet) {
        this.estimatedSquareFeet = estimatedSquareFeet;
    }

    /**
     *
     * @return
     * The lotSqftApprox
     */
    public Object getLotSqftApprox() {
        return lotSqftApprox;
    }

    /**
     *
     * @param lotSqftApprox
     * The lot_sqft_approx
     */
    public void setLotSqftApprox(Object lotSqftApprox) {
        this.lotSqftApprox = lotSqftApprox;
    }

    /**
     *
     * @return
     * The garage
     */
    public Object getGarage() {
        return garage;
    }

    /**
     *
     * @param garage
     * The garage
     */
    public void setGarage(Object garage) {
        this.garage = garage;
    }

    /**
     *
     * @return
     * The agentPhone
     */
    public Object getAgentPhone() {
        return agentPhone;
    }

    /**
     *
     * @param agentPhone
     * The agent_phone
     */
    public void setAgentPhone(Object agentPhone) {
        this.agentPhone = agentPhone;
    }

    /**
     *
     * @return
     * The parkingSpacesTotal
     */
    public Object getParkingSpacesTotal() {
        return parkingSpacesTotal;
    }

    /**
     *
     * @param parkingSpacesTotal
     * The parking_spaces_total
     */
    public void setParkingSpacesTotal(Object parkingSpacesTotal) {
        this.parkingSpacesTotal = parkingSpacesTotal;
    }

    /**
     *
     * @return
     * The monthlyTotalFees
     */
    public Object getMonthlyTotalFees() {
        return monthlyTotalFees;
    }

    /**
     *
     * @param monthlyTotalFees
     * The monthly_total_fees
     */
    public void setMonthlyTotalFees(Object monthlyTotalFees) {
        this.monthlyTotalFees = monthlyTotalFees;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The street
     */
    public Object getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    public void setStreet(Object street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The city
     */
    public Object getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(Object city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    public Object getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(Object state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The agentId
     */
    public Object getAgentId() {
        return agentId;
    }

    /**
     *
     * @param agentId
     * The agent_id
     */
    public void setAgentId(Object agentId) {
        this.agentId = agentId;
    }

    /**
     *
     * @return
     * The tourLink
     */
    public Object getTourLink() {
        return tourLink;
    }

    /**
     *
     * @param tourLink
     * The tour_link
     */
    public void setTourLink(Object tourLink) {
        this.tourLink = tourLink;
    }

    /**
     *
     * @return
     * The mlsName
     */
    public Object getMlsName() {
        return mlsName;
    }

    /**
     *
     * @param mlsName
     * The mls_name
     */
    public void setMlsName(Object mlsName) {
        this.mlsName = mlsName;
    }

    /**
     *
     * @return
     * The sysId
     */
    public Object getSysId() {
        return sysId;
    }

    /**
     *
     * @param sysId
     * The sys_id
     */
    public void setSysId(Object sysId) {
        this.sysId = sysId;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The upgrade1
     */
    public Object getUpgrade1() {
        return upgrade1;
    }

    /**
     *
     * @param upgrade1
     * The upgrade1
     */
    public void setUpgrade1(Object upgrade1) {
        this.upgrade1 = upgrade1;
    }

    /**
     *
     * @return
     * The upgrade2
     */
    public Object getUpgrade2() {
        return upgrade2;
    }

    /**
     *
     * @param upgrade2
     * The upgrade2
     */
    public void setUpgrade2(Object upgrade2) {
        this.upgrade2 = upgrade2;
    }

    /**
     *
     * @return
     * The upgrade3
     */
    public Object getUpgrade3() {
        return upgrade3;
    }

    /**
     *
     * @param upgrade3
     * The upgrade3
     */
    public void setUpgrade3(Object upgrade3) {
        this.upgrade3 = upgrade3;
    }

    /**
     *
     * @return
     * The upgrade4
     */
    public Object getUpgrade4() {
        return upgrade4;
    }

    /**
     *
     * @param upgrade4
     * The upgrade4
     */
    public void setUpgrade4(Object upgrade4) {
        this.upgrade4 = upgrade4;
    }

    /**
     *
     * @return
     * The upgrade1Pic
     */
    public Object getUpgrade1Pic() {
        return upgrade1Pic;
    }

    /**
     *
     * @param upgrade1Pic
     * The upgrade1_pic
     */
    public void setUpgrade1Pic(Object upgrade1Pic) {
        this.upgrade1Pic = upgrade1Pic;
    }

    /**
     *
     * @return
     * The upgrade2Pic
     */
    public Object getUpgrade2Pic() {
        return upgrade2Pic;
    }

    /**
     *
     * @param upgrade2Pic
     * The upgrade2_pic
     */
    public void setUpgrade2Pic(Object upgrade2Pic) {
        this.upgrade2Pic = upgrade2Pic;
    }

    /**
     *
     * @return
     * The upgrade3Pic
     */
    public Object getUpgrade3Pic() {
        return upgrade3Pic;
    }

    /**
     *
     * @param upgrade3Pic
     * The upgrade3_pic
     */
    public void setUpgrade3Pic(Object upgrade3Pic) {
        this.upgrade3Pic = upgrade3Pic;
    }

    /**
     *
     * @return
     * The upgrade4Pic
     */
    public Object getUpgrade4Pic() {
        return upgrade4Pic;
    }

    /**
     *
     * @param upgrade4Pic
     * The upgrade4_pic
     */
    public void setUpgrade4Pic(Object upgrade4Pic) {
        this.upgrade4Pic = upgrade4Pic;
    }

    /**
     *
     * @return
     * The greenFeature1
     */
    public Object getGreenFeature1() {
        return greenFeature1;
    }

    /**
     *
     * @param greenFeature1
     * The green_feature1
     */
    public void setGreenFeature1(Object greenFeature1) {
        this.greenFeature1 = greenFeature1;
    }

    /**
     *
     * @return
     * The greenFeature2
     */
    public Object getGreenFeature2() {
        return greenFeature2;
    }

    /**
     *
     * @param greenFeature2
     * The green_feature2
     */
    public void setGreenFeature2(Object greenFeature2) {
        this.greenFeature2 = greenFeature2;
    }

    /**
     *
     * @return
     * The greenFeature3
     */
    public Object getGreenFeature3() {
        return greenFeature3;
    }

    /**
     *
     * @param greenFeature3
     * The green_feature3
     */
    public void setGreenFeature3(Object greenFeature3) {
        this.greenFeature3 = greenFeature3;
    }

    /**
     *
     * @return
     * The greenFeature4
     */
    public Object getGreenFeature4() {
        return greenFeature4;
    }

    /**
     *
     * @param greenFeature4
     * The green_feature4
     */
    public void setGreenFeature4(Object greenFeature4) {
        this.greenFeature4 = greenFeature4;
    }

    /**
     *
     * @return
     * The greenFeature1Pic
     */
    public Object getGreenFeature1Pic() {
        return greenFeature1Pic;
    }

    /**
     *
     * @param greenFeature1Pic
     * The green_feature1_pic
     */
    public void setGreenFeature1Pic(Object greenFeature1Pic) {
        this.greenFeature1Pic = greenFeature1Pic;
    }

    /**
     *
     * @return
     * The greenFeature2Pic
     */
    public Object getGreenFeature2Pic() {
        return greenFeature2Pic;
    }

    /**
     *
     * @param greenFeature2Pic
     * The green_feature2_pic
     */
    public void setGreenFeature2Pic(Object greenFeature2Pic) {
        this.greenFeature2Pic = greenFeature2Pic;
    }

    /**
     *
     * @return
     * The greenFeature3Pic
     */
    public Object getGreenFeature3Pic() {
        return greenFeature3Pic;
    }

    /**
     *
     * @param greenFeature3Pic
     * The green_feature3_pic
     */
    public void setGreenFeature3Pic(Object greenFeature3Pic) {
        this.greenFeature3Pic = greenFeature3Pic;
    }

    /**
     *
     * @return
     * The greenFeature4Pic
     */
    public Object getGreenFeature4Pic() {
        return greenFeature4Pic;
    }

    /**
     *
     * @param greenFeature4Pic
     * The green_feature4_pic
     */
    public void setGreenFeature4Pic(Object greenFeature4Pic) {
        this.greenFeature4Pic = greenFeature4Pic;
    }

    /**
     *
     * @return
     * The favorited
     */
    public Boolean getFavorited() {
        return favorited;
    }

    /**
     *
     * @param favorited
     * The favorited
     */
    public void setFavorited(Boolean favorited) {
        this.favorited = favorited;
    }

    /**
     *
     * @return
     * The previousReviews
     */
    public List<PreviousReview> getPreviousReviews() {
        return previousReviews;
    }

    /**
     *
     * @param previousReviews
     * The previous_reviews
     */
    public void setPreviousReviews(List<PreviousReview> previousReviews) {
        this.previousReviews = previousReviews;
    }

    /**
     *
     * @return
     * The estimates
     */
    public List<Estimate> getEstimates() {
        return estimates;
    }

    /**
     *
     * @param estimates
     * The estimates
     */
    public void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }
}
