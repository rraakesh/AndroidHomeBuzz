package us.homebuzz.homebuzz.data.entity.pojo.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class InviteFriends {
    @SerializedName("success")
    @Expose
    private Boolean success;

    /**
     *
     * @return
     * The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     * The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
