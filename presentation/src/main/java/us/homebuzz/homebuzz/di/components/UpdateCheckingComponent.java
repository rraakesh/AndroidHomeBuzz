package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.UpdateCheckingModule;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;

/**
 * Created by amit.singh on 11/9/2015.
 */
@PerActivity
@Subcomponent( modules = {UpdateCheckingModule.class})
public interface UpdateCheckingComponent {
    void inject(UpdateCheckInEstimateFragment updateCheckInEstimateFragment);
}
