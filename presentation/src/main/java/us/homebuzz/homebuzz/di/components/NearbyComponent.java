package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInListFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInNearBy;
import us.homebuzz.homebuzz.view.fragment.NearBy.ListingFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.NearByDecorator;
import us.homebuzz.homebuzz.view.fragment.NearBy.NearbyFilterFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhoto;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhotoFragment;
import us.homebuzz.homebuzz.view.fragment.Notification.JustListedFragment;
import us.homebuzz.homebuzz.view.fragment.Notification.JustSoldListingFragment;

/**
 * Created by amit.singh on 10/13/2015.
 */
@PerActivity
@Subcomponent( modules = {NearbyModule.class})
public interface NearbyComponent {
    void inject(ListingPhotoFragment checkInNearBy);
    void inject(CheckInListFragment checkInNearBy);
    void inject(NearByDecorator nearByDecorator);
    void inject(ListingFragment listingFragment);
    void inject(CheckInNearby checkInNearby);
    void inject(NearbyFilterFragment nearbyFilterFragment);
    void inject(JustSoldListingFragment justSoldListingFragment);
    void inject(JustListedFragment justListedFragment);
    void inject(ListingPhoto listingPhoto);
}