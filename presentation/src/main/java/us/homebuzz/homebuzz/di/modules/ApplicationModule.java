/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.AndroidApplication;
import us.homebuzz.homebuzz.UIThread;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.data.Utility.NetworkStateManager;
import us.homebuzz.homebuzz.data.cache.UserCache;
import us.homebuzz.homebuzz.data.cache.UserCacheImpl;
import us.homebuzz.homebuzz.data.executor.JobExecutor;
import us.homebuzz.homebuzz.data.repository.UserDataRepository;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.repository.UserRepository;
import us.homebuzz.homebuzz.navigation.Navigator;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
  private final AndroidApplication application;

  public ApplicationModule(AndroidApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return this.application;
  }

  @Provides @Singleton Navigator provideNavigator() {
    return new Navigator();
  }

  @Provides @Singleton ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
    return jobExecutor;
  }

  @Provides @Singleton PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides @Singleton UserCache provideUserCache(UserCacheImpl userCache) {
    return userCache;
  }

  @Provides @Singleton UserRepository provideUserRepository(UserDataRepository userDataRepository) {
    return userDataRepository;
  }

  @Provides
  @Singleton
  SharedPreferences providePreferenceManager() {
    return PreferenceManager.getDefaultSharedPreferences(application);
  }
  @Provides
  @Singleton
  Constants provideConstants(){
    return new Constants();
  }
  @Provides
  @Singleton
  ConnectivityManager provideConnectivityManager() {
    return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
  }

  @Provides
  @Singleton
  GpsLocation provideGpsLocation() {
     return new GpsLocation(application.getBaseContext());
  }
  @Provides
  @Singleton
  Picasso providePicasso(){return Picasso.with(application);}
  @Provides
  @Singleton
    //Method parameter injected by Dagger2
  NetworkStateManager provideNetworkStateManager(ConnectivityManager connectivityManagerCompat) {
    return new NetworkStateManager(connectivityManagerCompat);
  }
}
