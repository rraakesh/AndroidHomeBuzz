package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.PreviousEstimateModule;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderPreviousEstimate;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderPreviousEstimateAccuracy;

/**
 * Created by amit.singh on 10/28/2015.
 */
@PerActivity
@Subcomponent( modules = {PreviousEstimateModule.class})
public interface PreviousEstimateComponent {
    void inject(LeaderPreviousEstimateAccuracy leaderPreviousEstimateAccuracy);
    void inject(LeaderPreviousEstimate leaderPreviousEstimate);
}
