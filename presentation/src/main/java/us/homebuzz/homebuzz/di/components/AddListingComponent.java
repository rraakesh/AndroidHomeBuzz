package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.view.fragment.addlisting.AddListing;
import us.homebuzz.homebuzz.view.fragment.addlisting._Nov.AddListingStep1;
import us.homebuzz.homebuzz.view.fragment.addlisting._Nov.AddListingStep2;

/**
 * Created by amit.singh on 11/5/2015.
 */
@PerActivity
@Subcomponent( modules = {AddListingModule.class})
public interface AddListingComponent {
    void inject(AddListing addListing);
    void inject(us.homebuzz.homebuzz.view.fragment.addlisting._Nov.AddListing addListing);
    void inject(AddListingStep1 addListingStep1);
    void inject(AddListingStep2 addListingStep2);
}
