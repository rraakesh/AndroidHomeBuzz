package us.homebuzz.homebuzz.di.components;

import dagger.Component;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.ActivityModule;
import us.homebuzz.homebuzz.di.modules.UserLoginModule;
import us.homebuzz.homebuzz.view.fragment.LoginFragment;
import us.homebuzz.homebuzz.view.fragment.SignUpFragment;

/**
 * Created by amit.singh on 10/8/2015.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UserLoginModule.class})
public interface UserLoginComponent extends ActivityComponent {
    void inject(LoginFragment userLoginFragment);
    void inject(SignUpFragment userSignUpFragment);

}
