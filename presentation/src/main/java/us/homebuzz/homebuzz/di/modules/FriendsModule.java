package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetUserFriendFollowers;
import us.homebuzz.homebuzz.domain.interactor.GetUserFriendfollow;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/28/2015.
 */
@Module
public class FriendsModule {
    public FriendsModule(){}
    @Provides
    @PerActivity
    @Named("FriendsFollowedList")
    UseCase provideGetfollowedListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUserFriendfollow(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("FriendsFollowersList")
    UseCase provideGetfollowersListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUserFriendFollowers(userRepository, threadExecutor, postExecutionThread);
    }
}
