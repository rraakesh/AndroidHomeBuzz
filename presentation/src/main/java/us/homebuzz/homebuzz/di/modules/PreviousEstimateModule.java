package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderPreviousEstimate;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/28/2015.
 */
@Module
public class PreviousEstimateModule {
    private int  user_id=-1;
    public PreviousEstimateModule(){}
    public PreviousEstimateModule(int user_id){
        this.user_id=user_id;
    }
    @Provides
    @PerActivity
    @Named("LeaderBoardPreviousEstimateList")
    GetLeaderPreviousEstimate provideGetPreviousEstimateListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        System.out.println("UserID" + this.user_id);
        return new GetLeaderPreviousEstimate(userRepository, threadExecutor, postExecutionThread);
    }
}
