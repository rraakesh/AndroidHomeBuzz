package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderByAccuracy;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderByEstimates;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/21/2015.
 */
@Module
public class LeaderBoardModule {
    public LeaderBoardModule(){}
    private int  user_id=-1;
    public LeaderBoardModule(int user_id){
        this.user_id=user_id;
    }
    //Provide the Data for the LeaderBoard Module
    @Provides
    @PerActivity
    @Named("LeaderBoardAccuracyList")
    GetLeaderByAccuracy provideGetAccuracyListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetLeaderByAccuracy(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("LeaderBoardEstimateList")
    GetLeaderByEstimates provideGetEstimateListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetLeaderByEstimates(userRepository, threadExecutor, postExecutionThread);
    }


}
