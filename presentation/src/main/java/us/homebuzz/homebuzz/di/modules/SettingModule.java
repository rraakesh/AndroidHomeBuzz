package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.EditProfilePic;
import us.homebuzz.homebuzz.domain.interactor.EditUserOtherProfile;
import us.homebuzz.homebuzz.domain.interactor.EditUserProfile;
import us.homebuzz.homebuzz.domain.interactor.GetFollowUnfollow;
import us.homebuzz.homebuzz.domain.interactor.GetInviteFriends;
import us.homebuzz.homebuzz.domain.interactor.GetSearchPeople;
import us.homebuzz.homebuzz.domain.interactor.GetSubscriptionInteractor;
import us.homebuzz.homebuzz.domain.interactor.GetUpdatedZipCode;
import us.homebuzz.homebuzz.domain.interactor.GetUserProfile;
import us.homebuzz.homebuzz.domain.interactor.GetZipCodeAdd;
import us.homebuzz.homebuzz.domain.interactor.GetZipCodePurchase;
import us.homebuzz.homebuzz.domain.interactor.GetZipDeleteInteractor;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/29/2015.
 */
@Module
public class SettingModule {
    public SettingModule() {
    }

    @Provides
    @PerActivity
    @Named("UserProfile")
    GetUserProfile provideGetUserProfile(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUserProfile(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("UserEditProfile")
    EditUserProfile provideGetUserEditProfile(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new EditUserProfile(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("UserOtherEditProfile")
    EditUserOtherProfile provideGetUserOtherEditProfile(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new EditUserOtherProfile(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("FollowUnfollow")
    GetFollowUnfollow provideFollowUnfollow(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetFollowUnfollow(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("InviteFriends")
    GetInviteFriends provideInviteFriends(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetInviteFriends(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("ZipCodePurchase")
    UseCase provideZipCodePurchseGet(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetZipCodePurchase(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("ProfilePicUpdate")
    EditProfilePic provideProfilePicUpdate(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new EditProfilePic(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("PeopleSearch")
    GetSearchPeople provideSearchPeople(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetSearchPeople(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("ZipCodeUpdate")
    GetUpdatedZipCode provideUpdateZipCode(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUpdatedZipCode(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("ZipCodeAdd")
    GetZipCodeAdd provideZipCodeAdd(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetZipCodeAdd(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("SubscriptionPlan")
    GetSubscriptionInteractor provideSubscriptionPlan(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetSubscriptionInteractor(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("DeleteZipCode")
    GetZipDeleteInteractor provideZipDelete(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetZipDeleteInteractor(userRepository, threadExecutor, postExecutionThread);
    }

}
