package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.view.fragment.leaderboard.AccuracyFragment;
import us.homebuzz.homebuzz.view.fragment.leaderboard.EstimateFragment;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoard;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoardDecorator;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoardFilterFragment;

/**
 * Created by amit.singh on 10/21/2015.
 */
@PerActivity
@Subcomponent( modules = {LeaderBoardModule.class})
public interface LeaderBoardComponent {
    void inject(LeaderBoard leaderBoard);

    void inject(LeaderBoardDecorator leaderBoardDecorator);

    void inject(AccuracyFragment accuracyFragment);

    void inject(EstimateFragment estimateFragment);
    void inject(LeaderBoardFilterFragment filterFragment);

}
