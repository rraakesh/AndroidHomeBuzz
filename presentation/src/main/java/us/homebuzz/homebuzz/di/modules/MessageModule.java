package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetAllMessage;
import us.homebuzz.homebuzz.domain.interactor.GetMessageReply;
import us.homebuzz.homebuzz.domain.interactor.GetUnreadMessage;
import us.homebuzz.homebuzz.domain.interactor.MarkMessageRead;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/22/2015.
 */
@Module
public class MessageModule {
    public MessageModule() {
    }

    //Provide Method for All Messages
    @Provides
    @PerActivity
    @Named("AllMessagesList")
    UseCase provideAllMessagesList(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetAllMessage(userRepository, threadExecutor, postExecutionThread);
    }

    //Provide Method for Reply Messages

    @Provides
    @PerActivity
    @Named("ReplyMessages")
    GetMessageReply provideReplyMessage(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetMessageReply(userRepository, threadExecutor, postExecutionThread);
    }


    @Provides
    @PerActivity
    @Named("UnreadMessages")
    UseCase provideUnreadMessage(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUnreadMessage(userRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    @PerActivity
    @Named("MarkreadMessages")
    MarkMessageRead provideMarkMessageRead(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new MarkMessageRead(userRepository, threadExecutor, postExecutionThread);
    }
}
