package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.EstimateListing;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.EstimateListingDetails;
import us.homebuzz.homebuzz.view.fragment.friends.BaseFriends;
import us.homebuzz.homebuzz.view.fragment.friends.FriendsDecorator;
import us.homebuzz.homebuzz.view.fragment.friends.FriendsFollowed;
import us.homebuzz.homebuzz.view.fragment.messages.selectFriends.SelectFriend;
import us.homebuzz.homebuzz.view.profile.MyReview;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendsFollowersList;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendsFollowingList;

/**
 * Created by amit.singh on 10/28/2015.
 */
@PerActivity
@Subcomponent( modules = {FriendsModule.class})
public interface FriendsComponent {
    void inject(FriendsDecorator friendsDecorator);
    void inject(BaseFriends baseFriends);
    void inject(FriendsFollowed friendsFollowed);
    void inject(FriendsFollowingList friendsFollowed);
    void inject(FriendsFollowersList friendsFollowersList);
    void inject(EstimateListing estimateListing);
    void inject(EstimateListingDetails estimateListingDetails);
    void inject(MyReview MyReview);
    void inject(SelectFriend MyReview);
}
