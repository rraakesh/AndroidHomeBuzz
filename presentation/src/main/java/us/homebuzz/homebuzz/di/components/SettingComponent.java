package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhoto;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.WriteReviewFragment;
import us.homebuzz.homebuzz.view.fragment.searchPeople.SearchPeople;
import us.homebuzz.homebuzz.view.fragment.setting.AutoLogout;
import us.homebuzz.homebuzz.view.fragment.setting.UnlimitedZipFragment;
import us.homebuzz.homebuzz.view.fragment.setting.UserProfileFragment;
import us.homebuzz.homebuzz.view.fragment.setting.UserSettingsFragment;
import us.homebuzz.homebuzz.view.fragment.setting.ZipCodeAddFragment;
import us.homebuzz.homebuzz.view.fragment.setting.ZipCodePuchaseFragment1;
import us.homebuzz.homebuzz.view.fragment.setting.ZipCodePurchaseFragment;
import us.homebuzz.homebuzz.view.fragment.setting.ZipCodeUpdateFragment;
import us.homebuzz.homebuzz.view.profile.EditAgentProfile;
import us.homebuzz.homebuzz.view.profile.EditBioProfile;
import us.homebuzz.homebuzz.view.profile.EditProfile;
import us.homebuzz.homebuzz.view.profile.EditSocialMediaProfile;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendFollowingFollowersProfileView;
import us.homebuzz.homebuzz.view.profile.friendsprofile.InviteFriendsFragment;

/**
 * Created by amit.singh on 10/29/2015.
 */
@PerActivity
@Subcomponent( modules = {SettingModule.class})
public interface SettingComponent {
    void inject(UserSettingsFragment userSettings);
    void inject(UserProfileFragment userProfile);
    void inject(EditAgentProfile editAgentProfile);
    void inject(FriendFollowingFollowersProfileView friendFollowingFollowersProfileView);
    void inject(EditProfile editProfile);
    void inject(EditSocialMediaProfile editSocialMediaProfile);
    void inject(EditBioProfile editBioProfile);
    void inject(AutoLogout autoLogout);
    void inject(ListingPhoto listingPhoto);
    void inject(WriteReviewFragment writeReviewFragment);
    void inject(ZipCodePurchaseFragment zipCodePurchaseFragment);
    void inject(ZipCodeUpdateFragment zipCodeUpdateFragment);
    void inject(ZipCodeAddFragment zipCodeAddFragment);
    void inject(InviteFriendsFragment inviteFriendsFragment);
    void inject(SearchPeople searchPeople);
    void inject(UnlimitedZipFragment searchPeople);
    void inject(ZipCodePuchaseFragment1 ZipCodePuchaseFragment1);
}
