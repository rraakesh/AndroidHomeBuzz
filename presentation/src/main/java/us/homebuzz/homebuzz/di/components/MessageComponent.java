package us.homebuzz.homebuzz.di.components;

import dagger.Subcomponent;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;
import us.homebuzz.homebuzz.view.fragment.Notification.UnReadMessageDetails;
import us.homebuzz.homebuzz.view.fragment.Notification.UnreadMessageFragment;
import us.homebuzz.homebuzz.view.fragment.messages.AllMessages;
import us.homebuzz.homebuzz.view.fragment.messages.CreateMessage;
import us.homebuzz.homebuzz.view.fragment.messages.Messages;
import us.homebuzz.homebuzz.view.fragment.messages.MessagesDecorator;
import us.homebuzz.homebuzz.view.fragment.messages.ReplyMessageFragment;
import us.homebuzz.homebuzz.view.fragment.messages.UnreadMessages;
import us.homebuzz.homebuzz.view.fragment.messages.selectFriends.NewSendMessage;
import us.homebuzz.homebuzz.view.fragment.messages.selectFriends.SelectFriend;

/**
 * Created by amit.singh on 10/22/2015.
 */
@PerActivity
@Subcomponent(modules = {MessageModule.class})
public interface MessageComponent {
    void inject(Messages messages);

    void inject(MessagesDecorator messagesDecorator);

    void inject(AllMessages allMessages);

    void inject(UnreadMessages unreadMessages);

    void inject(ReplyMessageFragment replyMessageFragment);

    void inject(CheckInNearby checkInNearby);
    void inject(UnreadMessageFragment unreadMessageFragment);
    void inject(CreateMessage createMessage);
    void inject(UnReadMessageDetails unReadMessageDetails);
    void inject(SelectFriend selectFriend);
    void inject(NewSendMessage selectFriend);
}
