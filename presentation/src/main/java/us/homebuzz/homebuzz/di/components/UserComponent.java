/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.di.components;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.ActivityModule;
import us.homebuzz.homebuzz.di.modules.UserLoginModule;
import us.homebuzz.homebuzz.di.modules.UserModule;
import us.homebuzz.homebuzz.view.fragment.LoginFragment;
import us.homebuzz.homebuzz.view.fragment.SignUpFragment;
import dagger.Component;

/**
 * A scope {@link us.homebuzz.homebuzz.di.PerActivity} component.
 * Injects user specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class,  UserLoginModule.class})
public interface UserComponent extends ActivityComponent {
  void inject(LoginFragment userLoginFragment);
  void inject(SignUpFragment userSignUpFragment);
}
