package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetAddListing;
import us.homebuzz.homebuzz.domain.interactor.GetAddListingPhoto;
import us.homebuzz.homebuzz.domain.interactor.GetOnBoardProp;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/5/2015.
 */
@Module
public class AddListingModule {
    public AddListingModule(){}

    @Provides
    @PerActivity
    @Named("AddListing")
    GetAddListing provideGetAddListingUseCase(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetAddListing(userRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    @PerActivity
    @Named("AddListingPhoto")
    GetAddListingPhoto provideGetAddListingPhotoUseCase(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetAddListingPhoto(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("GetProp")
    GetOnBoardProp provideGetOnBoardPropUseCase(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetOnBoardProp(userRepository, threadExecutor, postExecutionThread);
    }
}
