package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetAverageEstimate;
import us.homebuzz.homebuzz.domain.interactor.GetJustListedListing;
import us.homebuzz.homebuzz.domain.interactor.GetJustSoldListing;
import us.homebuzz.homebuzz.domain.interactor.GetNearbyListing;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/13/2015.
 */
@Module
public class NearbyModule {
    String lat, lon, radius;

    public NearbyModule() {
    }

    //Provide the Data for the Nearby Module
    @Provides
    @PerActivity
    @Named("nearbyListing")
    GetNearbyListing provideGetNearbyListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetNearbyListing(userRepository, threadExecutor, postExecutionThread);
    }

    //Provide the Data for the Just Listed Listing
    @Provides
    @PerActivity
    @Named("justListedListing")
    GetJustListedListing provideGetJustListedListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetJustListedListing(userRepository, threadExecutor, postExecutionThread);
    }

    //Provide the Data for the Just Sold Listing
    @Provides
    @PerActivity
    @Named("justSoldListing")
    GetJustSoldListing provideGetJustSoldListing(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetJustSoldListing(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("EstimateAverage")
    GetAverageEstimate provideGetEstimateAverage(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetAverageEstimate(userRepository, threadExecutor, postExecutionThread);
    }
}
