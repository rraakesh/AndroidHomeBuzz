package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.Utility.LoginDetails;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetUserDetailsLogin;
import us.homebuzz.homebuzz.domain.interactor.GetUserList;
import us.homebuzz.homebuzz.domain.interactor.GetUserSignUp;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 10/8/2015.
 */
@Module(

)
public class UserLoginModule {

    public UserLoginModule() {}

    @Provides
    @PerActivity
    @Named("LoginCredential")
    LoginDetails provideUserLoginDetails(){
        return new LoginDetails();
    }



    @Provides
    @PerActivity
    @Named("userList")
    UseCase provideGetUserListUseCase(
            GetUserList getUserList) {
        return getUserList;
    }

    @Provides @PerActivity @Named("userDetailsLogin") GetUserDetailsLogin provideGetUserDetailsUseCase(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUserDetailsLogin(userRepository, threadExecutor, postExecutionThread);
    }
    @Provides @PerActivity @Named("userRegistration")
    GetUserSignUp provideGetUserSignUpUseCase(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUserSignUp(userRepository, threadExecutor, postExecutionThread);
    }


}
