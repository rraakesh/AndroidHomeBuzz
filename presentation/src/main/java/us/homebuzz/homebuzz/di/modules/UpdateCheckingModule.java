package us.homebuzz.homebuzz.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.executor.PostExecutionThread;
import us.homebuzz.homebuzz.domain.executor.ThreadExecutor;
import us.homebuzz.homebuzz.domain.interactor.GetAddCheckInPhoto;
import us.homebuzz.homebuzz.domain.interactor.GetUpdateChecking;
import us.homebuzz.homebuzz.domain.repository.UserRepository;

/**
 * Created by amit.singh on 11/9/2015.
 */
@Module
public class UpdateCheckingModule {
    public UpdateCheckingModule(){}

    @Provides
    @PerActivity
    @Named("UpdateCheck_in")
    GetUpdateChecking provideGetUpdateChecking(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetUpdateChecking(userRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @PerActivity
    @Named("AddCheckInPhoto")
    GetAddCheckInPhoto provideGetCheckInPhoto(
            UserRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetAddCheckInPhoto(userRepository, threadExecutor, postExecutionThread);
    }
}
