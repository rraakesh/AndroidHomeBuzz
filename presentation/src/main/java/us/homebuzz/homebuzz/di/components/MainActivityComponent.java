package us.homebuzz.homebuzz.di.components;

import dagger.Component;
import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.di.modules.ActivityModule;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.di.modules.PreviousEstimateModule;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.di.modules.UpdateCheckingModule;
import us.homebuzz.homebuzz.view.fragment.LandingFragment;

/**
 * Created by amitsingh on 14/10/15.
 */
@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {ActivityModule.class, NearbyModule.class, AddListingModule.class,LeaderBoardModule.class, PreviousEstimateModule.class,MessageModule.class, FriendsModule.class,SettingModule.class, UpdateCheckingModule.class})
public interface MainActivityComponent extends ActivityComponent{
    void inject(LandingFragment landingFragment);
    NearbyComponent nearbyComponent(NearbyModule nearbyModule);
    AddListingComponent addListingComponent(AddListingModule addListingModule);
    LeaderBoardComponent leaderBoardComponent(LeaderBoardModule leaderBoardModule);
    PreviousEstimateComponent previousEstimateComponent(PreviousEstimateModule previousEstimateModule);
    MessageComponent messageComponent(MessageModule messageModule);
    FriendsComponent friendsComponent(FriendsModule friendsModule);
    SettingComponent settingComponent(SettingModule settingModule);
    UpdateCheckingComponent upaUpdateCheckingComponent(UpdateCheckingModule updateCheckingModule);
}
