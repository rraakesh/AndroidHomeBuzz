package us.homebuzz.homebuzz.Utility;

import us.homebuzz.homebuzz.R;

/**
 * Created by amit.singh on 9/21/2015.
 */
public class Constants {
    public final String KEY_EMAIL = "email";
    public final String AUTO_LOGOUT = "autoLogout";
    public final String FIRST_NAME = "first_name";
    public final String LAST_NAME = "last_name";
    public final String GENDER = "gender";
    public final String ACCURATE = "accurate";
    public final String ESTIMATE_COUNT = "estimatesCount";
    public final String DEVICE_ID = "device_id";
    public final String MOBILE_NO = "phone";
    public final String KEY_TOKEN = "token";
    public final String KEY_ROLE = "role";
    public final String KEY_NAME = "name";
    public final String KEY_ZIP = "zip_code";
    public final String KEY_OS_VERSION = "os_version";
    public final String KEY_OS_NAME = "os_name";
    public final String KEY_MODEL = "model";
    public final String FACEBOOK_TOKEN = "facebook_token";
    public final String LISTING_ZIP = "listing[zip]";
    public final String LISTING_REMARKS = "listing[remarks]";
    public final String LISTING_HALFBATH = "listing[bathshalf]";
    public final String LISTING_FIREPLACES = "listing[fireplaces]";
    public final String LISTING_PRICENUMBER = "listing[price]";
    public final String LISTING_PRICELNUMBER = "listing[parcel_num]";
    public final String LISTING_BEDROOMS = "listing[bedrooms]";
    public final String LISTING_ACERS = "listing[acres]";
    public final String LISTING_YEAR_BUILT = "listing[year_built]";
    public final String LISTING_FULL_BATH = "listing[baths]";
    public final String LISTING_STATUS = "listing[status]";
    public final String LISTING_LON = "listing[lon]";
    public final String LISTING_LAT = "listing[lat]";
    public final String LISTING_PHOTO = "listing[photo]";
    public final String LISTING_LAST_SOLD_PRICE = "listing[last_sold_price]";
    public final String LISTING_RECOREDED_MORTAGE = "listing[recorded_mortgage]";
    public final String LISTING_ASSESED_VALUE = "listing[assesed_value]";
    public final String LISTING_CUREENT_TAXES = "listing[current_taxes]";
    public final String LISTING_HOME_OWNER_FEES = "listing[home_owner_fees]";
    public final String LISTING_HOME_OWNER_TOTAL_FEES = "listing[home_owner_total_fees]";
    public final String LISTING_OTHER_FEES = "listing[other_fees]";
    public final String LISTING_PARKING = "listing[parkng_non_garaged_spaces]";
    public final String LISTING_ESTIMATED_SQUARE_FEET = "listing[estimated_square_feet]";
    public final String LISTING_LOT_SQFT = "listing[lot_sqft_approx]";
    public final String LISTING_GARAGE = "listing[garage]";
    public final String LISTING_ADDRESS = "listing[address]";
    public final String LISTING_PARKING_SAPCE = "listing[parking_spaces_total]";
    public final String LISTING_MONTHLY_TOTAL_FEES = "listing[monthly_total_fees]";
    public final String LISTING_CONDITION_RATING = "listing[condition_rating]";
    public final String LISTING_OPTIONAL_BEDROOMS = "listing[optional_bedrooms]";
    public final String LISTING_FOR_SALE = "listing[for_sale]";
    public final String LISTING_REQUEST_ESTIMATE = "listing[request_estimate]";

    //for estimate a listing property
    public final String ESTIMATE_LISTING_ID = "estimate[listing_id]";
    public final String ESTIMATE_LISTIING_CONDITION_RATING = "estimate[condition_rating]";
    public final String ESTIMATE_LISTING_PRICE = "estimate[price]";
    public final String ESTIMATE_LISTING_REASON = "estimate[reason]";
    public final String ESTIMATE_LISTING_LOCATION_RATING = "estimate[location_rating]";

    //for uploading the photo in listing property
    public final String ESTIMATE_PHOTO_LISTING_ID = "listing_id";
    public final String ESTIMATE_PHOTO_ESTIMATE_ID = "estimate_id";
    public final String ESTIMATE_PHOTO_ID = "id";
    public final String ESTIMATE_PHOTO = "photo";

    public final String _LISTING_CITY = "listing[city]";
    public final String _LISTING_STATE = "listing[state]";
    public final String _LISTING_STREET = "listing[street]";
    public final String _LISTING_ZIP = "listing[zip]";
    public final String _LISTING_SUBMIT = "listing[submit]";
    public final String MESSAGE_BODY = "body";
    public final String SENDER_ID = "user_id";
    public final String MESSAGE = "message";
    public final String PHONE_NUMBER = "phone";
    //    public final String KEY_FBID = "fb_id";
    public final String KEY_FBID = "id";
    public final String KEY_VERIFY_CODE = "v_code";
    public final String KEY_PHONE = "phone";
    public final String KEY_PASS = "password";
    public final String KEY_CONFIRM_PASS = "password_confirmation";
    public final String KEY_ID = "id";
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public final String KEY_LISTING_CLASS = "listing[listing_class]";
    public final String KEY_LISTING_CLASS_TYPE = "listing[listing_class_type]";

    public final String KEY_ACCURACY = "accuracy";
    public final String KEY_ESTIMATE = "estimate";
    public static int PICK_IMAGE_REQUEST_LIB = 200;
    public static int PICK_IMAGE_REQUEST_CAMERA = 201;
    public static final String IS_LOGIN_FB = "IsLoggedInFB";
    public static final int KEY_VIEW_POSITION = 0;
    public final String KEY_IS_LOGIN = "is_login";
    public final String KEY_IS_AUTO_LOGOUT = "is_auto_logout";
    public final String _LAT = "lat";
    public final String _LON = "lon";
    public final String _RADIUS = "radius";
    public float RADIUS = 0.0f;
    public final String FB_TOKEN = "facebook_token";
    /*User Edit profile parameters*/
    public final String KEY_USERID = "user[user_id]";
    public final String KEY_FIRSTNAME = "user[first_name]";
    public final String KEY_LASTNAME = "user[last_name]";
    public final String KEY_EMAILID = "user[email]";
    public final String KEY_ZIPCODE = "user[zip_code]";
    public final String KEY_PHONENO = "user[phone]";
    public final String KEY_ROLE_TYPE = "user[role]";
    public final String KEY_PCOLLEGE = "user[college]";

    public final String KEY_PUSERID = "profile[user_id]";
    public final String KEY_PFIRSTNAME = "profile[first_name]";
    public final String KEY_PLASTNAME = "profile[last_name]";
    public final String KEY_PEMAILID = "profile[email]";
    public final String KEY_PZIPCODE = "profile[zip_code]";
    public final String KEY_PPHONENO = "profile[phone]";
    public final String KEY_PROLE_TYPE = "profile[role]";

    // public final String KEY_USERID1 = "profile[user_id]";
    public final String KEY_STREET = "profile[street]";
    public final String KEY_CITY = "profile[city]";
    public final String KEY_STATE = "profile[state]";
    public final String KEY_FACEBOOK_URL = "profile[facebook_url]";
    public final String KEY_TWITTER_URL = "profile[twitter_url]";
    public final String KEY_LINKED_URL = "profile[linked_in]";
    public final String KEY_BIO = "profile[bio]";
    public final String KEY_COLLEGE = "profile[college]";

    public final String KEY_DEGREE = "profile[degree]";
    public final String KEY_YR_GRADUATED = "profile[yr_graduated]";
    public final String KEY_OTHER_SCHOOL = "profile[other_school]";
    public final String KEY_DESIGNATION = "profile[designations]";
    public final String KEY_MLS_NAME = "profile[mls_name]";
    public final String KEY_LICENSE_NO = "profile[license_num]";
    public final String KEY_AGENT_ID = "profile[agent_id]";
    public final String KEY_NAP_NO = "profile[nap_num]";
    public final String KEY_PROFILE_PIC = "profile[profile_pic]";
    public final String KEY_PROFILE_ID = "profile[profile_id]";
    public final String KEY_PPROFILE_ID = "profile_id";
    public final String KEY_PPROFILE_PHOTO = "photo";
    public final String KEY_PROFILE_WEBSITE = "profile[website]";

    public final String EXTRA_ERROR = "HomeBuzz_Error";
    public final String CATEGORY_SEARCH = "Location_Search";
    public final String ACTION_SEARCH = "Action_Search";
    public static final String RESPONSE_LAT = "Lat_Search";
    public static final String RESPONSE_LON = "Lon_Search";
    public static final String LEADERBOARD_FILTER_LIMIT = "filter[limit]";
    public static final String LEADERBOARD_FILTER_OFFICE = "filter[office]";
    public static final String LEADERBOARD_FILTER_TEAM = "filter[team]";
    public static final String LEADERBOARD_FILTER_ZIP_CODE = "filter[zip_code]";

    public static final String ADD_NEW_ZIP = "zip_code";
    public static final String UPDATE_ZIP = "extra_zip[zip_code]";

    public static final String SEARCH_CHOICE = "choices";
    public static final String SEARCH_EMAIL = "email";
    public static final String SEARCH_ID = "id";
    public static final String SEARCH_NAME = "name";
    public static final int REQUEST_CHECK_SETTINGS = 500;

    public final String MESSAGE_ID = "message_id";

    public static int[] backGroundImages = {R.drawable.regsign, R.drawable.bg2, R.drawable.bg3, R.drawable.bg4};
}
