package us.homebuzz.homebuzz.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import javax.inject.Inject;



public class GpsLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int REQUEST_CHECK_SETTINGS = 500;
    Context _context;
    Location latKnowLocation;
    GoogleApiClient mGoogleApiClient;
   private LocationInterface loIn;
    PendingResult<LocationSettingsResult> result;
    LocationSettingsRequest.Builder builder;
    Activity activity;
    @Inject
    public GpsLocation(Context con) {
        _context = con;
       /* this.initializeLocation();*/
    }

    public void initializeLocation(Activity activity) {
        this.activity=activity;
        //Location requests
        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(10 * 60 * 1000) // every 10 minutes
              // .setExpirationDuration(10 * 1000) // After 10 seconds
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(1 * 60 * 1000);//Fastest InterVal is Set To 1 Min

        mGoogleApiClient = new GoogleApiClient.Builder(_context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
        //initialize the builder and add location request paramenter like HIGH Aurracy
        builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        // set builder to always true (Shows the dialog after never operation too)
        builder.setAlwaysShow(true);
        // Then check whether current location settings are satisfied:
        result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
    }

    // call back for other class
    public void callbacksResults() {
        // Then check whether current location settings are satisfied:
        result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        // call backs for lcoation status
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        if (mGoogleApiClient.isConnected()) {
                            latKnowLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            setLocation(latKnowLocation);
                            if (latKnowLocation != null)
                                loIn.onLocation(latKnowLocation);
                            else
                                loIn.onLocationFailed();
                        }
                        System.out.println("Location in Gps Location"+loIn);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.

                        try {
                            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            if (latKnowLocation == null)
                                loIn.onLocationFailed();
                            else
                                loIn.onLocation(latKnowLocation);
                        } catch (IntentSender.SendIntentException e) {

                            e.printStackTrace();
                        }

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {


    }

    // initialize the location listener
    public void setonLocationListener(LocationInterface locationInterface) {
        loIn = locationInterface;
    }


    @Override
    public void onConnectionSuspended(int i) {

        loIn.onConnectionSuspended();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        loIn.onLocationFailed();
    }

    // method to discoonect GoogleApi client
    public void disconnect() {
        loIn=null;
        mGoogleApiClient.disconnect();
        mGoogleApiClient = null;
    }

    //setlocation
    public void setLocation(Location loc) {
        this.latKnowLocation = loc;
    }

    //get location
    public Location getLatKnowLocation() {

        return latKnowLocation;
    }

}
