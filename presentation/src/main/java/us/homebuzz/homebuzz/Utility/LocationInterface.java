package us.homebuzz.homebuzz.Utility;

import android.location.Location;

/**
 * Created by devesh.bhandari on 4/28/2015.
 */
public interface LocationInterface {

    public void onLocation(Location location);
    public void onLocationFailed();
    public void onConnectionSuspended();
}
