package us.homebuzz.homebuzz.Utility;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import us.homebuzz.homebuzz.R;

/**
 * Created by amit.singh on 11/6/2015.
 */
@SuppressWarnings("ALL")
public class HomeBuzzToolbar extends AppBarLayout {

    private ImageView icon_Home,iconRight;
    private TextView app_home_text,app_title,app_right_title;
    private Context context;
    public HomeBuzzToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.homebuzz_toolbar, this, true);
        icon_Home=(ImageView)this.getChildAt(0).findViewById(R.id.icon_Home);
        iconRight=(ImageView)this.getChildAt(0).findViewById(R.id.iconRight);
        app_home_text=(TextView)this.getChildAt(0).findViewById(R.id.app_home_text);
        app_title=(TextView)this.getChildAt(0).findViewById(R.id.app_title);
        app_right_title=(TextView)this.getChildAt(0).findViewById(R.id.app_right_title);
    }

    public HomeBuzzToolbar(Context context) {
        this(context, null);
        this.context=context;
    }

    public void restToolBar(){
        icon_Home.setVisibility(View.VISIBLE);
        iconRight.setVisibility(View.VISIBLE);
        app_home_text.setVisibility(View.GONE);
        app_right_title.setVisibility(View.GONE);
        app_title.setText("HomeBuzz");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.changeIconRight(this.getResources().getDrawable(R.drawable.search_icon, this.getContext().getTheme()));
        } else {
            this.changeIconRight(this.getResources().getDrawable(R.drawable.search_icon));
        }
    }

    public void setIcon_HomeVisible(boolean visible) {
        icon_Home.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
    public void setIconRightVisible(boolean visible) {
        iconRight.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
    public TextView getAppTitleText(){
        return app_title;
    }
    public void setTextRightVisible(boolean visible) {
        app_right_title.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
    public void changeIconRight(Drawable rightIcon) {
        iconRight.setImageDrawable(rightIcon);
    }
    /*public void seticon_HomeVisible(boolean visible) {
        icon_Home.setVisibility(visible ? View.VISIBLE : View.GONE);
    }*/
}
