package us.homebuzz.homebuzz.Utility;

/**
 * Created by amitsingh on 11/10/15.
 */
public class LoginDetails {
    String email,password,token;
    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The name
     */
    public String getPassword() {
        return password;
    }
    /**
     *
     * @return
     * The name
     */
    public String getToken() {
        return token;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
