package us.homebuzz.homebuzz;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import butterknife.ButterKnife;
import us.homebuzz.homebuzz.view.activity.BaseActivity;

/**
 * Created by amit.singh on 9/21/2015.
 */
public class Splash extends BaseActivity {

    private static int SPLASH_TIME_OUT = 2000;
    public static String TAG = Splash.class.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      //  requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        //Request for the Location Or initialize it if it was provided before
        movetoActivity();

    }

     private void movetoActivity(){
        Log.d(TAG, "User is login" + sharedPreferences.getBoolean(constants.KEY_IS_LOGIN, false));
        // splash handler
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // check for location
                if (sharedPreferences.getBoolean(constants.KEY_IS_LOGIN,false) == true) {
                    //If user is logged in Start the main Activity
                    navigator.navigateToMainActivity(Splash.this);
                } else {
                    navigator.navigateToLoginActivity(Splash.this);

                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
