package us.homebuzz.homebuzz.view.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import us.homebuzz.homebuzz.AndroidApplication;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.di.components.ApplicationComponent;
import us.homebuzz.homebuzz.di.modules.ActivityModule;
import us.homebuzz.homebuzz.navigation.Navigator;
import us.homebuzz.homebuzz.view.activity.LoginSignUp.Login;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;


/**
 * Base {@link android.app.Activity} class for every Activity in this application.
 */
public abstract class BaseActivity extends AppCompatActivity implements ReplaceFragment, FragmentManager.OnBackStackChangedListener {
    @Inject
    protected Navigator navigator;
    @Inject
    protected SharedPreferences sharedPreferences;
    @Inject
    protected Constants constants;
    @Inject
    protected GpsLocation gpsLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //Handle when activity is recreated like on orientation Change
       // shouldDisplayHomeUp();
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    protected void addFragment(int containerViewId, Fragment fragment) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    protected void replaceFragment(int containerViewId, Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        android.support.v4.app.FragmentManager manager = this.getSupportFragmentManager();
        boolean fragmentPopped = manager
                .popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            // fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            ft.replace(containerViewId, fragment, fragmentTag);
            ft.addToBackStack(backStateName);
           ft.commit();
        }
    }

    /**
     * Call this method from Fragment To replace Fragment
     *
     * @param containerViewId
     * @param mFragment
     */
    @Override
    public void CallReplaceFragment(int containerViewId, Fragment mFragment) {
        replaceFragment(containerViewId, mFragment);
    }

    /**
     * Call this method from Fragment To Navigate to Activity
     *
     * @param mActivity
     */
    @Override
    public void CallNavigateToActivity(AppCompatActivity mActivity) {
       if (mActivity instanceof Login) {
            this.navigator.navigateToLoginActivity(this);
       }
    }

    /**
     * Get the Main Application component for dependency injection.
     *
     * @return {@link us.homebuzz.homebuzz.di.components.ApplicationComponent}
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((AndroidApplication) getApplication()).getApplicationComponent();
    }

    /**
     * Get an Activity module for dependency injection.
     *
     * @return {@link us.homebuzz.homebuzz.di.modules.ActivityModule}
     */
    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    @Override
    public void onBackStackChanged() {
     //   shouldDisplayHomeUp();
    }

    @Override
    public void onBackPressed() {
        if (this.getSupportFragmentManager().getBackStackEntryCount() == 1)
            this.finish();
        else {
            super.onBackPressed();
        }
      /*// if there is a fragment and the back stack of this fragment is not empty,
      // then emulate 'onBackPressed' behaviour, because in default, it is not working
      FragmentManager fm = getSupportFragmentManager();
      for (Fragment frag : fm.getFragments()) {
          if (frag.isVisible()) {
              FragmentManager childFm = frag.getChildFragmentManager();
              if (childFm.getBackStackEntryCount() > 0) {
                  childFm.popBackStack();
                  return;
              }
          }
      }
      super.onBackPressed();*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        return true;
    }

    public void shouldDisplayHomeUp() {
        if (this.getSupportActionBar() != null) {
            //Enable Up button only  if there are entries in the back stack
            boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 1;
           // getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
            getSupportActionBar().setDisplayShowHomeEnabled(canback);
        }
    }


}
