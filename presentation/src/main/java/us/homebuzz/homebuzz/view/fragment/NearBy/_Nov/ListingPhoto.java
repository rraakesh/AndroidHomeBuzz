package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.adapter.nearby.ViewPagerAdapter;

/**
 * Created by rakesh.kumar on 11/24/2015.
 */
public class ListingPhoto extends android.support.v4.app.DialogFragment {
    private static final String TAG = ListingPhoto.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_NEARBY_LIST_MODEL = "userNearbyModel";
    public UserNearbyModel userNearbyModel;

    public static ListingPhoto newInstance(UserNearbyModel userAccuracyModel) {
        ListingPhoto listingPhoto = new ListingPhoto();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL, userAccuracyModel);
        listingPhoto.setArguments(argumentsBundle);
        return listingPhoto;
    }
    public ViewPagerAdapter viewPagerAdapter;
    @Bind(R.id.pager)
    ViewPager viewpager;
    @Bind(R.id.indicator)
    CirclePageIndicator mIndicator;
    Dialog dialog;
    @Inject
    Picasso picasso;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.listing_photo);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL) != null) {
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL);
            Log.d(TAG, "UserModel in Photo Previous" + userNearbyModel.getId());
        }
       /* final Drawable d = new ColorDrawable(Color.argb(140, 223, 222, 220));
        dialog.getWindow().setBackgroundDrawable(d);*/
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        float bottomMapPadding= 100 * getResources().getDisplayMetrics().density;
        params.width = WindowManager.LayoutParams.MATCH_PARENT/*-(int)bottomMapPadding*/;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT/*-(int)bottomMapPadding*/;
        params.y = (int)bottomMapPadding;
        dialog.setCanceledOnTouchOutside(true);
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        return dialog;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
    }

    private void setupUI() {
        viewPagerAdapter = new ViewPagerAdapter(getActivity(), (ArrayList<String>) userNearbyModel.getPhotos(), picasso,true);
        viewpager.setAdapter(viewPagerAdapter);
        System.out.println("Image" + String.valueOf(userNearbyModel.getPhotos().size()));

        if (userNearbyModel.getPhotos().size() > 0) {
            mIndicator.setViewPager(viewpager);
        }

    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}

