package us.homebuzz.homebuzz.view.fragment.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.presenter.MessageReplyPresenter;
import us.homebuzz.homebuzz.view.MessageReplyView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rohitkumar on 30/10/15.
 */
public class ReplyMessageFragment extends BaseFragment implements MessageReplyView {
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.txtAppName)
    TextView txtAppName;
    @Bind(R.id.txtFollow)
    TextView txtFollow;
    @Bind(R.id.reply_send)
    TextView reply_send;
    @Bind(R.id.delete_cancel)
    TextView delete_cancel;
    @Bind(R.id.txtBackMsg)
    TextView txtBackMsg;
    @Bind(R.id.edbody)
    EditText edbody;
    @Bind(R.id.llFollow)
    LinearLayout llFollow;
//    @Bind(R.id.rl_progress)
//    RelativeLayout rl_progress;

    @Inject
    Constants constants;
    @Inject
    MessageReplyPresenter messageReplyPresenter;

    private UserAllMessagesModel userAllMessagesModel;
    View view;
    private static final String ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL = "UserAllMessagesModel";

    public static ReplyMessageFragment newInstance(UserAllMessagesModel userAllMessagesModel) {
        ReplyMessageFragment replyMessageFragment = new ReplyMessageFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL, userAllMessagesModel);
        replyMessageFragment.setArguments(argumentsBundle);
        return replyMessageFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view_leader_previous = inflater.inflate(R.layout.read_and_reply_message, container, false);
        ButterKnife.bind(this, view_leader_previous);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL) != null) {
            this.userAllMessagesModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL);
            Log.e(TAG, "UserModel in Leader Previous" + userAllMessagesModel.getId());
            Log.e(TAG, "Sender id" + userAllMessagesModel.getSender().getId());
        }
        return view_leader_previous;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupUI();
        this.initialize();

    }

    private void setupUI() {
        //show_message.setText(userAllMessagesModel.getBody());
        Log.d(TAG, "Outside the Toolbar");

    }

    /*@OnClick(R.id.send_reply_text)
    public void sendMessage() {
        if (!TextUtils.isEmpty(replyMessage.getText().toString())) {
            WeakHashMap<String, String> replyMessageData = new WeakHashMap<>();
            replyMessageData.put(constants.MESSAGE_BODY, replyMessage.getText().toString());
            replyMessageData.put(constants.SENDER_ID, String.valueOf(userAllMessagesModel.getSender().getId()));
            this.messageReplyPresenter.initialize(replyMessageData);
        } else {
            super.showToastMessage("PLease enter your message");
        }

    }*/

    @Override
    public void onResume() {
        super.onResume();
        this.messageReplyPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.messageReplyPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.messageReplyPresenter.destroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        this.messageReplyPresenter.setView(this);
        // Log.d(TAG, "Check for the SharedPreference in Login" + sharedPreferences);
    }

    @Override
    public void renderAfterMessageReply(UserMessageReplyModel userMessageReplyModel) {

    }

    @Override
    public void showLoading() {
        //this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        //this.rl_progress.setVisibility(View.GONE);
        getActivity().onBackPressed();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }
}
