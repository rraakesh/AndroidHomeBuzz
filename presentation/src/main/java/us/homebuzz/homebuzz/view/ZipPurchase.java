package us.homebuzz.homebuzz.view;

/**
 * Created by amit.singh on 12/15/2015.
 */
public interface ZipPurchase {
    void onZipCodePurchaseSuccess(boolean isSuccesss);
    void onPurchaseIdReceived(String orderId);
}
