package us.homebuzz.homebuzz.view.profile.friendsprofile;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.presenter.FollowUnfollowPresenter;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.view.FollowUnfollowView;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.messages.CreateMessage;

/**
 * Created by abhishek.singh on 11/18/2015.
 */
public class FriendFollowingFollowersProfileView extends BaseFragment implements UserProfileView, FollowUnfollowView {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";
    private static Integer id;


    public static FriendFollowingFollowersProfileView newInstance(String id) {
        FriendFollowingFollowersProfileView userProfileFragment = new FriendFollowingFollowersProfileView();
        FriendFollowingFollowersProfileView.id = Integer.valueOf(id);
        return userProfileFragment;
    }

    private UserProfileModel userProfileModel;
    @Inject
    UserProfilePresenter userProfilePresenter;
    @Inject
    FollowUnfollowPresenter followUnfollowPresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.txtFollow)
    TextView txtFollow;
    @Bind(R.id.follow_image)
    ImageView follow_image;
    @Bind(R.id.btnMessage)
    LinearLayout btnMessage;
    @Bind(R.id.btnFollowUnFollow)
    LinearLayout btnFollowUnFollow;

    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.txtEmail)
    TextView txtEmail;
    @Bind(R.id.txtPhone)
    TextView txtPhone;

    @Bind(R.id.check_in_val)
    TextView check_in_val;
    @Bind(R.id.Review_val)
    TextView Review_val;
    @Bind(R.id.photos_val)
    TextView photos_val;
    @Bind(R.id.BuzzPoint_val)
    TextView BuzzPoint_val;
    @Bind(R.id.Accuracy_val)
    TextView Accuracy_val;
    @Bind(R.id.estimate_val)
    TextView estimate_val;
    @Inject
    Picasso picasso;
    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    private boolean isProfileExist;
    String number;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View user_settings = inflater.inflate(R.layout.friends_following_profile_view, container, false);
        ButterKnife.bind(FriendFollowingFollowersProfileView.this, user_settings);
        return user_settings;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText(String.valueOf(userProfileModel.getName()));
        initialize();
        userProfilePresenter.setView(this);
        followUnfollowPresenter.setView(this);
        Log.e("User_id", String.valueOf(id));
        userProfilePresenter.initialize(id);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showError(String message) {
        showToastMessage("Can't load profile");
        btnMessage.setClickable(false);
        btnFollowUnFollow.setClickable(false);
        isProfileExist = false;
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        btnMessage.setClickable(false);
        btnFollowUnFollow.setClickable(false);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        btnMessage.setClickable(true);
        btnFollowUnFollow.setClickable(true);

    }

    @SuppressLint("NewApi")
    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        this.userProfileModel = userProfileModel;
        isProfileExist = true;
        txtProfileName.setText(userProfileModel.getName());

        if (userProfileModel.getName() != null) {

            ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText(String.valueOf(userProfileModel.getName()));
        } else {
            ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        }
        if (userProfileModel.getEmail() != null) {
            txtEmail.setVisibility(View.VISIBLE);
            txtEmail.setText(userProfileModel.getEmail());
        } else {
            txtEmail.setVisibility(View.GONE);
        }
        if (userProfileModel.getPhone() != null) {
            txtPhone.setVisibility(View.VISIBLE);
            txtPhone.setText(PhoneNumberUtils.formatNumber(userProfileModel.getPhone(), "US"));
        } else {
            txtPhone.setVisibility(View.GONE);
        }
        Accuracy_val.setText(userProfileModel.getAccurate() != null && userProfileModel.getAccurate() != 0 ? df.format(userProfileModel.getAccurate() * 100) + " %" : "0.0" + "%");
        estimate_val.setText(userProfileModel.getEstimatesCount() != null ? userProfileModel.getEstimatesCount().toString() : "0");
        photos_val.setText(userProfileModel.getPhotos() != null ? userProfileModel.getPhotos().toString() : "0");
        BuzzPoint_val.setText(userProfileModel.getPoints() != null ? userProfileModel.getPoints().toString() : "0");

        if (userProfileModel.getFollowed()) {
            btnFollowUnFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            txtFollow.setText("Un-Follow");
            follow_image.setImageResource(R.drawable.logout_new);
        } else {
            btnFollowUnFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
            follow_image.setImageResource(R.drawable.follow_icon);
        }


        // // TODO: 11/17/2015 find  the review and checkIn value and fix it..
        check_in_val.setText(userProfileModel.getCheckinCount() != null ? userProfileModel.getCheckinCount().toString() : "0");
        Review_val.setText(userProfileModel.getReviewsCount() != null ? userProfileModel.getReviewsCount().toString() : "0");

        System.out.println("check_in" + userProfileModel.getCheckinCount());
        System.out.println("estmate" + userProfileModel.getEstimatesCount());


        if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().equals("null"))
            picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    @OnClick(R.id.txtPhone)
    public void onClickPhone() {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse(number.toString()));
//        startActivity(intent);

    }

    @OnClick(R.id.btnMessage)
    public void onClickMessage() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, CreateMessage.newInstance(userProfileModel));
    }

    @OnClick(R.id.btnFollowUnFollow)
    public void btnFollowUnFollow() {
        followUnfollowPresenter.initialize(userProfileModel.getId());
    }

    @Override
    public void viewUser(FollowUnfollowModel FollowUnfollowModel) {

    }

    @Override
    public void renderFollowUnfollow(FollowUnfollowModel FollowUnfollowModel) {
        if (FollowUnfollowModel.getFollowed()) {
            btnFollowUnFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            userProfileModel.setFollowed(true);
            txtFollow.setText("Un-Follow");
            follow_image.setImageResource(R.drawable.logout_new);
        } else {
            btnFollowUnFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
            userProfileModel.setFollowed(false);
            follow_image.setImageResource(R.drawable.follow_icon);
        }
    }

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Leaderboard");
        super.onDestroyView();

    }
}

