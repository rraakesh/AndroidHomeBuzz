package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.SupportMapFragment;

import java.util.Collection;

import butterknife.Bind;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.UserNearbyView;

/**
 * Created by amit.singh on 10/5/2015.
 */
public class CheckInMapFragment extends SupportMapFragment {

    private static final String TAG = CheckInMapFragment.class.getSimpleName();

    public CheckInMapFragment() {
        super();

    }

    public static CheckInMapFragment newInstance() {
        CheckInMapFragment fragment = new CheckInMapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater arg0, ViewGroup arg1, Bundle arg2) {
        View v = super.onCreateView(arg0, arg1, arg2);



        return v;
    }


   /* *//**
     * Listener interface to tell when the map is ready
     *//*
    public interface OnMapReadyListener {

        void onMapReady();
    }
*/

}
