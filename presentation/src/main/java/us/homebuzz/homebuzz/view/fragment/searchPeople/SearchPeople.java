package us.homebuzz.homebuzz.view.fragment.searchPeople;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.SearchPeopleModel;
import us.homebuzz.homebuzz.presenter.SearchPeoplePresenter;
import us.homebuzz.homebuzz.view.SearchPeopleView;
import us.homebuzz.homebuzz.view.adapter.SearchPeopleAdapter;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.leaderboard.AccuracyFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendFollowingFollowersProfileView;

/**
 * Created by amit.singh on 12/8/2015.
 */
public class SearchPeople extends BaseFragment implements SearchPeopleView {
    private static final String TAG = AccuracyFragment.class.getSimpleName();
    @Bind(R.id.edEmail)
    EditText edEmail;
    @Bind(R.id.edName)
    EditText edName;
    @Bind(R.id.btnSearchForPeople)
    Button btnSearchForPeople;

    @Inject
    SearchPeoplePresenter searchPeoplePresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    private UsersLayoutManager usersLayoutManager;
    private SearchPeopleAdapter searchPeopleAdapter;
    @Inject
    Picasso picasso;
    private Collection<SearchPeopleModel> data = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View searchForPeople = inflater.inflate(R.layout.search_for_people, container, false);
        ButterKnife.bind(this, searchForPeople);
        edEmail.setFocusable(true);
        return searchForPeople;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        searchPeoplePresenter.setView(this);

    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.searchPeopleAdapter = new SearchPeopleAdapter(getActivity(), new ArrayList<SearchPeopleModel>(), picasso);
        this.searchPeopleAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(searchPeopleAdapter);
    }

    @OnClick(R.id.btnSearchForPeople)
    public void onSearchPeopleClick() {
        hideKeyboard();
        WeakHashMap<String, String> data = new WeakHashMap<>(4);
        data.put(constants.SEARCH_CHOICE, "1");
        data.put(constants.SEARCH_EMAIL, edEmail.getText().toString());
        data.put(constants.SEARCH_ID, String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        data.put(constants.SEARCH_NAME, edName.getText().toString());
        data.put(constants.SENDER_ID, String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        searchPeoplePresenter.initialize(data);
        edEmail.setText("");
        edName.setText("");
    }
    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        this.searchPeoplePresenter.resume();
        this.searchPeopleAdapter.setUsersCollection(data);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.searchPeoplePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.searchPeoplePresenter.destroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void showError(String message) {
        super.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }


    private SearchPeopleAdapter.OnItemClickListener onItemClickListener = new SearchPeopleAdapter.OnItemClickListener() {
        @Override
        public void onSearchClicked(SearchPeopleModel searchPeopleModel) {
            if (SearchPeople.this.searchPeoplePresenter != null && searchPeopleModel != null) {
                SearchPeople.this.searchPeoplePresenter.onUserClicked(searchPeopleModel);
            }
        }
    };

    @Override
    public void viewSearchPeople(Collection<SearchPeopleModel> searchPeopleModel) {
        Log.d(TAG, "Value Email " + searchPeopleModel.size());
        this.searchPeopleAdapter.setUsersCollection(searchPeopleModel);
        data = searchPeopleModel;
    }

    @Override
    public void viewUser(SearchPeopleModel searchPeopleModel) {
        String id = String.valueOf(searchPeopleModel.getId());
        Log.e("The Search  id ", id);
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, FriendFollowingFollowersProfileView.newInstance(id));
    }
}
