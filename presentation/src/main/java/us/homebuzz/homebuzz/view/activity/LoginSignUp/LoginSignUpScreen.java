package us.homebuzz.homebuzz.view.activity.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.view.activity.BaseActivity;
import us.homebuzz.homebuzz.view.adapter.LoginSignUp.LoginSignUpViewPageAdapter;

/**
 * Created by amit.singh on 9/22/2015.
 */

/**
 * This class is responsible for the Three page viewPager and Login and SignUp Screen Navigation with faceBook Login Handling
 */

public class LoginSignUpScreen extends BaseActivity {

    public static Intent getCallingIntent(Context context) {
        Intent callingIntent = new Intent(context, LoginSignUpScreen.class);
        return callingIntent;
    }

    @Bind(R.id.facebook_login_button)
    LoginButton facebooSignInButton;
    @Bind(R.id.pager)
    ViewPager viewPager;
    @Bind(R.id.image_text_layout)
    RelativeLayout imageTextRelativeLayout;
    @Bind(R.id.text1)
    TextView pageTextView;
    @Bind(R.id.indicator)
    CirclePageIndicator mIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        //Inject and Set the View
        setContentView(R.layout.login_signup_screen);
        //Transition of View Animation
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        ButterKnife.bind(this);
      //  this.navigator.navigateToMainActivity(this);
        setupUI();
    }
    private void setupUI() {

        // Pass results to ViewPagerAdapter Class
        LoginSignUpViewPageAdapter  adapter = new LoginSignUpViewPageAdapter(this, Constants.backGroundImages);
        // Binds the Adapter to the ViewPager
        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        imageTextRelativeLayout.setVisibility(View.GONE);
                        break;
                    case 1:
                        imageTextRelativeLayout.setVisibility(View.VISIBLE);
                        pageTextView.setText(getResources().getString(R.string.page2));
                        break;
                    case 2:
                        imageTextRelativeLayout.setVisibility(View.VISIBLE);
                        pageTextView.setText(getResources().getString(R.string.page3));
                        break;
                    case 3:
                        imageTextRelativeLayout.setVisibility(View.VISIBLE);
                        pageTextView.setText(getResources().getString(R.string.page4));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    //TODO Set the View Pager Adapter ,FaceBook Sdk Initialization and ViewPager Handling with Facebook login Handling

    @OnClick(R.id.btnSignUp)
    void openSignUpLayout(View v)
    {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        animation.setDuration(100);
        v.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                //start activity Sign up
                LoginSignUpScreen.this.navigator.navigateToRegistrationActivity(LoginSignUpScreen.this);
            }
        });
    }

    @OnClick(R.id.btnSignIn)
    void openSignInLayout(View v)
    {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);
        animation.setDuration(100);
        v.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                LoginSignUpScreen.this.navigator.navigateToLoginActivity(LoginSignUpScreen.this);
            }
        });
    }


}
