package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserUpdateModel;

/**
 * Created by amit.singh on 12/1/2015.
 */
public interface UserUpdateProfileView2 extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  userUpdateModel The user that will be shown.
     */
    void viewUserProfile2(UserUpdateModel userUpdateModel);

}
