package us.homebuzz.homebuzz.view.profile.friendsprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.presenter.FriendsFollowersPresenter;
import us.homebuzz.homebuzz.view.UserFollowersView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.friends.FriendFollowersAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.searchPeople.SearchPeople;

/**
 * Created by abhishek.singh on 11/23/2015.
 */
public class FriendsFollowersList extends BaseFragment implements UserFollowersView {
    private static final String TAG = FriendsFollowingList.class.getSimpleName();

    public static FriendsFollowersList newInstance() {
        FriendsFollowersList friendsFollowersList = new FriendsFollowersList();
        return friendsFollowersList;
    }
    @Bind(R.id.search)
    EditText search;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.llSearchPeople)
    LinearLayout llSearchPeople;
    @Inject
    Picasso picasso;
    @Inject
    FriendsFollowersPresenter friendsFollowersPresenter;
    private UsersLayoutManager usersLayoutManager;
    private FriendFollowersAdapter friendFollowerAdapter;
    FriendsFollowersList friendsFollowersList;
    ArrayList<UserFriendFollowedModel> mdata = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View friendsFollowersList = inflater.inflate(R.layout.friend_following_list, container, false);
        ButterKnife.bind(this, friendsFollowersList);

        return friendsFollowersList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Friends");
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
        this.friendFollowerAdapter = new FriendFollowersAdapter(getActivity(),mdata,picasso);
        friendsFollowersPresenter.setView(this);
        friendsFollowersPresenter.initialize();
        this.base_list.setAdapter(friendFollowerAdapter);
        friendFollowerAdapter.notifyDataSetChanged();
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.friendFollowerAdapter = new FriendFollowersAdapter(getActivity(), new ArrayList<UserFriendFollowedModel>(), picasso);
        this.friendFollowerAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(friendFollowerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.friendsFollowersPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.friendsFollowersPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.friendsFollowersPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderUserFollowersList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection) {
        Log.d(TAG, "The Followed List " + userFriendFollowedModelCollection.size());
        this.friendFollowerAdapter.setUsersCollection(userFriendFollowedModelCollection);
        mdata= (ArrayList<UserFriendFollowedModel>) userFriendFollowedModelCollection;
    }


    private FriendFollowersAdapter.OnItemClickListener onItemClickListener = new FriendFollowersAdapter.OnItemClickListener() {
        @Override
        public void onFollowedClicked(UserFriendFollowedModel userFriendFollowedModel) {
            if (FriendsFollowersList.this.friendsFollowersPresenter != null && userFriendFollowedModel != null) {
                FriendsFollowersList.this.friendsFollowersPresenter.onUserClicked(userFriendFollowedModel);
            }
        }
    };

    @Override
    public void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection) {
        String id = String.valueOf(userFriendFollowedModelCollection.getId());
        Log.e("The Followed List id ", id);
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, FriendFollowingFollowersProfileView.newInstance(id));
    }

    @OnClick(R.id.llSearchPeople)
    public void onClickSearchPeople() {
        Fragment fragment = new SearchPeople();
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, fragment);
    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Friends");
        super.onDestroyView();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new UsersLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    searchList(charSequence);
                } else {
                    FriendsFollowersList.this.friendFollowerAdapter.setUsersCollection(mdata);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    private void searchList(CharSequence str){
        Collection<UserFriendFollowedModel> tempArrayList = new ArrayList<>();
        if(str.length() > 0){
            for(UserFriendFollowedModel userFriendFollowedModel: mdata){
                if(userFriendFollowedModel.getName().toLowerCase().contains(str)){
                    tempArrayList.add(userFriendFollowedModel);
                }
                FriendsFollowersList.this.friendFollowerAdapter.setUsersCollection(tempArrayList);
            }
        }
    }
}

