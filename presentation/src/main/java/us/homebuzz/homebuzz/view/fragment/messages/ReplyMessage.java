package us.homebuzz.homebuzz.view.fragment.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rohitkumar on 30/10/15.
 */
public class ReplyMessage extends BaseFragment {

    private UserAllMessagesModel userAllMessagesModel;
    private static final String ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL = "UserAllMessagesModel";

    public static ReplyMessage newInstance(UserAllMessagesModel userAllMessagesModel) {
        ReplyMessage replyMessage =new ReplyMessage();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL, userAllMessagesModel);
        replyMessage.setArguments(argumentsBundle);

        return replyMessage;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view_leader_previous =inflater.inflate(R.layout.read_and_reply_message,container,false);
        ButterKnife.bind(this, view_leader_previous);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL) != null) {
            this.userAllMessagesModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL);
            Log.d(TAG, "UserModel in Leader Previous" + userAllMessagesModel.getId());
        }
        return view_leader_previous;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       setupUI();
    }

    private void setupUI() {
         //showMessage.setText(userAllMessagesModel.getBody());
    }
}
