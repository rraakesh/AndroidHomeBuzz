package us.homebuzz.homebuzz.view.activity.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.DaggerUserLoginComponent;
import us.homebuzz.homebuzz.di.components.UserLoginComponent;
import us.homebuzz.homebuzz.di.modules.UserLoginModule;
import us.homebuzz.homebuzz.view.activity.BaseActivity;
import us.homebuzz.homebuzz.view.fragment.LoginFragment;

/**
 * Created by amit.singh on 9/23/2015.
 */
public class Login extends BaseActivity implements HasComponent<UserLoginComponent>, LoginFragment.UserLoginFragmentListener {
    public static String TAG = Login.class.getClass().getSimpleName();

    public static Intent getCallingIntent(Context context) {
        Intent callingIntent = new Intent(context, Login.class);
        return callingIntent;
    }

    private UserLoginComponent userComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        this.initializeInjector();
    }

    private void initializeInjector() {
        //TODO CHANGE the Module dependancy to inject the Value of Login Data to passed to the LoginModule
        this.userComponent = DaggerUserLoginComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule()).userLoginModule(new UserLoginModule())
                        //         .activityModule(getActivityModule()).userLoginModule(new UserLoginModule("buyer@kayosys.com","buyer@123","token123"))
                .build();
        //   Log.d(TAG, "Check for the dependency" + userComponent);
    }


    @Override
    public UserLoginComponent getComponent() {
        return userComponent;
    }

    /**
     * Navigate to SignUp Or Registration After SignUp Click
     */
    @Override
    public void onUserSignUpClicked() {
        this.navigator.navigateToRegistrationActivity(this);
        Login.this.finish();
    }

    /**
     * Navigate to MainActivity After the User Is Successfully Login
     */
    @Override
    public void onUserLoginSuccess() {
        this.sharedPreferences.edit().putBoolean(Login.this.constants.KEY_IS_LOGIN, true).commit();
        this.navigator.navigateToMainActivity(this);
        Login.this.finish();
    }

}
