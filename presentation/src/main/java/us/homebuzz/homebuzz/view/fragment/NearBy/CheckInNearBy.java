package us.homebuzz.homebuzz.view.fragment.NearBy;

/**
 * Created by amit.singh on 10/3/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.Utility.InfoWindowRefresher;
import us.homebuzz.homebuzz.Utility.LocationInterface;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.presenter.NearByPresenter;
import us.homebuzz.homebuzz.view.UserNearbyView;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * From LandingFragment when Click on the Check-In
 * Responsibility of this Class
 * 1.Nearby Data Based on the user current location
 * 2.Data Source for the list (Check-In ) List
 * 3.Filter View Handling and Update the Data
 */
public class CheckInNearBy extends NearByDecorator implements View.OnClickListener,OnMapReadyCallback,UserNearbyView,LocationInterface,NearbyFilterFragment.NearbyFilter {
    private static final String TAG = CheckInNearBy.class.getSimpleName();
    private Collection<UserNearbyModel> userNearbyModelCollection;
    public static CheckInNearBy newInstance() {
        CheckInNearBy checkInNearBy = new CheckInNearBy();
        return checkInNearBy;
    }
    public CheckInNearBy(){super();}
    GoogleMap mMap;
    CheckInMapFragment mMapFragment;
    @Inject
    NearByPresenter nearByPresenter;
    @Inject
    public GpsLocation gpsLocation;
    @Inject
    Constants constants;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    SharedPreferences sharedPreferences;
    CheckInListFragment checkInListFragment;
    protected ReplaceFragment replaceFragmentNearby;
    @Inject
    Picasso picasso;
    private FrameLayout frameLayoutinfowindow;
    private boolean infowindowShow;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.frameLayoutinfowindow  = new FrameLayout(this.getContext());
        setupLowerTab();
        if(checkPlayServices()) {
            mMapFragment = CheckInMapFragment.newInstance();
            mMapFragment.getMapAsync(this);
            super.addFragment(R.id.container_layout_tab, mMapFragment);
        }else{
            super.showToastMessage("Google Play Services Required.");
        }
    }
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
            //Todo Consider a Better Solution to Have Nested Fragment Access the Listener
         this.replaceFragmentNearby =replaceListener;
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
        gpsLocation.setonLocationListener(this);
        this.nearByPresenter.setView(this);

    }
    /**
     * Inflate the Lower Tab Strip and Associate the Click Listener
     * @See onMiddleIconClick(Fragment fragment) for the middle icon Click and so on
     * @See onLeftIconClick(Fragment fragment)
     * @See onRightIconClick(Fragment fragment)
     */
    @Override
   public void  setupLowerTab(){
        LinearLayout tab_strip=(LinearLayout)this.getActivity().getLayoutInflater().inflate(R.layout.nearby_lower_tab,tab_fragment,false);
        tab_fragment.addView(tab_strip);
        ButterKnife.bind(super.getClass(), tab_strip);
        LinearLayout listbutton=(LinearLayout)tab_fragment.findViewById(R.id.listbutton);
        LinearLayout mapbutton=(LinearLayout)tab_fragment.findViewById(R.id.mapbutton);
        LinearLayout filterbutton=(LinearLayout)tab_fragment.findViewById(R.id.filterbutton);
        ((TextView) mapbutton.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
        ((TextView)listbutton.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        ((ImageView)listbutton.findViewById(R.id.list_icon)).setImageResource(R.drawable.list_white);
        ((ImageView)mapbutton.findViewById(R.id.map_icon)).setImageResource(R.drawable.map_yellow);

        listbutton.setOnClickListener(this);
        mapbutton.setOnClickListener(this);
        filterbutton.setOnClickListener(this);
    }
    @Override
    public void onMapReady(GoogleMap map) {
        /*map.addMarker        System.out.println("Getting data" + userNearbyModel.getPreviousReviews().size());(new MarkerOptions()
                .position(new LatLng(0, 0))
                .title("Marker"));*/
        if(map!=null) {
            this.initializeGoogleMap(map);
            gpsLocation.callbacksResults();
            //Calling the Presenter here
            //callNearbyPresenter(String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)), String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)), getDistanceFromMap());
           /* this.nearByPresenter.initialize(,,String.valueOf(constants.RADIUS/1000));*/
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.listbutton:
                if(userNearbyModelCollection!=null && userNearbyModelCollection.size()>0) {
                    checkInListFragment=CheckInListFragment.newInstance((ArrayList) userNearbyModelCollection);
                    LinearLayout parent=(LinearLayout)v.getParent();
                    ((TextView) parent.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                    ((TextView)v.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
                    ((ImageView)v.findViewById(R.id.list_icon)).setImageResource(R.drawable.list_yellow);
                    ((ImageView)parent.findViewById(R.id.map_icon)).setImageResource(R.drawable.map_white);
                    super.onRightIconClick(checkInListFragment);
                }
                break;
            case R.id.mapbutton:
                LinearLayout parent=(LinearLayout)v.getParent();
                ((TextView) v.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
                ((TextView)parent.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                ((ImageView)parent.findViewById(R.id.list_icon)).setImageResource(R.drawable.list_white);
                 ((ImageView)parent.findViewById(R.id.map_icon)).setImageResource(R.drawable.map_yellow);
                super.onLeftIconClick(mMapFragment);
                break;
            case R.id.filterbutton:
                super.onFilterIconClick(NearbyFilterFragment.newInstance());
                break;
        }
    }
    @Override public void onResume() {
        super.onResume();
        this.nearByPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        this.nearByPresenter.pause();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        this.nearByPresenter.destroy();
    }

    private void initializeGoogleMap(final GoogleMap googleMap){
        //  System.out.println("Inside the Map CallBack");
        this.mMap = googleMap;
        this.mMap.setMyLocationEnabled(true);
        this.mMap.getUiSettings().setZoomControlsEnabled(true);
        //Bottom Padding
       float bottomMapPadding= 50 * getResources().getDisplayMetrics().density;
        this.mMap.setPadding(0,0,0,(int)bottomMapPadding);
        //Handling the Marker Click Listener
        this.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                marker.showInfoWindow();
                return true;
            }
        });
        //Handling the Info Window Click Listener
        this.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
                UserNearbyModel   userNearbyModel=(UserNearbyModel)((List)userNearbyModelCollection).get(Integer.parseInt(marker.getTitle()));
                replaceFragmentNearby.CallReplaceFragment(R.id.conatainer_layout, ListingFragment.newInstance(userNearbyModel, true));
            }
        });
        //Setup the Camera Position Change Listener
        this.mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                System.out.println("zoom " + cameraPosition.zoom);
                VisibleRegion vr = mMap.getProjection().getVisibleRegion();
                double left = vr.latLngBounds.southwest.longitude;
                double top = vr.latLngBounds.northeast.latitude;
                double right = vr.latLngBounds.northeast.longitude;
                double bottom = vr.latLngBounds.southwest.latitude;
                Location middle = new Location("MiddleLeftCornerLocation");
                middle.setLongitude(bottom);
                middle.setLatitude(vr.latLngBounds.getCenter().longitude);
                if (cameraPosition.target.longitude != 0.0 && getDistanceFromMap(middle)/1000>20 /*Calculate the Diatance*/) {
                    float[] results = new float[2];
                    Location.distanceBetween(
                            cameraPosition.target.latitude,
                            cameraPosition.target.longitude,
                            top,
                            right,
                            results);
                    Log.d(TAG, "Radius received" + results[0]);
                    sharedPreferences.edit().putFloat(constants._RADIUS, results[0]/1000).putFloat(constants._LAT, (float) cameraPosition.target.latitude).putFloat(constants._LON, (float) cameraPosition.target.longitude).commit();
                    SortedMap<String,String> nearbyData=new TreeMap<>();
                    nearbyData.put(constants._RADIUS, String.valueOf(sharedPreferences.getFloat(constants._RADIUS, 0.0f)));
                    nearbyData.put(constants._LON, String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)));
                    nearbyData.put(constants._LAT, String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));
                   // callNearbyPresenter(String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)), String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)), results[0]);
                    callNearbyPresenter(nearbyData);
                }
            }
        });
    }
    private void callNearbyPresenter(/*String lat,String lon,float distance*/SortedMap<String,String> data){

        this.nearByPresenter.initialize(/*lat, lon, String.valueOf(distance / 1000)*/sharedPreferences.getInt(constants.KEY_ID,0),data);

    }
    private float getDistanceFromMap(Location middleleftlocation){
        Location MiddleLeftCornerLocation=middleleftlocation;//(center's latitude,vr.latLngBounds.southwest.longitude)
        Location center=new Location("center");
        center.setLatitude(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().latitude);
        center.setLongitude(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().longitude);
        System.out.println("Distance is" + center.distanceTo(MiddleLeftCornerLocation));
        return center.distanceTo(MiddleLeftCornerLocation);
    }
    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }
    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }
    @Override
    public void showRetry() {

    }
    @Override
    public void hideRetry() {

    }
    @Override public void showError(String message) {
        /*this.showToastMessage(message);*/
    }

    @Override public Context getContext() {
        return this.getActivity().getApplicationContext();
    }

    //Update the Data After we get it From NearbyPresenter->GetNearbyListing->UserRepository->Threads and Observer
    @Override
    public void renderUserNearbyList(Collection<UserNearbyModel> userNearbyModelCollection) {
        this.userNearbyModelCollection=userNearbyModelCollection;
        Log.d(TAG,"Checking the Nearby List"+userNearbyModelCollection.size());
       // Log.d(TAG, "" + (((List) userNearbyModelCollection).get(0)));
        if(checkInListFragment!=null){
            checkInListFragment.updateList(userNearbyModelCollection);
        }
        this.setMapMarker((List) userNearbyModelCollection);
    }

    private void setMapMarker(List<UserNearbyModel> userNearbyModelCollection){
        if(mMap!=null){
            mMap.clear();
            for (int i = 0; i < userNearbyModelCollection.size();i++) {
               final UserNearbyModel mdata=userNearbyModelCollection.get(i);
                Double latitude  = userNearbyModelCollection.get(i).getLat();

                Double longitude  = userNearbyModelCollection.get(i).getLon();
                MarkerOptions marker = new MarkerOptions().position(new LatLng((latitude),
                        (longitude)));
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin));
                marker.title("" + i);
                mMap.addMarker(marker);
            }
        }
    }

    @Override
    public void onConnectionSuspended() {
    }

    @Override
    public void onLocationFailed() {
    }
    @Override
    public void onLocation(Location location) {
        Log.d(TAG,"Location in Nearby "+location.getLongitude());
        if (location != null) {
            //Putting the Location to Shared Preferences
            sharedPreferences.edit().putFloat(constants._LON, (float) location.getLongitude()).putFloat(constants._LAT, (float) location.getLongitude()).commit();
            if (mMap != null)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.5f));
        }
    }
//marker Info window get the Data based ont the Title we set the position and get the Data from UserNearbyModelCollection with Position Value
    private class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
       UserNearbyModel userNearbyModel;
        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.info_window,frameLayoutinfowindow,false);
            userNearbyModel=(UserNearbyModel)((List)userNearbyModelCollection).get(Integer.parseInt(marker.getTitle()));
            ImageView imageview = (ImageView) v.findViewById(R.id.image);
            TextView price = (TextView) v.findViewById(R.id.text1);
            TextView bathTextView = (TextView) v.findViewById(R.id.bath_text);
            TextView bedTextView = (TextView) v.findViewById(R.id.bed_text);
            //      RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratebar);
         //   TextView address = (TextView) v.findViewById(R.id.text);
         /*   String addr = userNearbyModel.getAddress();
         //   address.setText(addr + "");
            if (addr == null || addr.equals("")) {
                address.setText("NA");
                *//*address.setTextColor(CheckInNearBy.this.getActivity().getResources().getColor(R.color.color_black));*//*
            } else {
                if(addr.length()>10) {
                    addr = addr.substring(0, 10);
                    addr=addr+"....";
                    address.setText(addr);
                }
            }
*/
            price.setText("$" +  new DecimalFormat("#,###.0").format(userNearbyModel.getPrice()).toString());


            bathTextView.setText(userNearbyModel.getBaths()+" Baths");
            bedTextView.setText(userNearbyModel.getBedrooms() + "Beds");
            List<String> thumbnail = userNearbyModel.getPhotos();
            if (thumbnail.size()>0 ) {
                if (!(thumbnail.get(0).equalsIgnoreCase("null"))) {
                    String image_url = thumbnail.get(0).toString();
                    picasso.load(image_url).resize(100, 100).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(imageview, new InfoWindowRefresher(marker));
                    System.out.println("image url = "+image_url);
                }

            } else
                imageview.setImageResource(R.drawable.ic_launcher);

            return v;

        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

    }

    @Override
    public void onUpdateClick(SortedMap<String, String> filterData) {
        Log.d(TAG,"Checking the FilterData"+filterData.size());
        callNearbyPresenter(filterData);
    }
}
