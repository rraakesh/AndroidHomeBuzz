package us.homebuzz.homebuzz.view.fragment.Notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class NotificationFragment extends BaseFragment {

    private static final String ARGUMENT_KEY_USER_JUST_SOLD_MODEL = "UserJustSoldModel";
    private static final String ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL = "UserUnReadMessagesModel";
    private static final String ARGUMENT_KEY_USER_JUST_LISTED_MODEL = "UserJustListedModel";

    public static NotificationFragment newInstance(ArrayList<UserJustSoldModel> userJustSoldModel, ArrayList<UserJustListedModel> userJustListedModel, ArrayList<UserUnreadMessagesModel> UserUnreadMessagesModel) {
        NotificationFragment notificationFragment = new NotificationFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL, userJustSoldModel);
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL, UserUnreadMessagesModel);
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL, userJustListedModel);
        notificationFragment.setArguments(argumentsBundle);
        return notificationFragment;
    }

    @Bind(R.id.message_count)
    TextView message_count;
    @Bind(R.id.just_sold_count)
    TextView just_sold_count;
    @Bind(R.id.just_listed_count)
    TextView just_listed_count;

    private ArrayList<UserUnreadMessagesModel> unreadMessagesModels;
    private ArrayList<UserJustListedModel> userJustListedModels;
    private ArrayList<UserJustSoldModel> userJustSoldModels;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View notifyView = inflater.inflate(R.layout.notofication_main, container, false);
        ButterKnife.bind(this, notifyView);

        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL) != null)
            userJustSoldModels = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL);

        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL) != null)
            unreadMessagesModels = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL);

        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL) != null)
            userJustListedModels = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL);

        return notifyView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Notifications");
        setUpUI();
    }

    private void setUpUI() {
        if (userJustSoldModels != null && userJustSoldModels.size() > 0) {
            just_sold_count.setText("Just-Sold(" + userJustSoldModels.size() + ")");
        } else
            just_sold_count.setText("Just-Sold(0)");
        if (userJustListedModels != null && userJustListedModels.size() > 0) {
            just_listed_count.setText("Just-Listed(" + userJustListedModels.size() + ")");
        } else
            just_listed_count.setText("Just-Listed(0)");
        if (unreadMessagesModels != null && unreadMessagesModels.size() > 0) {
            message_count.setText("Messages(" + unreadMessagesModels.size() + ")");
        } else
            message_count.setText("Messages(0)");
    }

    @OnClick(R.id.message_count)
    public void onMessageClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UnreadMessageFragment.newInstance(unreadMessagesModels));
    }

    @OnClick(R.id.just_sold_count)
    public void onSoldClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, JustSoldListingFragment.newInstance(userJustSoldModels));
    }

    @OnClick(R.id.just_listed_count)
    public void onListedClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, JustListedFragment.newInstance(userJustListedModels));
    }

    @Override
    public void onDestroy() {
        ButterKnife.unbind(this);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroy();
    }
}
