package us.homebuzz.homebuzz.view.adapter.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class JustSoldAdapter extends RecyclerView.Adapter<JustSoldAdapter.UserViewHolder> {
    public static String TAG = JustSoldAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        void JustSoldItemClick(UserJustSoldModel userEstimateModel);
    }

    @Inject
    Picasso picasso;
    private List<UserJustSoldModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener1;

    public JustSoldAdapter(Context context, Collection<UserJustSoldModel> usersCollection, Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserJustSoldModel>) usersCollection;
        this.picasso = picasso;
    }

    @Override
    public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.notification_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserJustSoldModel userJustSoldModel = this.usersCollection.get(position);

        if (userJustSoldModel.getStreet() != null) {
            holder.Listing_title.setText(userJustSoldModel.getStreet());
        } else {
            holder.Listing_title.setText("");
        }
        if (userJustSoldModel.getStreet() != null) {
            holder.city.setText(userJustSoldModel.getCity() + ",");
        } else {
            holder.city.setText("");
        }
        if (userJustSoldModel.getZip() != null) {
            holder.zip.setText(userJustSoldModel.getZip());
        } else {
            holder.zip.setText("");
        }
        if (userJustSoldModel.getState() != null) {
            holder.state.setText(userJustSoldModel.getState());

        } else {
            holder.state.setText("");
        }
        if (userJustSoldModel.getPrice() != null) {
            holder.price.setText("$ " + new DecimalFormat("#,###").format(userJustSoldModel.getPrice().intValue()));
        } else {
            holder.price.setText("");
        }


        holder.leader_accuracy.setText(String.valueOf(userJustSoldModel.getBedrooms() + "BR" + " / " + userJustSoldModel.getBaths() + "BA"));
        if (null != userJustSoldModel && userJustSoldModel.getPhotos().size() > 0) {
            System.out.println(TAG + "Pic" + String.valueOf(userJustSoldModel.getPhotos()));
            picasso.load(String.valueOf(userJustSoldModel.getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
        } else {
            //TODO Place A DEFAULT IMAGE
            holder._image.setImagePlaceHolder(R.drawable.holder_home);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (JustSoldAdapter.this.onItemClickListener1 != null) {
                    JustSoldAdapter.this.onItemClickListener1.JustSoldItemClick(userJustSoldModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserJustSoldModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserJustSoldModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener1) {
        this.onItemClickListener1 = onItemClickListener1;
    }

    private void validateUsersCollection(Collection<UserJustSoldModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.Listing_title)
        TextView Listing_title;
        @Bind(R.id.state)
        TextView state;
        @Bind(R.id.price)
        TextView price;
        @Bind(R.id.city)
        TextView city;
        @Bind(R.id.zip)
        TextView zip;
        @Bind(R.id.row_second)
        TextView row_second;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id._image)
        AutoLoadImageView _image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
