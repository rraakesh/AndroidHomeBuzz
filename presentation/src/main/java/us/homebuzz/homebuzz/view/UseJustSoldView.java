package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserJustSoldModel;

/**
 * Created by amit.singh on 11/18/2015.
 */
public interface UseJustSoldView extends LoadDataView {
    /**
     * View a {@link } UnreadMessages
     *
     * @param userJustSoldModelCollection Messages that a user has not Read
     */
    void viewJustSoldListing(Collection<UserJustSoldModel> userJustSoldModelCollection);

}
