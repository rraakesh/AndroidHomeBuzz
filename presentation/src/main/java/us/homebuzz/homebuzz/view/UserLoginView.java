package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserLoginModel;

/**
 * Created by amit.singh on 10/7/2015.
 */
public interface UserLoginView extends LoadDataView {
    /**
     * Render a user in the UI.
     *
     * @param user The {@link UserLoginModel} that will be shown.
     */
    void renderAfterUserLogin(UserLoginModel user);
}
