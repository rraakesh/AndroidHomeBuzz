package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 11/25/2015.
 */
public class EstimateListingDetails extends BaseFragment {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";


    public static EstimateListingDetails newInstance(UserNearbyModel userNearbyModel, int pos) {
        EstimateListingDetails estimateListingDetails = new EstimateListingDetails();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_IS_USER_PROFILE, userNearbyModel);
        estimateListingDetails.setArguments(argumentsBundle);
        position = pos;
        return estimateListingDetails;
    }

    public static int position;
    @Inject
    Picasso picasso;
    private UserNearbyModel userNearbyModel;
    @Bind(R.id.image)
    AutoLoadImageView image;
    @Bind(R.id.rate_text)
    TextView rate_text;
    @Bind(R.id.address)
    TextView address;
    @Bind(R.id.bedsbaths)
    TextView bedsbaths;
    @Bind(R.id.txtReason)
    TextView txtReason;
    @Bind(R.id.my_estimated_value)
    TextView my_estimated_value;
    @Bind(R.id.previous_location_rating)
    RatingBar previous_location_rating;
    @Bind(R.id.previous_condition_rating)
    RatingBar previous_condition_rating;
    @Bind(R.id.txtClose)
    LinearLayout txtClose;
    @Bind(R.id.show_images)
    LinearLayout show_images;
    ArrayList<String> photoListFromModel = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewEstimateDetails = inflater.inflate(R.layout.estimate_listing_details, container, false);
        ButterKnife.bind(this, viewEstimateDetails);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_IS_USER_PROFILE) != null) {
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_IS_USER_PROFILE);
            Log.d(TAG, "UserModel in Estimate listing" + userNearbyModel.getId());
        }
        return viewEstimateDetails;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Check In");
        this.initialize();
        this.setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
    }

    private void setupUI() {
        photoListFromModel.clear();
        if (userNearbyModel.getEstimates().get(position).getImageUrl().size() > 0) {
            for (int j = 0; j < userNearbyModel.getEstimates().get(position).getImageUrl().size(); j++) {
                photoListFromModel.add(userNearbyModel.getEstimates().get(position).getImageUrl().get(j));
            }
            System.out.println("image_size" + photoListFromModel.size());
            for (int i = 0; i < photoListFromModel.size(); i++) {
                ImageView imageView = new ImageView(EstimateListingDetails.this.getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
                imageView.setLayoutParams(params);
                imageView.setPadding(3, 5, 3, 5);
                show_images.addView(imageView);
                picasso.load(photoListFromModel.get(i)/*split(",")[0].replace("[","")*/).into(imageView);
                System.out.println("picasso_image" + photoListFromModel.get(i)/*.split(",")*/);
            }
        }
        txtReason.setText(userNearbyModel.getEstimates().get(position).getReason() != null ? userNearbyModel.getEstimates().get(position).getReason() : "");
        my_estimated_value.setText("$"+ (new DecimalFormat("###,###").format(userNearbyModel.getEstimates().get(position).getPrice() != null ? userNearbyModel.getEstimates().get(position).getPrice().intValue() : 0)));
        rate_text.setText("$ " + String.valueOf(new DecimalFormat("###,###").format(userNearbyModel.getPrice().intValue())));
        bedsbaths.setText(String.valueOf(userNearbyModel.getBedrooms()) + "BR /" + String.valueOf(userNearbyModel.getBaths()) + " BA");
        address.setText((userNearbyModel.getStreet() != null && !(userNearbyModel.getStreet().contains("null")) ? String.valueOf(userNearbyModel.getStreet()) + "\n" : ""));
        previous_condition_rating.setRating(userNearbyModel.getEstimates().get(position).getConditionRating() != null ? userNearbyModel.getEstimates().get(position).getConditionRating().floatValue() : 0);
        previous_location_rating.setRating(userNearbyModel.getEstimates().get(position).getLocationRating() != null ? userNearbyModel.getEstimates().get(position).getLocationRating().floatValue() : 0);
        address.append((userNearbyModel.getCity() != null && !(userNearbyModel.getCity().contains("null"))) ? userNearbyModel.getCity() : "" + "," + (userNearbyModel.getState() != null && !(userNearbyModel.getState().contains("null")) ? userNearbyModel.getState() : ""));
        address.append(userNearbyModel.getZip());


        System.out.println("image_url" + userNearbyModel.getEstimates().get(position).getImageUrl());

        //TODO
        System.out.println("image");
        if (userNearbyModel.getPhotos().size() > 0) {
            picasso.load(userNearbyModel.getPhotos().get(0)).into(image);
        } else {
            image.setImageResource(R.drawable.ic_launcher);
        }

    }

    @OnClick(R.id.show_images)
    public void imageShow() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingPhotoFragment.newInstance1(photoListFromModel, false));
    }

    @OnClick(R.id.txtClose)
    public void btnClose() {
        getActivity().onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Reviews");

    }
}


