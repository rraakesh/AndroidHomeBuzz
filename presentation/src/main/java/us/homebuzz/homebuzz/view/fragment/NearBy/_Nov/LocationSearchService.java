package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by amit.singh on 11/24/2015.
 */
public class LocationSearchService extends IntentService {
    private static final String TAG = LocationSearchService.class.getSimpleName();
    private static final String REQUEST_SEARCH = "search_location";
    private static final String RESPONSE_LAT = "Lat_Search";
    private static final String RESPONSE_LON = "Lon_Search";
    public final String EXTRA_ERROR= "HomeBuzz_Error";
    private static Context context;
    public static void startSearchService(Context context, String Query,String action,String category) {
        LocationSearchService.context=context;
        Intent msgIntent = new Intent(context, LocationSearchService.class);
        msgIntent.putExtra(LocationSearchService.REQUEST_SEARCH, Query);
        msgIntent.addCategory(category);
        msgIntent.setAction(action)  ;
        context.startService(msgIntent);
    }


    public LocationSearchService() {
        super("LocationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String searchText = intent.getStringExtra(REQUEST_SEARCH);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(intent.getAction());
        Log.d(TAG,"Search"+searchText);
        try {
            Geocoder geocoder = new Geocoder(context);
            final List<Address> addresses = geocoder.getFromLocationName(searchText, 3);
            if(addresses.size()>0) {

                Address address = (Address) addresses.get(0);
                // Creating an instance of GeoPoint, to display in Google Map
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                broadcastIntent.putExtra(RESPONSE_LAT, address.getLatitude());
                broadcastIntent.putExtra(RESPONSE_LON, address.getLongitude());
                Log.d(TAG, "Search" + address.getLatitude());
            }

        } catch (Exception e) {
            e.printStackTrace();
            broadcastIntent.putExtra(EXTRA_ERROR, " Location Not Found !");
        }
        sendBroadcast(broadcastIntent);
    }
}
