package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserUpdateModel;

/**
 * Created by amit.singh on 11/14/2015.
 */
public interface UserUpdateProfileView extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  userUpdateModel The user that will be shown.
     */
    void viewUserProfile(UserUpdateModel userUpdateModel);

}
