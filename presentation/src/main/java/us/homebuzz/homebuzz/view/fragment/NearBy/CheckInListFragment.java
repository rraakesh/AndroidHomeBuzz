package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.adapter.nearby.NearByAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseNestedFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;

/**
 * Created by amit.singh on 10/5/2015.
 */
public class CheckInListFragment extends BaseNestedFragment {

    private static final String TAG = CheckInListFragment.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_NEARBY_MODEL = "NearbyModel";

    public static CheckInListFragment newInstance(ArrayList<UserNearbyModel> mUserNearbyModel) {
        CheckInListFragment checkInListFragment = new CheckInListFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_NEARBY_MODEL, mUserNearbyModel);
        checkInListFragment.setArguments(argumentsBundle);
        return checkInListFragment;
    }

    private NearByAdapter nearByAdapter;
    private ArrayList<UserNearbyModel> mdata = new ArrayList<>();


    @Bind(R.id.nearby_list)
    RecyclerView nearby_list;
    @Bind(R.id.search)
    EditText search;
    @Inject
    Picasso picasso;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.nearby_list_fragment, container, false);
        ButterKnife.bind(this, list_nearby);
        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_NEARBY_MODEL) != null)
            mdata = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_NEARBY_MODEL);
        System.out.println("List size =" + mdata.size());
        return list_nearby;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        nearby_list.setLayoutManager(layoutManager);
        // search.setThreshold(1);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    searchList(s);
                } else {
                    CheckInListFragment.this.nearByAdapter.setUsersCollection(mdata);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    private void searchList(CharSequence str) {
        Collection<UserNearbyModel> tempArrayList = new ArrayList<>();
        if (str.length() > 0) {
            System.out.println("size mdata" + mdata.size());
            for (UserNearbyModel model : mdata) {

                    if (model.getZip().toString().contains(str)
                            || model.getPrice().toString().contains(str)
                            ||((model.getAddress()!= null) && (model.getAddress().toString().toUpperCase().contains(str)))
                            ||((model.getAddress()!= null) && (model.getAddress().toLowerCase().contains(str)))){
                         // || model.getBedrooms().toString().contains(str)
                        //|| model.getBaths().toString().contains(str)
                        tempArrayList.add(model);
                    }
                    CheckInListFragment.this.nearByAdapter.setUsersCollection(tempArrayList);

                }

            }


            }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
        this.nearByAdapter = new NearByAdapter(getActivity(), mdata, picasso);
        this.nearByAdapter.setOnItemClickListener(onItemClickListener);
        this.nearby_list.setAdapter(nearByAdapter);

        nearByAdapter.notifyDataSetChanged();
    }

    //Update the RecyclerView
    public void updateList(Collection<UserNearbyModel> filterDataData) {
        mdata.clear();
        mdata.addAll(filterDataData);
        nearByAdapter.notifyDataSetChanged();
    }

    private NearByAdapter.OnItemClickListener onItemClickListener =
            new NearByAdapter.OnItemClickListener() {
                @Override
                public void onUserItemClicked(UserNearbyModel userNearbyModel) {
                    // Log.d(TAG,"Clicked");
                    if (CheckInListFragment.this.getParentFragment() instanceof CheckInNearby) {
                        ((CheckInNearby) CheckInListFragment.this.getParentFragment())
                                .replaceFragmentNearby.CallReplaceFragment(R.id.conatainer_layout, ListingFragment.newInstance(userNearbyModel, true));
                    }
                }

                @Override
                public void onUserItemCheckInClicked(UserNearbyModel userNearbyModel) {
                    ((CheckInNearby) CheckInListFragment.this.getParentFragment())
                            .replaceFragmentNearby.CallReplaceFragment(R.id.conatainer_layout, UpdateCheckInEstimateFragment.newInstance1(userNearbyModel, true));
                }
            };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
