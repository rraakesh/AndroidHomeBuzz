package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.SubscriptionModel;
import us.homebuzz.homebuzz.presenter.SubscriptionPlanPresenter;
import us.homebuzz.homebuzz.view.SubcriptionPlanView;
import us.homebuzz.homebuzz.view.ZipPurchase;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.RequireZipPurchase;

/**
 * Created by amit.singh on 12/15/2015.
 */
public class UnlimitedZipFragment extends BaseFragment implements ZipPurchase, SubcriptionPlanView {
    private static final String TAG = UnlimitedZipFragment.class.getSimpleName();

    public UnlimitedZipFragment() {
        super();
    }

    private RequireZipPurchase requireZipPurchase;

    @Inject
    SubscriptionPlanPresenter subscriptionPlanPresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.zip_packages, container, false);
        ButterKnife.bind(this, list_nearby);
        return list_nearby;
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        subscriptionPlanPresenter.setView(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialize();
        if (this.getActivity() instanceof RequireZipPurchase)
            requireZipPurchase = (RequireZipPurchase) this.getActivity();


    }

    @OnClick(R.id.unlimited)
    public void onUnlimitedClick() {
        //enable this to call the google play in app Billing
        requireZipPurchase.initPurchase();
    }

    @Override
    public void onZipCodePurchaseSuccess(boolean isSuccesss) {
        Log.d(TAG, "ZipPurchase" + isSuccesss);
    }

    @Override
    public void onPurchaseIdReceived(String orderId) {
        WeakHashMap<String, String> data = new WeakHashMap<>(2);
        data.put("id", String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        data.put("user[android_receipt]", orderId);
        subscriptionPlanPresenter.initialize(data);
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void viewSubPlan(SubscriptionModel subscriptionModel) {
        if (subscriptionModel != null) {
            Log.d(TAG, "Subscription" + subscriptionModel.getId());
            this.getActivity().onBackPressed();
        }
    }
}
