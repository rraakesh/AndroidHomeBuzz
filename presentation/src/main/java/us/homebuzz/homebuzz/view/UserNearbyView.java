package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;

/**
 * Created by amit.singh on 10/14/2015.
 */
public interface UserNearbyView extends LoadDataView{
    /**
     * Render a user list in the UI.
     *
     * @param userModelCollection The collection of {@link UserModel} that will be shown.
     */
    void renderUserNearbyList(Collection<UserNearbyModel> userModelCollection);
    //void viewUser(UserNearbyModel UserNearbyModel);

}
