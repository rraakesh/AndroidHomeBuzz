package us.homebuzz.homebuzz.view.adapter.leaderboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by amit.singh on 10/27/2015.
 */
public class PreviousEstimateAdapter  extends RecyclerView.Adapter<PreviousEstimateAdapter.UserViewHolder> {
    public static String TAG = AccuracyAdapter.class.getClass().getSimpleName();
    public interface OnItemClickListener {
        void onPreviousEstimateAdapterItemClicked(UserPreviousEstimateModel userModel);
    }
    private List<UserPreviousEstimateModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private DecimalFormat accuracyFormatter;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;
    public PreviousEstimateAdapter(Context context, Collection<UserPreviousEstimateModel> usersCollection,Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserPreviousEstimateModel>) usersCollection;
        this.accuracyFormatter=new DecimalFormat("#.##");
        this.picasso=picasso;
    }

    @Override public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.leaderboard_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserPreviousEstimateModel userModel = this.usersCollection.get(position);
        holder.leader_name.setText(userModel.getUser().getName());
        holder.leader_accuracy.setText(this.accuracyFormatter.format(userModel.getUser().getAccurate() * 100));
        if(userModel.getListing()!=null && (userModel.getListing().getPhotos().size()>0) ) {
            System.out.println(TAG + "Pic" + userModel.getListing().getPhotos().get(0));
            picasso.load(String.valueOf(userModel.getListing().getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder.person_image);
          //  holder.person_image.setImageUrl(String.valueOf(userModel.getListing().getPhotos().get(0)));
        }else{
            //TODO Place A DEFAULT IMAGE
            holder.person_image.setImagePlaceHolder(R.drawable.holder_home);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (PreviousEstimateAdapter.this.onItemClickListener != null) {
                    PreviousEstimateAdapter.this.onItemClickListener.onPreviousEstimateAdapterItemClicked(userModel);
                }
            }
        });
    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserPreviousEstimateModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserPreviousEstimateModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<UserPreviousEstimateModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.leader_name)
        TextView leader_name;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id.leader_image)
        AutoLoadImageView person_image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
