package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.estimatelisting.EstimateListingAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 11/25/2015.
 */
public class EstimateListing extends BaseFragment implements EstimateListingAdapter.OnItemClickListener {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isEstimateList";

    public static EstimateListing newInstance(UserNearbyModel userNearbyModel) {
        EstimateListing estimateListing = new EstimateListing();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_IS_USER_PROFILE, userNearbyModel);
        estimateListing.setArguments(argumentsBundle);
        return estimateListing;
    }

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Inject
    Picasso picasso;
    private UsersLayoutManager usersLayoutManager;
    private UserNearbyModel userNearbyModel;
    EstimateListingAdapter estimateListingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View friendsFollowersList = inflater.inflate(R.layout.estimate_listing, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_IS_USER_PROFILE) != null)
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_IS_USER_PROFILE);
        ButterKnife.bind(this, friendsFollowersList);
        return friendsFollowersList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Reviews");
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.estimateListingAdapter = new EstimateListingAdapter(getActivity(), userNearbyModel, picasso, this);
        this.base_list.setAdapter(estimateListingAdapter);
    }

      @Override
    public void onEstimateClick(UserNearbyModel userNearbyModel, int position) {
              replaceListener.CallReplaceFragment(R.id.conatainer_layout, EstimateListingDetails.newInstance(userNearbyModel,position));

    }
}
