package us.homebuzz.homebuzz.view.adapter.nearby;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;


/**
 * Created by rohitkumar on 21/10/15.
 */
public class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.UserViewHolder> {
    public interface OnItemClickListener {
        void onUserItemClicked(UserNearbyModel userNearbyModel);

        void onUserItemCheckInClicked(UserNearbyModel userNearbyModel);
    }

    private  final String TAG = NearByAdapter.this.getClass().getSimpleName();
    private List<UserNearbyModel> usernearbyCollection;
    private final LayoutInflater layoutInflater;
    private DecimalFormat numberFormatter;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;
    private Context context;

    public NearByAdapter(Context context, Collection<UserNearbyModel> usersnearbyCollection, Picasso picasso) {
        this.nearbyCollection(usersnearbyCollection);
        this.context = context;
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usernearbyCollection = (List<UserNearbyModel>) usersnearbyCollection;

        this.numberFormatter = new DecimalFormat("#,###");
        this.picasso = picasso;
    }

    @Override
    public int getItemCount() {
        return (this.usernearbyCollection != null) ? this.usernearbyCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.listing_detail_new, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        final UserNearbyModel userNearbyModel = this.usernearbyCollection.get(position);
        holder.address.setText(userNearbyModel.getAddress());
        holder.bath.setText(userNearbyModel.getBaths().toString() + " BA");
        holder.bedrooms.setText(userNearbyModel.getBedrooms().toString() + "BR/");
        holder.city.setText(userNearbyModel.getCity());

        if (userNearbyModel.getZip().toString() == null || userNearbyModel.getZip().toString().equals("null")) {
            holder.zip.setText("NA");
        } else {
            holder.zip.setText(userNearbyModel.getZip() + "");
        }

        holder.image.setImageResource(R.drawable.ic_launcher);

        if (userNearbyModel.getPhotos().size() != 0) {
            if (userNearbyModel.getPhotos() != null && !(userNearbyModel.getPhotos().isEmpty()) && (userNearbyModel.getPhotos().size() > 0)) {
                picasso.load(userNearbyModel.getPhotos().get(0)).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder.image);
            }
        }

        holder.checking.setTag(position);
        if (userNearbyModel.getIsEstimated()) {
            LinearLayout mParent = (LinearLayout) holder.checking.getParent();

        } else {
            LinearLayout mParent = (LinearLayout) holder.checking.getParent();

            holder.state.setText(userNearbyModel.getState());
            holder.price.setText("$ " + new DecimalFormat("#,###").format(userNearbyModel.getPrice().intValue()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NearByAdapter.this.onItemClickListener != null) {
                        NearByAdapter.this.onItemClickListener.onUserItemClicked(userNearbyModel);

                    }
                }
            });

            holder.checking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NearByAdapter.this.onItemClickListener.onUserItemCheckInClicked(userNearbyModel);
                }
            });
        }
    }
    public void setUsersCollection(Collection<UserNearbyModel> usernearbyCOllection) {
        this.nearbyCollection(usernearbyCOllection);
        this.usernearbyCollection = (List<UserNearbyModel>) usernearbyCOllection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void nearbyCollection(Collection<UserNearbyModel> usernearbyCOllection) {
        if (usernearbyCOllection == null) {
            throw new IllegalArgumentException("not null");
        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.addresstxt)
        TextView address;
       /* @Bind(R.id.saletxt)
        TextView sale;*/
       @Bind(R.id.bed_txt)
       TextView bedrooms;

        @Bind(R.id.citytxt)
        TextView city;
        @Bind(R.id.statetxt)
        TextView state;
        @Bind(R.id.ziptxt)
        TextView zip;
        @Bind(R.id.bathtxt)
        TextView bath;
       /* @Bind(R.id.feettxt)
        TextView feet;*/
       @Bind(R.id.pricetxt)
        TextView price;
      //  @Bind(R.id.esttext)
      //  TextView estimate;
      /*  @Bind(R.id.ratebar)
        RatingBar ratingBar;*/
        @Bind(R.id.image)
        AutoLoadImageView image;
        @Bind(R.id.checking)
        TextView checking;
      /*  @Bind(R.id.imagesign)
        ImageView imagesign;*/

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @OnClick(R.id.bottom_layout2)
    public void checkIN() {

        Log.d(TAG, "CLicked Checkin Button");
    }

}
