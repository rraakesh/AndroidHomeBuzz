package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.Utility.LocationInterface;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.EstimateAverageModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.presenter.EstimateAveragePresenter;
import us.homebuzz.homebuzz.view.EstimateAverageView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.EstimateListing;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhotoFragment;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;

/**
 * Created by rohitkumar on 28/10/15.
 */
public class ListingFragment extends BaseFragment implements LocationInterface,EstimateAverageView {
    private static final String TAG = ListingFragment.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_NEARBY_LIST_MODEL = "userNearbyModel";
    private UserNearbyModel userNearbyModel;
    @Bind(R.id.last_sold_price_txt)
    TextView lastsoldprice;
    @Bind(R.id.Last_sold_price)
    TextView LastSoldPrice;
    @Bind(R.id.remarks_txt)
    TextView remarks;
    @Bind(R.id.txtEstimate)
    TextView txtEstimate;
    @Bind(R.id.rate_text)
    TextView price;
    @Bind(R.id.bedsbaths)
    TextView bedsbaths;
    @Bind(R.id.zipcode)
    TextView Zipcode;
    @Bind(R.id.address)
    TextView Address;
    @Bind(R.id.lot_acres)
    TextView LotAcres;
    @Bind(R.id.lot_acr)
    TextView lot_acr;
    @Bind(R.id.fireplace)
    TextView Fireplace;
    @Bind(R.id.year_built_text)
    TextView year;
    @Bind(R.id.txtReview)
    TextView txtReview;
    @Bind(R.id.previous_condition_rating)
    RatingBar previous_condition_rating;
    @Bind(R.id.estimate)
    TextView Estimates;
    @Bind(R.id.park_sp)
    TextView park_sp;
    @Bind(R.id.image)
    AutoLoadImageView image;
    @Bind(R.id.Est_Sq_ft)
    TextView Est_Sq_ft;
    @Bind(R.id.park_space)
    TextView park_space;
    @Bind(R.id.fireplaces)
    TextView fireplaces;
    @Bind(R.id.Est_Sq_txt)
    TextView Est_Sq_txt;
    @Bind(R.id.estimate_layout)
    LinearLayout estimate_layout;
    @Bind(R.id.review_layout)
    LinearLayout review_layout;
    @Bind(R.id.lot_Est_Sq_ft)
    TextView lot_Est_Sq_ft;
    @Bind(R.id.lot_Est_Sq_txt)
    TextView lot_Est_Sq_txt;
    @Bind(R.id.photo)
    LinearLayout Photo;
    @Bind(R.id.city_txt)
    TextView city;
    @Bind(R.id.state_txt)
    TextView state;
    @Bind(R.id.zipcode_txt)
    TextView zip;
    @Bind(R.id.update_checkin)
    LinearLayout update_checkin;
    @Bind(R.id.text_checkin)
    TextView text_checkin;
    @Bind(R.id.optional_bed)
    TextView optional_bed;
    @Bind(R.id.optional_bed_txt)
    TextView optional_bed_txt;
    @Bind(R.id.edit_chekin_image)
    ImageView edit_chekin_image;
    @Inject
    public GpsLocation gpsLocation;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    Picasso picasso;
    boolean isgrater;
    private static int LocationdFailCount;
    @Inject
    EstimateAveragePresenter estimateAveragePresenter;

    public static ListingFragment newInstance(UserNearbyModel userAccuracyModel, boolean b) {
        ListingFragment listingFragment = new ListingFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL, userAccuracyModel);
        listingFragment.setArguments(argumentsBundle);
        return listingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view_nearby = inflater.inflate(R.layout.check_in_estimate, container, false);
        ButterKnife.bind(this, view_nearby);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL) != null) {
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL);
         //   Log.d(TAG, "UserModel in Leader Previous" + userNearbyModel.getId());
        }
        return view_nearby;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Property Details");
        this.initialize();
        this.gpsLocation.initializeLocation(this.getActivity());
        this.gpsLocation.setonLocationListener(this);
        this.gpsLocation.callbacksResults();
        this.setupUI();
        //  this.gpsLocation.initializeLocation(this.getActivity());
        this.estimateAveragePresenter.setView(this);
        WeakHashMap<String,String> data=new WeakHashMap<>(1);
        data.put(constants.ESTIMATE_PHOTO_LISTING_ID,String.valueOf(userNearbyModel.getId()));
        this.estimateAveragePresenter.initialize(data);
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
    }

    private void setupUI() {
        Double price1 = 0.0;
        float avg_rating = 0;
//        System.out.println("Photo Count"+userNearbyModel.getEstimates().get(0).getImageUrl().size());
        //  price.setText("$" +  new DecimalFormat("#,###.0").format(userNearbyModel.getPrice()).toString());
        update_checkin.setBackgroundResource(R.drawable.edit_button_bg_gray);

        price.setText("$ " + new DecimalFormat("###,###,###").format(userNearbyModel.getPrice().intValue()));
        bedsbaths.setText(String.valueOf(userNearbyModel.getBedrooms()) + "BR /" + String.valueOf(userNearbyModel.getBaths()) + " BA");
        //  Zipcode.setText(String.valueOf(userNearbyModel.getZip()));
        remarks.setText(userNearbyModel.getRemarks());
        if (userNearbyModel.getStreet() != null) {
            Address.setText(String.valueOf(userNearbyModel.getStreet()));
        } else {
            Address.setText("");
        }
        //Address.append(", " + userNearbyModel.getZip());
        System.out.println("street" + userNearbyModel.getStreet());
        if (userNearbyModel.getCity() != null) {
            city.setText(String.valueOf(userNearbyModel.getCity()));
        } else {
            city.setText("");
        }
        if (userNearbyModel.getState() != null) {
            state.setText(String.valueOf(userNearbyModel.getState() + ","));
        } else {
            state.setText("");
        }
        if (userNearbyModel.getZip() != null) {
            zip.setText(String.valueOf(userNearbyModel.getZip()));
        } else {
            zip.setText("");
        }


        if (userNearbyModel.getAcres() != null && userNearbyModel.getAcres() != 0) {
            LotAcres.setText(String.valueOf(userNearbyModel.getAcres()));
            lot_acr.setText("Lot(Acres)");
        } else {
            LotAcres.setVisibility(View.GONE);
            lot_acr.setVisibility(View.GONE);
        }


        if (userNearbyModel.getEstimatedSquareFeet() != null && (Double) userNearbyModel.getEstimatedSquareFeet() != 0) {
            Est_Sq_ft.setText(String.valueOf(((Double) userNearbyModel.getEstimatedSquareFeet()).intValue()));
            Est_Sq_txt.setText("Est.Sq.Ft");
        } else {
            Est_Sq_ft.setVisibility(View.GONE);
            Est_Sq_txt.setVisibility(View.GONE);

        }
        if (userNearbyModel.getLotSqftApprox() != null && (Double) userNearbyModel.getLotSqftApprox() != 0) {
            lot_Est_Sq_ft.setText(String.valueOf(((Double) userNearbyModel.getLotSqftApprox()).intValue()));
            lot_Est_Sq_txt.setText("Lot.Sq.Ft");
        } else {
            lot_Est_Sq_ft.setVisibility(View.GONE);
            lot_Est_Sq_txt.setVisibility(View.GONE);

        }
        if (userNearbyModel.getLastSoldPrice() != null && (Double) userNearbyModel.getLastSoldPrice() != 0) {
            lastsoldprice.setText("$ " + new DecimalFormat("###,###,###").format(((Double) userNearbyModel.getLastSoldPrice()).intValue()));
            LastSoldPrice.setText("Last Sold Price");
        } else {
            lastsoldprice.setVisibility(View.GONE);
            LastSoldPrice.setVisibility(View.GONE);
        }

        if(userNearbyModel.getOptionalBedrooms()!=null && (Double)userNearbyModel.getOptionalBedrooms()!=0) {
            optional_bed_txt.setText(String.valueOf(((Double)userNearbyModel.getOptionalBedrooms()).intValue()));
            optional_bed.setText("Optional Bedrooms");



        }
        else {
            optional_bed_txt.setVisibility(View.GONE);
            optional_bed.setVisibility(View.GONE);
        }


        if (userNearbyModel.getFireplaces() != null && (Integer) userNearbyModel.getFireplaces() != 0) {
            Fireplace.setText(String.valueOf(userNearbyModel.getFireplaces()));
            fireplaces.setText("Fireplaces");
        } else {
            Fireplace.setVisibility(View.GONE);
            fireplaces.setVisibility(View.GONE);
        }
        if (userNearbyModel.getGarage() != null && (Double) userNearbyModel.getGarage() != 0) {
            park_space.setText(String.valueOf(String.valueOf(((Double) userNearbyModel.getGarage()).intValue())));
            System.out.println("space" + String.valueOf(userNearbyModel.getParkingSpacesTotal()));
            park_sp.setText("Garage Parking Spaces");
        } else {
            park_space.setVisibility(View.GONE);
            park_sp.setVisibility(View.GONE);
        }
        year.setText(userNearbyModel.getYearBuilt().toString());
        image.setImageUrl(String.valueOf(userNearbyModel.getPhotos()));
        if (userNearbyModel.getPhotos().size() > 0) {
            picasso.load(userNearbyModel.getPhotos().get(0)).into(image);
        } else {
            image.setImageResource(R.drawable.ic_launcher);
        }
        if (userNearbyModel.getEstimates() != null && userNearbyModel.getEstimates().size() > 0) {

            int i;
            for (i = 0; i < userNearbyModel.getEstimates().size(); i++) {
                //Log.e("avg rating before ", String.valueOf(userNearbyModel.getEstimates().get(i).getLocationRating()));
                price1 += (null != userNearbyModel.getEstimates().get(i).getPrice()) ? userNearbyModel.getEstimates().get(i).getPrice() : 0;
                //  avg_rating += (userNearbyModel.getEstimates().get(i).getLocationRating() != null && userNearbyModel.getEstimates().get(i).getConditionRating() != null) ? (userNearbyModel.getEstimates().get(i).getLocationRating() + userNearbyModel.getEstimates().get(i).getConditionRating()) / 2 : 0;
                avg_rating += (userNearbyModel.getEstimates().get(i).getLocationRating() != null ? (userNearbyModel.getEstimates().get(i).getLocationRating()) : 0);
            }

            Double newPrice = price1 / i;
            float newAvgRating = avg_rating / i;
            Log.e("avg rating is ", String.valueOf(newAvgRating));

            previous_condition_rating.setRating(newAvgRating);
            Estimates.setText(userNearbyModel.getEstimates_count() + " Estimate");
            txtReview.setText(userNearbyModel.getCheckin_count() + " Check-ins");
        } else {
            if (userNearbyModel.getPrice() != null)
                txtEstimate.setText("$ " + new DecimalFormat("###,###,###").format(userNearbyModel.getPrice().intValue()));

                Estimates.setText("0 Estimate");
                txtReview.setText("0 Review");

        }

        updateCheckinView();
    }

    private void updateCheckinView(){
        if(userNearbyModel!=null) {
            for (int i = 0; i < userNearbyModel.getEstimates().size(); i++) {
                if (userNearbyModel.getEstimates().get(i).getUserId() == sharedPreferences.getInt(constants.KEY_ID, 0)) {
                    update_checkin.setBackgroundResource(R.drawable.edit_button_bg_gray);
                    text_checkin.setText("Edit My Check In");
                    edit_chekin_image.setImageResource(R.drawable.edit_message);
                }
            }
        }
    }
    @OnClick(R.id.estimate_layout)
    public void EstimateClick() {
        if (userNearbyModel.getEstimates() != null && !userNearbyModel.getEstimates_count().equalsIgnoreCase("0")) {
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, EstimateListing.newInstance(userNearbyModel));
        }
    }

    @OnClick(R.id.review_layout)
    public void reviewClick() {
        if (userNearbyModel.getEstimates() != null && !userNearbyModel.getCheckin_count().equalsIgnoreCase("0")) {
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, EstimateListing.newInstance(userNearbyModel));
        }
    }


    @OnClick(R.id.image)
    public void imageClick() {
        if (userNearbyModel.getPhotos().size() > 0)
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingPhotoFragment.newInstance(userNearbyModel, true));


    }

    @OnClick(R.id.photo)
    public void photoClick() {
        if (userNearbyModel.getPhotos().size() > 0)
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingPhotoFragment.newInstance(userNearbyModel, true));


    }


   /* private void onPhotoClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());
    }*/

    //  edit_button_bg
    @OnClick(R.id.update_checkin)
    public void updateCheckin() {
        if (userNearbyModel != null && !isgrater) {

            replaceListener.CallReplaceFragment(R.id.conatainer_layout, UpdateCheckInEstimateFragment.newInstance1(userNearbyModel, true));
        } else {
            showDialog("You must be at the Property to check in");
        }
    }

    @Override
    public void onLocation(Location location) {
        //  Log.d("latitude", String.valueOf(location.getLatitude()));
        Double distance = distFrom((float) location.getLatitude(), (float) location.getLongitude(), userNearbyModel.getLat(), userNearbyModel.getLon());
        //1 km Radius check for the Checkin
        if (distance.intValue() < 1000) {
            isgrater = false;
            update_checkin.setBackgroundResource(R.drawable.edit_button_bg);
            update_checkin.requestFocus();
        } else {
            isgrater = true;
            update_checkin.setBackgroundResource(R.drawable.edit_button_bg_gray);

        }
        System.out.println("Distance in Listingfragment " + distance.intValue());
    }

    @Override
    public void onLocationFailed() {
        // TODO HANDLE THE Better Exponetial Back Off
        new Handler().postDelayed(new Runnable() {
            public void run() {
                // try your request here; if it fails, then repost:
                if (LocationdFailCount < 3) {
                    gpsLocation.callbacksResults();
                } else {
                    showToastMessage("Fail To get the Location");
                }
            }
        },  5 * 1000);
        LocationdFailCount++;

    }

    @Override
    public void onConnectionSuspended() {

    }

    //Distance Between two Lat lon it return the VALUE IN meter
    public double distFrom(float lat1, float lng1, Double lat2, Double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist =  (earthRadius * c);
        return dist;
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void viewGetEstimateAverage(EstimateAverageModel estimateAverageModel) {
        if(estimateAverageModel!=null) {
            txtEstimate.setText("$ " + new DecimalFormat("###,###,###").format(Double.valueOf(estimateAverageModel.getPrice())));
         //   Log.d(TAG,"Price Average"+estimateAverageModel.getPrice());
        }
      //  Log.d(TAG," Average"+estimateAverageModel);
    }

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }

}

