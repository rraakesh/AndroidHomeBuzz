package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UpdateZipcodeModel;

/**
 * Created by amit.singh on 12/15/2015.
 */
public interface ZipUpdateView extends LoadDataView {
    void renderAfterUpdateZip(UpdateZipcodeModel updateZipcodeModel);
}
