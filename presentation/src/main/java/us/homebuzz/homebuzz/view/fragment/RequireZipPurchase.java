package us.homebuzz.homebuzz.view.fragment;

/**
 * Created by amit.singh on 12/15/2015.
 */
public interface RequireZipPurchase {
    void initPurchase();
}
