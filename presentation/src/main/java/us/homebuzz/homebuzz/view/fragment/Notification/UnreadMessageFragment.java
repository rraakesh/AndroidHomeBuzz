package us.homebuzz.homebuzz.view.fragment.Notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.presenter.AllMessagesPresenter;
import us.homebuzz.homebuzz.presenter.MessageReadPrsenter;
import us.homebuzz.homebuzz.view.AllMessageView;
import us.homebuzz.homebuzz.view.MessageReplyView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.Message.AllMessageAdapter;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.notification.UnMessageAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.messages.selectFriends.NewSendMessage;
import us.homebuzz.homebuzz.view.fragment.messages.selectFriends.SelectFriend;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class UnreadMessageFragment extends BaseFragment implements AllMessageView ,MessageReplyView,SelectFriend.SelectedFriend {

    private static final String ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL = "UserUnReadMessagesModel";


    public static UnreadMessageFragment newInstance(ArrayList<UserUnreadMessagesModel> UserUnreadMessagesModel) {
        UnreadMessageFragment notificationFragment = new UnreadMessageFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL, UserUnreadMessagesModel);
        notificationFragment.setArguments(argumentsBundle);
        return notificationFragment;
    }

    @Bind(R.id.search)
    EditText search;

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;
    private UsersLayoutManager usersLayoutManager;
    private ArrayList<UserUnreadMessagesModel> userUnreadMessagesModelArrayList;
    UnMessageAdapter msgAdapter;
    @Inject
    AllMessagesPresenter allMessagesPresenter;
    private AllMessageAdapter allMessageAdapter;
    @Inject
    MessageReadPrsenter messageReadPrsenter;

    @Inject
    Constants constants;
    ArrayList<UserAllMessagesModel> mdata = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_base = inflater.inflate(R.layout.base_recycler_reviews, container, false);
        ButterKnife.bind(this, list_base);
        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL) != null)
            userUnreadMessagesModelArrayList = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_UNREAD_MESSAGE_MODEL);

        return list_base;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Messages");
        this.initialize();
        this.setupUI();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        allMessagesPresenter.setView(this);
        messageReadPrsenter.setView(this);
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(this.getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);


        if (userUnreadMessagesModelArrayList != null && userUnreadMessagesModelArrayList.size() > 0) {
            this.msgAdapter = new UnMessageAdapter(getActivity(), new ArrayList<UserUnreadMessagesModel>(), picasso);
            this.msgAdapter.setOnItemClickListener(onItemClickListener);
            this.base_list.setAdapter(msgAdapter);
            this.msgAdapter.setUsersCollection(userUnreadMessagesModelArrayList);
        } else {
            allMessagesPresenter.initialize();
            this.allMessageAdapter = new AllMessageAdapter(getActivity(), new ArrayList<UserAllMessagesModel>(), picasso);
            this.allMessageAdapter.setOnItemClickListener(onItemClickListenerAllMessages);
            this.base_list.setAdapter(allMessageAdapter);
        }

    }

    private AllMessageAdapter.OnItemClickListener onItemClickListenerAllMessages =
            new AllMessageAdapter.OnItemClickListener() {
                @Override
                public void onMessItemClicked(UserAllMessagesModel userAllMessagesModel) {
                    if (UnreadMessageFragment.this.allMessagesPresenter != null && userAllMessagesModel != null) {
                        UnreadMessageFragment.this.allMessagesPresenter.onUserClicked(userAllMessagesModel);

                    }

                }


            };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    searchList(s);
                } else {
                    UnreadMessageFragment.this.allMessageAdapter.setUsersCollection(mdata);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    private void searchList(CharSequence str){
        Collection<UserAllMessagesModel> tempArrayList = new ArrayList<>();
        if(str.length() > 0){
            System.out.println("size mdata" + mdata.size());
            for(UserAllMessagesModel userallEstimateModel: mdata){
                if((userallEstimateModel.getSender().getName()!=null&& userallEstimateModel.getSender().getName().toUpperCase().toString().contains(str))
                        ||(userallEstimateModel.getSender().getName()!=null&&userallEstimateModel.getSender().getName().toLowerCase().toString().contains(str))
                        ){
                    tempArrayList.add( userallEstimateModel);
                }
                UnreadMessageFragment.this.allMessageAdapter.setUsersCollection( tempArrayList );
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.allMessagesPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.allMessagesPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.allMessagesPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.message_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_newMessage:
                //   android.R.style.Theme_Light_NoTitleBar_Fullscreen)
                this.onDialogClick(SelectFriend.newInstance());
                // replaceListener.CallReplaceFragment(R.id.conatainer_layout, SelectFriend.newInstance());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderUserAllMessageList(Collection<UserAllMessagesModel> userAllMessagesModels) {
        Log.d(TAG, "The All Messages List " + userAllMessagesModels.size());
        //TODO Set the ADAPTER OF RECYCLER VIEW HERE
        mdata= (ArrayList<UserAllMessagesModel>) userAllMessagesModels;
        this.allMessageAdapter.setUsersCollection(userAllMessagesModels);
    }


    @Override
    public void viewUser(UserAllMessagesModel userAllMessagesModel) {
        Log.d(TAG, "The Clicked UserID " + userAllMessagesModel.getId());
       /* UserUnreadMessagesModel allMessagesModel =new UserUnreadMessagesModel();
        allMessagesModel.setId(userAllMessagesModel.getId());
        allMessagesModel.setListingId(userAllMessagesModel.getListingId());
        allMessagesModel.setStatus(userAllMessagesModel.getStatus());
        allMessagesModel.setBody(userAllMessagesModel.getBody());
        allMessagesModel.setCreatedAt(userAllMessagesModel.getCreatedAt());
        // allMessagesModel.set(allMessages.getCreatedAt());
        if(userAllMessagesModel.getSender()!=null) {
            UserUnreadMessagesModel.Sender sender = allMessagesModel.new Sender();
            sender.setId(userAllMessagesModel.getSender().getId());
            sender.setEmail(userAllMessagesModel.getSender().getEmail());
            sender.setName(userAllMessagesModel.getSender().getName());
           // sender.setProfilePic(userAllMessagesModel.getSender().getProfile_pic());
//            System.out.println("Sender in Data" + sender.getId());
            allMessagesModel.setSender(sender);
        }*/
       /* if(userAllMessagesModel.getRecipient()!=null) {
            UserUnreadMessagesModel.Recipient recipient = allMessagesModel.new Recipient();
            recipient.setId(allMessages.getRecipient().getId());
            recipient.setEmail(allMessages.getRecipient().getEmail());
            recipient.setName(allMessages.getRecipient().getName());
            allMessagesModel.setRecipient(recipient);
        }
*/

        // this.onDialogClick(SelectFriend.newInstance());
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UnReadMessageDetails.newInstance( transFromUserAllMessagesModel(userAllMessagesModel)));
    }

    //Transform from  UserAllMessagesModel to UserUnreadMessagesModel
    private UserUnreadMessagesModel transFromUserAllMessagesModel(UserAllMessagesModel userAllMessagesModel){
        UserUnreadMessagesModel allMessagesModel =new UserUnreadMessagesModel();
        allMessagesModel.setId(userAllMessagesModel.getId());
        allMessagesModel.setListingId(userAllMessagesModel.getListingId());
        allMessagesModel.setStatus(userAllMessagesModel.getStatus());
        allMessagesModel.setBody(userAllMessagesModel.getBody());
        allMessagesModel.setCreatedAt(userAllMessagesModel.getCreatedAt());
        // allMessagesModel.set(allMessages.getCreatedAt());
        if(userAllMessagesModel.getSender()!=null) {
            UserUnreadMessagesModel.Sender sender = allMessagesModel.new Sender();
            sender.setId(userAllMessagesModel.getSender().getId());
            sender.setEmail(userAllMessagesModel.getSender().getEmail());
            sender.setName(userAllMessagesModel.getSender().getName());
            // sender.setProfilePic(userAllMessagesModel.getSender().getProfile_pic());
//            System.out.println("Sender in Data" + sender.getId());
            allMessagesModel.setSender(sender);
        }

        return allMessagesModel;
    }
    private UnMessageAdapter.OnItemClickListener onItemClickListener = new UnMessageAdapter.OnItemClickListener() {
        @Override
        public void UnReadMessageClick(UserUnreadMessagesModel unreadMessagesModels) {
            WeakHashMap<String, String> data = new WeakHashMap<>(1);
            data.put(constants.MESSAGE_ID,String.valueOf(unreadMessagesModels.getId()));
            messageReadPrsenter.initialize(data);
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, UnReadMessageDetails.newInstance(unreadMessagesModels));

        }
    };


    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }

    @Override
    public void renderAfterMessageReply(UserMessageReplyModel userMessageReplyModel) {

    }
    private void onDialogClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment) {
            dialogFragment.setStyle(dialogFragment.STYLE_NORMAL,R.style.AppTheme);
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());
        }
    }

    @Override
    public void onFriendClick(UserFriendFollowedModel userFriendFollowedModel) {
        Log.e("The Clicked List id ",""+ userFriendFollowedModel.getId());
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, NewSendMessage.newInstance(userFriendFollowedModel));
    }

    @Override
    public void onFriendSearchClick() {
        //TODO GOTO Search People Fragment
        //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, NewSendMessage.newInstance(userFriendFollowedModel));
    }
}
