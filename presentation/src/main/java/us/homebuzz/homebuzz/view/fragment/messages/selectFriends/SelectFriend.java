package us.homebuzz.homebuzz.view.fragment.messages.selectFriends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.presenter.FriendsFollowersPresenter;
import us.homebuzz.homebuzz.view.UserFollowersView;
import us.homebuzz.homebuzz.view.adapter.Message.SelectdFriendAdapter;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.fragment.Notification.UnreadMessageFragment;

/**
 * Created by amit.singh on 12/4/2015.
 */
public class SelectFriend extends DialogFragment implements UserFollowersView  {
    public static String TAG = SelectFriend.class.getClass().getSimpleName();

    public static SelectFriend newInstance() {
        SelectFriend createMessage = new SelectFriend();
        Bundle argumentsBundle = new Bundle();
        createMessage.setArguments(argumentsBundle);
        return createMessage;
    }

    public interface SelectedFriend{
        void onFriendClick(UserFriendFollowedModel userFriendFollowedModel);
        void onFriendSearchClick();
    }

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;
    @Inject
    FriendsFollowersPresenter friendsFollowersPresenter;
    private SelectedFriend selectedFriend;
    private UsersLayoutManager usersLayoutManager;
    private SelectdFriendAdapter friendFollowerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View friendsFollowersList = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, friendsFollowersList);

        return friendsFollowersList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (this.getParentFragment() instanceof UnreadMessageFragment) {
            selectedFriend = (UnreadMessageFragment) this.getParentFragment();
        }
        System.out.println("Printing the 2nd"+selectedFriend);
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        friendsFollowersPresenter.setView(this);
        friendsFollowersPresenter.initialize();


    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.friendFollowerAdapter = new SelectdFriendAdapter(getActivity(), new ArrayList<UserFriendFollowedModel>(), picasso);
        this.friendFollowerAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(friendFollowerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.friendsFollowersPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.friendsFollowersPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.friendsFollowersPresenter.destroy();
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderUserFollowersList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection) {
        Log.d(TAG, "The Followed List " + userFriendFollowedModelCollection.size());
        this.friendFollowerAdapter.setUsersCollection(userFriendFollowedModelCollection);
        this.friendFollowerAdapter.notifyDataSetChanged();

    }

    private SelectdFriendAdapter.OnItemClickListener onItemClickListener = new SelectdFriendAdapter.OnItemClickListener() {
        @Override
        public void onFollowedClicked(UserFriendFollowedModel userFriendFollowedModel) {
            if (SelectFriend.this.friendsFollowersPresenter != null && userFriendFollowedModel != null) {
                SelectFriend.this.friendsFollowersPresenter.onUserClicked(userFriendFollowedModel);
            }
        }
    };


    @Override
    public void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection) {
        String id = String.valueOf(userFriendFollowedModelCollection.getId());
        Log.e("The Followed List id ", id);
        selectedFriend.onFriendClick(userFriendFollowedModelCollection);
        //replaceListener.CallReplaceFragment(R.id.conatainer_layout, NewSendMessage.newInstance(userFriendFollowedModelCollection));
        this.dismiss();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new UsersLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);

    }
    private void onDialogClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());
    }
    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }


}
