package us.homebuzz.homebuzz.view.adapter.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by abhishek.singh on 11/23/2015.
 */
public class FriendFollowersAdapter extends RecyclerView.Adapter<FriendFollowersAdapter.UserViewHolder> {

    public static String TAG = FriendFollowAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        public void onFollowedClicked(UserFriendFollowedModel userFriendFollowedModel);
    }

    private List<UserFriendFollowedModel> userModelCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;

    public FriendFollowersAdapter(Context context, Collection<UserFriendFollowedModel> usersCollection, Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.userModelCollection = (List<UserFriendFollowedModel>) usersCollection;
        this.picasso = picasso;
        Log.d(TAG, "ADAPTER PICASSO" + picasso);
    }

    @Override
    public int getItemCount() {
        return (this.userModelCollection != null) ? this.userModelCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.friends_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserFriendFollowedModel userModel = this.userModelCollection.get(position);
        holder.followedName.setText((userModel.getName() != null) ? userModel.getName() : "");
        Log.d(TAG, "Followed Name" + userModel.getName());
        Log.e(TAG, "Followed id" + userModel.getId());
        Log.d(TAG, "Profile Pics" + userModel.getProfilePic());
        Log.d(TAG, "Profile Pics" + userModel);


        if (userModel.getProfilePic() != null) {
            if (userModel.getProfilePic() != null && !(userModel.getProfilePic().isEmpty()) && !(userModel.getProfilePic().matches("null"))) {
                picasso.load(String.valueOf(userModel.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(holder.profilePic);

                Log.d(TAG, "Profile Pics" + userModel.getProfilePic());
            } else {
                holder.profilePic.setImagePlaceHolder(R.drawable.ic_launcher);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "Friend list  before clicked");
                if (FriendFollowersAdapter.this.onItemClickListener != null) {
                    Log.e("click", "Friend list clicked");
                    onItemClickListener.onFollowedClicked(userModel);
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserFriendFollowedModel> userAllMessagesModels) {
        this.validateUsersCollection(userModelCollection);
        this.userModelCollection = (List<UserFriendFollowedModel>) userAllMessagesModels;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<UserFriendFollowedModel> userModelCollection) {
        if (userModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.followed_name)
        TextView followedName;
        @Bind(R.id.profile_pic)
        AutoLoadImageView profilePic;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

