package us.homebuzz.homebuzz.view.adapter.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by abhishek.singh on 11/19/2015.
 */
public class UnMessageAdapter extends RecyclerView.Adapter<UnMessageAdapter.UserViewHolder> {
    public static String TAG = JustSoldAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        void UnReadMessageClick(UserUnreadMessagesModel unreadMessagesModels);
    }

    @Inject
    Picasso picasso;
    private List<UserUnreadMessagesModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener1;

    public UnMessageAdapter(Context context, Collection<UserUnreadMessagesModel> usersCollection, Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserUnreadMessagesModel>) usersCollection;
        this.picasso = picasso;
    }

    @Override
    public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.notification_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserUnreadMessagesModel userUnreadMessagesModel = this.usersCollection.get(position);
        holder.Listing_title.setText(userUnreadMessagesModel.getSender().getName());
        holder.leader_accuracy.setText(userUnreadMessagesModel.getBody());
        holder.row_second.setVisibility(View.VISIBLE);
        holder.row_second.setText(userUnreadMessagesModel.getCreatedAt());
//        if (null != userUnreadMessagesModel && null != userUnreadMessagesModel.getPhotos()) {
//            System.out.println(TAG + "Pic" + String.valueOf(userUnreadMessagesModel.getPhotos()));
//            picasso.load(String.valueOf(userUnreadMessagesModel.getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
//        } else {
//            //TODO Place A DEFAULT IMAGE
        if (userUnreadMessagesModel.getSender().getProfile_pic() != null)
            picasso.load(userUnreadMessagesModel.getSender().getProfile_pic()).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
        else
            holder._image.setImagePlaceHolder(R.drawable.holder_home);
        //   }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UnMessageAdapter.this.onItemClickListener1 != null) {
                    UnMessageAdapter.this.onItemClickListener1.UnReadMessageClick(userUnreadMessagesModel);
                   usersCollection.remove(usersCollection.get(position));
                }
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserUnreadMessagesModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserUnreadMessagesModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener1) {
        this.onItemClickListener1 = onItemClickListener1;
    }

    private void validateUsersCollection(Collection<UserUnreadMessagesModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.Listing_title)
        TextView Listing_title;
        @Bind(R.id.row_second)
        TextView row_second;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id._image)
        AutoLoadImageView _image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
