/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package us.homebuzz.homebuzz.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import us.homebuzz.homebuzz.di.HasComponent;

/**
 * Base {@link android.app.Fragment} class for every fragment in this application.
 */
public abstract class BaseFragment extends Fragment {
    // Arbitrary value; set it to some reasonable default
    public static String TAG = BaseFragment.class.getClass().getSimpleName();
    protected ReplaceFragment replaceListener;//listener for replacing the Fragment from the Fragment to Activity

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
          setRetainInstance(true);
    }

    /**
     * Shows a {@link android.widget.Toast} message.
     *
     * @param message An string representing a message to be shown.
     */
    protected void showToastMessage(String message) {
        if(this.getActivity()!=null)
        Toast.makeText(this.getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
   /*   Snackbar.make(this.getView(), message, Snackbar.LENGTH_LONG)
              .setAction("Dismiss", null).show();*/
    }

    public void showDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Homebuzz");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("ok", null);
        alertDialog.show();
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    /**
     * Control example Calling a fragment to replace the Fragment Delegate the call to Activity
     *
     * @see us.homebuzz.homebuzz.view.activity.BaseActivity
     * Better use the  ReplaceFragment Listner to replce the Fragment from fragment to Activity
     * EX:-
     * ((MainActivity)getActivity()).replaceFragment(R.id.conatainer_layout, new LandingFragment());
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        try {
            if (context instanceof Activity) {
                activity = (Activity) context;
            }
            replaceListener = (ReplaceFragment) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            throw new ClassCastException(activity.toString() + "Must implement the ReplaceFragment listener");
        }
    }


}
