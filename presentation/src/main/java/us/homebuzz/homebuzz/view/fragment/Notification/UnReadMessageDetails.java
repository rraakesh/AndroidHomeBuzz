package us.homebuzz.homebuzz.view.fragment.Notification;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.presenter.FollowUnfollowPresenter;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.view.FollowUnfollowView;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.messages.CreateMessage;

/**
 * Created by abhishek.singh on 11/30/2015.
 */
public class UnReadMessageDetails extends BaseFragment implements FollowUnfollowView, UserProfileView {
    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.txtAppName)
    TextView txtAppName;
    @Bind(R.id.txtFollow)
    TextView txtFollow;
    @Bind(R.id.reply_send)
    TextView reply_send;
    @Bind(R.id.delete_cancel)
    TextView delete_cancel;
    @Bind(R.id.txtBackMsg)
    TextView txtBackMsg;
    @Bind(R.id.edbody)
    EditText edbody;
    @Bind(R.id.llFollow)
    LinearLayout llFollow;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Constants constants;
    @Inject
    FollowUnfollowPresenter followUnfollowPresenter;
    @Inject
    UserProfilePresenter userProfilePresenter;
    @Inject
    Picasso picasso;
    @Inject
    SharedPreferences sharedPreferences;
    private UserProfileModel userProfileModel;
    private boolean check = true;
    private static final String ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL = "UserMessagesModel";
    private static final String ARGUMENT_KEY_USER_ALL_MESSAGE_MODEL = "UserMessagesModel";
    UserUnreadMessagesModel unreadMessagesModels;

    public static UnReadMessageDetails newInstance(UserUnreadMessagesModel unreadMessagesModels) {
        UnReadMessageDetails UnMessageDetails = new UnReadMessageDetails();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL, unreadMessagesModels);
        UnMessageDetails.setArguments(argumentsBundle);
        return UnMessageDetails;
    }
    public static UnReadMessageDetails newInstance(UserAllMessagesModel unreadMessagesModels) {
        UnReadMessageDetails UnMessageDetails = new UnReadMessageDetails();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_ALL_MESSAGE_MODEL, unreadMessagesModels);
        UnMessageDetails.setArguments(argumentsBundle);
        return UnMessageDetails;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View unreadMEssageDetails = inflater.inflate(R.layout.read_and_reply_message, container, false);
        ButterKnife.bind(UnReadMessageDetails.this, unreadMEssageDetails);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL) != null) {
            this.unreadMessagesModels = this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL);
        }
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_ALL_MESSAGE_MODEL) != null) {
            this.unreadMessagesModels = this.getArguments().getParcelable(ARGUMENT_KEY_USER_ALL_MESSAGE_MODEL);
        }
        return unreadMEssageDetails;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Messages");
        this.initialize();
        setUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        followUnfollowPresenter.setView(this);
        userProfilePresenter.setView(this);
        Log.e("UserId", unreadMessagesModels.getSender().getId().toString());
        userProfilePresenter.initialize(unreadMessagesModels.getSender().getId());
    }

    private void setUI() {
        if (unreadMessagesModels.getBody() != null) {
            edbody.setEnabled(false);
            edbody.setText(unreadMessagesModels.getBody());
        }
    }


    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        reply_send.setClickable(false);
        delete_cancel.setClickable(false);
        txtBackMsg.setClickable(false);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        reply_send.setClickable(true);
        delete_cancel.setClickable(true);
        txtBackMsg.setClickable(true);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        showToastMessage("Something went wrong,Please try again");
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.llFollow)
    public void btnFollowClick() {
        followUnfollowPresenter.initialize(unreadMessagesModels.getSender().getId());
    }

    @Override
    public void viewUser(FollowUnfollowModel FollowUnfollowModel) {

    }

    @Override
    public void renderFollowUnfollow(FollowUnfollowModel FollowUnfollowModel) {
        if (FollowUnfollowModel.getFollowed()) {
            llFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            txtFollow.setText("Un-Follow");
            userProfileModel.setFollowed(true);
        } else {
            llFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
            userProfileModel.setFollowed(false);
        }
    }

    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        this.userProfileModel = userProfileModel;
        txtProfileName.setText(userProfileModel.getName());
        if (userProfileModel.getFollowed()) {
            llFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            txtFollow.setText("Un-Follow");
        } else {
            llFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
        }
        if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().equals("null"))
            picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
    }

    @OnClick(R.id.reply_send)
    public void setBtnSend() {
        hideKeyboard();
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, CreateMessage.newInstance(userProfileModel));
    }

    @OnClick(R.id.delete_cancel)
    public void setBtnDelete() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.txtBackMsg)
    public void setBtnBack() {
        getActivity().onBackPressed();
    }
}

