package us.homebuzz.homebuzz.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.data.entity.pojo.profile.SelectUser;

/**
 * Created by rohitkumar on 1/12/15.
 */
public class SelectUserAdapter extends BaseAdapter {

    public List<SelectUser> _data;
    private ArrayList<SelectUser> arraylist;
    Context _ctx;
    ViewHolder viewHolder;
    public List<String> selectedUser = new ArrayList<String>();

    @Bind(R.id.name)
    LinearLayout linearLayout;

    public SelectUserAdapter(List<SelectUser> selectUsers, Context context) {
        _data = selectUsers;
        _ctx = context;
        this.arraylist = new ArrayList<SelectUser>();
        this.arraylist.addAll(_data);

    }

    @Override
    public int getCount() {

        return (_data != null && _data.size() > 0) ? _data.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return _data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        /*View view = convertView;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
            view = li.inflate(R.layout.contact, viewGroup,false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.nametext);
            viewHolder.check = (CheckBox) view.findViewById(R.id.check);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

      final SelectUser data = (SelectUser) _data.get(i);
      viewHolder.title.setText(data.getName());
      viewHolder.check.setChecked(data.getCheckedBox());

      viewHolder.check.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              CheckBox checkBox = (CheckBox) view;

              if (checkBox.isChecked()) {
                  data.setCheckedBox(true);
                  selectedUser.add(data.getPhone());

              } else {
                  selectedUser.remove(data.getPhone());
                  data.setCheckedBox(false);
              }

              System.out.println("List_data" + data.getPhone());
          }
      });
        view.setTag(data);*/

        ViewHolder viewHolder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.contact, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.nametext);
            viewHolder.check = (CheckBox) convertView.findViewById(R.id.check);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.check.setTag(position);
        final SelectUser data = (SelectUser) _data.get(position);
        viewHolder.title.setText(data.getName());
        viewHolder.check.setChecked(data.getCheckedBox());

        viewHolder.check.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                int position = (int) cb.getTag();

                if (cb.isChecked()) {
                    data.setCheckedBox(true);
                    selectedUser.add(_data.get(position).getPhone());

                } else {
                    selectedUser.remove(_data.get(position).getPhone());
                    data.setCheckedBox(false);
                }
            }
        });

        return convertView;


    }

    public void filter(String charText) {
        //charText = charText.toLowerCase(Locale.getDefault());
        if (charText.length() > 0) {
            //_data.clear();
            System.out.println("Data_recived" + charText);
            System.out.println("array" + arraylist);
            for (SelectUser wp : arraylist) {
                if (wp.getName().toLowerCase().contains(charText)) {
                    System.out.println("Char rec" + wp.getName().toLowerCase());
                }
            }

            //_data.addAll(arraylist);
        } else {
            // _data.clear();
            System.out.println("Char rec" + charText);
        }

        this.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView title;
        CheckBox check;

    }

}