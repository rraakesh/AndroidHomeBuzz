package us.homebuzz.homebuzz.view.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by amit.singh on 10/27/2015.
 */
public interface ToolBarController {
    void NavBarIconController(Fragment fragmentName);
}
