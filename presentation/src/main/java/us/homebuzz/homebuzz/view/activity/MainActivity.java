package us.homebuzz.homebuzz.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.LocationInterface;
import us.homebuzz.homebuzz.Utility.PermissionUtil;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.DaggerMainActivityComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.di.modules.PreviousEstimateModule;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.di.modules.UpdateCheckingModule;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.presenter.UserJustListedPresenter;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.view.DrawerProfileBus;
import us.homebuzz.homebuzz.view.NotificationUpdateBus;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireCamera;
import us.homebuzz.homebuzz.view.RequireContact;
import us.homebuzz.homebuzz.view.RequireLocation;
import us.homebuzz.homebuzz.view.RequireStorage;
import us.homebuzz.homebuzz.view.ZipPurchase;
import us.homebuzz.homebuzz.view.activity.LoginSignUp.Login;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;
import us.homebuzz.homebuzz.view.fragment.Notification.NotificationFragment;
import us.homebuzz.homebuzz.view.fragment.RequireZipPurchase;
import us.homebuzz.homebuzz.view.fragment.ToolBarController;
import us.homebuzz.homebuzz.view.fragment.addlisting._Nov.AddListing;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoard;
import us.homebuzz.homebuzz.view.fragment.setting.AutoLogout;
import us.homebuzz.homebuzz.view.fragment.setting.UserProfileFragment;
import us.homebuzz.homebuzz.view.fragment.setting.UserSettingsFragment;
import us.homebuzz.homebuzz.view.fragment.setting.ZipCodePuchaseFragment1;
import us.homebuzz.homebuzz.zipUtil.IabHelper;
import us.homebuzz.homebuzz.zipUtil.IabResult;
import us.homebuzz.homebuzz.zipUtil.Inventory;
import us.homebuzz.homebuzz.zipUtil.Purchase;

/**
 * Created by amit.singh on 9/21/2015...
 */

/**
 * This Class Is Entry Point of the Application Logic
 */
public class MainActivity extends BaseActivity implements ToolBarController, LocationInterface, HasComponent<MainActivityComponent>,
        ActivityCompat.OnRequestPermissionsResultCallback, RequireLocation, RequireCamera, RequireContact, RequireStorage,RequireZipPurchase,
        NavigationView.OnNavigationItemSelectedListener,DrawerProfileBus,NotificationUpdateBus {

    public static Intent getCallingIntent(Context context) {
        Intent callingIntent = new Intent(context, MainActivity.class);
        return callingIntent;
    }

    private static final String TAG = MainActivity.class.getSimpleName();
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.conatainer_layout)
    FrameLayout container_layout;
    @Bind(R.id.toolbar)
    public Toolbar toolbar;
    private MainActivityComponent mainActivityComponent;
    private PermissionBus permissionBus;
    private ZipPurchase ZipPurchase;
    @Inject
    UserJustListedPresenter userJustListedPresenter;
    private ArrayList<UserJustSoldModel> userJustSoldModelArrayList;
    private ArrayList<UserUnreadMessagesModel> userUnreadMessagesModelArrayList;
    private ArrayList<UserJustListedModel> userJustListedModelArrayList;
    /**
     * Permissions required to read and write contacts. Used by the {@link us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends}.
     */
    private static final int REQUEST_LOCATION = 2;
    private static final int REQUEST_STORAGE = 3;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.WRITE_CONTACTS};
    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_CAMERA = 0;

    /**
     * Id to identify a contacts permission request.
     */
    private static final int REQUEST_CONTACTS = 1;

    boolean mSubscribedToInfiniteGas = false;
    // SKU for our subscription (infinite gas)
    static final String SKU_UNLIMITED_ZIP = "unlimited_monthly_zip";
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;
    // The helper object
    IabHelper mHelper;
    DrawerLayout dlDrawer;
    ActionBarDrawerToggle  drawerToggle;
    @Inject
    UserProfilePresenter userProfilePresenter;
    NavigationView navigationView;
    CircleImageView profilePic;
    TextView Profileemail,Profilename;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        initializeInjector();
        gpsLocation.initializeLocation(this);
        // gpsLocation.setonLocationListener(this);
        ButterKnife.bind(this);
        FacebookSdk.sdkInitialize(this);
      /* ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();*/
      /*  userProfilePresenter.setView(this);
        userProfilePresenter.initialize(sharedPreferences.getInt(constants.KEY_ID, 0));*/
         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        profilePic=(CircleImageView)navigationView.getHeaderView(0).findViewById(R.id.profile_img);
        Profileemail=(TextView)navigationView.getHeaderView(0).findViewById(R.id.Profile_name);
        Profilename=(TextView)navigationView.getHeaderView(0).findViewById(R.id.Profile_email);
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            this.showStorage();
           /* this.showCamera();
            this.showContacts();*/
        } else {
            // Pre-Marshmallow
            // gpsLocation.callbacksResults();
        }
        this.setSupportActionBar(toolbar);
// Find our drawer view
        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
          drawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        dlDrawer.setDrawerListener(drawerToggle);

     /*   this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);

        replaceFragment(R.id.conatainer_layout, CheckInNearby.newInstance());

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }
    private void initializeInjector() {
        this.mainActivityComponent = DaggerMainActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .nearbyModule(new NearbyModule())
                .addListingModule(new AddListingModule())
                .leaderBoardModule(new LeaderBoardModule())
                .previousEstimateModule(new PreviousEstimateModule())
                .messageModule(new MessageModule())
                .friendsModule(new FriendsModule())
                .settingModule(new SettingModule())
                .updateCheckingModule(new UpdateCheckingModule())
                .build();

        //System.out.println("Check to See IF the GPS LOCATION is ok"+gpsLocation);
        initZipPurchase();
    }

    private void initZipPurchase() {
  /* base64EncodedPublicKey should be YOUR APPLICATION'S PUBLIC KEY
         * (that you got from the Google Play developer console). This is not your
         * developer public key, it's the *app-specific* public key.
         *
         * Instead of just storing the entire literal string here embedded in the
         * program,  construct the key at runtime from pieces or
         * use bit manipulation (for example, XOR with some other string) to hide
         * the actual key.  The key itself is not secret information, but we don't
         * want to make it easy for an attacker to replace the public key with one
         * of their own and then fake messages from the server.
         */
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhjfsapNsqgv0LZy8rnsYKAGJ1Vl0fVUrAGcIJXjyrEx/OW3MwOhyqlykz7EIaOMoc/RaiFdkTMgiuTSe9d4d74aJe4TAp68YRFg2TkmYa4U155pDzFnO3OONNnBRs1epIdCPHCqIM4PFkm8ruFOE7UlJzjBUkJfjuXvQvkjJg1aomEz2oHQkakM0BKW74vAMzNXtPpeqtSXZjafKBqgQzwoFngJahPz9K4DYdiE73XKAFetByx31uy7gtLpGPIDhsRP0Ey2gyNT/x7qTfSxddMpoZK2nbWSY9wwm3iuu3NRu3Zsy2rDFgixySMimU6JwK3UhBvR6oSWRT9mohf6vawIDAQAB";

        // Some sanity checks to see if the developer (that's you!) really followed the
        // instructions to run this sample (don't put these checks on your app!)
        if (base64EncodedPublicKey.contains("CONSTRUCT_YOUR")) {
            throw new RuntimeException("Please put your app's public key in MainActivity.java. See README.");
        }
        /*if (getPackageName().startsWith("com.example")) {
            throw new RuntimeException("Please change the sample's package name! See README.");
        }*/

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.e(TAG, "Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;
                ArrayList<String> skusToBeListed = null;

                    skusToBeListed = new ArrayList<String> ();
                    skusToBeListed.add (SKU_UNLIMITED_ZIP);

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(true,skusToBeListed,mGotInventoryListener);
            }
        });
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                Log.e(TAG, "Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");
            // Do we have the infinite gas plan?
            Purchase infiniteGasPurchase = inventory.getPurchase(SKU_UNLIMITED_ZIP);
            mSubscribedToInfiniteGas = (infiniteGasPurchase != null &&
                    verifyDeveloperPayload(infiniteGasPurchase));
            Log.d(TAG, "User " + (mSubscribedToInfiniteGas ? "HAS" : "DOES NOT HAVE")
                    + " infinite gas subscription.");


            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            Purchase gasPurchase = inventory.getPurchase(SKU_UNLIMITED_ZIP);
            if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_UNLIMITED_ZIP), mConsumeFinishedListener);
                return;
            }

            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    // flow for subscription.
    public void onInfiniteGasButtonClicked() {

        if (!mHelper.subscriptionsSupported()) {
            Log.e(TAG, "Subscriptions not supported on your device yet. Sorry!");
            return;
        }

        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "";

        Log.d(TAG, "Launching purchase flow for infinite gas subscription.");
        mHelper.launchPurchaseFlow(this,
                SKU_UNLIMITED_ZIP, IabHelper.ITEM_TYPE_SUBS,
                RC_REQUEST, mPurchaseFinishedListener, payload);

    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                Log.e(TAG, "Error purchasing: " + result);
                //  setWaitScreen(false);
                if(initZipCallBack()!=null){ZipPurchase.onZipCodePurchaseSuccess(false);}
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Log.e(TAG, "Error purchasing. Authenticity verification failed.");
                if(initZipCallBack()!=null){ZipPurchase.onZipCodePurchaseSuccess(false);}
                return;
            }

            Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(SKU_UNLIMITED_ZIP)) {
                // bought the infinite gas subscription
                Log.d(TAG, "Unlimited zip subscription purchased.");
                Log.d(TAG, "Thank you for subscribing to unlimited Zip!"+ purchase.getOrderId());
                mSubscribedToInfiniteGas = true;
               if(initZipCallBack()!=null){
                   ZipPurchase.onZipCodePurchaseSuccess(true);
                   ZipPurchase.onPurchaseIdReceived(purchase.getToken());
               }
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");

            } else {
                Log.e(TAG, "Error while consuming: " + result);
            }

            Log.d(TAG, "End consumption flow.");
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    /**
     * Goes to the setting Fragment.
     */



    /**
     * Right Side Frgment NavBarIconController call this method from Freagment and pass the fragment name
     *
     * @param fragmentName
     */
    @Override
    public void NavBarIconController(Fragment fragmentName) {
        Log.d(TAG, "FragmentName Passed" + fragmentName.getClass().getName());

    }

    @Override
    public void onConnectionSuspended() {

    }

    @Override
    public void onLocationFailed() {

    }

    @Override
    public void onLocation(Location location) {
        //Putting the Location to Shared Preferences
        sharedPreferences.edit().putFloat(constants._LON, (float) location.getLongitude()).putFloat(constants._LAT, (float) location.getLatitude()).commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);

        if (this.sharedPreferences!=null &&this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_LOGIN, false).commit();
        }
        //  gpsLocation.disconnect();
    }

    //For AutoLogout When user Kill the app
    @Override
    protected void onPause() {
        super.onPause();
        if (this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_LOGIN, false).commit();
        }
    }

    @Override
    public MainActivityComponent getComponent() {
        return mainActivityComponent;
    }


    /**
     * Called when the 'show camera' button is clicked.
     * Callback is defined in resource layout definition.
     */
    public void showCamera() {
        Log.i(TAG, "Show camera button pressed. Checking permission.");
        // BEGIN_INCLUDE(camera_permission)
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

            requestCameraPermission();

        } else {

            // Camera permissions is already available, show the camera preview.
            Log.i(TAG,
                    "CAMERA permission has already been granted. Displaying camera preview.");
            if (initPermissionBus() != null) {
                permissionBus.onCameraPermissionSuccess(true);
            }
        }
        // END_INCLUDE(camera_permission)

    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestCameraPermission() {
        android.util.Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            android.util.Log.i(TAG,
                    "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(container_layout, R.string.permission_camera_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }

    /**
     * Called when the 'show camera' button is clicked.
     * Callback is defined in resource layout definition.
     */
    public void showContacts() {
        Log.i(TAG, "Show contacts button pressed. Checking permissions.");
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i(TAG, "Contact permissions has NOT been granted. Requesting permissions.");
            requestContactsPermissions();

        } else {
            // Contact permissions have been granted. Show the contacts fragment.
            Log.i(TAG,
                    "Contact permissions have already been granted. Displaying contact details.");
            if (initPermissionBus() != null) {
                permissionBus.onContactPermissionSuccess(true);
            }
        }
    }

    /**
     * Requests the Contacts permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestContactsPermissions() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            android.util.Log.i(TAG,
                    "Displaying contacts permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(container_layout, R.string.permission_contacts_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(MainActivity.this, PERMISSIONS_CONTACT,
                                            REQUEST_CONTACTS);
                        }
                    })
                    .show();
        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, REQUEST_CONTACTS);
        }
        // END_INCLUDE(contacts_permission_request)
    }

    /**
     * Called when the Location is needed
     * Callback is defined in resource layout definition.
     */
    public void showLocation() {
        android.util.Log.i(TAG, "Show camera button pressed. Checking permission.");
        // BEGIN_INCLUDE(camera_permission)
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

            requestLocationPermissions();

        } else {

            // Camera permissions is already available, show the camera preview.
            android.util.Log.i(TAG,
                    "LOCATION  permission has already been granted. Displaying camera preview.");
            if (initPermissionBus() != null) {
                permissionBus.onLocationPermissionSuccess(true);
            }
        }
        // END_INCLUDE(camera_permission)

    }

    /**
     * Requests the Location permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestLocationPermissions() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            android.util.Log.i(TAG,
                    "Displaying contacts permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(container_layout, R.string.permission_Location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(MainActivity.this, PERMISSIONS_LOCATION,
                                            REQUEST_LOCATION);
                        }
                    })
                    .show();
        } else {
            // Location permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
        }
        // END_INCLUDE(contacts_permission_request)
    }

    /**
     * Called when the Location is needed
     * Callback is defined in resource layout definition.
     */
    public void showStorage() {
        android.util.Log.i(TAG, "Show Storage  Checking permission.");
        // BEGIN_INCLUDE(camera_permission)
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int hasWriteContactPermission = ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED || hasWriteContactPermission != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

            requestStoragePermissions();

        } else {

            // Camera permissions is already available, show the camera preview.
            android.util.Log.i(TAG,
                    "LOCATION  permission has already been granted. Displaying camera preview.");
            if (initPermissionBus() != null) {
                permissionBus.onStoragePermissionSuccess(true);
            }
        }
        // END_INCLUDE(camera_permission)

    }

    /**
     * Requests the Location permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestStoragePermissions() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                ) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            android.util.Log.i(TAG,
                    "Displaying STORAGE permission rationale to provide.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            /*Snackbar.make(container_layout, R.string.permission_Storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(MainActivity.this, PERMISSIONS_STORAGE,
                                            REQUEST_STORAGE);
                        }
                    })
                    .show();*/

            //TODO FIND the Solution of Multiple Instance of SnackBar
            // ISSUE WITH Multiple instance of SnackBar is not able to Display at all so Change that to Alert Dialog
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Homebuzz");
            alertDialog.setMessage(R.string.permission_Storage_rationale);
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Log.i(TAG, "Storage Permission is Denied");
                    if (initPermissionBus() != null) {
                        permissionBus.onStoragePermissionSuccess(false);
                    }
                }
            });
            alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat
                            .requestPermissions(MainActivity.this, PERMISSIONS_STORAGE,
                                    REQUEST_STORAGE);
                }
            });

            alertDialog.show();
        } else {
            // Location permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_STORAGE);
        }
        // END_INCLUDE(contacts_permission_request)
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            android.util.Log.i(TAG, "Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                android.util.Log.i(TAG, "CAMERA permission has now been granted. Showing preview.");
                Snackbar.make(container_layout, R.string.permision_available_camera,
                        Snackbar.LENGTH_SHORT).show();
                if (initPermissionBus() != null) {
                    permissionBus.onCameraPermissionSuccess(true);
                }
            } else {
                android.util.Log.i(TAG, "CAMERA permission was NOT granted.");
                Snackbar.make(container_layout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
                if (initPermissionBus() != null) {
                    permissionBus.onCameraPermissionSuccess(false);
                }

            }
            // END_INCLUDE(permission_result)

        } else if (requestCode == REQUEST_CONTACTS) {
            android.util.Log.i(TAG, "Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (PermissionUtil.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
                Snackbar.make(container_layout, R.string.permision_available_contacts,
                        Snackbar.LENGTH_SHORT)
                        .show();
                if (initPermissionBus() != null && permissionBus != null) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            // EXECUTE ACTIONS (LIKE FRAGMENT TRANSACTION ETC.)
                            permissionBus.onContactPermissionSuccess(true);
                        }
                    }, 10);

                }
            } else {
                android.util.Log.i(TAG, "Contacts permissions were NOT granted.");
                Snackbar.make(container_layout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
                if (initPermissionBus() != null && permissionBus != null) {
                    //This is the Bug in support library fragment Workaround @see https://code.google.com/p/android-developer-preview/issues/detail?id=2823 for more info
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            // EXECUTE ACTIONS (LIKE FRAGMENT TRANSACTION ETC.)
                            permissionBus.onContactPermissionSuccess(false);
                        }
                    }, 10);
                }
            }

        } else if (requestCode == REQUEST_LOCATION) {
            android.util.Log.i(TAG, "Received response for Location permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (PermissionUtil.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
                Snackbar.make(container_layout, R.string.permision_available_location,
                        Snackbar.LENGTH_SHORT)
                        .show();
                if (initPermissionBus() != null) {
                    permissionBus.onLocationPermissionSuccess(true);
                }
            } else {
                android.util.Log.i(TAG, "Location permissions were NOT granted.");
                Snackbar.make(container_layout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
                if (initPermissionBus() != null) {
                    permissionBus.onLocationPermissionSuccess(false);
                }
            }
        } else if (requestCode == REQUEST_STORAGE) {
            android.util.Log.i(TAG, "Received response for EXTERNAL permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (PermissionUtil.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
               /* Snackbar.make(container_layout, R.string.permision_available_Storage,
                        Snackbar.LENGTH_SHORT)
                        .show();*/
                if (initPermissionBus() != null) {
                    permissionBus.onStoragePermissionSuccess(true);
                }
            } else {
                android.util.Log.i(TAG, "Storage permissions were NOT granted.");
           /*     Snackbar.make(container_layout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();*/
                if (initPermissionBus() != null) {
                    permissionBus.onStoragePermissionSuccess(false);
                }
            }
        } else {
            Log.d(TAG, "Non Handled Value" + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private PermissionBus initPermissionBus() {
        if (this.getSupportFragmentManager().findFragmentById(R.id.conatainer_layout) instanceof PermissionBus && this.getSupportFragmentManager().findFragmentById(R.id.conatainer_layout).isVisible()) {
            permissionBus = ((PermissionBus) this.getSupportFragmentManager().findFragmentById(R.id.conatainer_layout));
        } else
            permissionBus = null;

        return permissionBus;
    }

    private ZipPurchase initZipCallBack(){
        if (this.getSupportFragmentManager().findFragmentById(R.id.conatainer_layout) instanceof ZipPurchase ) {
            ZipPurchase = ((ZipPurchase) this.getSupportFragmentManager().findFragmentById(R.id.conatainer_layout));
        } else
            ZipPurchase = null;
        Log.d(TAG, "check for Zip Purchase" + ZipPurchase);
        return ZipPurchase;
    }

    @Override
    public void onStorageRequest() {
        showStorage();
    }

    @Override
    public void onLocationRequest() {
        showLocation();
    }

    @Override
    public void onContactRequest() {
        showContacts();
    }

    @Override
    public void onCameraRequest() {
        showCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }

    }

    @Override
    public void initPurchase() {
        onInfiniteGasButtonClicked();
      //  {initZipCallBack().onZipCodePurchaseSuccess(false);}
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.checkin) {
            replaceFragment(R.id.conatainer_layout, CheckInNearby.newInstance());
        }
        else if (id == R.id.add_home) {
            replaceFragment(R.id.conatainer_layout, AddListing.newInstance());
        }
        else if (id == R.id.leaderboard) {
            replaceFragment(R.id.conatainer_layout, LeaderBoard.newInstance());
        }
        else if (id == R.id.notification) {
            replaceFragment(R.id.conatainer_layout, NotificationFragment.newInstance(userJustSoldModelArrayList, userJustListedModelArrayList, userUnreadMessagesModelArrayList));
        }
        else if (id == R.id.my_profile) {
            replaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());
        }

        else if (id == R.id.zip_code_purchase) {
            replaceFragment(R.id.conatainer_layout, ZipCodePuchaseFragment1.newInstance());
        }
        else if (id == R.id.autoLogout) {
            replaceFragment(R.id.conatainer_layout, AutoLogout.newInstance());
        }
        else if (id == R.id.help) {
            Intent internetIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://homebuzzinc.zendesk.com/hc/en-us"));
            this.startActivity(internetIntent);

        } else if (id == R.id.Settings) {
            replaceFragment(R.id.conatainer_layout, UserSettingsFragment.newInstance());
            /*if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null) {
                LoginManager.getInstance().logOut();
                sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
                this.finish();
                CallNavigateToActivity(new Login());

            } else {
                sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
                this.finish();
                CallNavigateToActivity(new Login());
            }*/

        }else if(id == R.id.logout){
            if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null && this.sharedPreferences!=null) {
                LoginManager.getInstance().logOut();
                this.sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
                this.finish();
                CallNavigateToActivity(new Login());

            } else {
                if(this.sharedPreferences!=null)
                this.sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
                this.finish();
                CallNavigateToActivity(new Login());
            }
        }
        item.setChecked(true);
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    public void upDateProfileData(String name, String email) {
        Profileemail.setText(email);
        Profilename.setText(name);
    }

    @Override
    public void upDateProfilePic(Picasso mpPicasso, String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("null"))
            mpPicasso.load(imageUrl).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
    }

    @Override
    public void UpdateNoticationCount(int count) {
        navigationView.getMenu().getItem(3).setTitle("Notification   ("+count+")");
        Log.d(TAG,"Getting the Count"+count);

    }
}
