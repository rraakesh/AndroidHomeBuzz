package us.homebuzz.homebuzz.view.profile.friendsprofile;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.data.entity.pojo.profile.SelectUser;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.InviteFreindsModel;
import us.homebuzz.homebuzz.presenter.InviteFriendsPresenter;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireContact;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.SelectUserAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.profile.InviteFriendsView;


/**
 * Created by rohitkumar on 1/12/15.
 */
public class InviteFriendsFragment extends BaseFragment implements InviteFriendsView, PermissionBus {
    private static final String TAG = InviteFriendsFragment.class.getSimpleName();
    ArrayList<SelectUser> selectUsers;
    @Inject
    InviteFriendsPresenter inviteFriendsPresenter;
    Cursor phones;
    ContentResolver resolver;
    @Inject
    Constants constants;
    SelectUserAdapter adapter;
    @Bind(R.id.send_invitation)
    TextView send;

    @Bind(R.id.contacts_list)
    ListView listView;
    @Bind(R.id.searchView)
    EditText search;
    static int selected = 0;
    private RequireContact requireContact;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invite_friends, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialize();
        if (this.getActivity() instanceof RequireContact)
            requireContact = (RequireContact) this.getActivity();
        setupUI();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.inviteFriendsPresenter.setView(this);
        //this.adapter._data.size();
    }

    public void setupUI() {
        selectUsers = new ArrayList<SelectUser>();
        this.adapter = new SelectUserAdapter(selectUsers, InviteFriendsFragment.this.getActivity().getBaseContext());
        this.listView.setAdapter(this.adapter);
        this.listView.setFastScrollEnabled(true);
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.requireContact != null)
              this.requireContact.onContactRequest();
        }else{
            onContactSuccess();
        }

    }

    @Override
    public void renderAfterInviteFreinds(InviteFreindsModel inviteFreindsModel) {
        ++selected;
        if (this.getActivity() != null /*&& selected==adapter.selectedUser.size()*/)
            getActivity().onBackPressed();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }


    class LoadContact extends AsyncTask<Void, Void, ArrayList<SelectUser>> {

        @Override
        protected ArrayList<SelectUser> doInBackground(Void... voids) {
            ArrayList<SelectUser> selectUsers = new ArrayList<>();
            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {

                }
                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String valinNUmber = phoneNumber.replace("+1", "");
                    String number = valinNUmber.replaceAll("\\D", "");
                    SelectUser selectUser = new SelectUser();
                    selectUser.setName(name);
                    selectUser.setPhone(number);
                    selectUser.setCheckedBox(false);
                    selectUsers.add(selectUser);
                }
            } else {

            }

            return selectUsers;
        }

        @Override
        protected void onPostExecute(ArrayList<SelectUser> mdata) {
            selectUsers.addAll(mdata);
            InviteFriendsFragment.this.adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        this.inviteFriendsPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();

        this.inviteFriendsPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.inviteFriendsPresenter.destroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        phones.close();
    }

    @OnClick(R.id.send_invitation)
    public void sendInvitation() {
        selected = adapter.selectedUser.size();
        for (int i = 0; i < adapter.selectedUser.size(); i++) {
            WeakHashMap<String, String> invite = new WeakHashMap<>();
            invite.put(constants.MESSAGE, "Join HomeBuzz! Search nearby real estate for sale and check-in, review, or,estimates sale price. https://play.google.com/store/apps/details?id=us.homebuzz.homebuzz");
            invite.put(constants.PHONE_NUMBER, "+1" + adapter.selectedUser.get(i));
            this.inviteFriendsPresenter.initilize(invite);
        }
    }

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Invite Friends");
        super.onDestroyView();

    }

    public void searchList(CharSequence Character) {
        ArrayList<SelectUser> temp = new ArrayList<>();
        for (SelectUser str : selectUsers) {
            if (str.getName().toLowerCase().contains(Character)) {
                temp.add(str);
                System.out.println("frag" + temp.size());
            }
        }
        this.adapter = new SelectUserAdapter(temp, InviteFriendsFragment.this.getActivity().getBaseContext());
        listView.setAdapter(adapter);
    }

    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {
        if (isSuccess == true) {
            onContactSuccess();
        } else {
            Snackbar.make(this.getView(), R.string.permissions_not_granted,
                    Snackbar.LENGTH_SHORT)
                    .show();
        }
        Log.d(TAG, "Status of Contact Permission" + isSuccess);
    }

    private void onContactSuccess() {
        resolver = this.getActivity().getContentResolver();
        phones = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        LoadContact loadContact = new LoadContact();


        loadContact.execute();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence newText, int i, int i1, int i2) {
                if (newText.length() > 0)
                    searchList(newText);
                else {
                    adapter = new SelectUserAdapter(selectUsers, InviteFriendsFragment.this.getActivity().getBaseContext());
                    listView.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {

    }
}