package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserJustListedModel;

/**
 * Created by amit.singh on 11/18/2015.
 */
public interface UserJustListedView extends LoadDataView {
    /**
     * View a {@link } UnreadMessages
     *
     * @param userJustListedModelCollection Messages that a user has not Read
     */
    void viewJustListedListing(Collection<UserJustListedModel> userJustListedModelCollection);

}
