package us.homebuzz.homebuzz.view.adapter.estimatelisting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.adapter.friends.FriendFollowAdapter;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.EstimateListing;

/**
 * Created by abhishek.singh on 11/25/2015.
 */
public class EstimateListingAdapter extends RecyclerView.Adapter<EstimateListingAdapter.UserViewHolder> {

    public static String TAG = FriendFollowAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        public void onEstimateClick(UserNearbyModel userNearbyModel, int position);
    }

    private UserNearbyModel userNearbyModel;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;

    public EstimateListingAdapter(Context context, UserNearbyModel usersCollection, Picasso picasso, EstimateListing estimateListing) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.userNearbyModel = usersCollection;
        this.picasso = picasso;
        this.onItemClickListener = estimateListing;
    }

    @Override
    public int getItemCount() {
        return (this.userNearbyModel != null) ? this.userNearbyModel.getEstimates().size() : 0;

    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.estimate_listing_adatpter, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserNearbyModel.Estimate userModel = this.userNearbyModel.getEstimates().get(position);
        if (userModel != null) {
            if (userModel.getImageUrl() != null && userModel.getImageUrl().size()>0 && !(userModel.getImageUrl().isEmpty()) &&!(userModel.getImageUrl().equals("null"))) {
                picasso.load(String.valueOf(userModel.getImageUrl().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(holder.profile_pic);
            } else {
                holder.profile_pic.setImagePlaceHolder(R.drawable.ic_launcher);
            }
        }
        if ((userModel.getName() != null)) {
            holder.txtName.setVisibility(View.VISIBLE);
            holder.txtName.setText(userModel.getName());
        } else {
            holder.txtName.setVisibility(View.GONE);
        }
        if ((userModel.getReason() != null)) {
            holder.txtReason.setVisibility(View.VISIBLE);
            holder.txtReason.setText(userModel.getReason());
        } else {
            holder.txtReason.setVisibility(View.GONE);
        }
        if (userModel.getPrice() != null) {
            holder.txtPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setText("$ " + new DecimalFormat("###,###,###").format(userModel.getPrice().intValue()));
        } else {
            holder.txtPrice.setVisibility(View.GONE);
        }

        holder.txtBeds.setText(String.valueOf(userModel.getImageUrl().size())+" Photos");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EstimateListingAdapter.this.onItemClickListener != null) {
                    System.out.println("View"+position);
                    onItemClickListener.onEstimateClick(userNearbyModel,position);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(UserNearbyModel userNearbyModel) {
        this.validateUsersCollection(userNearbyModel);
        this.userNearbyModel = userNearbyModel;
        this.notifyDataSetChanged();
    }


    private void validateUsersCollection(UserNearbyModel userModelCollection) {
        if (userModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.photocount)
        TextView txtBeds;
        @Bind(R.id.txtReason)
        TextView txtReason;
        @Bind(R.id.txtName)
        TextView txtName;
        @Bind(R.id.txtPrice)
        TextView txtPrice;
        @Bind(R.id.profile_pic)
        AutoLoadImageView profile_pic;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}