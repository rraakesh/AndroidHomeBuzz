package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/22/2015.
 */
public interface AllMessageView extends LoadDataView {
    /**
     * Render a user list in the UI.
     *
     * @param userModelCollection The collection of {@link UserModel} that will be shown.
     */
    void renderUserAllMessageList(Collection<UserAllMessagesModel> userModelCollection);

    void viewUser(UserAllMessagesModel userAllMessagesModel);
}
