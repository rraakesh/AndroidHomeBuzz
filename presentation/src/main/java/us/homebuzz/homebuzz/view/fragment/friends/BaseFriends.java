package us.homebuzz.homebuzz.view.fragment.friends;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * Created by amit.singh on 10/28/2015.
 */
public class BaseFriends extends FriendsDecorator implements View.OnClickListener {
    private static final String TAG = BaseFriends.class.getSimpleName();

    public static BaseFriends newInstance() {
        BaseFriends baseFriends = new BaseFriends();
        return baseFriends;
    }
    public BaseFriends() {
        super();
    }
    protected ReplaceFragment replaceFragmentFriends;

    @Bind(R.id.messsage_text_top)
    TextView message_top;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupLowerTab();
        //Todo Change that to String
        super.setTopText("Followed");
        //Initialize the Friends First Fragment
        super.replaceFragment(R.id.container_layout_tab,new FriendsFollowed());
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);

    }
    @Override
    public void setupLowerTab() {
        LinearLayout tab_strip=(LinearLayout)this.getActivity().getLayoutInflater().inflate(R.layout.friends_lower_tab,tab_fragment,false);
        tab_fragment.addView(tab_strip);
        ButterKnife.bind(super.getClass(), tab_strip);
        LinearLayout fr_follow=(LinearLayout)tab_fragment.findViewById(R.id.fr_follow);
        LinearLayout fr_office=(LinearLayout)tab_fragment.findViewById(R.id.fr_office);
        LinearLayout fr_team=(LinearLayout)tab_fragment.findViewById(R.id.fr_team);
        LinearLayout fr_all=(LinearLayout)tab_fragment.findViewById(R.id.fr_all);
        //ToDO Setup the Images Resource and TextColor
        ((TextView) fr_follow.findViewById(R.id.text_fr_follow)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
        ((TextView)fr_office.findViewById(R.id.text_fr_office)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        ((TextView)fr_team.findViewById(R.id.text_fr_team)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        ((TextView)fr_all.findViewById(R.id.text_fr_all)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        ((ImageView)fr_follow.findViewById(R.id.fr_img_followed)).setImageResource(R.drawable.list_yellow);
        ((ImageView)fr_office.findViewById(R.id.fr_img_office)).setImageResource(R.drawable.list_white);
        ((ImageView)fr_team.findViewById(R.id.fr_img_team)).setImageResource(R.drawable.list_white);
        ((ImageView)fr_all.findViewById(R.id.fr_img_all)).setImageResource(R.drawable.list_white);
        fr_follow.setOnClickListener(this);
        fr_office.setOnClickListener(this);
        fr_team.setOnClickListener(this);
        fr_all.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr_follow:
               super.onLeftIconClick(new FriendsFollowed());
                break;
            case R.id.fr_office:
              //  super.onSecondIconClick();
                break;
            case R.id.fr_team:
              //  super.onThirdIconClick();
                break;
            case R.id.fr_all:
            //    super.onRightIconClick();
                break;
        }
    }

}
