package us.homebuzz.homebuzz.view.adapter.leaderboard;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by rohitkumar on 31/10/15.
 */
public class ViewPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    List<String> photo;
    private Picasso picasso;

    public ViewPagerAdapter(Context context, UserPreviousEstimateModel.Listing photo,Picasso picasso) {
        this.mContext = context;
        this.photo = (List<String>) photo;
        this.picasso=picasso;
    }



    @Override
    public int getCount() {
        return (photo.size() == 0) ? 1 : photo.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.view_pager_adapter, container,
                false);
        AutoLoadImageView autoLoadImageView;
        autoLoadImageView = (AutoLoadImageView)itemView.findViewById(R.id.image);

        //container.addView(itemView);
        if(photo!=null &&photo.size() > 0) {
            if (!photo.get(position).toString().matches("null")) {
                picasso.load(photo.get(position).toString()).into(autoLoadImageView);
            }
            else
                autoLoadImageView.setImageResource(R.drawable.home_icon);
        }
        else
            autoLoadImageView.setImageResource(R.drawable.home_icon);

        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}

