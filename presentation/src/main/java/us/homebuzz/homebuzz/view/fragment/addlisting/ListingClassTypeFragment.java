package us.homebuzz.homebuzz.view.fragment.addlisting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/5/2015.
 */
public class ListingClassTypeFragment extends BaseFragment {

    private static final String TAG = ListingClassTypeFragment.class.getSimpleName();

    public static ListingClassTypeFragment newInstance() {
        ListingClassTypeFragment listingClassTypeFragment = new ListingClassTypeFragment();
        return listingClassTypeFragment;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.property_type_layout, container, false);

        ButterKnife.bind(this, view);
        return view;
    }
}
