package us.homebuzz.homebuzz.view.fragment.messages;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.presenter.FollowUnfollowPresenter;
import us.homebuzz.homebuzz.presenter.MessageReplyPresenter;
import us.homebuzz.homebuzz.view.FollowUnfollowView;
import us.homebuzz.homebuzz.view.MessageReplyView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 11/19/2015.
 */
public class CreateMessage extends BaseFragment implements MessageReplyView, FollowUnfollowView {
    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.txtAppName)
    TextView txtAppName;
    @Bind(R.id.txtFollow)
    TextView txtFollow;
    @Bind(R.id.reply_send)
    TextView reply_send;
    @Bind(R.id.delete_cancel)
    TextView delete_cancel;
    @Bind(R.id.txtBackMsg)
    TextView txtBackMsg;
    @Bind(R.id.edbody)
    EditText edbody;
    @Bind(R.id.llFollow)
    LinearLayout llFollow;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Constants constants;
    @Inject
    MessageReplyPresenter messageReplyPresenter;
    @Inject
    FollowUnfollowPresenter followUnfollowPresenter;
    private boolean check = true;

    @Inject
    Picasso picasso;
    UserProfileModel userProfileModel;
    private static final String ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL = "UserMessagesModel";

    public static CreateMessage newInstance(UserProfileModel userProfileModel) {
        CreateMessage createMessage = new CreateMessage();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL, userProfileModel);
        createMessage.setArguments(argumentsBundle);
        return createMessage;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createMessage = inflater.inflate(R.layout.read_and_reply_message, container, false);
        ButterKnife.bind(CreateMessage.this, createMessage);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL) != null) {
            this.userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_REPLY_MESSAGE_MODEL);
        }
        return createMessage;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        this.messageReplyPresenter.setView(this);
        followUnfollowPresenter.setView(this);
    }

    private void setUI() {
        reply_send.setText("Send");
        delete_cancel.setText("Cancel");
        txtBackMsg.setVisibility(View.GONE);
        txtProfileName.setText(userProfileModel.getName());
        if (userProfileModel.getFollowed()) {
            llFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            txtFollow.setText("Un-Follow");
        } else {
            llFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
        }
        if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().equals("null"))
            picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);

    }

    @Override
    public void renderAfterMessageReply(UserMessageReplyModel userMessageReplyModel) {

    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        if (check) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Homebuzz");
            alertDialog.setMessage("Your message has been sent to " + userProfileModel.getName());
            alertDialog.setPositiveButton("ok", null);
            alertDialog.show();
            getActivity().onBackPressed();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @OnClick(R.id.reply_send)
    public void setBtnSend() {
        check = true;
        hideKeyboard();
        if (!TextUtils.isEmpty(edbody.getText().toString())) {
            WeakHashMap<String, String> createMsg = new WeakHashMap<>();
            createMsg.put(constants.MESSAGE_BODY, edbody.getText().toString());
            createMsg.put(constants.SENDER_ID, String.valueOf(userProfileModel.getId()));
            this.messageReplyPresenter.initialize(createMsg);
        } else {
            super.showToastMessage("PLease enter your message");
        }

    }

    @OnClick(R.id.delete_cancel)
    public void setBtnCancel() {
        getActivity().onBackPressed();
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.llFollow)
    public void btnFolowClick() {
        check = false;
        followUnfollowPresenter.initialize(userProfileModel.getId());
    }

    @Override
    public void viewUser(FollowUnfollowModel FollowUnfollowModel) {

    }

    @Override
    public void renderFollowUnfollow(FollowUnfollowModel FollowUnfollowModel) {
        if (FollowUnfollowModel.getFollowed()) {
            llFollow.setBackgroundResource(R.drawable.un_follow_bacground);
            txtFollow.setText("Un-Follow");
        } else {
            llFollow.setBackgroundResource(R.drawable.follow_background);
            txtFollow.setText("Follow");
        }
    }
}
