package us.homebuzz.homebuzz.view;

/**
 * Created by amit.singh on 12/18/2015.
 */
public interface NotificationUpdateBus {
    void UpdateNoticationCount(int count);
}
