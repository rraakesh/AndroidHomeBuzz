package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/21/2015.
 */
public interface UserEstimatesView extends LoadDataView {


    void viewUser(UserEstimateModel userEstimateModel);

    /**
     * Render a user list in the UI.
     *
     * @param userEstimateModels The collection of {@link UserModel} that will be shown.
     */
    void renderUserEstimateList(Collection<UserEstimateModel> userEstimateModels);
    

}
