package us.homebuzz.homebuzz.view.profile;

import us.homebuzz.homebuzz.model.InviteFreindsModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.view.LoadDataView;

/**
 * Created by rohitkumar on 2/12/15.
 */
public interface InviteFriendsView extends LoadDataView {
    void renderAfterInviteFreinds(InviteFreindsModel inviteFreindsModel);
}
