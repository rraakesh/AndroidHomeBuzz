package us.homebuzz.homebuzz.view.fragment.messages;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * Created by amit.singh on 10/22/2015.
 */
public class Messages extends MessagesDecorator implements View.OnClickListener{
    private static final String TAG = Messages.class.getSimpleName();
    public static Messages newInstance() {
        Messages messages = new Messages();
        return messages;
    }
    public Messages() {
        super();
    }

    protected ReplaceFragment replaceFragmentMessage;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupLowerTab();
        //Todo Change that to String
        super.setTopText("All Messages");
        super.replaceFragment(R.id.container_layout_tab, new AllMessages());
    }
    @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            this.initialize();
             this.replaceFragmentMessage=replaceListener;
        /*if(this.getActivity() instanceof MainActivity){
            Log.d(TAG, "Inside the Toolbar");
            ((MainActivity) this.getActivity()).toolbar.getAppTitleText().setText("Messages");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((MainActivity) this.getActivity()).toolbar.changeIconRight(this.getResources().getDrawable(R.drawable.ic_action_message, this.getContext().getTheme()));
            } else {
                ((MainActivity) this.getActivity()).toolbar.changeIconRight(this.getResources().getDrawable(R.drawable.ic_action_message));
            }

            *//*((MainActivity) this.getActivity()).toolbar.setTextRightVisible(true);*//*
        }*/
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.left_icon:
                super.setTopText("All Messages");
                ((TextView)v.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
                ((ImageView) v.findViewById(R.id.all_message)).setImageResource(R.drawable.list_yellow);
                LinearLayout parent= (LinearLayout)v.getParent();
                ((TextView)parent.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                ((ImageView)parent.findViewById(R.id.unread_message)).setImageResource(R.drawable.list_white);
                super.replaceFragment(R.id.container_layout_tab, new AllMessages());
                break;
            case R.id.right_icon:
                super.setTopText("Unread Messages");
                ((TextView)v.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
                ((ImageView) v.findViewById(R.id.unread_message)).setImageResource(R.drawable.list_yellow);
                LinearLayout p1= (LinearLayout)v.getParent();
                ((TextView)p1.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                ((ImageView) p1.findViewById(R.id.all_message)).setImageResource(R.drawable.list_white);
                super.replaceFragment(R.id.container_layout_tab, new UnreadMessages());
                break;


        }
    }

    @Override
    public void setupLowerTab() {
        LinearLayout tab_strip = (LinearLayout) this.getActivity().getLayoutInflater().inflate(R.layout.base_search_tab_message_layout, tab_fragment, false);
        tab_fragment.addView(tab_strip);
        ButterKnife.bind(super.getClass(), tab_strip);
        LinearLayout left_layout = (LinearLayout) tab_fragment.findViewById(R.id.left_icon);
        LinearLayout right_layout = (LinearLayout) tab_fragment.findViewById(R.id.right_icon);
        ((TextView)left_layout.findViewById(R.id.text_map)).setTextColor(getActivity().getResources().getColor(R.color.color_yellow));
        ((ImageView) left_layout.findViewById(R.id.all_message)).setImageResource(R.drawable.list_yellow);

        ((TextView)right_layout.findViewById(R.id.text_list)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        ((ImageView)right_layout.findViewById(R.id.unread_message)).setImageResource(R.drawable.list_white);
        left_layout.setOnClickListener(this);
        right_layout.setOnClickListener(this);
    }
}
