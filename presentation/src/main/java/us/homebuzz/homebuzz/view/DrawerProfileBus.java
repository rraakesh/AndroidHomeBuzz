package us.homebuzz.homebuzz.view;

import com.squareup.picasso.Picasso;

/**
 * Created by rakesh.kumar on 12/17/2015.
 */
public interface DrawerProfileBus {
    void upDateProfilePic(Picasso mpPicasso,String imageUrl);
    void upDateProfileData(String name,String email);
}
