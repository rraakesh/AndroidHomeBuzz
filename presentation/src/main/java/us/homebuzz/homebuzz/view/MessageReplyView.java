package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserMessageReplyModel;

/**
 * Created by abhishek.singh on 11/5/2015.
 */
public interface MessageReplyView extends LoadDataView {
    void renderAfterMessageReply(UserMessageReplyModel userMessageReplyModel);
}
