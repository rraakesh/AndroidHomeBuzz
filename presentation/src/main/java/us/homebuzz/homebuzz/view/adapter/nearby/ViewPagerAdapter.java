package us.homebuzz.homebuzz.view.adapter.nearby;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by rohitkumar on 29/10/15.
 */
public class ViewPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> photo;
    private Picasso picasso;
    private boolean check = false;

    public ViewPagerAdapter(Context context, ArrayList<String> photo, Picasso picasso, boolean check) {
        this.mContext = context;
        this.photo = photo;
        this.picasso = picasso;
        this.check = check;
    }


    @Override
    public int getCount() {
        return (photo.size() == 0) ? 1 : photo.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.view_pager_adapter, container, false);
        AutoLoadImageView autoLoadImageView;
        autoLoadImageView = (AutoLoadImageView) itemView.findViewById(R.id.image);

        container.addView(itemView);
        //Log.e("image url  ", photo.get(position).toString());

        if (photo != null && photo.size() > 0) {
            if (check) {
                if (!photo.get(position).toString().matches("null")) {
                    picasso.load(photo.get(position).toString()).into(autoLoadImageView);
                } else
                    autoLoadImageView.setImageResource(R.drawable.home_icon);
            } else {
                if (!photo.get(position).toString().matches("null")) {
                    if (photo.get(position).startsWith("https")) {
                        Log.e("image jhvcxzjhvf  ", photo.get(position).toString());
                        picasso.load(photo.get(position).toString()).into(autoLoadImageView);
                    } else
                        picasso.load(new File(photo.get(position).toString())).into(autoLoadImageView);

                } else
                    autoLoadImageView.setImageResource(R.drawable.home_icon);
            }
        } else
            autoLoadImageView.setImageResource(R.drawable.home_icon);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}