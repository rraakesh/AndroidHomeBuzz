package us.homebuzz.homebuzz.view.adapter.addnewlisting;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import us.homebuzz.homebuzz.R;

/**
 * Created by rakesh.kumar on 10/31/2015.
 */
public class AddPhotoAdapter extends BaseAdapter {
    Context context;
    Animation inAnimation;
    Animation outAnimation;
    List<String> mPhotoPath;

    public AddPhotoAdapter(Context context, List<String> mPhotoPath) {
        this.context = context;
        this.mPhotoPath = mPhotoPath;
        initAnimations();
    }

    @Override
    public Object getItem(int position) {
        return mPhotoPath.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return (this.mPhotoPath != null) ? this.mPhotoPath.size() : 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.new_listing_add_photo_adapter, parent, false);
            holder = new ViewHolder();
            //holder = new ViewHolder(convertView);
            holder.setImageView = (us.homebuzz.homebuzz.view.component.AutoLoadImageView) convertView.findViewById(R.id.set_image);
            holder.deleteRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.delete_layout);
            holder.leftRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.left_layout);
            holder.cancelRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.cancel_layout);
            holder.deleteImageView = (ImageView) convertView.findViewById(R.id.delete_image);
            holder.menuImageView = (ImageView) convertView.findViewById(R.id.menu_image);
            holder.deleteImageView.setTag(holder.leftRelativeLayout);
            holder.cancelRelativeLayout.setTag(holder.leftRelativeLayout);
          /*  holder.cancel_layout.setTag(holder.left_layout);
            holder.delete_layout.setTag(holder.left_layout);*/
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (mPhotoPath.get(position) != null && !mPhotoPath.get(position).equals("null"))
            holder.setImageView.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath.get(position)));

        holder.deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout rl = ((RelativeLayout) v.getTag());
                rl.startAnimation(inAnimation);
                rl.setVisibility(View.VISIBLE);
            }
        });

        holder.cancelRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout rl = ((RelativeLayout) v.getTag());
                rl.startAnimation(outAnimation);
                rl.setVisibility(View.GONE);
            }
        });

        holder.deleteRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RelativeLayout) v.getParent()).setVisibility(View.GONE);
                mPhotoPath.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        /*@Bind(R.id.set_image)
        AutoLoadImageView image;
        @Bind(R.id.menu_image)
        ImageView menu_image;
        @Bind(R.id.delete_image)
        ImageView delete_image;
        @Bind(R.id.delete_layout)
        RelativeLayout delete_layout;
        @Bind(R.id.cancel_layout)
        RelativeLayout cancel_layout;
        @Bind(R.id.left_layout)
        RelativeLayout left_layout;
        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }*/
        us.homebuzz.homebuzz.view.component.AutoLoadImageView setImageView;
        ImageView deleteImageView, menuImageView;
        RelativeLayout deleteRelativeLayout, cancelRelativeLayout, leftRelativeLayout;
    }

    private void initAnimations() {
        inAnimation = (AnimationSet) AnimationUtils.loadAnimation(context, R.anim.enter_from_left);
        outAnimation = (AnimationSet) AnimationUtils.loadAnimation(context, R.anim.exit_to_left);
    }

    /* public void setPhotoCollection(Collection<String> PhotoPath) {
         this.mPhotoPath .addAll( PhotoPath);
         this.notifyDataSetChanged();
     }
 */
    @Override
    public boolean isEmpty() {
        return false;
    }
}
