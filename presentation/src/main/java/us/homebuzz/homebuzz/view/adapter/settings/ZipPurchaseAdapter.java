package us.homebuzz.homebuzz.view.adapter.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipPurchaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = ZipPurchaseAdapter.this.getClass().getSimpleName();
    private static final int _HEADER = 0;
    private static final int _MAINVIEW = 1;
    private static final int SECOND_HEADER = 2;

    public interface OnItemClickListener {
        void onUserItemClicked(ZipPurchaseModel zipPurchaseModel);
        void onUserSwipe(ZipPurchaseModel zipPurchaseModel);
    }

    private List<ZipPurchaseModel> zipPurchaseModelCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public ZipPurchaseAdapter(Context context, Collection<ZipPurchaseModel> zipPurchaseModelCollection) {
        this.nearbyCollection(zipPurchaseModelCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.zipPurchaseModelCollection = (List<ZipPurchaseModel>) zipPurchaseModelCollection;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return (this.zipPurchaseModelCollection != null) ? this.zipPurchaseModelCollection.size() + 3 : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == _HEADER)
            return _HEADER;
        else if (position == 2)
            return _HEADER;
        else
            return _MAINVIEW;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == _HEADER) {
            View view = this.layoutInflater.inflate(R.layout.zipcode_header, parent, false);
            return new Header(view);
        } else {
            View view = this.layoutInflater.inflate(R.layout.row_user, parent, false);
            return new ListItems(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof Header && position == _HEADER) {
            ((Header) holder).zip_header.setText(" User Zip Codes");
        } else if (holder instanceof Header && position == SECOND_HEADER) {
            ((Header) holder).zip_header.setText("Extra zip codes(Swipe to delete)");
        } else if (holder instanceof ListItems && position == 1 && zipPurchaseModelCollection.size()>0) {
            final ZipPurchaseModel zipPurchaseModel = this.zipPurchaseModelCollection.get(0);
            ((ListItems) holder).zipCodetext.setText(zipPurchaseModel.getZipCode());
        } else {
            final ZipPurchaseModel zipPurchaseModel = this.zipPurchaseModelCollection.get(position - 3);
            ((ListItems) holder).zipCodetext.setText(zipPurchaseModel.getZipCode());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ZipPurchaseAdapter.this.onItemClickListener != null) {
                    ZipPurchaseAdapter.this.onItemClickListener.onUserItemClicked(zipPurchaseModel);
                }
            }
        });
        }

    }

    public void setUsersCollection(Collection<ZipPurchaseModel> zipPurchaseModelCollection) {
        this.nearbyCollection(zipPurchaseModelCollection);
        this.zipPurchaseModelCollection = (List<ZipPurchaseModel>) zipPurchaseModelCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    private void nearbyCollection(Collection<ZipPurchaseModel> zipPurchaseModelCollection) {
        if (zipPurchaseModelCollection == null) {
            throw new IllegalArgumentException("not null");
        }
    }





    class ListItems extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView zipCodetext;

        public ListItems(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class Header extends RecyclerView.ViewHolder {
        @Bind(R.id.zip_header)
        TextView zip_header;

        public Header(View itemView1) {
            super(itemView1);
            ButterKnife.bind(this, itemView1);
        }
    }

}
