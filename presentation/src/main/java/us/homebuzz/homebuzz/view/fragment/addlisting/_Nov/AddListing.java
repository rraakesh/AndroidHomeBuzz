package us.homebuzz.homebuzz.view.fragment.addlisting._Nov;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.model.SearchAdressModel;
import us.homebuzz.homebuzz.presenter.UserGetOnBoardPropPresenter;
import us.homebuzz.homebuzz.view.GetUserPropView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/28/2015.
 */
public class AddListing extends BaseFragment implements GetUserPropView {
    private static final String TAG = AddListing.class.getSimpleName();

    public static AddListing newInstance() {
        AddListing addListing = new AddListing();
        return addListing;
    }

    @Bind(R.id.address)
    EditText address;
    @Bind(R.id.city)
    EditText city;
    @Bind(R.id.state)
    EditText state;
    @Bind(R.id.zipcode)
    EditText zipcode;
    @Bind(R.id.rl_progress)
    public RelativeLayout rl_progress;
    @Inject
    UserGetOnBoardPropPresenter propPresenter;
    @Inject
    Constants constants;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_listing, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add New Listing");

        this.initialize();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).addListingComponent(new AddListingModule()).inject(this);
        this.propPresenter.setView(this);
    }

    //Search the Location Using the Service Call
    @OnClick(R.id.btnEditProfileContinue)
    public void onSearchButtonClick() {


        WeakHashMap<String, String> searchData = new WeakHashMap<>(5);
        searchData.put(constants._LISTING_CITY, city.getText().toString());
        searchData.put(constants._LISTING_STREET, address.getText().toString());
        searchData.put(constants._LISTING_STATE, state.getText().toString());
        searchData.put(constants._LISTING_SUBMIT, "");
        searchData.put(constants._LISTING_ZIP, zipcode.getText().toString());

        this.propPresenter.initialize(searchData);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.propPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.propPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //this.propPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void hideLoading() {
        rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void showLoading() {
        rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void viewGetProp(SearchAdressModel searchAdressModel) {
        Log.d(TAG, "Address " + searchAdressModel.getAddress());
        if (searchAdressModel.getAddress() == null)
            showToastMessage("No address Found!!");
        else {
            //Show the Dialog
            AddListingData addListingData =new AddListingData();
            addListingData.setLat(searchAdressModel.getLat());
            addListingData.setLon(searchAdressModel.getLon());
            addListingData.setParcelNum(searchAdressModel.getParcelNum());
            addListingData.setCity(searchAdressModel.getCity());
            addListingData.setState(searchAdressModel.getState());
            addListingData.setAcres(searchAdressModel.getAcres());
            addListingData.setBaths(searchAdressModel.getBaths());
            addListingData.setZip(searchAdressModel.getZip());
            addListingData.setAddress(searchAdressModel.getAddress());
            addListingData.setStreet(searchAdressModel.getStreet());
            Log.d(TAG,"street in 1st"+searchAdressModel.getStreet());
            showSearchialog(searchAdressModel.getAddress() + ","+searchAdressModel.getState() +  ","+searchAdressModel.getCity()+"?",addListingData);
        }
    }

    private void showSearchialog(String address,final AddListingData addListingData) {
        final Dialog dialog = new Dialog(getActivity());
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //dialog.setTitle("Is This \n" + address);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photo_dailog);

        dialog.setCancelable(true);
        dialog.show();
        TextView dialog_title=(TextView)dialog.findViewById(R.id.dialog_title) ;
        dialog_title.setVisibility(View.VISIBLE);
        dialog_title.setText("Is This \n" + address);

        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        TextView take_photo = (TextView) dialog.findViewById(R.id.take_photo);
        TextView photo_gallery = (TextView) dialog.findViewById(R.id.photo_gallery);
        take_photo.setText("Yes");
        photo_gallery.setText("No");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"Listing Data Send"+addListingData.getAddress());
                dialog.dismiss();
                //Move to Next Fragment
                replaceListener.CallReplaceFragment(R.id.conatainer_layout,AddListingStep1.newInstance(addListingData));
              //  replaceListener.CallReplaceFragment(R.id.conatainer_layout,AddListingStep2.newInstance(addListingData));
            }
        });

        photo_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }


}
