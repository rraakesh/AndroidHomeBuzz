package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.ZipPurchaseModel;

/**
 * Created by amit.singh on 11/27/2015.
 */
public interface ZipPurchaseView extends LoadDataView {
    /**
     * View a {@link } ZipCodePurchase.
     *
     * @param  zipPurchaseModel The user that will be shown.
     */
    void viewZipCodePurchaseList(Collection<ZipPurchaseModel> zipPurchaseModel);
    void viewUser(ZipPurchaseModel zipPurchaseModel);
}
