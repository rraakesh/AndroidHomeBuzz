package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.ZipCodeDeleteModel;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.presenter.ZipCodeDeletePresenter;
import us.homebuzz.homebuzz.presenter.ZipCodePurchasePresenter;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.ZipCodeDeleteView;
import us.homebuzz.homebuzz.view.ZipPurchaseView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.swipelistview.BaseSwipListAdapter;
import us.homebuzz.homebuzz.view.swipelistview.SwipeMenu;
import us.homebuzz.homebuzz.view.swipelistview.SwipeMenuCreator;
import us.homebuzz.homebuzz.view.swipelistview.SwipeMenuItem;
import us.homebuzz.homebuzz.view.swipelistview.SwipeMenuListView;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipCodePuchaseFragment1 extends BaseFragment implements ZipPurchaseView, UserProfileView, ZipCodeDeleteView {

    private SwipeMenuListView mListView;
    private ArrayList<ZipPurchaseModel> zipData;

    public static ZipCodePuchaseFragment1 newInstance() {
        ZipCodePuchaseFragment1 zipCodePurchase = new ZipCodePuchaseFragment1();
        return zipCodePurchase;
    }

    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;

    private UsersLayoutManager usersLayoutManager;
    private AppAdapter zipPurchaseAdapter;
    @Inject
    ZipCodePurchasePresenter zipCodePurchasePresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    UserProfilePresenter userProfilePresenter;
    @Inject
    ZipCodeDeletePresenter zipCodeDeletePresenter;
    boolean isSubscriptionActive;
    @Bind(R.id.title)
    TextView firstZip;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View zip_purchase = inflater.inflate(R.layout.zip_purchase_listview, container, false);
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.zipcode_header, mListView, false);
        mListView = (SwipeMenuListView) zip_purchase.findViewById(R.id.listView);
        mListView.addHeaderView(header, null, false);


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(75));
                // set a icon
                //  deleteItem.setTitle("Delete");
                //   deleteItem.setTitleSize(18);
                // set item title font color
                //   deleteItem.setTitleColor(Color.WHITE);
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);

        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                ZipPurchaseModel item = zipData.get(position);
                switch (index) {
                    case 0:
                        // delete
//					delete(item);

                        zipData.remove(position);
                        zipPurchaseAdapter.notifyDataSetChanged();
                        WeakHashMap<String, String> data = new WeakHashMap<>(1);
                        data.put(constants.SEARCH_ID, item.getId().toString());
                        zipCodeDeletePresenter.initialize(data);
                        break;
                }
                return false;
            }
        });


        // set SwipeListener
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });

        // set MenuStateChangeListener
        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });


        ButterKnife.bind(this, zip_purchase);

        return zip_purchase;
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Zip Codes");
        this.initialize();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.userProfilePresenter.setView(this);
        this.userProfilePresenter.initialize(sharedPreferences.getInt(constants.KEY_ID, 0));
        this.zipCodePurchasePresenter.setView(this);
        this.zipCodePurchasePresenter.initialize();
        this.zipCodeDeletePresenter.setView(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.zipCodePurchasePresenter.resume();
        this.userProfilePresenter.resume();
        this.zipCodeDeletePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.zipCodePurchasePresenter.pause();
        this.userProfilePresenter.pause();
        this.zipCodeDeletePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.zipCodePurchasePresenter.destroy();
        this.userProfilePresenter.destroy();
        this.zipCodeDeletePresenter.destroy();
    }

    @Override
    public void showError(String message) {
        //  showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.zipcode_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_add:
                if (isSubscriptionActive)
                    replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodeAddFragment.newInstance());
                else
                    showToastMessage("Maximum zips reached,Please subscribe to add more");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void viewZipCodePurchaseList(Collection<ZipPurchaseModel> zipPurchaseModel) {
        Log.d(TAG, "Zip Purchase List" + zipPurchaseModel.size());
        if (zipPurchaseModel != null && zipPurchaseModel.size() > 0) {
            this.zipPurchaseAdapter = new AppAdapter(this.getContext(), zipPurchaseModel);
            mListView.setAdapter(zipPurchaseAdapter);
            zipData = (ArrayList<ZipPurchaseModel>) zipPurchaseModel;
            ZipPurchaseModel zipPurchaseModela = ((ArrayList<ZipPurchaseModel>) zipPurchaseModel).get(0);
            firstZip.setText(zipPurchaseModela.getZipCode());
        } else {
            showToastMessage("No Purchase Zip Found!!");
        }
    }

    @Override
    public void viewUser(ZipPurchaseModel zipPurchaseModel) {
        //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodeUpdateFragment.newInstance(zipPurchaseModel));
    }

    @OnClick(R.id.purchase_zipCode)
    public void onZipCodePurchaseClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, new UnlimitedZipFragment());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.zipCodePurchasePresenter.destroy();
        this.userProfilePresenter.destroy();
        this.zipCodeDeletePresenter.destroy();
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Settings");
        ButterKnife.unbind(this);
    }

    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        Log.e(TAG, "Checking theUserProfileModel" + userProfileModel.getSubscriptionActive());
        if (userProfileModel != null) {
            this.isSubscriptionActive = (userProfileModel.getSubscriptionActive().equalsIgnoreCase("true") ? true : false);
        }
    }

    @Override
    public void viewZipDelete(ZipCodeDeleteModel zipCodeDeleteModel) {

    }
//Adapter class

    class AppAdapter extends BaseSwipListAdapter {
        ArrayList<ZipPurchaseModel> zipPurchaseModel;
        Context ctx;

        public AppAdapter(Context context, Collection<ZipPurchaseModel> zipPurchaseModel) {
            this.zipPurchaseModel = (ArrayList<ZipPurchaseModel>) zipPurchaseModel;
            this.ctx = context;
        }

        @Override
        public int getCount() {
            return zipPurchaseModel.size();

        }

        @Override
        public Object getItem(int position) {
            return zipPurchaseModel.get(position);
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(ctx,
                        R.layout.row_user, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            final ZipPurchaseModel item = zipPurchaseModel.get(position);

            holder.title.setText(item.getZipCode());

            //  holder.title.setText(item.getZipCode());
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodeUpdateFragment.newInstance(item));
                }
            });
            return convertView;
        }

        class ViewHolder {

            TextView title;

            public ViewHolder(View view) {
                title = (TextView) view.findViewById(R.id.title);
                view.setTag(this);
            }
        }

        @Override
        public boolean getSwipEnableByPosition(int position) {
//            if(position % 2 == 0){
//                return false;
//            }
            return true;
        }
    }
}
