package us.homebuzz.homebuzz.view.profile;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.EditProfileModelValues;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireContact;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.InviteFriendsFragment;

/**
 * Created by abhishek.singh on 11/16/2015.
 */
public class EditSocialMediaProfile extends BaseFragment implements PermissionBus {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";
    private static final String ARGUMENT_KEY_USER_PROFILE1 = "isProfile1";
    private static final String ARGUMENT_KEY_USER_PROFILE = "Profile";

    public static EditSocialMediaProfile newInstance(boolean isProfileExist, UserProfileModel userProfileModel, EditProfileModelValues values) {
        EditSocialMediaProfile userEditProfile = new EditSocialMediaProfile();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE, userProfileModel);
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE1, values);
        argumentsBundle.putBoolean(ARGUMENT_KEY_IS_USER_PROFILE, isProfileExist);
        Log.e("social profile",values.getProfilePic());
        userEditProfile.setArguments(argumentsBundle);
        return userEditProfile;
    }

   public EditSocialMediaProfile(){super();}

    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.facebook_url)
    EditText facebook_url;
    @Bind(R.id.twitter_url)
    EditText twitter_url;
    @Bind(R.id.linkedin_url)
    EditText linkedin_url;
    @Bind(R.id.website_url)
    EditText website_url;
    @Bind(R.id.invite_friends)
    Button invite_friends;
    @Bind(R.id.buzz_point)
    TextView buzz_point;
    @Bind(R.id.btnEditProfileContinue)
    Button btnEditProfileContinue;
    private UserProfileModel userProfileModel;
    private EditProfileModelValues values;
    private boolean isProfileExist;
    @Inject
    Picasso picasso;

    private RequireContact requireContact;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_editSocialMedia = inflater.inflate(R.layout.edit_profile_new_social, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE) != null)
            userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE);
        values = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE1);
        isProfileExist = this.getArguments().getBoolean(ARGUMENT_KEY_IS_USER_PROFILE);
        System.out.println("Boolean Profile =" + isProfileExist + "UserProfile " + userProfileModel.getId());
        System.out.println("Boolean Profile =" + isProfileExist + "values  " + values.getName());
        ButterKnife.bind(this, list_editSocialMedia);
        return list_editSocialMedia;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Social");
        this.initialize();
        setUpUI();
        if(this.getActivity() instanceof RequireContact)
            requireContact=(RequireContact) this.getActivity();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    private void setUpUI() {
        txtProfileName.setText((userProfileModel.getName() != null && !(userProfileModel.getName().matches("null")) ? userProfileModel.getName() : ""));
        facebook_url.setText((userProfileModel.getFacebookUrl() != null && !(userProfileModel.getFacebookUrl().matches("null")) ? userProfileModel.getFacebookUrl() : ""));
        twitter_url.setText((userProfileModel.getTwitterUrl() != null && !(userProfileModel.getTwitterUrl().matches("null")) ? userProfileModel.getTwitterUrl() : ""));
        linkedin_url.setText((userProfileModel.getLinkedIn() != null && !(userProfileModel.getLinkedIn().matches("null")) ? userProfileModel.getLinkedIn() : ""));
        website_url.setText((userProfileModel.getWebsite() != null && !(userProfileModel.getWebsite().matches("null"))) ? userProfileModel.getWebsite().toString() : "");

        //// TODO: 11/17/2015 find the value of buzz point...
        buzz_point.setText("+5 Buzz Points");
        if (!values.getProfilePic().equalsIgnoreCase("")) {
            picasso.load(new File(values.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
        } else{
            if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().matches("null")){
                picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
            }else {

            }

        }
    }

    @OnClick(R.id.invite_friends)
        public void inviteFriends(){
        /*Intent intent = new Intent(EditSocialMediaProfile.this.getActivity(),InviteFriendsFragment.class);
        startActivity(intent);*/
        if (Build.VERSION.SDK_INT >= 23) {
        requireContact.onContactRequest();}
        else{
              this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, new InviteFriendsFragment());
        }

        System.out.println("Invite_Friends");
    }


    @OnClick(R.id.btnEditProfileContinue)
    public void onClickEditSocialProfile() {
        values.setFacebookUrl(facebook_url.getText().toString());
        values.setTwitterUrl(twitter_url.getText().toString());
        values.setLinkedIn(linkedin_url.getText().toString());
        values.setWebsite(website_url.getText().toString());
        //  Log.e("values are", values.getLastName());
       this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, EditBioProfile.newInstance(isProfileExist, userProfileModel, values));
    }
    @Override
    public void onDestroyView() {

        super.onDestroyView();

    }

    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {
        if(isSuccess==true) {
            navigateToInviteFragment();
        }else {
            Snackbar.make(this.getView(), R.string.permissions_not_granted,
                    Snackbar.LENGTH_SHORT)
                    .show();
        }
        Log.d(TAG,"Status of Contact Permission"+isSuccess);
    }

    private void navigateToInviteFragment(){
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Your code to run in GUI thread here
                EditSocialMediaProfile.this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, new InviteFriendsFragment());
            }//public void run() {
        });

    }
    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {

    }
}
