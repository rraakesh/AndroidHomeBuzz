package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.presenter.ZipCodePurchasePresenter;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.ZipPurchaseView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.settings.ZipPurchaseAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipCodePurchaseFragment extends BaseFragment implements ZipPurchaseView, UserProfileView {
    private static final String TAG = ZipCodePurchaseFragment.class.getSimpleName();

    public static ZipCodePurchaseFragment newInstance() {
        ZipCodePurchaseFragment zipCodePurchase = new ZipCodePurchaseFragment();
        return zipCodePurchase;
    }

    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    private UsersLayoutManager usersLayoutManager;
    private ZipPurchaseAdapter zipPurchaseAdapter;
    @Inject
    ZipCodePurchasePresenter zipCodePurchasePresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    UserProfilePresenter userProfilePresenter;
    boolean isSubscriptionActive;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View zip_purchase = inflater.inflate(R.layout.base_recycler1, container, false);
        ButterKnife.bind(this, zip_purchase);
        return zip_purchase;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Zip Codes");
        this.initialize();
        setUpUI();
    }

    private void setUpUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.userProfilePresenter.setView(this);
        this.userProfilePresenter.initialize(sharedPreferences.getInt(constants.KEY_ID, 0));
        this.zipCodePurchasePresenter.setView(this);
        this.zipCodePurchasePresenter.initialize();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.zipCodePurchasePresenter.resume();
        this.userProfilePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.zipCodePurchasePresenter.pause();
        this.userProfilePresenter.pause();
    }


    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.zipcode_add, menu);
    }

    private ZipPurchaseAdapter.OnItemClickListener onItemClickListener =
            new ZipPurchaseAdapter.OnItemClickListener() {
                @Override
                public void onUserItemClicked(ZipPurchaseModel zipPurchaseModel) {
                    if (zipPurchaseModel != null) {
                        ZipCodePurchaseFragment.this.zipCodePurchasePresenter.onUserClicked(zipPurchaseModel);
                    }
                }

                @Override
                public void onUserSwipe(ZipPurchaseModel zipPurchaseModel) {

                }
            };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_add:
                if (isSubscriptionActive)
                    replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodeAddFragment.newInstance());
                else
                    showToastMessage("Maximum zips reached,Please subscribe to add more");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void viewZipCodePurchaseList(Collection<ZipPurchaseModel> zipPurchaseModel) {
        Log.d(TAG, "Zip Purchase List" + zipPurchaseModel.size());
        if (zipPurchaseModel != null && zipPurchaseModel.size() > 0) {
            this.zipPurchaseAdapter = new ZipPurchaseAdapter(this.getContext(), zipPurchaseModel);
            this.base_list.setAdapter(zipPurchaseAdapter);
            this.zipPurchaseAdapter.setOnItemClickListener(onItemClickListener);
            // this.zipPurchaseAdapter.setUsersCollection(zipPurchaseModel);
        } else {
            showToastMessage("No Purchase Zip Found!!");
        }
    }

    @Override
    public void viewUser(ZipPurchaseModel zipPurchaseModel) {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodeUpdateFragment.newInstance(zipPurchaseModel));
    }

    @OnClick(R.id.purchase_zipCode)
    public void onZipCodePurchaseClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, new UnlimitedZipFragment());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.zipCodePurchasePresenter.destroy();
        this.userProfilePresenter.destroy();
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Settings");
        ButterKnife.unbind(this);
    }

    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        Log.d(TAG, "Checking theUserProfileModel" + userProfileModel.getSubscriptionActive());
        if (userProfileModel != null)
            this.isSubscriptionActive = (userProfileModel.getSubscriptionActive().equalsIgnoreCase("true") ? true : false);
    }
}
