package us.homebuzz.homebuzz.view.profile.friendsprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.presenter.FriendsFollowedPresenter;
import us.homebuzz.homebuzz.view.UserFrFriendsView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.friends.FriendFollowAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.searchPeople.SearchPeople;

/**
 * Created by abhishek.singh on 11/18/2015.
 */
public class FriendsFollowingList extends BaseFragment implements UserFrFriendsView {
    private static final String TAG = FriendsFollowingList.class.getSimpleName();

    public static FriendsFollowingList newInstance() {
        FriendsFollowingList friendsFollowingList = new FriendsFollowingList();
        return friendsFollowingList;
    }

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.search)
    EditText search;
    @Bind(R.id.llSearchPeople)
    LinearLayout llSearchPeople;
    @Inject
    Picasso picasso;
    @Inject
    FriendsFollowedPresenter friendsFollowedPresenter;
    private UsersLayoutManager usersLayoutManager;
    private FriendFollowAdapter friendFollowAdapter;

    ArrayList<UserFriendFollowedModel> mdata = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View friendsFollowingList = inflater.inflate(R.layout.friend_following_list, container, false);
        ButterKnife.bind(this, friendsFollowingList);
        return friendsFollowingList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Friends");
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
        friendsFollowedPresenter.setView(this);
        friendsFollowedPresenter.initialize();
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.friendFollowAdapter = new FriendFollowAdapter(getActivity(), new ArrayList<UserFriendFollowedModel>(), picasso);
        this.friendFollowAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(friendFollowAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.friendsFollowedPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.friendsFollowedPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.friendsFollowedPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderUserEstimateList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection) {
        Log.d(TAG, "The Followed List " + userFriendFollowedModelCollection.size());
        mdata = (ArrayList<UserFriendFollowedModel>) userFriendFollowedModelCollection;
        this.friendFollowAdapter.setUsersCollection(userFriendFollowedModelCollection);
    }

    private FriendFollowAdapter.OnItemClickListener onItemClickListener = new FriendFollowAdapter.OnItemClickListener() {
        @Override
        public void onFollowedClicked(UserFriendFollowedModel userFriendFollowedModel) {
            if (FriendsFollowingList.this.friendsFollowedPresenter != null && userFriendFollowedModel != null) {
                FriendsFollowingList.this.friendsFollowedPresenter.onUserClicked(userFriendFollowedModel);
            }
        }
    };

    @Override
    public void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection) {
        String id = String.valueOf(userFriendFollowedModelCollection.getId());
        Log.e("The Followed List id ", id);
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, FriendFollowingFollowersProfileView.newInstance(id));
    }

    @OnClick(R.id.llSearchPeople)
    public void onClickSearchPeople() {
        Fragment fragment = new SearchPeople();
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, fragment);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new UsersLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    searchList(charSequence);
                } else {
                    FriendsFollowingList.this.friendFollowAdapter.setUsersCollection(mdata);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void searchList(CharSequence str) {
        Collection<UserFriendFollowedModel> tempArrayList = new ArrayList<>();
        if (str.length() > 0) {
            for (UserFriendFollowedModel userFriendFollowedModel : mdata) {
                if (userFriendFollowedModel.getName().toLowerCase().contains(str)) {
                    tempArrayList.add(userFriendFollowedModel);
                }
                FriendsFollowingList.this.friendFollowAdapter.setUsersCollection(tempArrayList);
            }
        }
    }
}
