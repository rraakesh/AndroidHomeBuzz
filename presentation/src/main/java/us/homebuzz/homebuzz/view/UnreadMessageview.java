package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;

/**
 * Created by amit.singh on 11/18/2015.
 */
public interface UnreadMessageview extends LoadDataView {
    /**
     * View a {@link } UnreadMessages
     *
     * @param userUnreadMessagesModel Messages that a user has not Read
     */
    void viewUnreadMessages(Collection<UserUnreadMessagesModel> userUnreadMessagesModel);

}
