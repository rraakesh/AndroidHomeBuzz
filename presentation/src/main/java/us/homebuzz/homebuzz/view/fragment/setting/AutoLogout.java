package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rakesh.kumar on 11/24/2015.
 */
public class AutoLogout extends BaseFragment {
    private static final String TAG = AutoLogout.class.getSimpleName();

    public static AutoLogout newInstance() {
        AutoLogout autoLogout = new AutoLogout();
        return autoLogout;
    }

    @Bind(R.id.autoLogout)
    ImageView autoLogout;
    @Inject
    public SharedPreferences sharedPreferences;
    @Inject
    Constants constants;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("General Settings");
        this.initialize();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View auto_log = inflater.inflate(R.layout.auto_logout, container, false);
        ButterKnife.bind(this, auto_log);

        return auto_log;
    }

    private void setUpUI() {
        if (this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            autoLogout.setImageResource(R.drawable.toggle_on);
        } else {
            autoLogout.setImageResource(R.drawable.toggle_off);
        }

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        setUpUI();
    }

    @OnClick(R.id.autoLogout)
    public void imageClick() {
        if (this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            autoLogout.setImageResource(R.drawable.toggle_off);
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_AUTO_LOGOUT, false).commit();
        } else {
            autoLogout.setImageResource(R.drawable.toggle_on);
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_AUTO_LOGOUT, true).commit();
        }
        Log.d(TAG, "AUTo" + this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false));
    }

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Settings");
        super.onDestroyView();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (this.sharedPreferences != null && this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_LOGIN, false).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.sharedPreferences != null && this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_LOGIN, false).commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.sharedPreferences != null && this.sharedPreferences.getBoolean(constants.KEY_IS_AUTO_LOGOUT, false)) {
            this.sharedPreferences.edit().putBoolean(constants.KEY_IS_LOGIN, false).commit();
        }
    }
}


