package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.ZipCodeDeleteModel;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public interface ZipCodeDeleteView  extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  zipCodeDeleteModel The user that will be shown.
     */
    void viewZipDelete(ZipCodeDeleteModel zipCodeDeleteModel);
}
