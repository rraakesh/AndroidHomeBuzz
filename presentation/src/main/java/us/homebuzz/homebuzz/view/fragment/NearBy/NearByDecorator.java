package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.BaseDecorator;

/**
 * Created by amit.singh on 10/5/2015.
 */
public abstract  class NearByDecorator extends BaseDecorator implements NearByTabStrip  {

    public NearByDecorator(){}
    @Bind(R.id.container_layout_tab)
    FrameLayout container_layout_tab;
    @Bind(R.id.tab_view)
    LinearLayout tab_fragment;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ButterKnife.setDebug(true);
        View LowerTabFrameLayout = inflater.inflate(R.layout.lower_tab_framelayout, container, false);
        ButterKnife.bind(this, LowerTabFrameLayout);


//        tab_fragment.setAlpha(0.4f);
        return LowerTabFrameLayout;
    }

    public void onLeftIconClick(Fragment leftFragment){
        addFragment(R.id.container_layout_tab,leftFragment);
    }
    public void onFilterIconClick(android.support.v4.app.DialogFragment dialogFragment){
        if(dialogFragment instanceof android.support.v4.app.DialogFragment/* && dialogFragment instanceof NearbyFilterFragment*/)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());

    }
    public void onRightIconClick(Fragment rightFragment){
        addFragment(R.id.container_layout_tab,rightFragment);

    }


}
