package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.support.v4.app.Fragment;

/**
 * Created by amit.singh on 10/3/2015.
 */
public interface LowerTabStrip {
    void onLeftIconClick(Fragment leftFragment);
    void onRightIconClick(Fragment leftFragment);
}
