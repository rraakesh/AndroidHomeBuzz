package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.presenter.AccuracyPresenter;
import us.homebuzz.homebuzz.view.UserAccuracyView;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.leaderboard.AccuracyAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseNestedFragment;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendFollowingFollowersProfileView;

/**
 * Created by amit.singh on 10/21/2015.
 */
public class AccuracyFragment extends BaseNestedFragment implements UserAccuracyView ,LeaderBoard.LederBoardFilter{
    private static final String TAG = AccuracyFragment.class.getSimpleName();
    @Inject
    AccuracyPresenter accuracyPresenter;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;
    private ReplaceFragment replaceFragment;
    private AccuracyAdapter accuracyAdapter;
    private UsersLayoutManager usersLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, list_nearby);
        return list_nearby;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        if(getParentFragment() instanceof LeaderBoard){
            Log.d(TAG, "LearBoard" + ((LeaderBoard) this.getParentFragment()).replaceFragmentLarboard);
            this.replaceFragment=((LeaderBoard) this.getParentFragment()).replaceFragmentLarboard;
        }
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).leaderBoardComponent(new LeaderBoardModule()).inject(this);
        accuracyPresenter.setView(this);
        accuracyPresenter.initialize(null);
    }
    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.accuracyAdapter = new AccuracyAdapter(getActivity(), new ArrayList<UserAccuracyModel>(),picasso);
        this.accuracyAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(accuracyAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.accuracyPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.accuracyPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.accuracyPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        super.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }



    @Override
    public void viewUser(UserAccuracyModel userAccuracyModel) {
        //Todo Navigate to Next Fragment From Here
        String id = String.valueOf(userAccuracyModel.getId());
        Log.e("The Followed List id ", id);
        replaceFragment.CallReplaceFragment(R.id.conatainer_layout, FriendFollowingFollowersProfileView.newInstance(id));

    }

    private AccuracyAdapter.OnItemClickListener onItemClickListener =
            new AccuracyAdapter.OnItemClickListener() {
                @Override
                public void onAccuracyItemClicked(UserAccuracyModel userAccuracyModel) {
                    if (AccuracyFragment.this.accuracyPresenter != null && userAccuracyModel != null) {
                        AccuracyFragment.this.accuracyPresenter.onUserClicked(userAccuracyModel);
                    }
                }
            };
    @Override
    public void renderUserAccuracyList(Collection<UserAccuracyModel> userAccuracyModelsCollection) {
        Log.d(TAG, "The UserAccuracyModel List " + userAccuracyModelsCollection.size()+"Picasso"/*+ picasso*/);
        if (userAccuracyModelsCollection != null) {
            this.accuracyAdapter.setUsersCollection(userAccuracyModelsCollection);
        }
    }

    @Override
    public void performFilter(WeakHashMap<String, String> filterData) {
        Log.d(TAG, "The UserAccuracyModel  " +filterData.size());
        accuracyPresenter.initialize(filterData);
    }
}
