package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 11/3/2015.
 */
public interface UserFrFriendsView extends LoadDataView {

/*
    void viewUser(UserEstimateModel userEstimateModel);*/

    /**
     * Render a user list in the UI.
     *
     * @param userFriendFollowedModelCollection The collection of {@link UserModel} that will be shown.
     */
    void renderUserEstimateList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection);
    void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection);

}
