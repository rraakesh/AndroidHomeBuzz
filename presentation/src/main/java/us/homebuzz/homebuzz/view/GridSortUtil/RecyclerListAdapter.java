/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.homebuzz.homebuzz.view.GridSortUtil;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import us.homebuzz.homebuzz.R;

/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter{
    private static  final String TAG=RecyclerListAdapter.class.getSimpleName();
    /**
     * Max allowed duration for a "click", in milliseconds.
     */
    private static final int MAX_CLICK_DURATION = 2000;

    /**
     * Max allowed distance to move during a "click", in DP.
     */
    private static final int MAX_CLICK_DISTANCE = 15;
    private final List<String> mItems = new ArrayList<>();
    private final List<Integer> mItems1 = new ArrayList<>();
    private final OnStartDragListener mDragStartListener;
    private static RecyclerViewClickListener mRecyclerViewClickListener;
    private long pressStartTime;
    private float pressedX;
    private float pressedY;
    private Context context;
    private final List<Integer> mItems2 = new ArrayList<>();

    public RecyclerListAdapter(Context context, OnStartDragListener dragStartListener,RecyclerViewClickListener recyclerViewClickListener) {
        mDragStartListener = dragStartListener;
        this.context=context;
        mRecyclerViewClickListener=recyclerViewClickListener;
        mItems.addAll(Arrays.asList(context.getResources().getStringArray(R.array.cards_name)));
        int[] rainbow = context.getResources().getIntArray(R.array.landing_color);
        mItems2.add(rainbow[0]);
        mItems2.add(rainbow[1]);
       // mItems2.add(rainbow[2]);
        mItems2.add(rainbow[3]);
       // mItems2.add(rainbow[4]);
       // mItems2.add(rainbow[5]);
       // mItems2.add(rainbow[6]);
        mItems2.add(rainbow[7]);
       // System.out.println(mItems2.get(0));
        mItems1.add(R.drawable.homebuzz_logo);
        mItems1.add(R.drawable.checkin_bg);
      //  mItems1.add(R.drawable.addlisting_bg);
        mItems1.add(R.drawable.leaderboard_bg);
       // mItems1.add(R.drawable.friends_bg);
       // mItems1.add(R.drawable.message_bg_new);
      //  mItems1.add(R.drawable.mybuzz_bg_new);
        mItems1.add(R.drawable.settings_bg_new);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        holder.textView.setText(mItems.get(position));
        final int mposition=position;
        holder.handleView.setImageResource(mItems1.get(position));
        holder.card.setCardBackgroundColor(mItems2.get(position));
        holder.card.setTag(mposition);
        if(position>3) {
            StaggeredGridLayoutManager.LayoutParams layoutParams1 = ((StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams());
            layoutParams1.setFullSpan(false);
        }
        else {
            StaggeredGridLayoutManager.LayoutParams layoutParams1 = ((StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams());
            layoutParams1.setFullSpan(true);
        }
        /**
         * This Method Handle the Click and Touch Both
         * There are two things to Consider for touch and Click when Handling both
         * 1.Time Duration of Click and Touch
         * 2.When to calculate the Time ie:-start the time Calculation
         *  Here we have MAX_CLICK_DURATION and this is responsible for click duration multiples  with the Device dp
         */
        // Start a drag whenever the handle view it touched
        holder.card.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (MotionEventCompat.getActionMasked(event)){
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        long pressDuration = System.currentTimeMillis() - pressStartTime;
                        if (pressDuration < MAX_CLICK_DURATION && distance(pressedX, pressedY, event.getX(), event.getY()) < MAX_CLICK_DISTANCE) {
                            Log.i(TAG, "Click detected");
                            // Click event has occurred
                            holder.card.performClick();
                        }else{
                            Log.i(TAG, "movement detected");
                            mDragStartListener.onStartDrag(holder);
                        }
                        break;
                    case  MotionEvent.ACTION_DOWN:
                        pressStartTime = System.currentTimeMillis();
                        pressedX = event.getX();
                        pressedY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    default:
                        break;
                }
                return true;
            }

        });


    }

    /**
     * @Link http://stackoverflow.com/questions/9965695/how-to-distinguish-between-move-and-click-in-ontouchevent
     * Distance between two Points for More info visit the Above Link
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    private  float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }
    //Convert the px to Dp
    private  float pxToDp(float px) {
        return px / this.context.getResources().getDisplayMetrics().density;
    }
    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        mItems1.remove(position);
        mItems2.remove(position);
        notifyItemRemoved(position);

    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mItems, fromPosition, toPosition);
        Collections.swap(mItems1, fromPosition, toPosition);
        Collections.swap(mItems2, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
       // System.out.println("Item  Count"+mItems.size());
        return mItems.size();
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder, View.OnClickListener {
        public final TextView textView;
        public final ImageView handleView;
        public final android.support.v7.widget.CardView card;
        public ItemViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.title);
            handleView = (ImageView) itemView.findViewById(R.id.img);
            card=(android.support.v7.widget.CardView)itemView.findViewById(R.id.card_layout);
            card.setOnClickListener(this);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }

        @Override
        public void onClick(View v) {
            if (v instanceof android.support.v7.widget.CardView){
                mRecyclerViewClickListener.recyclerViewListClicked(v, (int)v.getTag());
            }
        }

    }

}
