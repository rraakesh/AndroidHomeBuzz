package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.SearchAdressModel;

/**
 * Created by amit.singh on 11/28/2015.
 */
public interface GetUserPropView extends LoadDataView {
    /**
     * View a {@link } UnreadMessages
     *
     * @param searchAdressModel Messages that a user has not Read
     */
    void viewGetProp(SearchAdressModel searchAdressModel);
}
