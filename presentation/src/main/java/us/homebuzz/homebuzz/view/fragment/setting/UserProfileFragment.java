package us.homebuzz.homebuzz.view.fragment.setting;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.view.DrawerProfileBus;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.profile.EditProfile;
import us.homebuzz.homebuzz.view.profile.MyReview;
import us.homebuzz.homebuzz.view.profile.friendsprofile.ProfileFriends;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class UserProfileFragment extends BaseFragment implements UserProfileView {

    public static UserProfileFragment newInstance() {
        UserProfileFragment userProfileFragment = new UserProfileFragment();
        return userProfileFragment;
    }

    private UserProfileModel userProfileModel;
    @Inject
    UserProfilePresenter userProfilePresenter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.edit_profile_layout)
    LinearLayout edit_profile_layout;
    @Bind(R.id.btnFriends)
    LinearLayout btnFriends;
    @Bind(R.id.btnMyReviews)
    LinearLayout btnMyReviews;

    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.txtEmail)
    TextView txtEmail;
    @Bind(R.id.txtPhone)
    TextView txtPhone;

    @Bind(R.id.check_in_val)
    TextView check_in_val;
    @Bind(R.id.Review_val)
    TextView Review_val;
    @Bind(R.id.photos_val)
    TextView photos_val;
    @Bind(R.id.BuzzPoint_val)
    TextView BuzzPoint_val;
    @Bind(R.id.Accuracy_val)
    TextView Accuracy_val;
    @Bind(R.id.estimate_val)
    TextView estimate_val;
    @Inject
    Picasso picasso;
    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    private boolean isProfileExist;
    DrawerProfileBus drawerProfileBus;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View user_settings = inflater.inflate(R.layout.profile_new, container, false);
        ButterKnife.bind(UserProfileFragment.this, user_settings);
        return user_settings;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialize();
        if (this.getActivity() instanceof DrawerProfileBus)
            drawerProfileBus = (DrawerProfileBus) this.getActivity();

        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("My profile");
        userProfilePresenter.setView(this);
        System.out.println("UserId" + sharedPreferences.getInt(constants.KEY_ID, 0));
        userProfilePresenter.initialize(sharedPreferences.getInt(constants.KEY_ID, 0));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showError(String message) {
        showToastMessage("Can't load profile");
        isProfileExist = false;
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        edit_profile_layout.setClickable(false);
        btnMyReviews.setClickable(false);
        btnFriends.setClickable(false);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        edit_profile_layout.setClickable(true);
        btnMyReviews.setClickable(true);
        btnFriends.setClickable(true);

    }


    @SuppressLint("NewApi")
    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        // StringBuilder sb=new StringBuilder();
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        Log.d(TAG, "Checking theUserProfileModel" + userProfileModel.getId());
        this.userProfileModel = userProfileModel;
        isProfileExist = true;
        if (userProfileModel.getId() != null) {
            drawerProfileBus.upDateProfilePic(picasso, userProfileModel.getProfilePic());
            drawerProfileBus.upDateProfileData(userProfileModel.getName(), userProfileModel.getEmail());
            txtProfileName.setText(userProfileModel.getName());
            if (userProfileModel.getEmail() != null) {
                txtEmail.setVisibility(View.VISIBLE);
                txtEmail.setText(userProfileModel.getEmail());
            } else {
                txtEmail.setVisibility(View.GONE);
            }
            if (userProfileModel.getPhone() != null) {
                txtPhone.setVisibility(View.VISIBLE);
                txtPhone.setText(PhoneNumberUtils.formatNumber(userProfileModel.getPhone(), "US"));
            } else {
                txtPhone.setVisibility(View.GONE);
            }
            Accuracy_val.setText(userProfileModel.getAccurate() != null && userProfileModel.getAccurate() != 0 ? df.format(userProfileModel.getAccurate() * 100) + " %" : "0.0" + "%");
            estimate_val.setText(userProfileModel.getEstimatesCount() != null ? userProfileModel.getEstimatesCount().toString() : "0");
            photos_val.setText(userProfileModel.getPhotos() != null ? userProfileModel.getPhotos().toString() : "0");
            BuzzPoint_val.setText(userProfileModel.getPoints() != null ? userProfileModel.getPoints().toString() : "0");
            estimate_val.setText(userProfileModel.getEstimatesCount() != null ? userProfileModel.getEstimatesCount().toString() : "0");

            // // TODO: 11/17/2015 find  the review and checkIn value and fix it..
            Review_val.setText(userProfileModel.getReviewsCount() != null ? userProfileModel.getReviewsCount().toString() : "0");
            check_in_val.setText(userProfileModel.getCheckinCount() != null ? userProfileModel.getCheckinCount().toString() : "0");

            if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().equals("null"))
                picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
        } else {
            showToastMessage("Can't load profile");
        }
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    @OnClick(R.id.edit_profile_layout)
    public void onEditProfileClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, EditProfile.newInstance(isProfileExist, userProfileModel));
    }

    @OnClick(R.id.btnFriends)
    public void onBtnFriends() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, ProfileFriends.newInstance(userProfileModel));
    }

    @OnClick(R.id.btnMyReviews)
    public void onBtnMyReviews() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, MyReview.newInstance());
    }

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }
}
