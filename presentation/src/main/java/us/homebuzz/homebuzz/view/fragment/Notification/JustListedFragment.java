package us.homebuzz.homebuzz.view.fragment.Notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.notification.JustListedAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.ListingFragment;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class JustListedFragment extends BaseFragment {

    private static final String ARGUMENT_KEY_USER_JUST_LISTED_MODEL = "UserJustListedModel";

    public static JustListedFragment newInstance(ArrayList<UserJustListedModel> userJustListedModel) {
        JustListedFragment notificationFragment = new JustListedFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL, userJustListedModel);
        notificationFragment.setArguments(argumentsBundle);
        return notificationFragment;
    }

    @Bind(R.id.search)
    EditText search;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    private UsersLayoutManager usersLayoutManager;
    private ArrayList<UserJustListedModel> userJustListedModelList = new ArrayList<>();
    private JustListedAdapter justListedAdapter;
    private UserNearbyModel userNearbyModel = new UserNearbyModel();
    @Inject
    Picasso picasso;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_base = inflater.inflate(R.layout.base_recycler_just_message, container, false);
        ButterKnife.bind(this, list_base);
        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL) != null)
            userJustListedModelList = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_LISTED_MODEL);
        return list_base;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        this.initialize();
        this.setupUI();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
        this.justListedAdapter = new JustListedAdapter(getActivity(), userJustListedModelList, picasso);
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);

        this.justListedAdapter = new JustListedAdapter(getActivity(), new ArrayList<UserJustListedModel>(), picasso);
        this.justListedAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(justListedAdapter);
        if (userJustListedModelList != null && userJustListedModelList.size() > 0)
            this.justListedAdapter.setUsersCollection(userJustListedModelList);


    }

    private JustListedAdapter.OnItemClickListener onItemClickListener = new JustListedAdapter.OnItemClickListener() {
        @Override
        public void JustListedItemClick(UserJustListedModel userJustListedModel) {
            if (userJustListedModel != null) {
                //Transform data from just listed model to User Model
                //Check some errors
                if (userJustListedModel.getPrice() != null)
                    userNearbyModel.setPrice(Double.valueOf(userJustListedModel.getPrice()));
                userNearbyModel.setBedrooms(userJustListedModel.getBedrooms());
                userNearbyModel.setRemarks(userJustListedModel.getRemarks());
                userNearbyModel.setBaths(userJustListedModel.getBaths());
                userNearbyModel.setStreet(userJustListedModel.getStreet() != null ? userJustListedModel.getStreet() : "");
                userNearbyModel.setCity(userJustListedModel.getCity() != null ? userJustListedModel.getCity() : "");
                userNearbyModel.setState(userJustListedModel.getState() != null ? userJustListedModel.getState() : "");
                userNearbyModel.setZip(userJustListedModel.getZip() != null ? userJustListedModel.getZip() : "");
                userNearbyModel.setYearBuilt(userJustListedModel.getYearBuilt() != null ? userJustListedModel.getYearBuilt() : 0);
                userNearbyModel.setAcres(Double.valueOf(userJustListedModel.getAcres()) != null ? Double.valueOf(userJustListedModel.getAcres()) : 0.0);
                userNearbyModel.setEstimatedSquareFeet(Double.valueOf(userJustListedModel.getEstimatedSquareFeet()) != null ? Double.valueOf(userJustListedModel.getEstimatedSquareFeet()) : 0.0);
                userNearbyModel.setLotSqftApprox( 0.0);
                userNearbyModel.setLastSoldPrice(0.0);
                userNearbyModel.setFireplaces( 0);
                userNearbyModel.setGarage(0.0);
                userNearbyModel.setLat(userJustListedModel.getLat());
                userNearbyModel.setLon(userJustListedModel.getLon());
                userNearbyModel.setFireplaces(userJustListedModel.getFireplaces());
                userNearbyModel.setOptionalBedrooms(userJustListedModel.getOptionalBedrooms());
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingFragment.newInstance(userNearbyModel, true));
            }
        }
    };

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new UsersLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    searchList(charSequence);
                } else {
                    JustListedFragment.this.justListedAdapter.setUsersCollection(userJustListedModelList);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void searchList(CharSequence str) {
        Collection<UserJustListedModel> tempArrayList = new ArrayList<>();
        if (str.length() > 0) {
            for (UserJustListedModel userJustListedModel : userJustListedModelList) {
                if (userJustListedModel.getAddress().toLowerCase().contains(str)
                        || userJustListedModel.getPrice().toString().contains(str)) {
                    tempArrayList.add(userJustListedModel);
                }
                JustListedFragment.this.justListedAdapter.setUsersCollection(tempArrayList);
            }
        }
    }
}
