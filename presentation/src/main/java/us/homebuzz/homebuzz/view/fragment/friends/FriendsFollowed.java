package us.homebuzz.homebuzz.view.fragment.friends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.presenter.FriendsFollowedPresenter;
import us.homebuzz.homebuzz.view.UserFrFriendsView;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.friends.FriendFollowAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseNestedFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInListFragment;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * Created by amit.singh on 10/28/2015.
 */
public class FriendsFollowed extends BaseNestedFragment implements UserFrFriendsView {
    private static final String TAG = FriendsFollowed.class.getSimpleName();

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;
    @Inject
    FriendsFollowedPresenter friendsFollowedPresenter;
    private UsersLayoutManager usersLayoutManager;
    private FriendFollowAdapter friendFollowAdapter;
    private ReplaceFragment replaceFragment;
    private ArrayList<UserFriendFollowedModel> mdata = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, list_nearby);
        return list_nearby;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setupUI();
        Log.d(TAG, "Frag PICASSO" + picasso);
        if (this.getParentFragment() instanceof BaseFriends) {
            this.replaceFragment = ((BaseFriends) this.getParentFragment()).replaceFragmentFriends;
        }

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
        friendsFollowedPresenter.setView(this);
        this.base_list.setAdapter(friendFollowAdapter);
        friendsFollowedPresenter.initialize();
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.friendFollowAdapter = new FriendFollowAdapter(getActivity(), new ArrayList<UserFriendFollowedModel>(), picasso);
        this.base_list.setAdapter(friendFollowAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.friendsFollowedPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.friendsFollowedPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.friendsFollowedPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderUserEstimateList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection) {
        Log.d(TAG, "The Followed List " + userFriendFollowedModelCollection.size());
        this.friendFollowAdapter.setUsersCollection(userFriendFollowedModelCollection);
    }

    @Override
    public void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection) {

    }

}
