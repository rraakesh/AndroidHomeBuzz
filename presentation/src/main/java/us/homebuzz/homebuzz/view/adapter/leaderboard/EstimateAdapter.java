package us.homebuzz.homebuzz.view.adapter.leaderboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;


/**
 * Created by amit.singh on 10/20/2015.
 */
public class EstimateAdapter extends RecyclerView.Adapter<EstimateAdapter.UserViewHolder> {
    public static String TAG = EstimateAdapter.class.getClass().getSimpleName();
    public interface OnItemClickListener {
        void onEsItemClicked(UserEstimateModel userEstimateModel);
    }
    @Inject
    Picasso picasso;
    private List<UserEstimateModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private DecimalFormat accuracyFormatter;
    private OnItemClickListener onItemClickListener1;

    public EstimateAdapter(Context context, Collection<UserEstimateModel> usersCollection,Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserEstimateModel>) usersCollection;
        this.accuracyFormatter=new DecimalFormat("#.##");
           this.picasso=picasso;
    }

    @Override public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }



    @Override public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.leaderboard_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserEstimateModel userEstimateModel = this.usersCollection.get(position);

        holder.number.setText(String.valueOf(position+1));
        holder.leader_zips.setText(userEstimateModel.getZipCode());
        holder.leader_name.setText(userEstimateModel.getName());
        holder.leader_accuracy.setText(String.valueOf(userEstimateModel.getEstimatesCount())+" Estimates,");
        holder.leader_estimate.setText(this.accuracyFormatter.format(userEstimateModel.getAccurate() * 100)+"%"+" Accuracy");
        holder.leader_zips.setText(userEstimateModel.getEstimateZips().toString().replace("[","").replace("]",""));
        if(null!=userEstimateModel && null!=userEstimateModel.getProfilePic()) {
            System.out.println(TAG + "Pic" + String.valueOf(userEstimateModel.getProfilePic()));
         picasso.load(String.valueOf(userEstimateModel.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder.person_image);
          // holder.person_image.setImageUrl(String.valueOf(userEstimateModel.getProfilePic()));
        }else{
        //TODO Place A DEFAULT IMAGE
       holder.person_image.setImagePlaceHolder(R.drawable.ic_launcher);
    }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (EstimateAdapter.this.onItemClickListener1 != null) {
                    EstimateAdapter.this.onItemClickListener1.onEsItemClicked(userEstimateModel);
                }
            }
        });
    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserEstimateModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserEstimateModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener1) {
        this.onItemClickListener1 = onItemClickListener1;
    }

    private void validateUsersCollection(Collection<UserEstimateModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.leader_estimate)
        TextView leader_estimate;
        @Bind(R.id.leader_zips)
        TextView leader_zips;
        @Bind(R.id.leader_name)
        TextView leader_name;
        @Bind(R.id.number)
        TextView number;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id.leader_image)
        AutoLoadImageView person_image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
