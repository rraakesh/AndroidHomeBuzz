package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.ProfilePicModel;

/**
 * Created by amit.singh on 12/3/2015.
 */
public interface ProfilePicUpdateView extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  profilePicModel The user that will be shown.
     */
    void viewUpdateProfilePic(ProfilePicModel profilePicModel);
}
