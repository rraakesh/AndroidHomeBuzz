package us.homebuzz.homebuzz.view.GridSortUtil;

import android.view.View;

/**
 * Created by amit.singh on 10/3/2015.
 */
public interface RecyclerViewClickListener {
     void recyclerViewListClicked(View v, int position);
}
