package us.homebuzz.homebuzz.view.fragment.NearBy;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;

/**
 * Created by amit.singh on 10/31/2015.
 */
public class NearbyFilterFragment extends android.support.v4.app.DialogFragment {
    private static final String TAG = NearbyFilterFragment.class.getSimpleName();
    public static NearbyFilterFragment newInstance() {
        NearbyFilterFragment nearbyFilterFragment = new NearbyFilterFragment();
        return nearbyFilterFragment;
    }

    public interface NearbyFilter {
        void onUpdateClick(SortedMap<String, String> filterData);
    }
   private NearbyFilter nearbyFilter;

    @Bind(R.id.editTextBeds)
    EditText textViewmin;

    @Bind(R.id.editTextBaths)
    EditText editTextBathMin;

/*    @Bind(R.id.editTextAddress)
    EditText Address;*/
    @Bind(R.id.clear)
    TextView Clear;
    @Bind(R.id.editTextPriceMin)
    EditText editTextPriceMin;
    @Bind(R.id.editTextPriceMax)
    EditText editTextPriceMax;
    @Bind(R.id.update)
    TextView update;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    Dialog dialog;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
         dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.filter_layout);
        final Drawable d = new ColorDrawable(Color.argb(140, 223, 222, 220));
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        float bottomMapPadding= 150 * getResources().getDisplayMetrics().density;
        params.y = (int)bottomMapPadding;
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setCanceledOnTouchOutside(true);
        ButterKnife.bind(this, dialog.getWindow().getDecorView());

        return dialog;
    }

    @OnClick(R.id.clear)
    public void onClear(){
        editTextBathMin.setText("");
        editTextPriceMax.setText("");
        editTextPriceMin.setText("");
       /* Address.setText("");*/
        textViewmin.setText("");
    }
    @OnClick(R.id.update)
    public void onUpdateClick() {
        //TODO Update The Value Here
        String min_price = editTextPriceMin.getText().toString();
        String max_price = editTextPriceMax.getText().toString();
      /*  String address =  Address.getText().toString();*/
         String beds_min =  textViewmin.getText().toString();
          String baths_min = editTextBathMin.getText().toString();

        float price_begin = 0,price_end = 0,area_begin =0,area_max=0;

        int min_beds = 0, min_baths = 0,max_beds = 0,max_baths = 0;


        try{
            price_begin = Float.parseFloat(min_price);
        }catch (Exception e){
            price_begin = 0.0f;
        }

        try{
            price_end = Float.parseFloat(max_price);
        }catch (Exception e){
            price_end = 0.0f;
        }


        try{
            min_beds = Integer.parseInt(beds_min);
        }catch (Exception e){
            min_beds = 0;
        }



        try{
            min_baths = Integer.parseInt(baths_min);
        }catch (Exception e){
            min_baths = 0;
        }

        SortedMap<String,String> filterData=new TreeMap<>();
        filterData.put(constants._RADIUS,String.valueOf(sharedPreferences.getFloat(constants._RADIUS,0.0f)));
        filterData.put(constants._LON,String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)));
        filterData.put(constants._LAT, String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));
        filterData.put("filter[price_begin]",String.valueOf(price_begin));
        filterData.put("filter[price_end]",String.valueOf(price_end));
        filterData.put("filter[min_beds]",String.valueOf(min_beds));
       /* filterData.put("filter[address]",String.valueOf(address));*/
        filterData.put("filter[min_baths]",String.valueOf(min_baths));
        nearbyFilter.onUpdateClick(filterData);
        dialog.dismiss();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        if(this.getParentFragment() instanceof CheckInNearBy){
            nearbyFilter=(NearbyFilter)this.getParentFragment();
        }else if(this.getParentFragment() instanceof CheckInNearby){
            nearbyFilter=(NearbyFilter)this.getParentFragment();
        }
        Log.d(TAG,"Filter callback"+nearbyFilter);
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
