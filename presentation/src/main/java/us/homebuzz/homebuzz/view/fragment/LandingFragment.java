package us.homebuzz.homebuzz.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.view.GridSortUtil.OnStartDragListener;
import us.homebuzz.homebuzz.view.GridSortUtil.RecyclerListAdapter;
import us.homebuzz.homebuzz.view.GridSortUtil.RecyclerViewClickListener;
import us.homebuzz.homebuzz.view.GridSortUtil.SimpleItemTouchHelperCallback;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInNearBy;
import us.homebuzz.homebuzz.view.fragment.friends.BaseFriends;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoard;
import us.homebuzz.homebuzz.view.fragment.messages.Messages;
import us.homebuzz.homebuzz.view.fragment.setting.UserSettingsFragment;

/**
 * Created by amit.singh on 9/25/2015.
 */
public class LandingFragment extends BaseFragment implements OnStartDragListener ,RecyclerViewClickListener{
    private static final String ARGUMENT_KEY_IS_LOGIN= "Islogin";
    private ItemTouchHelper mItemTouchHelper;
    private static  final String TAG=LandingFragment.class.getSimpleName();
    public LandingFragment() {
    }
    @Bind(R.id.rv_users) RecyclerView rv_users;
    private StaggeredGridLayoutManager sglm;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    private boolean isfromlogin;
    public static LandingFragment newInstance(/*boolean isLogin*/) {
        LandingFragment landingFragment = new LandingFragment();
        //Bundle argumentsBundle = new Bundle();
        //argumentsBundle.putBoolean(ARGUMENT_KEY_IS_LOGIN, isLogin);
        //landingFragment.setArguments(argumentsBundle);
        return landingFragment;

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_user_list, container, false);
       // isfromlogin = this.getArguments().getBoolean(ARGUMENT_KEY_IS_LOGIN);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialize();
        setupUI();
       // if(isfromlogin)

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
      //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, CheckInNearBy.newInstance());
    }

    private void setupUI(){
        RecyclerListAdapter adapter = new RecyclerListAdapter(getActivity(), this,this);
        sglm = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rv_users.setLayoutManager(sglm);
        rv_users.setHasFixedSize(false);
        rv_users.setAdapter(adapter);
        rv_users.setLayoutManager(sglm);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(rv_users);

    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).inject(this);
        Log.d(TAG, "TOKEN" + sharedPreferences.getString(constants.KEY_TOKEN, "") + "EMAIL " + sharedPreferences.getString(constants.KEY_EMAIL, ""));
    }
    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
       mItemTouchHelper.startDrag(viewHolder);
    }


    /**
     * Layout control Begin here with RecyclerView on iTem Click listener and
     * Based on that Pass the Fragment to the @replaceListener and call CallReplaceFragment(Fragment)
     */
    @Override
    public void recyclerViewListClicked(View v,int position) {
        //Based on the position call the Fragment
        switch (position){
            case 1:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout,CheckInNearBy.newInstance());
                break;
            case 2:
              //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, AddListing.newInstance());
                replaceListener.CallReplaceFragment(R.id.conatainer_layout,LeaderBoard.newInstance());
                break;
            case 3:
               // replaceListener.CallReplaceFragment(R.id.conatainer_layout,LeaderBoard.newInstance());
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserSettingsFragment.newInstance());
                break;
            case 4:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, BaseFriends.newInstance());
                break;
            case 5:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout,Messages.newInstance());
                break;
            case 7:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserSettingsFragment.newInstance());
        }
    }
}
