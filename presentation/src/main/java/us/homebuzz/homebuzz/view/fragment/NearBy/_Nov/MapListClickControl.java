package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.support.v4.app.Fragment;

/**
 * Created by amit.singh on 11/16/2015.
 */
public interface MapListClickControl {
     void onMapButtonClick(Fragment fragment);
    void onListButtonClick(Fragment fragment);
}
