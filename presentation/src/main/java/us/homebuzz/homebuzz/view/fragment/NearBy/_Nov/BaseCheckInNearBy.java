package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTouch;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.presenter.UnreadMessagesPresenter;
import us.homebuzz.homebuzz.presenter.UserJustListedPresenter;
import us.homebuzz.homebuzz.presenter.UserJustSoldPresenter;
import us.homebuzz.homebuzz.presenter.UserProfilePresenter;
import us.homebuzz.homebuzz.view.UnreadMessageview;
import us.homebuzz.homebuzz.view.UseJustSoldView;
import us.homebuzz.homebuzz.view.UserJustListedView;
import us.homebuzz.homebuzz.view.UserProfileView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.NearbyFilterFragment;
import us.homebuzz.homebuzz.view.fragment.Notification.NotificationFragment;
import us.homebuzz.homebuzz.view.fragment.addlisting._Nov.AddListing;
import us.homebuzz.homebuzz.view.fragment.leaderboard.LeaderBoard;
import us.homebuzz.homebuzz.view.fragment.setting.UserProfileFragment;

/**
 * Created by amit.singh on 11/16/2015.
 */
public abstract class BaseCheckInNearBy extends BaseFragment implements MapListClickControl, QuickMenuControl, UnreadMessageview, UserJustListedView, UseJustSoldView,UserProfileView {

    @Bind(R.id.sliding_layout)
    SlidingUpPanelLayout msSlidingUpPanelLayout;
    @Bind(R.id.map_top)
    RelativeLayout map_top;
    @Bind(R.id.notification_title)
    TextView notification_title;
    @Bind(R.id.rl_progress)
    public RelativeLayout rl_progress;
    @Inject
    UnreadMessagesPresenter unreadMessagesPresenter;
    @Inject
    UserJustSoldPresenter userJustSoldPresenter;
    @Inject
    UserJustListedPresenter userJustListedPresenter;
    private ArrayList<UserJustSoldModel> userJustSoldModelArrayList;
    private ArrayList<UserUnreadMessagesModel> userUnreadMessagesModelArrayList;
    private ArrayList<UserJustListedModel> userJustListedModelArrayList;
    private static int notificationCount;
    @Inject
    UserProfilePresenter userProfilePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_nearby, container, false);
        ButterKnife.bind(this, view);
        notificationCount = 0;

        return view;
    }

    @OnCheckedChanged(R.id.quick_menu)
    public void onQuickMenuClick(boolean checked) {
        msSlidingUpPanelLayout.setPanelState(checked ? SlidingUpPanelLayout.PanelState.EXPANDED : SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    @OnTouch(R.id.dragView)
    boolean onTouch() {
        msSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        return true;
    }

    @OnClick(R.id.check_in_layout)
    public void onCheckinClick() {
        this.onQuickMenuITemClick(0);
    }

    @OnClick(R.id.add_home_layout)
    public void onAdd_home_layoutClick() {
        this.onQuickMenuITemClick(1);
    }

    @OnClick(R.id.leader_layout)
    public void onLeader_layoutClick() {
        this.onQuickMenuITemClick(2);
    }

    @OnClick(R.id.userProfile_layout)
    public void onUserProfile_layoutClick() {
        this.onQuickMenuITemClick(4);
    }

    @OnClick(R.id.Notification_layout)
    public void onNotificationClick() {
        this.onQuickMenuITemClick(3);
    }

    @Override
    public void onListButtonClick(Fragment fragment) {
        this.replaceFragment(R.id.container_layout_tab, fragment);
    }

    @Override
    public void onMapButtonClick(Fragment fragment) {
        this.replaceFragment(R.id.container_layout_tab, fragment);
    }

    @Override
    public void onQuickMenuITemClick(int Position) {
        //ToDo Change that to the Concrete class
        switch (Position) {
            case 0:
                msSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                break;
            case 1:
               // msSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                 replaceListener.CallReplaceFragment(R.id.conatainer_layout, AddListing.newInstance());
                break;
            case 2:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, LeaderBoard.newInstance());
                break;
            case 3:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, NotificationFragment.newInstance(userJustSoldModelArrayList, userJustListedModelArrayList, userUnreadMessagesModelArrayList));
                break;
            case 4:
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());
                break;

        }
    }

    @Override
    public void viewUnreadMessages(Collection<UserUnreadMessagesModel> userUnreadMessagesModel) {
        Log.d(TAG, "Nearby  UNREAD MESSAGES" + userUnreadMessagesModel.size());
        notificationCount += userUnreadMessagesModel.size();
        UpdateNotificationCount();
        if (userUnreadMessagesModel != null && userUnreadMessagesModel.size() > 0)
            userUnreadMessagesModelArrayList = (ArrayList) userUnreadMessagesModel;
    }

    @Override
    public void viewJustListedListing(Collection<UserJustListedModel> userJustListedModelCollection) {
        Log.d(TAG, "Nearby JustListed Listing" + userJustListedModelCollection.size());
        notificationCount += userJustListedModelCollection.size();
        UpdateNotificationCount();
        if (userJustListedModelCollection != null && userJustListedModelCollection.size() > 0)
            userJustListedModelArrayList = (ArrayList) userJustListedModelCollection;
    }

    @Override
    public void viewJustSoldListing(Collection<UserJustSoldModel> userJustSoldModelCollection) {
        Log.d(TAG, "Nearby Just Sold Listing" + userJustSoldModelCollection.size());
        notificationCount += userJustSoldModelCollection.size();
        UpdateNotificationCount();
        if (userJustSoldModelCollection != null && userJustSoldModelCollection.size() > 0)
            userJustSoldModelArrayList = (ArrayList) userJustSoldModelCollection;
    }

    //Message Unread Api Call And Get the Result
    protected void initializeUnread() {
        this.unreadMessagesPresenter.setView(this);
        this.unreadMessagesPresenter.initialize();

    }
    protected void initializeProfile(int user_id) {
        this.userProfilePresenter.setView(this);
        this.userProfilePresenter.initialize(user_id);

    }

    protected void initlaizeSoldAndListed(WeakHashMap<String, String> UserCurrentLocation) {
        this.userJustSoldPresenter.setView(this);
        this.userJustListedPresenter.setView(this);
        this.userJustListedPresenter.initialize(UserCurrentLocation);
        this.userJustSoldPresenter.initialize(UserCurrentLocation);
    }

    private void UpdateNotificationCount() {
        UpdateNotificationCount(notificationCount);
        notification_title.setText("Notification(" + notificationCount + ")");
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be Replaced.
     */
    protected void replaceFragment(int containerViewId, Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        android.support.v4.app.FragmentManager manager = this.getChildFragmentManager();
        boolean fragmentPopped = manager
                .popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            // fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(containerViewId, fragment, fragmentTag);
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            ft.addToBackStack(backStateName);
            ft.commit();

        }
    }

    protected abstract void onQuerySubmitted(String Query);

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_main, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) this.getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(this.getActivity().getComponentName()));
        searchView.setIconified(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println("search query submit" + query);
                onQuerySubmitted(query);
                menu.findItem(R.id.action_search).collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //System.out.println("tap");
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_filter:
                this.onFilterIconClick(NearbyFilterFragment.newInstance());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onFilterIconClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());

    }
    abstract void initProifleUpdate(UserProfileModel userProfileModel);
    abstract void UpdateNotificationCount(int notificationCount);
    @Override
    public void onPause() {
        super.onPause();
      this.unreadMessagesPresenter.pause();
     this.userJustSoldPresenter.pause();
     this.userJustListedPresenter.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.unreadMessagesPresenter.resume();
        this.userJustSoldPresenter.resume();
        this.userJustListedPresenter.resume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unreadMessagesPresenter.destroy();
        userJustSoldPresenter.destroy();
        userJustListedPresenter.destroy();
    }

    @Override
    public void viewUserProfile(UserProfileModel userProfileModel) {
        if(userProfileModel!=null)
        initProifleUpdate(userProfileModel);
    }
}
