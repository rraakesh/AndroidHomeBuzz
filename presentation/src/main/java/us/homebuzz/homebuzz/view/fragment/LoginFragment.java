package us.homebuzz.homebuzz.view.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.EmailFormatValidator;
import us.homebuzz.homebuzz.di.components.UserLoginComponent;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.presenter.LoginPresenter;
import us.homebuzz.homebuzz.view.UserLoginView;
import us.homebuzz.homebuzz.view.activity.LoginSignUp.Login;

/**
 * Created by amit.singh on 10/8/2015.
 */
public class LoginFragment extends BaseFragment implements UserLoginView {

    public static String TAG = LoginFragment.class.getClass().getSimpleName();
    private String Email = "";
    CallbackManager callbackManager;


    public static LoginFragment newInstance() {
        LoginFragment MyReview = new LoginFragment();
        return MyReview;
    }

    /**
     * Interface for listening user list events.
     */
    public interface UserLoginFragmentListener {
        void onUserSignUpClicked();

        void onUserLoginSuccess();
    }

    private UserLoginFragmentListener userLoginFragmentListener;
    @Inject
    LoginPresenter userLoginPresenter;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.rl_retry)
    RelativeLayout rl_retry;
    @Bind(R.id.email_address)
    EditText email_address;
    @Bind(R.id.pass_edt)
    EditText pass_edt;
    @Bind(R.id.facebook_login_button)
    LoginButton facebook_login_button;

    @Inject
    public SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Bind(R.id.forget_password)
    TextView forgotPassword;
    @Bind(R.id.sign_up)
    TextView signUp;
    @Bind(R.id.login_btn)
    Button loginButton;
    @Bind(R.id.logout_facebook)
    TextView logout_facebook;
    public LoginFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity());
        View fragmentView = inflater.inflate(R.layout.login, container, false);
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    "us.homebuzz.homebuzz",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Your Tag", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


        ButterKnife.bind(this, fragmentView);
        facebookLogin();
        setupUI();
        final LinearLayout fb = (LinearLayout)fragmentView.findViewById(R.id.fb);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == fb) {
                    facebook_login_button.performClick();
                }
            }
        });
        return fragmentView;
    }

    private void facebookLogin () {

        facebook_login_button.setFragment(LoginFragment.this);
        callbackManager = CallbackManager.Factory.create();
        facebook_login_button.setReadPermissions(Arrays.asList("public_profile,email,user_likes"));

        facebook_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Toast.makeText(getActivity(), "User Connected", Toast.LENGTH_SHORT).show();
                logout_facebook.setText("Logout");


                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.e("response", response.toString());
                        Log.e("Object", object.toString());
                        Log.e("Access token", loginResult.getAccessToken().getToken().toString());
                        String fb_token = loginResult.getAccessToken().getToken().toString();
                        String release = Build.VERSION.RELEASE;
                        try {
                            String FB_id = object.getString("id");
                            Log.e(TAG, "User ID : " + FB_id);
                            String Fname = object.getString("first_name");
                            Log.e(TAG, "First Name : " + Fname);
                            String Lname = object.getString("last_name");
                            Log.e(TAG, "Last Name : " + Lname);
                            Email = object.optString("email");
                            String gender = object.getString("gender");
                            Log.e(TAG, "Email : " + Email);
                            Log.e(TAG, "gender : " + gender);
                            if (!Email.equalsIgnoreCase("")) {
                                String deviceId = Settings.Secure.getString(getActivity().getContentResolver(),
                                        Settings.Secure.ANDROID_ID);
                                WeakHashMap<String, String> value = new WeakHashMap<String, String>();
                                value.put(constants.KEY_FBID, FB_id);
                                value.put(constants.FACEBOOK_TOKEN, fb_token);
                                value.put(constants.KEY_OS_NAME, OSName());
                                value.put(constants.KEY_NAME, getLocalBluetoothName());
                                value.put(constants.KEY_MODEL, getDeviceName());
                                value.put(constants.DEVICE_ID, deviceId);
                                value.put(constants.KEY_OS_VERSION, release);
                                loginUser(value, true);
                            } else {
                                LoginManager.getInstance().logOut();
                                Toast.makeText(getActivity(), "Email is not found", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            LoginManager.getInstance().logOut();
                            e.printStackTrace();
                            Log.e("Exception", "Exception");
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fieldss", "id,first_name,last_name, name, email, gender, birthday, locale, timezone, updated_time, verified");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("cancel", "login cancel ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("error", error.toString());

            }
        });
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        if (manufacturer.equalsIgnoreCase("HTC")) {
            // make sure "HTC" is fully capitalized.
            return "HTC " + model;
        }
        return /*capitalize(manufacturer) + " " + */model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public static String OSName() {
        String fieldName = null;
        StringBuilder builder = new StringBuilder();
        builder.append("android : ").append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" : ").append(fieldName).append(" : ");
                builder.append("sdk=").append(fieldValue);
            }
        }
        return fieldName;
    }

    public String getLocalBluetoothName() {
        BluetoothAdapter mBluetoothAdapter = null;
        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String name = mBluetoothAdapter.getName();
        if (name == null) {
            System.out.println("Name is null!");
            name = mBluetoothAdapter.getAddress();
        }
        return name;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult", "Facebook");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
    }

    /**
     * Listner initialization to Switching to Other Activity
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        try {
            if (context instanceof Activity) {
                activity = (Activity) context;
            }
            userLoginFragmentListener = (Login) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            throw new ClassCastException(activity.toString() + "Must implement the UserLoginFragmentListener listener");
        }
    }

    @OnTouch(R.id.main_parent)
    public boolean onOutsideTouch(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getActivity().getSystemService(this.getActivity().INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return false;
    }

    @OnClick(R.id.login_btn)
    public void loginBtnClick() {
        //Validate the input for Empty Value
        if (!TextUtils.isEmpty(email_address.getText().toString()) && !TextUtils.isEmpty(pass_edt.getText().toString())) {
            EmailFormatValidator emailFormatValidator = new EmailFormatValidator();
            if (!emailFormatValidator.validate(email_address.getText().toString())) {
                //Email Is not Valid
                email_address.setError("Please enter valid Email address!");
            } else if (pass_edt.length() < 8) {
                pass_edt.setError("Password cannot, be less than eight characters!");
            } else {
                String deviceId = Settings.Secure.getString(this.getContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                //Call the Webservice with EMail And Password
                Log.d(TAG, "Email " + email_address.getText() + "Password is" + pass_edt.getText().toString() + "Device Token" + deviceId);
                WeakHashMap<String, String> value = new WeakHashMap<String, String>();
                value.put(constants.KEY_EMAIL, email_address.getText().toString());
                value.put(constants.KEY_PASS, pass_edt.getText().toString());
                value.put(constants.DEVICE_ID, deviceId);
                value.put(constants.MOBILE_NO, "");
                loginUser(value, false);
            }
        } else {
            Toast.makeText(this.getContext(), "PLease enter the username and Password!!", Toast.LENGTH_SHORT).show();
        }

    }

    private void loginUser(final WeakHashMap<String, String> value, final boolean check) {
        this.userLoginPresenter.initialize(value, check);

    }
    @OnClick(R.id.forget_password)
    public void forgetPassword() {
        Intent  intent = new Intent(getActivity(),WebviewFragment.class);
        startActivity(intent);
    }

    @OnClick(R.id.sign_up)
    public void signUpClick() {

        userLoginFragmentListener.onUserSignUpClicked();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.userLoginPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.userLoginPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.userLoginPresenter.destroy();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void initialize() {
        this.getComponent(UserLoginComponent.class).inject(this);
        this.userLoginPresenter.setView(this);
        // Log.d(TAG, "Check for the SharedPreference in Login" + sharedPreferences);
    }

    private void setupUI() {
        forgotPassword.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        signUp.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        loginButton.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    public void showLoading() {

        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);

    }

    @Override
    public void showRetry() {
        //this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        //  this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void renderAfterUserLogin(UserLoginModel userModel) {
        if (userModel != null) {
            Log.d(TAG, "Value Email " + userModel.getEmail());
            Log.d(TAG, "Value getRole " + userModel.getRole());
            Log.d(TAG, "Value getZipCode " + userModel.getZipCode());
            sharedPreferences.edit().putString(constants.KEY_EMAIL, userModel.getEmail()).putString(constants.KEY_TOKEN, userModel.getToken())
                    .putString(constants.ACCURATE, String.valueOf(userModel.getAccurate())).putString(constants.ESTIMATE_COUNT, String.valueOf(userModel.getEstimatesCount()))
                    .putString(constants.KEY_ROLE, userModel.getRole()).putInt(constants.KEY_ID, userModel.getId()).putString(constants.KEY_NAME, userModel.getName()).commit();
            //Got to Main Activity
            userLoginFragmentListener.onUserLoginSuccess();
        }
    }


    @Override
    public void showError(String message) {
        this.showToastMessage("Please Check the Email and Password!!");
    }

    @Override
    public Context getContext() {
        return this.getActivity().getApplicationContext();
    }

}