package us.homebuzz.homebuzz.view.adapter.Message;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by rakesh.kumar on 10/27/2015.
 */
public class AllMessageAdapter extends RecyclerView.Adapter<AllMessageAdapter.UserViewHolder> {
    public static String TAG = AllMessageAdapter.class.getClass().getSimpleName();
    public interface OnItemClickListener {
        void onMessItemClicked(UserAllMessagesModel userAllMessagesModel);}

    private List<UserAllMessagesModel> userModelCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;
    public AllMessageAdapter(Context context, Collection<UserAllMessagesModel> usersCollection,Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.picasso=picasso;
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.userModelCollection = (List<UserAllMessagesModel>)usersCollection;

    }

    @Override public int getItemCount() {
        return (this.userModelCollection != null) ? this.userModelCollection.size() : 0;
    }

    @Override public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.notification_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserAllMessagesModel userModel = this.userModelCollection.get(position);
        holder.Listing_title.setText(userModel.getSender().getName());
        holder.leader_accuracy.setText(userModel.getBody());
        holder.row_second.setVisibility(View.VISIBLE);
        holder.row_second.setText(userModel.getCreatedAt());
//        if (null != userUnreadMessagesModel && null != userUnreadMessagesModel.getPhotos()) {
//            System.out.println(TAG + "Pic" + String.valueOf(userUnreadMessagesModel.getPhotos()));
//            picasso.load(String.valueOf(userUnreadMessagesModel.getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
//        } else {
//            //TODO Place A DEFAULT IMAGE
        if (userModel.getSender().getProfilePic() != null && !userModel.getSender().getProfilePic().matches("null") )
            picasso.load(userModel.getSender().getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
        else
            holder._image.setImagePlaceHolder(R.drawable.holder_home);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AllMessageAdapter.this.onItemClickListener != null) {
                    AllMessageAdapter.this.onItemClickListener.onMessItemClicked(userModel);
                }
            }}
        );
    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserAllMessagesModel> userAllMessagesModels) {
        this.validateUsersCollection(userModelCollection);
        this.userModelCollection = (List<UserAllMessagesModel>) userAllMessagesModels;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<UserAllMessagesModel> userModelCollection) {
        if (userModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.Listing_title)
        TextView Listing_title;
        @Bind(R.id.row_second)
        TextView row_second;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id._image)
        AutoLoadImageView _image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


