package us.homebuzz.homebuzz.view.fragment.friends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.BaseDecorator;

/**
 * Created by amit.singh on 10/28/2015.
 */
public abstract class FriendsDecorator extends BaseDecorator implements FriendsTabStrip {
    public FriendsDecorator(){}
    @Bind(R.id.messsage_text_top)
    TextView message_top;
    @Bind(R.id.container_layout_tab)
    FrameLayout container_layout_tab;
    @Bind(R.id.tab_view)
    LinearLayout tab_fragment;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View base_friends = inflater.inflate(R.layout.base_friend, container, false);
        ButterKnife.bind(this, base_friends);
        return base_friends;
    }
    @Override
    public void onThirdIconClick(Fragment ThirdFragment) {
        replaceFragment(R.id.container_layout_tab, ThirdFragment);
    }

    @Override
    public void onSecondIconClick(Fragment SecondFragment) {
        replaceFragment(R.id.container_layout_tab, SecondFragment);
    }
    protected void setTopText(String message){
        message_top.setText(message);
    }
}
