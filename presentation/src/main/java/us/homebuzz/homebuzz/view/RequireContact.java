package us.homebuzz.homebuzz.view;

/**
 * Created by amit.singh on 12/11/2015.
 */
public interface RequireContact {
    void onContactRequest();
}
