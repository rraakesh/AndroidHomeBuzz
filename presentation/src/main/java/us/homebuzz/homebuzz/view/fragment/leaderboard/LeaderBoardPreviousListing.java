package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;

/**
 * Created by rohitkumar on 30/10/15.
 */
@SuppressWarnings("ALL")
public class LeaderBoardPreviousListing extends BaseFragment {
    private PagerAdapter adapter;
    private static final String TAG = LeaderBoardPreviousListing.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_ACCURACY_LIST_MODEL = "userPreviousEstimateodel";
    private UserPreviousEstimateModel userPreviousEstimateModel;
    private UsersLayoutManager usersLayoutManager;
    View view;
    @Bind(R.id.no_of_beds)
    TextView beds;
    @Bind(R.id.no_of_baths)
    TextView baths;
    @Bind(R.id.arces)
    TextView acres;
    @Bind(R.id.year_built_text)
    TextView year;
    @Bind(R.id.rate_text)
    TextView price;
    @Bind(R.id.condition_rating)
    RatingBar condition_rating;
    @Bind(R.id.pager)
    ViewPager viewpager;
    @Bind(R.id.indicator)
    CirclePageIndicator mIndicator;
    @Bind(R.id.location_rating)
    RatingBar location_rating;
    @Inject
    Picasso picasso;
    @Bind(R.id.update_checkin)
    LinearLayout updateCheckin;

    public static LeaderBoardPreviousListing newInstance(UserPreviousEstimateModel userPreviousEstimateModel) {
        LeaderBoardPreviousListing leaderBoardPreviousListing = new LeaderBoardPreviousListing();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_ACCURACY_LIST_MODEL, userPreviousEstimateModel);
        leaderBoardPreviousListing.setArguments(argumentsBundle);
        return leaderBoardPreviousListing;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view_previous = inflater.inflate(R.layout.leaderboard_chech_in_estimate, container, false);
        ButterKnife.bind(this, view_previous);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_ACCURACY_LIST_MODEL) != null) {
            userPreviousEstimateModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_ACCURACY_LIST_MODEL);
            Log.d(TAG, "UserModel in Leader Previous" + userPreviousEstimateModel.getId());
        }
        return view_previous;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupUI();

    }

    private void setupUI() {
        LayerDrawable layerDrawable = (LayerDrawable) condition_rating.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), getResources().getColor(R.color.star_color));
        LayerDrawable layerDrawable1 = (LayerDrawable) location_rating.getProgressDrawable();
        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable1.getDrawable(2)), getResources().getColor(R.color.star_color));
        price.setText(userPreviousEstimateModel.getPrice().toString());
        condition_rating.setRating(Float.parseFloat(userPreviousEstimateModel.getListing().getConditionRating().toString()));
        location_rating.setRating(Float.parseFloat(userPreviousEstimateModel.getLocationRating().toString()));
        beds.setText(userPreviousEstimateModel.getListing().getBedrooms().toString());
        baths.setText(userPreviousEstimateModel.getListing().getBaths().toString());
        year.setText(userPreviousEstimateModel.getListing().getYearBuilt().toString());

        acres.setText(userPreviousEstimateModel.getListing().getAcres().toString());
        // acres.setText(userPreviousEstimateModel.getListing().getAcres().toString());
        Log.d(TAG, "userPreviousEstimateModel" + userPreviousEstimateModel.getListing().getBaths());


        Log.d(TAG, "image" + userPreviousEstimateModel.getListing().getPhotos().toString());


    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    @OnClick(R.id.update_checkin)
    public void updateCheckin() {
        if (userPreviousEstimateModel != null)
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, UpdateCheckInEstimateFragment.newInstance(String.valueOf(userPreviousEstimateModel.getId()),false));

    }


}



