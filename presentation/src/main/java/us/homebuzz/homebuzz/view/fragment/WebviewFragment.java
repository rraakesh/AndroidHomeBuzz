package us.homebuzz.homebuzz.view.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import butterknife.Bind;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.activity.BaseActivity;

/**
 * Created by rohitkumar on 10/12/15.
 */
public class WebviewFragment extends BaseActivity{

    String url ="https://realbuzzapp.herokuapp.com/users/password/new";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

    }


}
