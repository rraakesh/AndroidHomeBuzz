package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by abhishek.singh on 11/23/2015.
 */
public interface UserFollowersView extends LoadDataView {
/*
    void viewUser(UserEstimateModel userEstimateModel);*/

    /**
     * Render a user list in the UI.
     *
     * @param userFriendFollowedModelCollection The collection of {@link UserModel} that will be shown.
     */
    void renderUserFollowersList(Collection<UserFriendFollowedModel> userFriendFollowedModelCollection);

    void viewUser(UserFriendFollowedModel userFriendFollowedModelCollection);
}