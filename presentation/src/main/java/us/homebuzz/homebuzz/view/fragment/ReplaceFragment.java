package us.homebuzz.homebuzz.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by amit.singh on 10/1/2015.
 */
public interface ReplaceFragment {
   void  CallReplaceFragment(int containerViewId,Fragment mFragment);
   void  CallNavigateToActivity(AppCompatActivity mActivity);
}
