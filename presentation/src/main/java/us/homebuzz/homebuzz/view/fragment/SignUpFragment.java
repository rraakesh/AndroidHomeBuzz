package us.homebuzz.homebuzz.view.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.EmailFormatValidator;
import us.homebuzz.homebuzz.di.components.UserLoginComponent;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.presenter.LoginPresenter;
import us.homebuzz.homebuzz.presenter.UserRegPresenter;
import us.homebuzz.homebuzz.view.UserLoginView;
import us.homebuzz.homebuzz.view.UserSignupView;
import us.homebuzz.homebuzz.view.activity.LoginSignUp.SignUp;

/**
 * Created by amit.singh on 10/8/2015.
 */
public class SignUpFragment extends BaseFragment implements UserSignupView,UserLoginView {
    public static String TAG = LoginFragment.class.getClass().getSimpleName();

    /**
     * Interface for listening user list events.
     */
    public interface UserSignFragmentListener {
        void onUserSignUpClicked();

        void onUserLoginSuccess();
    }
    private String Email = "";
    private UserSignFragmentListener userSignFragmentListener;
    @Bind(R.id.facebook_login_button)
    LoginButton facebook_login_button;
    @Inject
    LoginPresenter loginPresenter;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.rl_retry)
    RelativeLayout rl_retry;
    @Bind(R.id.email_address)
    EditText email_edt;
    @Bind(R.id.pass_edt)
    EditText pass_edt;
    @Bind(R.id.pass_confirm_edt)
    EditText pass_confirm_edt;
    @Bind(R.id.zip_edt)
    EditText zip_edt;
    @Bind(R.id.name_first)
    EditText name_first;
    @Bind(R.id.phone_edt)
    EditText phone_edt;
    @Bind(R.id.name_Last)
    EditText name_Last;
    @Bind(R.id.txtAlreadyMember)
    TextView txtAlreadyMember;
    @Bind(R.id.toggleButton1)
    ToggleButton toggleButton;
    String role;
    @Inject
    public SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    UserRegPresenter userRegPresenter;
    CallbackManager callbackManager;

    public SignUpFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity());
        View fragmentView = inflater.inflate(R.layout.signup, container, true);
        ButterKnife.bind(this, fragmentView);
        facebookLogin();
        txtAlreadyMember.setPaintFlags(txtAlreadyMember.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        final LinearLayout fb = (LinearLayout)fragmentView.findViewById(R.id.fb);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == fb) {
                    facebook_login_button.performClick();
                }
            }
        });
        return fragmentView;
    }

    /**
     * Listner initialization to Switching to Other Activity
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        try {
            if (context instanceof Activity) {
                activity = (Activity) context;
            }
            userSignFragmentListener = (SignUp) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            throw new ClassCastException(activity.toString() + "Must implement the UserSignFragmentListener listener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();

    }

    @OnClick(R.id.signUpbtn)
    public void onSignUpclick() {
        WeakHashMap<String, String> signupData = new WeakHashMap<>(10);
        if (toggleButton.isChecked()) {
            role = "agent";
        } else {
            role = "buyer";
        }
        signupData.put(constants.KEY_ROLE, role);
        String email = email_edt.getText().toString().trim();
        String pass = pass_edt.getText().toString().trim();
        String pass_confirm = pass_confirm_edt.getText().toString().trim();
        String zip = zip_edt.getText().toString().trim();
        String name = name_first.getText().toString().trim() + name_Last.getText().toString().trim();
        String phone = phone_edt.getText().toString().trim();
        // Check the Password and Email
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) && !TextUtils.isEmpty(name) && !TextUtils.isEmpty(phone)) {
            EmailFormatValidator emailFormatValidator = new EmailFormatValidator();
            //validate Zip
            if (emailFormatValidator.validate(email)) {
                //validate password
                if (!TextUtils.isEmpty(zip)) {
                    if (pass.length() >= 8) {
                        if (pass.equals(pass_confirm)) {
                            if (!phone.equals("") && phone.length() == 10) {
                                //Calling the Data With Value
                                signupData.put(constants.KEY_EMAIL, email);
                                signupData.put(constants.KEY_CONFIRM_PASS, pass_confirm);
                                signupData.put(constants.KEY_PASS, pass);
                                signupData.put(constants.KEY_ZIP, zip);
                                signupData.put(constants.KEY_NAME, name);
                                signupData.put(constants.FIRST_NAME, name_first.getText().toString());
                                signupData.put(constants.LAST_NAME, name_Last.getText().toString());
                                signupData.put(constants.KEY_PHONE, phone);
                                signupData.put(constants.KEY_FBID, "");

                                this.userRegPresenter.initialize(signupData,false);
                            } else {
                                phone_edt.setError("Invalid mobile number!");
                                //ShowMessages.showDialog(SignUp.this, "Invalid mobile number!");
                            }
                        } else {
                            pass_confirm_edt.setError("Password does not match!");
                            //ShowMessages.showDialog(SignUp.this, "Password does not match!");
                        }
                    } else {
                        pass_edt.setError("Password cannot, be less than eight characters!");
                        //ShowMessages.showDialog(SignUp.this, "Password cannot, be less than eight characters!");
                    }
                } else {
                    zip_edt.setError("You need to enter zip code!");
                    //ShowMessages.showDialog(SignUp.this, "You need to enter zip code!");
                }
            } else {
                email_edt.setError("Please enter valid Email address!");
                //ShowMessages.showDialog(SignUp.this, "Please enter valid Email address!");
            }
        } else {
            //show dialog for sign in
            showToastMessage("Email ,password , name ,zip and phone should not be empty!");
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        this.userRegPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.userRegPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.userRegPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderAfterUserLogin(UserLoginModel userModel) {
        if (userModel.getErrors() != null) {
            showToastMessage(userModel.getErrors());
        } else {
            sharedPreferences.edit().putString(constants.KEY_EMAIL, userModel.getEmail()).putString(constants.KEY_TOKEN, userModel.getToken())
                    .putString(constants.ACCURATE, String.valueOf(userModel.getAccurate())).putString(constants.ESTIMATE_COUNT, String.valueOf(userModel.getEstimatesCount()))
                    .putString(constants.KEY_ROLE, userModel.getRole()).putInt(constants.KEY_ID, userModel.getId()).putString(constants.KEY_NAME, userModel.getName()).commit();
            //Got to Main Activity
            userSignFragmentListener.onUserLoginSuccess();
        }
    }

    @Override
    public void renderAfterUserSignUp(UserLoginModel user) {
        if (user != null) {
            Log.d(TAG, "Value Email " + user.getErrors());
            if (user.getErrors() != null) {
                showToastMessage(user.getErrors());
            } else {
                login();

            }
        }
    }

    private void loginUser(WeakHashMap<String, String> value, boolean check) {
        this.loginPresenter.initialize(value, check);
    }
    private void login() {
        String deviceId = Settings.Secure.getString(this.getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        WeakHashMap<String, String> value = new WeakHashMap<String, String>();
        value.put(constants.KEY_EMAIL, email_edt.getText().toString());
        value.put(constants.KEY_PASS, pass_edt.getText().toString());
        value.put(constants.DEVICE_ID, deviceId);
        value.put(constants.MOBILE_NO, phone_edt.getText().toString());
        loginUser(value, false);
    }

    private void initialize() {
        this.getComponent(UserLoginComponent.class).inject(this);
        this.userRegPresenter.setView(this);
        this.loginPresenter.setView(this);
        // Log.d(TAG, "Check for the SharedPreference in Login" + sharedPreferences);
    }

    @Override
    public Context getContext() {
        return this.getActivity().getApplicationContext();
    }

    @OnTouch(R.id.signupParent)
    public boolean onOutsideTouch(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getActivity().getSystemService(this.getActivity().INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return false;
    }

    @OnClick(R.id.txtAlreadyMember)
    public void goToLoginActivity() {
       userSignFragmentListener.onUserSignUpClicked();
    }


    private void facebookLogin() {
        facebook_login_button.setFragment(SignUpFragment.this);
        callbackManager = CallbackManager.Factory.create();
        facebook_login_button.setReadPermissions(Arrays.asList("public_profile,email,user_likes"));

        facebook_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Toast.makeText(getActivity(), "User Connected", Toast.LENGTH_SHORT).show();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.e("response", response.toString());
                        Log.e("Object", object.toString());
                        Log.e("Access token", loginResult.getAccessToken().getToken().toString());
                        String fb_token = loginResult.getAccessToken().getToken().toString();
                        String release = Build.VERSION.RELEASE;
                        try {
                            String FB_id = object.getString("id");
                            Log.e(TAG, "User ID : " + FB_id);
                            String Fname = object.getString("first_name");
                            Log.e(TAG, "First Name : " + Fname);
                            String Lname = object.getString("last_name");
                            Log.e(TAG, "Last Name : " + Lname);
                            Email = object.optString("email");
                            String gender = object.getString("gender");
                            Log.e(TAG, "Email : " + Email);
                            Log.e(TAG, "gender : " + gender);
                            if (!Email.equalsIgnoreCase("")) {
                                String deviceId = Settings.Secure.getString(getActivity().getContentResolver(),
                                        Settings.Secure.ANDROID_ID);
                                WeakHashMap<String, String> value = new WeakHashMap<String, String>();
                                value.put(constants.KEY_FBID, FB_id);
                                value.put(constants.FACEBOOK_TOKEN, fb_token);
                                value.put(constants.KEY_OS_NAME, OSName());
                                value.put(constants.KEY_NAME, getLocalBluetoothName());
                                value.put(constants.KEY_MODEL, getDeviceName());
                                value.put(constants.DEVICE_ID, deviceId);
                                value.put(constants.KEY_OS_VERSION, release);
                                loginUser(value, true);
                            } else {
                                LoginManager.getInstance().logOut();
                                Toast.makeText(getActivity(), "Email is not found", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            LoginManager.getInstance().logOut();
                            e.printStackTrace();
                            Log.e("Exception", "Exception");
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name, name, email, gender, birthday, locale, timezone, updated_time, verified");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("cancel", "login cancel ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("error", error.toString());

            }
        });
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        if (manufacturer.equalsIgnoreCase("HTC")) {
            // make sure "HTC" is fully capitalized.
            return "HTC " + model;
        }
        return /*capitalize(manufacturer) + " " + */model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public static String OSName() {
        String fieldName = null;
        StringBuilder builder = new StringBuilder();
        builder.append("android : ").append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" : ").append(fieldName).append(" : ");
                builder.append("sdk=").append(fieldValue);
            }
        }
        return fieldName;
    }

    public String getLocalBluetoothName() {
        BluetoothAdapter mBluetoothAdapter = null;
        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String name = mBluetoothAdapter.getName();
        if (name == null) {
            System.out.println("Name is null!");
            name = mBluetoothAdapter.getAddress();
        }
        return name;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult", "Facebook");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
