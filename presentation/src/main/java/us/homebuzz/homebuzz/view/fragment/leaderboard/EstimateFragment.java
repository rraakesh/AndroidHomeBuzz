package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.presenter.EstimatePresenter;
import us.homebuzz.homebuzz.view.UserEstimatesView;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.leaderboard.EstimateAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseNestedFragment;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendFollowingFollowersProfileView;

/**
 * Created by amit.singh on 10/21/2015.
 */

public class EstimateFragment extends BaseNestedFragment implements UserEstimatesView  ,LeaderBoard.LederBoardFilter{
    private static final String TAG = EstimateFragment.class.getSimpleName();
    @Inject
    EstimatePresenter estimatePresenter;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;
    private ReplaceFragment replaceFragment;
    private EstimateAdapter estmateAdapter;
    private UsersLayoutManager usersLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, list_nearby);
        return list_nearby;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        this.setupUI();
        if(getParentFragment() instanceof LeaderBoard){
            Log.d(TAG, "LearBoard" + ((LeaderBoard) this.getParentFragment()).replaceFragmentLarboard);
            this.replaceFragment=((LeaderBoard) this.getParentFragment()).replaceFragmentLarboard;
        }
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).leaderBoardComponent(new LeaderBoardModule()).inject(this);
        estimatePresenter.setView(this);
        estimatePresenter.initialize(null);
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);

        this.estmateAdapter = new EstimateAdapter(getActivity(), new ArrayList<UserEstimateModel>(),picasso);
        this.estmateAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(estmateAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.estimatePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.estimatePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.estimatePresenter.destroy();
    }



    @Override
    public void showError(String message) {
        super.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }


    @Override
    public void viewUser(UserEstimateModel userEstimateModel) {
        //Todo Navigate to Next Fragment From Here
     //Log.d(TAG, "The Clicked UserID " + userEstimateModel.getId());
      //  replaceFragment.CallReplaceFragment(R.id.conatainer_layout, LeaderPreviousEstimate.newInstance(userEstimateModel));
        String id = String.valueOf(userEstimateModel.getId());
        Log.e("The Followed List id ", id);
        replaceFragment.CallReplaceFragment(R.id.conatainer_layout, FriendFollowingFollowersProfileView.newInstance(id));

    }

    private EstimateAdapter.OnItemClickListener onItemClickListener =
            new EstimateAdapter.OnItemClickListener() {
                @Override
                public void onEsItemClicked(UserEstimateModel userEstimateModel) {
                    if (EstimateFragment.this.estimatePresenter != null && userEstimateModel != null) {
                        EstimateFragment.this.estimatePresenter.onUserClicked(userEstimateModel);
                    }


                }
            };




    @Override
    public void renderUserEstimateList(Collection<UserEstimateModel> userEstimateModels) {
        Log.d(TAG, "The UserEstimateModels List " + userEstimateModels.size());
        //TODO Set the ADAPTER OF RECYCLER VIEW HERE
        if (userEstimateModels != null) {
            this.estmateAdapter.setUsersCollection(userEstimateModels);
        }
    }
    @Override
    public void performFilter(WeakHashMap<String, String> filterData) {
        Log.d(TAG, "The UserEstimateModels  " +filterData.size());
        estimatePresenter.initialize(filterData);
    }

}
