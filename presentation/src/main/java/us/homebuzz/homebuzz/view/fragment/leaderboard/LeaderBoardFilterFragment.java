package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;

/**
 * Created by amit.singh on 11/25/2015.
 */
public class LeaderBoardFilterFragment extends android.support.v4.app.DialogFragment {
    private static final String TAG = LeaderBoardFilterFragment.class.getSimpleName();

    public static LeaderBoardFilterFragment newInstance() {
        LeaderBoardFilterFragment leaderBoardFilterFragment = new LeaderBoardFilterFragment();
        return leaderBoardFilterFragment;

    }

    public interface LeaderBoardFilter {
        void onUpdateClick(WeakHashMap<String, String> filterData);
    }

    Dialog dialog;

    @Bind(R.id.le_zip)
    EditText  le_zip;
    @Bind(R.id.le_team)
    EditText  le_team;
    @Bind(R.id.office_txt)
    EditText  office_txt;
    @Bind(R.id.le_all)
    RadioGroup le_all;
    @Inject
    Constants constants;
    LeaderBoardFilter leaderBoardFilter;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.filter_leaderboard);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        float bottomMapPadding= 70 * getResources().getDisplayMetrics().density;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.y = (int)bottomMapPadding;
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(this.getParentFragment() instanceof LeaderBoard) {
            leaderBoardFilter = (LeaderBoard) this.getParentFragment();
        }
    }


    @OnClick(R.id.Update_button)
    public void onUpdateButtonClick(){
        WeakHashMap<String,String> data=new WeakHashMap<>(3);
        data.put(constants.LEADERBOARD_FILTER_LIMIT,(le_all.getCheckedRadioButtonId()==R.id.radio_all)?" ":"10");
        data.put(constants.LEADERBOARD_FILTER_OFFICE,office_txt.getText().toString());
        data.put(constants.LEADERBOARD_FILTER_TEAM,le_team.getText().toString());
        data.put(constants.LEADERBOARD_FILTER_ZIP_CODE,le_zip.getText().toString());
        leaderBoardFilter.onUpdateClick(data);
        dialog.dismiss();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();

    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).leaderBoardComponent(new LeaderBoardModule()).inject(this);
    }
    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
