package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.ZipAddModel;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public interface ZipCodeAddView  extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  zipAddModel The user that will be shown.
     */
    void viewZipAdd(ZipAddModel zipAddModel);
    void viewDetails(ZipAddModel ZipAddModel);
}
