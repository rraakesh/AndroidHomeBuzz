package us.homebuzz.homebuzz.view.profile;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.FriendsModule;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.presenter.PreviousEstimatePresenter;
import us.homebuzz.homebuzz.view.UserPreviousEstimateView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.friends.MyReviewAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.FriendsFollowingList;

/**
 * Created by abhishek.singh on 11/27/2015.
 */
public class MyReview extends BaseFragment implements UserPreviousEstimateView {

    private static final String TAG = FriendsFollowingList.class.getSimpleName();
    public static MyReview newInstance() {
        MyReview MyReview = new MyReview();
        return MyReview;
    }
    @Bind(R.id.search)
    EditText search;

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    private UsersLayoutManager usersLayoutManager;
    @Inject
    Picasso picasso;
    @Inject
    PreviousEstimatePresenter previousEstimatePresenter;
    private MyReviewAdapter MyReviewAdapter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    ArrayList<UserPreviousEstimateModel> mdata = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View friendsFollowersList = inflater.inflate(R.layout.estimate_listing, container, false);
        ButterKnife.bind(this, friendsFollowersList);
        return friendsFollowersList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("My Reviews");
        this.initialize();
        setupUI();
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this.getActivity());
        base_list.setLayoutManager(linearLayoutManager);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    searchList(s);
                } else {
                    MyReview.this.MyReviewAdapter.setUsersCollection(mdata);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }
    private void searchList(CharSequence str){
        Collection<UserPreviousEstimateModel> tempArrayList = new ArrayList<>();
        if(str.length() > 0){
            System.out.println("size mdata" + mdata.size());
            for(UserPreviousEstimateModel userPreviousEstimateModel: mdata){
                if(userPreviousEstimateModel.getListing().getZip().toString().contains(str)
                        ||(userPreviousEstimateModel.getListing().getPrice()!=null&&userPreviousEstimateModel.getListing().getPrice().toString().contains(str))
                        ||(userPreviousEstimateModel.getListing().getAddress()!=null&&userPreviousEstimateModel.getListing().getAddress().toUpperCase().contains(str))
                        ||(userPreviousEstimateModel.getListing().getAddress()!=null&&userPreviousEstimateModel.getListing().getAddress().toLowerCase().contains(str))){
                    tempArrayList.add( userPreviousEstimateModel);
                }
                MyReview.this.MyReviewAdapter.setUsersCollection(tempArrayList);
            }
        }
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).friendsComponent(new FriendsModule()).inject(this);
        previousEstimatePresenter.setView(this);
        previousEstimatePresenter.initialize(sharedPreferences.getInt(constants.KEY_ID, 0));

    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.MyReviewAdapter = new MyReviewAdapter(getActivity(), new ArrayList<UserPreviousEstimateModel>(), picasso);
        this.MyReviewAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(MyReviewAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.previousEstimatePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.previousEstimatePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.previousEstimatePresenter.destroy();
    }
    @Override
    public void onDestroyView() {

        super.onDestroyView();

    }


    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);

    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);

    }

    @Override
    public void viewDetails(UserPreviousEstimateModel userPreviousEstimateModel) {
        //Todo Navigate to Next Fragment From Here
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UpdateCheckInEstimateFragment.newInstance2(userPreviousEstimateModel, false));
        //TODO Call the replaceListner Method Naviagte to Activity To Move to Activity
    }

    private MyReviewAdapter.OnItemClickListener onItemClickListener =
            new MyReviewAdapter.OnItemClickListener() {
                @Override
                public void onPreviousEstimateAdapterItemClicked(UserPreviousEstimateModel userPreviousEstimateModel) {
                    if (MyReview.this.previousEstimatePresenter != null && userPreviousEstimateModel != null) {
                        MyReview.this.previousEstimatePresenter.onUserClicked(userPreviousEstimateModel);
                    }
                }
            };

    @Override
    public void readerPreviousEstimate(Collection<UserPreviousEstimateModel> userPreviousEstimateModelCollection) {
        mdata= (ArrayList<UserPreviousEstimateModel>) userPreviousEstimateModelCollection;
        this.MyReviewAdapter.setUsersCollection(userPreviousEstimateModelCollection);

        }
    }



