package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserUpdateCheckingModel;

/**
 * Created by rohitkumar on 6/11/15.
 */
public interface UpdateCheckingView extends LoadDataView {
    void renderAfterUpdateCheckin(UserUpdateCheckingModel userUpdateCheckinModel);

}
