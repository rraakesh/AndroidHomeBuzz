package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/21/2015.
 */
public interface UserAccuracyView  extends LoadDataView  {
    /**
     * Render a user list in the UI.
     *
     * @param userAccuracyModels The collection of {@link UserModel} that will be shown.
     */
    void renderUserAccuracyList(Collection<UserAccuracyModel> userAccuracyModels);
    /**
     * View a {@link } profile/details.
     *
     * @param  userAccuracyModel The user that will be shown.
     */
    void viewUser(UserAccuracyModel userAccuracyModel);

}
