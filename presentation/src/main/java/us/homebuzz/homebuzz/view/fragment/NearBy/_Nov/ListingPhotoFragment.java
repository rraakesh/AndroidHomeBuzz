package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.HasComponent;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.nearby.ViewPagerAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rakesh.kumar on 11/30/2015.
 */
public class ListingPhotoFragment extends BaseFragment {
    private static final String TAG = ListingPhotoFragment.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_NEARBY_LIST_MODEL = "userNearbyModel";
    private static final String ARGUMENT_KEY_USER_PHOTO_LIST = "listPhoto";
    private static final String ARGUMENT_KEY_USER_NEARBY_LIST_BOOLEAN = "isBoolean";
    public UserNearbyModel userNearbyModel;
    private boolean isBoolean = false;
    private ArrayList<String> list = new ArrayList<>();

    public static ListingPhotoFragment newInstance(UserNearbyModel userAccuracyModel, boolean b) {
        ListingPhotoFragment listingPhoto = new ListingPhotoFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL, userAccuracyModel);
        argumentsBundle.putBoolean(ARGUMENT_KEY_USER_NEARBY_LIST_BOOLEAN, b);
        listingPhoto.setArguments(argumentsBundle);
        return listingPhoto;
    }

    public static ListingPhotoFragment newInstance1(ArrayList<String> list, boolean b) {
        ListingPhotoFragment listingPhoto = new ListingPhotoFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putStringArrayList(ARGUMENT_KEY_USER_PHOTO_LIST, list);
        argumentsBundle.putBoolean(ARGUMENT_KEY_USER_NEARBY_LIST_BOOLEAN, b);
        listingPhoto.setArguments(argumentsBundle);
        return listingPhoto;
    }

    public ViewPagerAdapter viewPagerAdapter;
    @Bind(R.id.pager)
    ViewPager viewpager;
    @Bind(R.id.indicator)
    CirclePageIndicator mIndicator;
    @Inject
    Picasso picasso;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewEstimateDetails = inflater.inflate(R.layout.listing_photo, container, false);
        ButterKnife.bind(this, viewEstimateDetails);
        if (this.getArguments().getBoolean(ARGUMENT_KEY_USER_NEARBY_LIST_BOOLEAN)) {
            isBoolean = true;
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL);
            isBoolean = this.getArguments().getBoolean(ARGUMENT_KEY_USER_NEARBY_LIST_BOOLEAN);
            Log.d(TAG, "UserModel in Estimate listing" + userNearbyModel.getId());
            ButterKnife.bind(this, viewEstimateDetails);
        } else {
            list = this.getArguments().getStringArrayList(ARGUMENT_KEY_USER_PHOTO_LIST);
            Log.e(TAG, "List size" +list);
        }
        return viewEstimateDetails;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     /*  ((MainActivity)this.getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
      ((MainActivity)this.getActivity()).setSupportActionBar(toolbar);*/
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Photos");
        this.initialize();
        setupUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
    }

    private void setupUI() {
        if (isBoolean) {
            viewPagerAdapter = new ViewPagerAdapter(getActivity(), (ArrayList) userNearbyModel.getPhotos(), picasso,isBoolean);
            viewpager.setAdapter(viewPagerAdapter);
          //  System.out.println("Image1 " + String.valueOf(userNearbyModel.getPhotos().size()));
            if (userNearbyModel.getPhotos().size() > 0) {
                mIndicator.setViewPager(viewpager);
            }
        } else {
            viewPagerAdapter = new ViewPagerAdapter(getActivity(), list, picasso,isBoolean);
            viewpager.setAdapter(viewPagerAdapter);
           // System.out.println("Image2   " + String.valueOf(list.size()));
            if (list.size() > 0) {
                mIndicator.setViewPager(viewpager);
            }
        }

    }
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }
}
