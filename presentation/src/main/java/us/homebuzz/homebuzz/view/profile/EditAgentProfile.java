package us.homebuzz.homebuzz.view.profile;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.EditProfileModelValues;
import us.homebuzz.homebuzz.model.ProfilePicModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.UserUpdateModel;
import us.homebuzz.homebuzz.presenter.ProfilePicUpdatePresenter;
import us.homebuzz.homebuzz.presenter.UserEditOtherProfilePresenter;
import us.homebuzz.homebuzz.presenter.UserEditProfilePresenter;
import us.homebuzz.homebuzz.view.ProfilePicUpdateView;
import us.homebuzz.homebuzz.view.UserUpdateProfileView;
import us.homebuzz.homebuzz.view.UserUpdateProfileView2;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.setting.UserProfileFragment;

/**
 * Created by abhishek.singh on 11/16/2015.
 */
public class EditAgentProfile extends BaseFragment implements UserUpdateProfileView ,UserUpdateProfileView2,ProfilePicUpdateView{
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";
    private static final String ARGUMENT_KEY_USER_PROFILE1 = "isProfile1";
    private static final String ARGUMENT_KEY_USER_PROFILE = "Profile";
    private String role = "";

    public static EditAgentProfile newInstance(boolean isProfileExist, UserProfileModel userProfileModel, EditProfileModelValues values) {
        EditAgentProfile EditAgentProfile = new EditAgentProfile();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE, userProfileModel);
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE1, values);
        argumentsBundle.putBoolean(ARGUMENT_KEY_IS_USER_PROFILE, isProfileExist);
        EditAgentProfile.setArguments(argumentsBundle);
        return EditAgentProfile;
    }

    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    @Bind(R.id.real_estate_image)
    ImageView real_estate_image;
    @Bind(R.id.mls_image)
    ImageView mls_image;
    @Bind(R.id.llView)
    LinearLayout llView;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.mls_id)
    EditText mls_id;
    @Bind(R.id.mls_name)
    EditText mls_name;
    @Bind(R.id.license_number)
    EditText license_number;
    @Bind(R.id.nar_number)
    EditText nar_number;
    @Bind(R.id.finish_button)
    Button finish_button;
    private UserProfileModel userProfileModel;
    private EditProfileModelValues values;
    private boolean isProfileExist;
    private boolean isAgent = true;
    @Inject
    Picasso picasso;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    UserEditProfilePresenter editProfilePresenter;
    @Inject
    UserEditOtherProfilePresenter editOtherProfilePresenter;
    @Inject
    ProfilePicUpdatePresenter profilePicUpdatePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_editAgentProfile = inflater.inflate(R.layout.edit_profile_agent, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE) != null)
            userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE);
        values = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE1);
        isProfileExist = this.getArguments().getBoolean(ARGUMENT_KEY_IS_USER_PROFILE);
        System.out.println("Boolean Profile =" + isProfileExist + "UserProfile " + userProfileModel.getId());
        System.out.println("Boolean Profile =" + isProfileExist + "values " + values.getName());
        ButterKnife.bind(this, list_editAgentProfile);
        if ((userProfileModel.getRole().equalsIgnoreCase("agent"))) {
            enableAgentProfile();
        } else {
            disableAgentProfile();
        }

        return list_editAgentProfile;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Agent Profile");
        this.initialize();
        setUpUI();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.editProfilePresenter.setView(this);
        this.editOtherProfilePresenter.setView(this);
        this.profilePicUpdatePresenter.setView(this);
    }

    private void setUpUI() {
        txtProfileName.setText((userProfileModel.getName() != null && !(userProfileModel.getName().matches("null")) ? userProfileModel.getName() : ""));
        mls_id.setText((userProfileModel.getAgentId() != null && !(userProfileModel.getAgentId().toString().matches("null")) ? userProfileModel.getAgentId().toString() : ""));
        nar_number.setText((userProfileModel.getNapNum() != null && !(userProfileModel.getNapNum().matches("null")) ? userProfileModel.getNapNum().toString() : ""));
        mls_name.setText((userProfileModel.getMlsName() != null && !(userProfileModel.getMlsName().matches("null")) ? userProfileModel.getMlsName().toString() : ""));
        license_number.setText((userProfileModel.getLicenseNum() != null && !(userProfileModel.getLicenseNum().matches("null")) ? userProfileModel.getLicenseNum().toString() : ""));
        if (!values.getProfilePic().equalsIgnoreCase("")) {
            picasso.load(new File(values.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
        } else {
            if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().matches("null")) {
                picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
            } else {
                profilePic.setImageResource(R.drawable.ic_launcher);
            }

        }
    }

    @OnClick(R.id.mls_image)
    public void mslInfo() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Homebuzz");
        alertDialog.setMessage("If you don't see your MLS listed,send us feedback(shake your phone)and we can add it");
        alertDialog.setPositiveButton("ok", null);
        alertDialog.show();
    }

    @OnClick(R.id.real_estate_image)
    public void imageClick() {
        if (isAgent) {
            enableAgentProfile();
            real_estate_image.setImageResource(R.drawable.toggle_on);
            isAgent = false;
        } else {
            disableAgentProfile();
            real_estate_image.setImageResource(R.drawable.toggle_off);
            isAgent = true;
        }
    }

    private void disableAgentProfile() {
        role = "buyer";
        real_estate_image.setImageResource(R.drawable.toggle_off);
        mls_image.setImageResource(R.drawable.query_no);
        mls_id.setEnabled(false);
        mls_name.setEnabled(false);
        license_number.setEnabled(false);
        nar_number.setEnabled(false);
        mls_image.setClickable(false);
    }

    private void enableAgentProfile() {
        role = "agent";
        real_estate_image.setImageResource(R.drawable.toggle_on);
        mls_image.setImageResource(R.drawable.query);
        mls_id.setEnabled(true);
        mls_name.setEnabled(true);
        license_number.setEnabled(true);
        nar_number.setEnabled(true);
        mls_image.setClickable(true);
    }

    @OnClick(R.id.finish_button)
    public void onClickFinishProfile() {
        if(values.getProfilePic()!=null && !values.getProfilePic().isEmpty()) {
            WeakHashMap<String,String>profilePic=new WeakHashMap<>(2);
            profilePic.put(constants.KEY_PROFILE_ID,values.getProfileId());
            profilePic.put(constants.KEY_PPROFILE_PHOTO,values.getProfilePic());
            profilePicUpdatePresenter.initialize(profilePic);
        }
        WeakHashMap<String, String> userData = new WeakHashMap<>();
        WeakHashMap<String, String> userOtherProfileData = new WeakHashMap<>();
        userData.put(constants.KEY_USERID, String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        userData.put(constants.KEY_FIRSTNAME, values.getFirstName());
        userData.put(constants.KEY_EMAILID, values.getEmail());
        userData.put(constants.KEY_LASTNAME, values.getLastName());
        userData.put(constants.KEY_ZIPCODE, values.getZipCode());
        userData.put(constants.KEY_PHONENO, values.getPhone());
        userData.put(constants.KEY_ROLE_TYPE, role);
        userData.put(constants.KEY_PCOLLEGE, values.getCollege());
        userData.put(constants.KEY_PROFILE_ID, values.getProfileId());

       /* userOtherProfileData.put(constants.KEY_PUSERID, String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        userOtherProfileData.put(constants.KEY_PFIRSTNAME, values.getFirstName());
        userOtherProfileData.put(constants.KEY_PEMAILID, values.getEmail());
        userOtherProfileData.put(constants.KEY_PLASTNAME, values.getLastName());
        userOtherProfileData.put(constants.KEY_PZIPCODE, values.getZipCode());
        userOtherProfileData.put(constants.KEY_PPHONENO, values.getPhone());
        userOtherProfileData.put(constants.KEY_PROLE_TYPE, role);
*/

        userOtherProfileData.put(constants.KEY_CITY, values.getCity());
        userOtherProfileData.put(constants.KEY_STATE, values.getState());
        userOtherProfileData.put(constants.KEY_STREET, values.getStreet());

       // userOtherProfileData.put(constants.KEY_USERID1, String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)));
        userOtherProfileData.put(constants.KEY_FACEBOOK_URL, values.getFacebookUrl());
        userOtherProfileData.put(constants.KEY_TWITTER_URL, values.getTwitterUrl());
        userOtherProfileData.put(constants.KEY_LINKED_URL, values.getLinkedIn());
        userOtherProfileData.put(constants.KEY_BIO, values.getBio());
        userOtherProfileData.put(constants.KEY_COLLEGE, values.getCollege());
        userOtherProfileData.put(constants.KEY_DEGREE, values.getDegree());
        userOtherProfileData.put(constants.KEY_YR_GRADUATED, String.valueOf(values.getYrGraduated()));
        userOtherProfileData.put(constants.KEY_OTHER_SCHOOL, values.getOtherSchool());
        userOtherProfileData.put(constants.KEY_DESIGNATION, values.getDesignations());
        userOtherProfileData.put(constants.KEY_MLS_NAME, mls_name.getText().toString());
        userOtherProfileData.put(constants.KEY_LICENSE_NO, license_number.getText().toString());
        userOtherProfileData.put(constants.KEY_AGENT_ID, mls_id.getText().toString());
        userOtherProfileData.put(constants.KEY_NAP_NO, nar_number.getText().toString());
        userOtherProfileData.put(constants.KEY_PPROFILE_ID, values.getProfileId());
        userOtherProfileData.put(constants.KEY_PROFILE_WEBSITE, values.getWebsite());
        //Must Add the Profile Without The profile prefix
        userOtherProfileData.put(constants.KEY_PROFILE_ID, values.getProfileId());

        Log.e("profile values", userData.toString());
        Log.e("other profile values", userOtherProfileData.toString());

        this.editProfilePresenter.initialize(null, userData);
        this.editOtherProfilePresenter.initialize(null,userOtherProfileData);

    }

    @Override
    public void onResume() {
        super.onResume();
        this.editProfilePresenter.resume();
        this.editOtherProfilePresenter.resume();
        this.profilePicUpdatePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.editProfilePresenter.pause();
        this.editOtherProfilePresenter.pause();
        this.profilePicUpdatePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.editProfilePresenter.destroy();
        this.editOtherProfilePresenter.destroy();
        this.profilePicUpdatePresenter.destroy();
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void viewUserProfile(UserUpdateModel userProfileModel) {
        Log.d(TAG, "Checking theUserEditProfileModel" + userProfileModel.getId());
        showToastMessage("your Profile is Update successfully!!");
       // replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());

    }

    @Override
    public void viewUserProfile2(UserUpdateModel userUpdateModel) {
        Log.d(TAG, "Checking theUserEditProfileModel 2" + userProfileModel.getId());
        super.showToastMessage("your Profile is Update successfully!!");
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());
    }

    @Override
    public void viewUpdateProfilePic(ProfilePicModel profilePicModel) {
        Log.d(TAG, "Checking Profile Pic After Update" + profilePicModel.getImageUrl());
        if(profilePicModel.getImageUrl()==null)
        super.showToastMessage("There was a error on your profile picture upload!!");
    }
}
