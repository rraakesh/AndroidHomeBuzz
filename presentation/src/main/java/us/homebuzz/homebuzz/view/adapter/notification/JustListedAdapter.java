package us.homebuzz.homebuzz.view.adapter.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class JustListedAdapter  extends RecyclerView.Adapter<JustListedAdapter.UserViewHolder> {
    public static String TAG = JustSoldAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        void JustListedItemClick(UserJustListedModel userEstimateModel);
    }

    @Inject
    Picasso picasso;
    private List<UserJustListedModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener1;

    public JustListedAdapter(Context context, Collection<UserJustListedModel> usersCollection, Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserJustListedModel>) usersCollection;
        this.picasso = picasso;
    }

    @Override
    public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.notification_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserJustListedModel userJustListedModel = this.usersCollection.get(position);
      //  Log.e("value",userJustListedModel.getAddress());
        if(userJustListedModel.getStreet()!=null)
        {
            holder.Listing_title.setText(userJustListedModel.getStreet());
        }else
        {
            holder.Listing_title.setText("");
        }
       // System.out.println("street"+userJustListedModel.getStreet());

        if(userJustListedModel.getCity()!=null)
        {
            holder.city.setText(userJustListedModel.getCity()+",");
        }else{
            holder.city.setText("");
        }
        if(userJustListedModel.getZip()!=null){
            holder.zip.setText(userJustListedModel.getZip());
        }else{
            holder.zip.setText("");
        }
        if(userJustListedModel.getState()!=null){
            holder.state.setText(userJustListedModel.getState());

        }
        else {
            holder.state.setText("");
        }
        if(userJustListedModel.getPrice()!=null){
            holder.price.setText("$ "+ new DecimalFormat("#,###").format(userJustListedModel.getPrice().intValue()));
        }else{
            holder.price.setText("");
        }




        holder.leader_accuracy.setText(String.valueOf(userJustListedModel.getBedrooms()+"BR"+" / "+userJustListedModel.getBaths()+"BA"));
        if (null != userJustListedModel && userJustListedModel.getPhotos().size()>0) {
         //   System.out.println(TAG + "Pic" + String.valueOf(userJustListedModel.getPhotos()));
            picasso.load(String.valueOf(userJustListedModel.getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder._image);
        } else {
            //TODO Place A DEFAULT IMAGE
            holder._image.setImagePlaceHolder(R.drawable.ic_launcher);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (JustListedAdapter.this.onItemClickListener1 != null) {
                      JustListedAdapter.this.onItemClickListener1.JustListedItemClick(userJustListedModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserJustListedModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserJustListedModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener1) {
        this.onItemClickListener1 = onItemClickListener1;
    }

    private void validateUsersCollection(Collection<UserJustListedModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.Listing_title)
        TextView Listing_title;
        @Bind(R.id.state)
        TextView state;
        @Bind(R.id.price)
        TextView price;
        @Bind(R.id.city)
        TextView city;
        @Bind(R.id.zip)
        TextView zip;
        @Bind(R.id.row_second)
        TextView row_second;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id._image)
        AutoLoadImageView _image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
