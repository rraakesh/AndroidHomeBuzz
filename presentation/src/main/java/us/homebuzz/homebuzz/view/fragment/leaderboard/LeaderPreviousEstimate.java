package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.PreviousEstimateModule;
import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.presenter.PreviousEstimatePresenter;
import us.homebuzz.homebuzz.view.UserPreviousEstimateView;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.leaderboard.PreviousEstimateAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.setting.UserProfileFragment;

/**
 * Created by rakesh.kumar on 10/29/2015.
 */
public class LeaderPreviousEstimate extends BaseFragment implements UserPreviousEstimateView {
    private static final String TAG = LeaderPreviousEstimate.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_ESTIMATE_MODEL = "UserEstimateModel";
    private UserEstimateModel userEstimateModel;
    @Bind(R.id.profile_pic)
    CircleImageView profAutoLoadImageView;
   /* @Bind(R.id.profile_designations)
    TextView profileDesignationTextview;*/
    @Bind(R.id.Profile_points)
    TextView Profile_points;
    @Bind(R.id.estimate_accuracy)
    TextView estimate_accuracy;
    @Bind(R.id.profile_name)
    TextView profile_name;
    @Bind(R.id.estimate_)
    TextView estimate_;
    @Bind(R.id.previous_estimate_list)
    RecyclerView previous_estimate_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.l_name)
    TextView follow;
    @Bind(R.id.follow_image)
    ImageView follow_image;
    @Bind(R.id.follow_layout)
    RelativeLayout follow_layout;
    private PreviousEstimateAdapter previousEstimateAdapter;
    private UsersLayoutManager usersLayoutManager;
    @Inject
    Picasso picasso;
    @Inject
    PreviousEstimatePresenter previousEstimatePresenter;

    public static LeaderPreviousEstimate newInstance( UserEstimateModel userEstimateModel) {
        LeaderPreviousEstimate leaderPreviousEstimate =new LeaderPreviousEstimate();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_ESTIMATE_MODEL, userEstimateModel);
        leaderPreviousEstimate.setArguments(argumentsBundle);

        return leaderPreviousEstimate;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view_leader_previous = inflater.inflate(R.layout.leader_previous_estimate, container, false);
        ButterKnife.bind(this, view_leader_previous);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_ESTIMATE_MODEL) != null) {
            this.userEstimateModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_ESTIMATE_MODEL);
            Log.d(TAG, "UserModel in Leader Previous" + userEstimateModel.getId());
        }

        return view_leader_previous;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        this.setupUI();
    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.previous_estimate_list.setLayoutManager(usersLayoutManager);
        Log.d(TAG, "UserModel in Leader Previous SETUP" + userEstimateModel.getId());
        this.previousEstimateAdapter = new PreviousEstimateAdapter(getActivity(), new ArrayList<UserPreviousEstimateModel>(),picasso);
        this.previousEstimateAdapter.setOnItemClickListener(onItemClickListener);
        this.previous_estimate_list.setAdapter(previousEstimateAdapter);
        if (userEstimateModel.getProfilePic() != null && !userEstimateModel.getProfilePic().equals("null")){
            picasso.load(String.valueOf(userEstimateModel.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(profAutoLoadImageView);
        }
           // profAutoLoadImageView.setImageUrl(String.valueOf(userEstimateModel.getProfilePic()));
        // profileDesignationTextview.setText(userAccuracyModel.getAccurate());
        //   Profile_points.setText(userAccuracyModel.get());
        estimate_accuracy.setText(String.valueOf(new DecimalFormat("#.##").format(userEstimateModel.getAccurate() * 100)));
        profile_name.setText(userEstimateModel.getName());
        profile_name.setText(userEstimateModel.getName());
        final String name=userEstimateModel.getName();
        String[] fname=name.split("\\s+");
        follow.setText("Follow " +fname[0]);


    }

    private void initialize() {
        System.out.println("Class UserID" + userEstimateModel.getId());
        this.getComponent(MainActivityComponent.class).previousEstimateComponent(new PreviousEstimateModule(userEstimateModel.getId())).inject(this);
        previousEstimatePresenter.setView(this);
        previousEstimatePresenter.initialize(userEstimateModel.getId());

    }
    @OnClick(R.id.black_img)
    public void onblack_img(){  //call to profile_read
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());

    }

    @Override
    public void onResume() {
        super.onResume();
        this.previousEstimatePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.previousEstimatePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.previousEstimatePresenter.destroy();
    }


    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {this.rl_progress.setVisibility(View.GONE);

    }

    @Override
    public void showLoading() {this.rl_progress.setVisibility(View.VISIBLE);

    }

    @Override
    public void viewDetails(UserPreviousEstimateModel userPreviousEstimateModel) {
        //Todo Navigate to Next Fragment From Here
        Log.d(TAG, "The Clicked UserID " + userPreviousEstimateModel.getId());
        this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, LeaderBoardPreviousListing.newInstance(userPreviousEstimateModel));
        //TODO Call the replaceListner Method Naviagte to Activity To Move to Activity
    }

    private PreviousEstimateAdapter.OnItemClickListener onItemClickListener =
            new PreviousEstimateAdapter.OnItemClickListener() {
                @Override
                public void onPreviousEstimateAdapterItemClicked(UserPreviousEstimateModel userPreviousEstimateModel) {
                    if (LeaderPreviousEstimate.this.previousEstimatePresenter != null && userPreviousEstimateModel != null) {
                        LeaderPreviousEstimate.this.previousEstimatePresenter.onUserClicked(userPreviousEstimateModel);
                    }
                }
            };

    @Override
    public void readerPreviousEstimate(Collection<UserPreviousEstimateModel> userPreviousEstimateModelCollection) {
        Log.d(TAG, "The UserPreviousEstimateModels List " + userPreviousEstimateModelCollection.size());
        if (userPreviousEstimateModelCollection != null) {
            //  Log.d(TAG, "The UserPreviousEstimateModels List " +((List)userPreviousEstimateModelCollection).get(0).toString());
            this.previousEstimateAdapter.setUsersCollection(userPreviousEstimateModelCollection);
            if(((List<UserPreviousEstimateModel>) userPreviousEstimateModelCollection).get(0).getUser().getFollowed().toString().matches("true")){
                final String name = ((List<UserPreviousEstimateModel>) userPreviousEstimateModelCollection).get(0).getUser().getName();
                System.out.println("name"+name);
                String[] fname=name.split("\\s+");
                follow.setText("Following " + fname[0]);
                follow_layout.setBackgroundColor(getResources().getColor(R.color.red));
                follow_image.setImageResource(R.drawable.following);}

        }
    }


}
