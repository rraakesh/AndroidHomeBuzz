package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.SearchPeopleModel;

/**
 * Created by amit.singh on 12/8/2015.
 */
public interface SearchPeopleView extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  SearchPeopleModel The user that will be shown.
     */
    void viewSearchPeople(Collection<SearchPeopleModel> SearchPeopleModel);
    void viewUser(SearchPeopleModel SearchPeopleModel);
}
