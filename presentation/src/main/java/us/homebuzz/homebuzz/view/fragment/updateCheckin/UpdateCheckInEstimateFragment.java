package us.homebuzz.homebuzz.view.fragment.updateCheckin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.PhotoProcessing;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.UpdateCheckingModule;
import us.homebuzz.homebuzz.model.AddCheckInPhotoModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.model.UserUpdateCheckingModel;
import us.homebuzz.homebuzz.presenter.AddCheckInPhotoPresenter;
import us.homebuzz.homebuzz.presenter.UpdateCheckingPresenter;
import us.homebuzz.homebuzz.view.AddCheckInPhotoView;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireCamera;
import us.homebuzz.homebuzz.view.RequireStorage;
import us.homebuzz.homebuzz.view.UpdateCheckingView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhotoFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.WriteReviewFragment;

/**
 * Created by rohitkumar on 4/11/15.
 */
public class UpdateCheckInEstimateFragment extends BaseFragment implements UpdateCheckingView, AddCheckInPhotoView, PermissionBus {
    private static final String TAG = UpdateCheckInEstimateFragment.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_LISTING_ID1 = "Listing_id";
    private static final String ARGUMENT_KEY_USER_LISTING_ID2 = "Listing_id2";
    private static final String ARGUMENT_KEY_USER_LISTING_ID3 = "Listing_id3";
    private static final String ARGUMENT_KEY_USER_BOOLEAN = "isBoolean";
    ArrayList<String> photoListStrings = new ArrayList<>();
    ArrayList<String> photoListFromModel = new ArrayList<>();
    ArrayList<String> photoListCamera = new ArrayList<>();

    // this method using in old UI screen and fragments....
    public static UpdateCheckInEstimateFragment newInstance(/*UserNearbyModel userAccuracyModel*/String listing_id, boolean value) {
        UpdateCheckInEstimateFragment checkInEstimate = new UpdateCheckInEstimateFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putString(ARGUMENT_KEY_USER_LISTING_ID1, listing_id);
        argumentsBundle.putBoolean(ARGUMENT_KEY_USER_BOOLEAN, value);
        checkInEstimate.setArguments(argumentsBundle);
        return checkInEstimate;
    }


    public static UpdateCheckInEstimateFragment newInstance1(UserNearbyModel userNearbyModel, boolean value) {
        UpdateCheckInEstimateFragment checkInEstimate = new UpdateCheckInEstimateFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_LISTING_ID2, userNearbyModel);
        argumentsBundle.putBoolean(ARGUMENT_KEY_USER_BOOLEAN, value);
        checkInEstimate.setArguments(argumentsBundle);
        return checkInEstimate;
    }

    public static UpdateCheckInEstimateFragment newInstance2(UserPreviousEstimateModel userPreviousEstimateModel, boolean value) {
        UpdateCheckInEstimateFragment checkInEstimate = new UpdateCheckInEstimateFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_LISTING_ID3, userPreviousEstimateModel);
        argumentsBundle.putBoolean(ARGUMENT_KEY_USER_BOOLEAN, value);
        checkInEstimate.setArguments(argumentsBundle);
        return checkInEstimate;
    }

    private UserNearbyModel userNearbyModel;
    private UserPreviousEstimateModel userPreviousEstimateModel;
    private String PhotoPath;
    private int position;
    private Uri file_uri;
    @Inject
    PhotoProcessing photoProcessing;
    @Bind(R.id.add_photo)
    TextView add_photo;
    @Bind(R.id.txtWriteReview)
    TextView txtWriteReview;
    @Bind(R.id.image)
    AutoLoadImageView image;
    @Bind(R.id.rate_text)
    TextView price;
    @Bind(R.id.address)
    TextView address;
    @Bind(R.id.bedsbaths)
    TextView bedsbaths;
    @Bind(R.id.edEstimateValue)
    EditText edEstimateValue;
    @Bind(R.id.location_rating)
    RatingBar location_rating;
    @Bind(R.id.condition_rating)
    RatingBar condition_rating;
    private String Listing_id;
    @Bind(R.id.zip)
    TextView Zip;
    @Inject
    UpdateCheckingPresenter updateCheckingPresenter;
    @Inject
    AddCheckInPhotoPresenter addCheckInPhotoPresenter;
    @Inject
    Picasso picasso;
    @Inject
    Constants constants;
    @Inject
    SharedPreferences sharedPreferences;
    /*@Bind(R.id.add_image)
    ImageView add_image;*/
    private boolean check = false;
    private String reason = "";
    @Bind(R.id.show_images)
    LinearLayout linearLayout;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.rl_retry)
    RelativeLayout rl_retry;
    private int count = 0;
    private RequireStorage requireStorage;
    private RequireCamera requireCamera;
    private boolean cameraPermission, storagePermission;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View updateChecking = inflater.inflate(R.layout.update_checkin_and_estimate, container, false);
        ButterKnife.bind(this, updateChecking);
        if (!this.getArguments().getBoolean(ARGUMENT_KEY_USER_BOOLEAN)) {
            check = false;
            userPreviousEstimateModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_LISTING_ID3);
            if ((userPreviousEstimateModel.getListingId() != null)) {
                Listing_id = String.valueOf(userPreviousEstimateModel.getListingId());
                // Log.e("Listing_id", Listing_id);
                // Log.e("id", userPreviousEstimateModel.getId().toString());
            }
            reason = userPreviousEstimateModel.getReason();
        } else {
            check = true;
            userNearbyModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_LISTING_ID2);
            Listing_id = String.valueOf(userNearbyModel.getId());
            // Log.e("Listing_id", Listing_id);
            //   reason = userNearbyModel.getEstimates().get(0).getReason();
        }
        //imageList(photoListStrings);
        return updateChecking;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Check In");
        this.initialize();
        this.setupUI();
        if ((MainActivity) this.getActivity() instanceof RequireStorage)
            requireStorage = (RequireStorage) this.getActivity();
        if ((MainActivity) this.getActivity() instanceof RequireCamera)
            requireCamera = (RequireCamera) this.getActivity();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).upaUpdateCheckingComponent(new UpdateCheckingModule()).inject(this);
        updateCheckingPresenter.SetView(this);
        addCheckInPhotoPresenter.setView(this);
    }

    //Todo add the version Check for the Color Drawable or Find the Better solution to Add a Custom ratingBar
    private void setupUI() {
        if (check) {
            photoListFromModel.clear();
            photoListStrings.clear();
            if (userNearbyModel.getEstimates() != null && userNearbyModel.getEstimates().size() > 0) {
                for (int i = 0; i < userNearbyModel.getEstimates().size(); i++) {
                    Log.e("user id is", String.valueOf(userNearbyModel.getEstimates().get(i).getUserId()));
                    if (String.valueOf(userNearbyModel.getEstimates().get(i).getUserId()).equals(String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)))) {
                        if (userNearbyModel.getEstimates().get(i).getImageUrl().size() > 0) {
                            Log.e("size is", String.valueOf(userNearbyModel.getEstimates().get(i).getImageUrl().size()));
                            //Set the previous price
                            if (userNearbyModel.getEstimates().get(i).getPrice() != null)
                                edEstimateValue.setText(String.valueOf(userNearbyModel.getEstimates().get(i).getPrice()));
                          /*  for (int j = 0; j < userNearbyModel.getEstimates().get(i).getImageUrl().size(); j++) {*/
                            photoListFromModel.addAll(userNearbyModel.getEstimates().get(i).getImageUrl());
                            //  }
                        }
                    }
                }
            }
            Log.e("list size", String.valueOf(photoListStrings.size()));
            if (photoListFromModel.size() > 0) {
                photoListStrings.addAll(photoListFromModel);
            }
            if (photoListCamera.size() > 0) {
                photoListStrings.addAll(photoListCamera);
            }

            imageList(photoListStrings);
            price.setText("$ " + new DecimalFormat("#,###").format(userNearbyModel.getPrice().intValue()));
            condition_rating.setRating(userNearbyModel.getConditionRating());
            location_rating.setRating(userNearbyModel.getLocationRating().floatValue());
            if (userNearbyModel.getBedrooms() != null || userNearbyModel.getBaths() != null) {
                bedsbaths.setText(String.valueOf(userNearbyModel.getBedrooms()) + "BR /" + String.valueOf(userNearbyModel.getBaths()) + " BA");
            }
            address.setText(userNearbyModel.getStreet() != null ? userNearbyModel.getStreet() : "");
            Zip.setText(userNearbyModel.getCity() != null ? userNearbyModel.getCity() : "");
            Zip.append(", " + (userNearbyModel.getState() != null ? userNearbyModel.getState() : ""));
            Zip.append(" " + (userNearbyModel.getZip() != null ? userNearbyModel.getZip() : ""));
            if (userNearbyModel.getPhotos().size() > 0 && userNearbyModel.getPhotos() != null) {
                picasso.load(userNearbyModel.getPhotos().get(0)).into(image);
            } else {
                image.setImageResource(R.drawable.ic_launcher);
            }

        } else {
            if (userPreviousEstimateModel.getListing().getPrice() != null) {
                price.setText(String.valueOf(new DecimalFormat("###,###").format(userPreviousEstimateModel.getListing().getPrice().intValue())));
            }
            if (userPreviousEstimateModel.getPrice() != null)
                edEstimateValue.setText("$ " + new DecimalFormat("#,###").format(userPreviousEstimateModel.getPrice().intValue()));
            if (userPreviousEstimateModel.getConditionRating() != null)
                condition_rating.setRating(userPreviousEstimateModel.getConditionRating().floatValue());
            if (userPreviousEstimateModel.getLocationRating() != null)
                location_rating.setRating(userPreviousEstimateModel.getLocationRating().floatValue());
            if (userPreviousEstimateModel.getBedrooms() != null && userPreviousEstimateModel.getListing().getBaths() != null)
                bedsbaths.setText(String.valueOf(userPreviousEstimateModel.getBedrooms()) + "BR /" + String.valueOf(userPreviousEstimateModel.getListing().getBaths()) + " BA");
            address.setText(userPreviousEstimateModel.getListing().getStreet() != null ? userPreviousEstimateModel.getListing().getStreet() : "");
            Zip.setText(userPreviousEstimateModel.getListing().getCity() != null ? userPreviousEstimateModel.getListing().getCity() : "");
            Zip.append(", " + (userPreviousEstimateModel.getListing().getState() != null ? userPreviousEstimateModel.getListing().getState() : ""));
            Zip.append(" " + (userPreviousEstimateModel.getListing().getZip() != null ? userPreviousEstimateModel.getListing().getZip() : ""));
            if (userPreviousEstimateModel.getListing().getPhotos().size() > 0 && userPreviousEstimateModel.getListing().getPhotos() != null) {
                picasso.load(String.valueOf(userPreviousEstimateModel.getListing().getPhotos().get(0))).into(image);
            } else {
                image.setImageResource(R.drawable.ic_launcher);
            }
        }
//        LayerDrawable layerDrawable = (LayerDrawable) location_rating.getProgressDrawable();
//
//        LayerDrawable layerDrawable1 = (LayerDrawable) condition_rating
//                .getProgressDrawable();

    }

    @OnClick(R.id.update_checkin)
    public void onUpdateCheckinClick() {
        if (check) {
            if (userNearbyModel.getForSale() != null) {
                if (userNearbyModel.getForSale()) {
                    if (condition_rating.getRating() != 0 && location_rating.getRating() != 0) {
                        WeakHashMap<String, String> updateCheckin = new WeakHashMap<>();
                        updateCheckin.put(constants.ESTIMATE_LISTING_ID, Listing_id);
                        updateCheckin.put(constants.ESTIMATE_LISTIING_CONDITION_RATING, String.valueOf(condition_rating.getRating()));
                        updateCheckin.put(constants.ESTIMATE_LISTING_LOCATION_RATING, String.valueOf(location_rating.getRating()));
                        updateCheckin.put(constants.ESTIMATE_LISTING_REASON, WriteReviewFragment.review);
                        updateCheckin.put(constants.ESTIMATE_LISTING_PRICE, edEstimateValue.getText().toString());
                        updateCheckingPresenter.initialize(Listing_id, updateCheckin);
                    } else {
                        showDialog("Please fill the condition and location rating first.");
                    }
                } else {
                    showDialog("You can only check-in at listing that are for-sale.");
                }
            } else {
                showDialog("You can only check-in at listing that are for-sale.");
            }
        } else {
            if (userPreviousEstimateModel.getListing() != null) {
                if (userPreviousEstimateModel.getListing().getForSale()) {
                    if (condition_rating.getRating() != 0 && location_rating.getRating() != 0) {
                        WeakHashMap<String, String> updateCheckin = new WeakHashMap<>();
                        updateCheckin.put(constants.ESTIMATE_LISTING_ID, Listing_id);
                        updateCheckin.put(constants.ESTIMATE_LISTIING_CONDITION_RATING, String.valueOf(condition_rating.getRating()));
                        updateCheckin.put(constants.ESTIMATE_LISTING_LOCATION_RATING, String.valueOf(location_rating.getRating()));
                        updateCheckin.put(constants.ESTIMATE_LISTING_REASON, WriteReviewFragment.review);
                        updateCheckin.put(constants.ESTIMATE_LISTING_PRICE, edEstimateValue.getText().toString());
//                        for (String photoUrl : photoListStrings) {
//                            // WeakHashMap<String,String> photo=new WeakHashMap<>(1);
//                            updateCheckin.put("photo", photoUrl);
//                            //this.updateCheckingPresenter.initialize(String.valueOf(userPreviousEstimateModel.getId()),updateCheckin,null);
//                        }
                        updateCheckingPresenter.initialize(Listing_id, updateCheckin);
                    } else {
                        showDialog("Please fill the condition and location rating first.");
                    }
                } else {
                    showDialog("You can only check-in at listing that are for-sale.");
                }
            } else {
                showDialog("You can only check-in at listing that are for-sale.");
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        System.out.println("On resume");
        if (photoListStrings.size() > 0) {
            imageList(photoListStrings);
        }
        this.updateCheckingPresenter.resume();
        this.addCheckInPhotoPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.updateCheckingPresenter.pause();
        this.addCheckInPhotoPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Property Details");
        this.updateCheckingPresenter.destroy();
        this.addCheckInPhotoPresenter.destroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderAfterUpdateCheckin(UserUpdateCheckingModel userUpdateCheckinModel) {
        WeakHashMap<String, String> updateListingPhoto = new WeakHashMap<>();
        WeakHashMap<String, String> upLoadPhoto = new WeakHashMap<>();
        if (userUpdateCheckinModel.getListing().getEstimates() != null && userUpdateCheckinModel.getListing().getEstimates().size() > 0) {
            String id = "";
            for (int i = 0; i < userUpdateCheckinModel.getListing().getEstimates().size(); i++) {
                Log.e("ID List ", String.valueOf(userUpdateCheckinModel.getListing().getEstimates().get(i).getId()));
                if (String.valueOf(userUpdateCheckinModel.getListing().getEstimates().get(i).getUserId()).equals(String.valueOf(sharedPreferences.getInt(constants.KEY_ID, 0)))) {
                    id = String.valueOf(userUpdateCheckinModel.getListing().getEstimates().get(i).getId());
                    Log.e("ID is ", String.valueOf(userUpdateCheckinModel.getListing().getEstimates().get(i).getId()));
                    break;
                }

            }
            /**
             * TODO REFACTOR THIS LATER
             * Ugly Way to Update the Details Screen If the User has Already checkin then we have to handle the update of Checkin Count and Estimate count
             * Else We simply add this to Estimate List if User is chekin in for the First Time
             *
             */
            if (check && null != userNearbyModel) {
                boolean isAllreadyCheckedIn = false;
                for (int i = 0; i < userNearbyModel.getEstimates().size(); i++) {
                    if (userNearbyModel.getEstimates().get(i).getUserId() == sharedPreferences.getInt(constants.KEY_ID, 0)) {
                        isAllreadyCheckedIn = true;
                        UserNearbyModel.Estimate estimate = userNearbyModel.getEstimates().get(i);
                        if (estimate.getReason() != null && (estimate.getReason().isEmpty()) && null != WriteReviewFragment.review && !(WriteReviewFragment.review.isEmpty()))
                            userNearbyModel.setCheckin_count(String.valueOf(Integer.valueOf(userNearbyModel.getCheckin_count()) + 1));
                        if (estimate.getPrice() != null && !(estimate.getPrice() > 0.0) && !(edEstimateValue.getText().toString().isEmpty()))
                            userNearbyModel.setEstimates_count(String.valueOf(Integer.valueOf(userNearbyModel.getEstimates_count()) + 1));
                        estimate.setConditionRating((double) condition_rating.getRating());
                        estimate.setLocationRating((double) location_rating.getRating());
                        estimate.setReason((WriteReviewFragment.review.isEmpty()) ? estimate.getReason() : WriteReviewFragment.review);
                        estimate.setPrice((edEstimateValue.getText().toString().isEmpty()) ? estimate.getPrice() : Double.valueOf(edEstimateValue.getText().toString()));

                    }
                }
                if (!(isAllreadyCheckedIn)) {
                    UserNearbyModel.Estimate estimate = userNearbyModel.new Estimate();
                    estimate.setConditionRating((double) condition_rating.getRating());
                    estimate.setLocationRating((double) location_rating.getRating());
                    estimate.setName(sharedPreferences.getString(constants.KEY_NAME, ""));
                    estimate.setReason(WriteReviewFragment.review);
                    estimate.setPrice(Double.valueOf(edEstimateValue.getText().toString()));
                    userNearbyModel.getEstimates().add(estimate);
                    if (null != WriteReviewFragment.review && !WriteReviewFragment.review.isEmpty())
                        userNearbyModel.setCheckin_count(String.valueOf(Integer.valueOf((userNearbyModel.getCheckin_count() == null) ? "0" : userNearbyModel.getCheckin_count()) + 1));
                    if (null != edEstimateValue.getText() && !(edEstimateValue.getText().toString().isEmpty()))
                        userNearbyModel.setEstimates_count(String.valueOf(Integer.valueOf((userNearbyModel.getEstimates_count() == null) ? "0" : userNearbyModel.getEstimates_count()) + 1));
                }
            }
            if (id != null && !id.matches("null") && photoListCamera.size() > 0) {
                for (String photoUrl : photoListCamera) {
                    Log.e(TAG, " photo uploading");
                    upLoadPhoto.put(constants.ESTIMATE_PHOTO, photoUrl);
                    updateListingPhoto.put(constants.ESTIMATE_PHOTO_LISTING_ID, Listing_id);
                    updateListingPhoto.put(constants.ESTIMATE_PHOTO_ESTIMATE_ID, id);
                    // updateListingPhoto.put(constants.ESTIMATE_PHOTO_ID, userUpdateCheckinModel.getId());
                    this.addCheckInPhotoPresenter.initialize(id, upLoadPhoto, updateListingPhoto);
                }
            } else {
                super.showToastMessage("Your Estimate is Added Successfully!!");
                getActivity().onBackPressed();
            }

        }
    }

    @OnClick(R.id.add_photo)
    public void addPhoto() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (requireStorage != null && requireCamera != null) {
                requireStorage.onStorageRequest();
                requireCamera.onCameraRequest();
            }
        } else {
            // Pre-Marshmallow
            StartPhotoDialog();
        }

    }

    private void StartPhotoDialog() {
        final Dialog dialog = new Dialog(getActivity());
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setTitle(null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photo_dailog);

        dialog.setCancelable(true);
        dialog.show();

        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        TextView take_photo = (TextView) dialog.findViewById(R.id.take_photo);
        TextView photo_gallery = (TextView) dialog.findViewById(R.id.photo_gallery);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //create a new file
                File file = new File(Environment.getExternalStorageDirectory(), "_" + System.currentTimeMillis() + ".jpg");
                file_uri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
                startActivityForResult(cameraIntent, Constants.PICK_IMAGE_REQUEST_CAMERA);
                dialog.cancel();
            }
        });

        photo_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get image from library
                Intent galleryintent = new Intent();
                // Show only images, no videos or anything else
                galleryintent.setType("image/*");
                galleryintent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(galleryintent, "Choose  Picture"), Constants.PICK_IMAGE_REQUEST_LIB);
                dialog.cancel();
            }
        });
    }

    private void permisssionStatus() {
        if(storagePermission==true && cameraPermission==true)
            StartPhotoDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PICK_IMAGE_REQUEST_LIB && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                String file_path = photoProcessing.getRealPathFromURI(uri, UpdateCheckInEstimateFragment.this.getContext());
                onImageSuccess(file_path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.PICK_IMAGE_REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            try {
                onImageSuccess(photoProcessing.compressImage(file_uri.getPath()));
                // delete the temp file from storage
                File fileTodelete = new File(file_uri.getPath());
//                if (fileTodelete.exists()) {
//                    fileTodelete.delete();
//                    Log.i("File", "deleted");
//                }
                // null the value of file uri
                file_uri = null;

            } catch (Exception e) {
                e.printStackTrace();
                file_uri = null;
            }
        } else {
            file_uri = null;
            position = -1;
        }
    }

    public void onImageSuccess(String filepath) {
        photoListCamera.add(filepath);
        photoListStrings.add(filepath);
        imageList(photoListStrings);
    }

    private void imageList(List<String> filepath) {
        if (linearLayout.getChildAt(0) != null)
            linearLayout.removeAllViews();
        Log.e(TAG, String.valueOf(filepath.size()));
        for (int i = 0; i < filepath.size(); i++) {
            Log.e(TAG, filepath.get(i).toString());
            ImageView imageView = new ImageView(UpdateCheckInEstimateFragment.this.getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
            imageView.setLayoutParams(params);
            imageView.setPadding(1, 5, 1, 5);
            linearLayout.addView(imageView);
            if (filepath.get(i).toString().startsWith("https")) {
                picasso.load(filepath.get(i).toString()).into(imageView);
            } else {
                picasso.load(new File(filepath.get(i).toString())).into(imageView);
            }
        }
    }

    @OnClick(R.id.show_images)
    public void imageShow() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingPhotoFragment.newInstance1(photoListStrings, false));
    }

    @OnClick(R.id.txtWriteReview)
    public void writeReview() {
        if (reason != null) {
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, WriteReviewFragment.newInstance(reason));
        } else {
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, WriteReviewFragment.newInstance(""));
        }

    }


    @Override
    public void viewAddCheckInPhoto(AddCheckInPhotoModel addCheckInPhotoModel) {
        count++;
        if (count == photoListCamera.size()) {
            super.showToastMessage("Your Estimate is Added Successfully!!");
            getActivity().onBackPressed();
        }
        // super.showToastMessage("photo added successfully");
    }

    //TODO BETTER NEGATIVE FEEDBACK HANDLING
    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {
        cameraPermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {
        storagePermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {

    }
}

