package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.Utility.InfoWindowRefresher;
import us.homebuzz.homebuzz.Utility.LocationInterface;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.presenter.NearByPresenter;
import us.homebuzz.homebuzz.view.DrawerProfileBus;
import us.homebuzz.homebuzz.view.NotificationUpdateBus;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireLocation;
import us.homebuzz.homebuzz.view.UnreadMessageview;
import us.homebuzz.homebuzz.view.UserNearbyView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInListFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInMapFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.CheckInNearBy;
import us.homebuzz.homebuzz.view.fragment.NearBy.ListingFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.NearbyFilterFragment;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;


/**
 * Created by amit.singh on 11/16/2015.
 */
@SuppressWarnings("ALL")
public class CheckInNearby extends BaseCheckInNearBy implements OnMapReadyCallback, UserNearbyView, LocationInterface, UnreadMessageview, NearbyFilterFragment.NearbyFilter, PermissionBus {
    private static final String TAG = CheckInNearBy.class.getSimpleName();
    private static CheckInNearby mCheckInNearby;
    private static final String ARGUMENT_KEY_ADD_LISTING_LON = "lon";
    private static final String ARGUMENT_KEY_ADD_LISTING_LAT = "lat";

    public static CheckInNearby newInstance() {
        if (CheckInNearby.mCheckInNearby == null) {
            CheckInNearby.mCheckInNearby = new CheckInNearby();
        }
      //  Bundle argumentsBundle = CheckInNearby.mCheckInNearby.getArguments();
        //CheckInNearby.mCheckInNearby.setArguments(argumentsBundle);
        return CheckInNearby.mCheckInNearby;
    }

    public static CheckInNearby newInstance(Double lat, Double lon) {
        if (CheckInNearby.mCheckInNearby == null)
            CheckInNearby.mCheckInNearby = new CheckInNearby();

        Bundle argumentsBundle = CheckInNearby.mCheckInNearby.getArguments();
        argumentsBundle.putDouble(ARGUMENT_KEY_ADD_LISTING_LAT, lat);
        argumentsBundle.putDouble(ARGUMENT_KEY_ADD_LISTING_LON, lon);
      /*  mCheckInNearby.(argumentsBundle);*/

        return CheckInNearby.mCheckInNearby;
    }


    public CheckInNearby() {
        super();
    }

    Double mlat = 0.0, mlon = 0.0;
    GoogleMap mMap;
    CheckInMapFragment mMapFragment;
    @Inject
    NearByPresenter nearByPresenter;
    @Inject
    public GpsLocation gpsLocation;
    @Inject
    Constants constants;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Picasso picasso;
    @Bind(R.id.btn_list)
    TextView list;
    @Bind(R.id.btn_map)
    TextView map;
    private static int LocationdFailCount;
    public ReplaceFragment replaceFragmentNearby;
    private Collection<UserNearbyModel> userNearbyModelCollection;
    private FrameLayout Layoutinfowindow;
    CheckInListFragment checkInListFragment;
    private RequireLocation requireLocation;
    public static final int REQUEST_CHECK_SETTINGS = 500;
    private DrawerProfileBus drawerProfileBus;
    private NotificationUpdateBus notificationUpdateBus;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.Layoutinfowindow = new FrameLayout(this.getContext());
        if (checkPlayServices()) {
            mMapFragment = CheckInMapFragment.newInstance();
            mMapFragment.getMapAsync(this);
            super.onMapButtonClick(mMapFragment);
        } else {
            super.showToastMessage("Google Play Services Required.");
        }

    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                showToastMessage("This device is not supported.");
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        if ((MainActivity) this.getActivity() instanceof RequireLocation)
            requireLocation = (RequireLocation) this.getActivity();
        //Todo Consider a Better Solution to Have Nested Fragment Access the Listener
        this.replaceFragmentNearby = replaceListener;
        //((MainActivity) getActivity()).toolbar.findViewById(R.id.setting_icon).setVisibility(View.VISIBLE);
        if (this.getArguments() != null && this.getArguments().getDouble(ARGUMENT_KEY_ADD_LISTING_LAT) != 0.0)
            mlat = this.getArguments().getDouble(ARGUMENT_KEY_ADD_LISTING_LAT);

        if (this.getArguments() != null && this.getArguments().getDouble(ARGUMENT_KEY_ADD_LISTING_LON) != 0.0)
            mlon = this.getArguments().getDouble(ARGUMENT_KEY_ADD_LISTING_LON);

        if(this.getActivity() instanceof DrawerProfileBus)
            drawerProfileBus=(DrawerProfileBus)this.getActivity();

        if(this.getActivity() instanceof NotificationUpdateBus)
            notificationUpdateBus=(NotificationUpdateBus) this.getActivity();

        Log.d(TAG, "LAt" + mlat + "LON" + mlon);
        if (mlat != null && mlon != null && mlat != 0.0 && mlon != 0.0) {
            LatLng mlatlon = new LatLng(mlat, mlon);
            Log.d(TAG, "After Object" + mlatlon.longitude + "LATI" + mlatlon.latitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mlat, mlon), 15.5f));
        }


    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);
        //TO Inject Itself to the Message Module to Access the Unread Messages
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        gpsLocation.setonLocationListener(this);
        this.nearByPresenter.setView(this);
        super.initializeUnread();
        super.initializeProfile(sharedPreferences.getInt(constants.KEY_ID, 0));
        map.setBackgroundColor(getResources().getColor(R.color.leaderboard_back));
        map.setTextColor(getResources().getColor(R.color.color_white));
        list.setTextColor(getResources().getColor(R.color.leaderboard_back));
        list.setBackgroundColor(getResources().getColor(R.color.color_white));

    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (map != null) {
            this.initializeGoogleMap(map);
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (requireLocation != null)
                    requireLocation.onLocationRequest();
            } else {
                // Pre-Marshmallow
                gpsLocation.callbacksResults();
            }

        }
    }

    @Override
    public void onLocationFailed() {

// TODO HANDLE THE Better Exponetial Back Off
        new Handler().postDelayed(new Runnable() {
            public void run() {
                // try your request here; if it fails, then repost:
                if (LocationdFailCount < 3) {
                    gpsLocation.callbacksResults();
                } else {
                    showToastMessage("Fail To get the Location");
                }
            }
        }, 5 * 1000);
        LocationdFailCount++;
    }

    @Override
    public void onConnectionSuspended() {
        //  gpsLocation.callbacksResults();
    }

    @Override
    public void onLocation(Location location) {
        Log.d(TAG, "Location in Nearby " + location.getLongitude());
        if (location != null) {
            //Putting the Location to Shared Preferences
            sharedPreferences.edit().putFloat(constants._LON, (float) location.getLongitude()).putFloat(constants._LAT, (float) location.getLatitude()).commit();
            //Initialize the Notification Other Just Sold And Just Listed
            WeakHashMap<String, String> userLoc = new WeakHashMap<>(2);
            userLoc.put(constants._LON, String.valueOf(location.getLongitude()));
            userLoc.put(constants._LAT, String.valueOf(location.getLatitude()));
            super.initlaizeSoldAndListed(userLoc);
            if (mMap != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15.5f));

            }
        }

    }

    //Update the Data After we get it From NearbyPresenter->GetNearbyListing->UserRepository->Threads and Observer
    @Override
    public void renderUserNearbyList(Collection<UserNearbyModel> userNearbyModelCollection) {
        this.userNearbyModelCollection = userNearbyModelCollection;
        Log.d(TAG, "Checking the Nearby List" + userNearbyModelCollection.size());
        // Log.d(TAG, "" + (((List) userNearbyModelCollection).get(0)));
        if (checkInListFragment != null) {
            checkInListFragment.updateList(userNearbyModelCollection);
        }
        this.setMapMarker((List) userNearbyModelCollection);
    }

    private void initializeGoogleMap(final GoogleMap googleMap) {
        //  System.out.println("Inside the Map CallBack");
        this.mMap = googleMap;
        this.mMap.setMyLocationEnabled(true);
        this.mMap.getUiSettings().setZoomControlsEnabled(true);
        //Bottom Padding
      //  float bottomMapPadding = 50 * getResources().getDisplayMetrics().density;
//      this.mMap.setPadding(0,0,0,(int)bottomMapPadding);
        //Handling the Marker Click Listener
        this.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                marker.showInfoWindow();
                return true;
            }
        });
        //Handling the Info Window Click Listener
        this.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
                UserNearbyModel userNearbyModel = (UserNearbyModel) ((List) userNearbyModelCollection).get(Integer.parseInt(marker.getTitle()));
                replaceFragmentNearby.CallReplaceFragment(R.id.conatainer_layout, ListingFragment.newInstance(userNearbyModel, true));
            }
        });
        //Setup the Camera Position Change Listener
        this.mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                System.out.println("zoom " + cameraPosition.zoom);
                VisibleRegion vr = mMap.getProjection().getVisibleRegion();
                double left = vr.latLngBounds.southwest.longitude;
                double top = vr.latLngBounds.northeast.latitude;
                double right = vr.latLngBounds.northeast.longitude;
                double bottom = vr.latLngBounds.southwest.latitude;
                Location middle = new Location("MiddleLeftCornerLocation");
                middle.setLongitude(bottom);
                middle.setLatitude(vr.latLngBounds.getCenter().longitude);
                if (cameraPosition.target.longitude != 0.0 && getDistanceFromMap(middle) / 1000 > 20 /*Calculate the Diatance*/) {
                    float[] results = new float[2];
                    Location.distanceBetween(
                            cameraPosition.target.latitude,
                            cameraPosition.target.longitude,
                            top,
                            right,
                            results);
                    Log.d(TAG, "Radius received" + results[0]);
                    sharedPreferences.edit().putFloat(constants._RADIUS, results[0] / 1000).putFloat(constants._LAT, (float) cameraPosition.target.latitude).putFloat(constants._LON, (float) cameraPosition.target.longitude).commit();
                    SortedMap<String, String> nearbyData = new TreeMap<>();
                    nearbyData.put(constants._RADIUS, String.valueOf(sharedPreferences.getFloat(constants._RADIUS, 0.0f)));
                    nearbyData.put(constants._LON, String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)));
                    nearbyData.put(constants._LAT, String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));
                    // callNearbyPresenter(String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)), String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)), results[0]);
                    callNearbyPresenter(nearbyData);
                }
            }
        });
    }

    private void callNearbyPresenter(/*String lat,String lon,float distance*/SortedMap<String, String> data) {

        this.nearByPresenter.initialize(/*lat, lon, String.valueOf(distance / 1000)*/sharedPreferences.getInt(constants.KEY_ID, 0), data);

    }

    private float getDistanceFromMap(Location middleleftlocation) {
        Location MiddleLeftCornerLocation = middleleftlocation;//(center's latitude,vr.latLngBounds.southwest.longitude)
        Location center = new Location("center");
        center.setLatitude(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().latitude);
        center.setLongitude(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().longitude);
        System.out.println("Distance" + center.distanceTo(MiddleLeftCornerLocation));
        return center.distanceTo(MiddleLeftCornerLocation);
    }

    //marker Info window get the Data based ont the Title we set the position and get the Data from UserNearbyModelCollection with Position Value
    private class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        UserNearbyModel userNearbyModel;

        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.info_window, Layoutinfowindow, false);
            userNearbyModel = (UserNearbyModel) ((List) userNearbyModelCollection).get(Integer.parseInt(marker.getTitle()));
            ImageView imageview = (ImageView) v.findViewById(R.id.image);
            TextView price = (TextView) v.findViewById(R.id.text1);
            TextView bathTextView = (TextView) v.findViewById(R.id.bath_text);
            TextView bedTextView = (TextView) v.findViewById(R.id.bed_text);
            //      RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratebar);
           /* TextView address = (TextView) v.findViewById(R.id.text);
            String addr = userNearbyModel.getAddress();
            address.setText(addr + "");
            if (addr == null || addr.equals("")) {
                address.setText("NA");
                *//*address.setTextColor(CheckInNearBy.this.getActivity().getResources().getColor(R.color.color_black));*//*
            } else {
                if (addr.length() > 10) {
                    addr = addr.substring(0, 10);
                    addr = addr + "....";
                    address.setText(addr);
                }
            }*/
            price.setText("$" + new DecimalFormat("###,###,###").format(userNearbyModel.getPrice()).toString());
            bathTextView.setText(userNearbyModel.getBaths() + " Baths");
            bedTextView.setText(userNearbyModel.getBedrooms() + "Beds");
            List<String> thumbnail = userNearbyModel.getPhotos();
            if (thumbnail.size() > 0) {
                if (!(thumbnail.get(0).equalsIgnoreCase("null"))) {
                    String image_url = thumbnail.get(0).toString();
                    picasso.load(image_url).resize(100, 100).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(imageview, new InfoWindowRefresher(marker));
                    System.out.println("image url = " + image_url);
                }

            } else
                imageview.setImageResource(R.drawable.ic_launcher);

            return v;

        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

    }

    private void setMapMarker(List<UserNearbyModel> userNearbyModelCollection) {
        if (mMap != null) {
            mMap.clear();
            for (int i = 0; i < userNearbyModelCollection.size(); i++) {
                final UserNearbyModel mdata = userNearbyModelCollection.get(i);
                Double latitude = userNearbyModelCollection.get(i).getLat();

                Double longitude = userNearbyModelCollection.get(i).getLon();
                MarkerOptions marker = new MarkerOptions().position(new LatLng((latitude),
                        (longitude)));
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin));
                marker.title("" + i);
                mMap.addMarker(marker);
            }
        }
    }


    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void onResume() {
        super.onResume();
        this.nearByPresenter.resume();
        this.getActivity().registerReceiver(mLocationSearchReceiver, getIntentFilter());
    }

    @Override
    public void onPause() {
        super.onPause();
        this.nearByPresenter.pause();
        this.getActivity().unregisterReceiver(mLocationSearchReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.nearByPresenter.destroy();
        //  gpsLocation.disconnect();
    }


    @OnClick(R.id.btn_map)
    public void onMapBtnClick() {
        this.onMapButtonClick(mMapFragment);
        map.setBackgroundColor(getResources().getColor(R.color.leaderboard_back));
        map.setTextColor(getResources().getColor(R.color.color_white));
        list.setTextColor(getResources().getColor(R.color.leaderboard_back));
        list.setBackgroundColor(getResources().getColor(R.color.color_white));

    }

    @OnClick(R.id.btn_list)
    public void onListBtnClick() {
        //Todo Change That to Have a mechanism for Updat the UI if user click and We Dont have Data In List
        if (userNearbyModelCollection != null && userNearbyModelCollection.size() > 0) {
            checkInListFragment = CheckInListFragment.newInstance((ArrayList) userNearbyModelCollection);
            this.onListButtonClick(checkInListFragment);
            list.setBackgroundColor(getResources().getColor(R.color.leaderboard_back));
            list.setTextColor(getResources().getColor(R.color.color_white));
            map.setTextColor(getResources().getColor(R.color.leaderboard_back));
            map.setBackgroundColor(getResources().getColor(R.color.color_white));
        }
    }

    @Override
    public void showError(String message) {
        /*this.showToastMessage(message);*/
    }

    @Override
    public Context getContext() {
        return this.getActivity().getApplicationContext();
    }

    @Override
    protected void onQuerySubmitted(final String Query) {
        showLoading();
        Log.d(TAG, Query);
        LocationSearchService.startSearchService(this.getContext(), Query, constants.ACTION_SEARCH, constants.CATEGORY_SEARCH);

    }

    public BroadcastReceiver mLocationSearchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(constants.EXTRA_ERROR)) {
                //MatchError error = (MatchError)intent.getParcelableExtra(MatchIntent.EXTRA_ERROR);
                Log.e(TAG, "" + intent.getStringExtra(constants.EXTRA_ERROR));
                showToastMessage(intent.getStringExtra(constants.EXTRA_ERROR));
            } else {
                //Update the Ui with Camera Position Changes
                System.out.println("LAT SEARCH" + intent.getDoubleExtra(constants.RESPONSE_LAT, 0) + "LON Search" + intent.getDoubleExtra(constants.RESPONSE_LON, 0));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(intent.getDoubleExtra(constants.RESPONSE_LAT, 0), intent.getDoubleExtra(constants.RESPONSE_LON, 0)), 15.5f));
            }
            hideLoading();
        }
    };

    private IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter(constants.ACTION_SEARCH);
        filter.addCategory(constants.CATEGORY_SEARCH);
        return filter;
    }

    @Override
    public void onUpdateClick(SortedMap<String, String> filterData) {
        Log.d(TAG, "Checking the FilterData" + filterData.size());
        callNearbyPresenter(filterData);
    }

    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {

    }

    //Ask for User 3 Times before Displaying the Dialog About the Location Permission
    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {
        if (isSuccess)
            gpsLocation.callbacksResults();
        else {
            //TODO HANDLE THE NEGATIVE FEEDBACK
            Snackbar.make(this.getView(), R.string.permission_Location_rationale,
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    void initProifleUpdate(UserProfileModel userProfileModel) {
        drawerProfileBus.upDateProfileData(userProfileModel.getName(), userProfileModel.getEmail());
        drawerProfileBus.upDateProfilePic(this.picasso,userProfileModel.getProfilePic());
    }

    @Override
    void UpdateNotificationCount(int notificationCount) {
        notificationUpdateBus.UpdateNoticationCount(notificationCount);
    }
}
