package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserLoginModel;

/**
 * Created by amit.singh on 10/31/2015.
 */
public interface UserSignupView extends  LoadDataView {
    /**
     * Render a user in the UI.
     *
     * @param user The {@link UserLoginModel} that will be shown.
     */
    void renderAfterUserSignUp(UserLoginModel user);
}
