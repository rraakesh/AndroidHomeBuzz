package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.FollowUnfollowModel;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
    public interface FollowUnfollowView extends LoadDataView {

    void viewUser(FollowUnfollowModel FollowUnfollowModel);
    void renderFollowUnfollow(FollowUnfollowModel FollowUnfollowModel);
    }