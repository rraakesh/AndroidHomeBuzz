package us.homebuzz.homebuzz.view.fragment.addlisting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobeta.android.dslv.DragSortListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.PhotoProcessing;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;
import us.homebuzz.homebuzz.presenter.AddListingPresenter;
import us.homebuzz.homebuzz.view.AddListingView;
import us.homebuzz.homebuzz.view.adapter.addnewlisting.AddPhotoAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rakesh.kumar on 10/31/2015.
 */
public class AddListing extends BaseFragment implements AddListingView {
    private static final String TAG = AddListing.class.getSimpleName();

    public static AddListing newInstance() {
        AddListing addListing = new AddListing();
        return addListing;
    }

    List<String> PhotoPath;
    private Uri file_uri;
    private int position;
    @Bind(R.id.addPhoto)
    DragSortListView dragSortListView;
    @Bind(R.id.add_listing_to_the_db_txt)
    TextView add_listing_txt;
    @Bind(R.id.zipcode_edittext)
    EditText zipcodeEditText;
    @Bind(R.id.acres_edittext)
    EditText acresEditText;
    @Bind(R.id.price_edittext)
    EditText priceEditText;
    @Bind(R.id.full_bath_value)
    TextView fullBathTextView;
    @Bind(R.id.bedroom_value)
    TextView bedRoomTextView;
    @Bind(R.id.half_bath_txt_value)
    TextView halfBathTextView;
    @Bind(R.id.bonusroom_value)
    TextView bonusroom_value;
    @Bind(R.id.last_sold_price_edittext)
    TextView last_sold_price_edittext;
    @Bind(R.id.recorded_mortgage_edittext)
    TextView recorded_mortgage_edittext;
    @Bind(R.id.assesed_value_edittext)
    TextView assesed_value_edittext;
    @Bind(R.id.home_owner_fees_edittext)
    TextView homeOwnerFees;
    @Bind(R.id.home_owner_assessments_edittext)
    TextView homeownerass;
    @Bind(R.id.other_fees1_edittext)
    TextView otherfees1;
    @Bind(R.id.other_fees2_edittext)
    TextView otherfees2;
    @Bind(R.id.parking_non_garaged_spaces_edittext)
    TextView parkingnon;
    @Bind(R.id.estimated_square_feet_edittext)
    TextView estimatedsquare;
    @Bind(R.id.lot_sqft_approx_edittext)
    TextView lotsqft;
    @Bind(R.id.garage_spaces_edittext)
    TextView garagespaces;
    @Bind(R.id.parking_spaces_edittext)
    TextView parkingspaces;
    @Bind(R.id.monthly_total_fees_edittext)
    TextView monthlytotal;

    @Bind(R.id.fire_place_txt_value)
    TextView firePlaceTextView;
    @Bind(R.id.year_built_edittext)
    EditText yearBuiltEditText;
    @Bind(R.id.remark_edittext)
    EditText remarkEditText;
    @Bind(R.id. current_taxes_edittext)
    EditText currentTaxes;


    @Bind(R.id.half_bath_minus)
    ImageView half_bath_minus;
    @Bind(R.id.half_bath_plus)
    ImageView half_bath_plus;
    @Bind(R.id.fire_place_minus)
    ImageView fire_place_minus;
    @Bind(R.id.fire_place_plus)
    ImageView fire_place_plus;
    @Bind(R.id.bedroom_minus)
    ImageView bedroom_minus;
    @Bind(R.id.bedroom_plus)
    ImageView bedroom_plus;
    @Bind(R.id.bonusroom_minus)
    ImageView bonusroom_minus;
    @Bind(R.id.bonusroom_plus)
    ImageView bonusroom_plus;
    @Bind(R.id.full_bath_minus)
    ImageView full_bath_minus;
    @Bind(R.id.full_bath_plus)
    ImageView full_bath_plus;
    @Bind(R.id.for_sale_image)
    ImageView  for_sale_image;
    View header,footer;
    AddPhotoAdapter addPhotoAdapter;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    PhotoProcessing photoProcessing;
    @Inject
    AddListingPresenter addListingPresenter;
    private boolean forSaleFlag;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_listing_fragment, container, false);
        PhotoPath=new ArrayList<>();
        addPhotoAdapter=new AddPhotoAdapter(this.getContext(),PhotoPath);
        header=/*this.getActivity().getLayoutInflater()*/inflater.inflate(R.layout.new_listing_header,dragSortListView,false);
        footer=/*this.getActivity().getLayoutInflater()*/inflater.inflate(R.layout.new_listing_footer, dragSortListView, false);
        ( (DragSortListView)view.findViewById(R.id.addPhoto)).addHeaderView(header);
        ( (DragSortListView)view.findViewById(R.id.addPhoto)).addFooterView(footer);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        this.setupUI();

    }
    private void setupUI() {
        dragSortListView.setDragEnabled(true);
        dragSortListView.setAdapter(addPhotoAdapter);

        dragSortListView.setRemoveListener(new DragSortListView.RemoveListener() {
            @Override
            public void remove(int i) {
                PhotoPath.remove(i);
                addPhotoAdapter.notifyDataSetChanged();
            }
        });

        dragSortListView.setDropListener(new DragSortListView.DropListener() {
            @Override
            public void drop(int from, int to) {
                if (from != to) {
                    Collections.swap(PhotoPath, from, to);
                    addPhotoAdapter.notifyDataSetChanged();
                }
            }
        });
        Log.d(TAG, "Lon" + String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)) + "Lat" + String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));
    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).addListingComponent(new AddListingModule()).inject(this);
        this.addListingPresenter.setView(this);
    }

    @Override public void onResume() {
        super.onResume();
        this.addListingPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        this.addListingPresenter.pause();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        this.addListingPresenter.destroy();
    }
    @OnClick(R.id.for_sale_image)
    public void onForSaleClick(){
        if (forSaleFlag) {
            forSaleFlag=false;
            for_sale_image.setImageResource(R.drawable.toggle_on);
        } else {
            forSaleFlag=true;
            for_sale_image.setImageResource(R.drawable.toggle_off);
        }
    }
    @OnItemClick(R.id.addPhoto)
    public void onDragSortClick(int position){
        showPictureialog(position - 1);
    }
    @OnClick(R.id.photo_layout)
    public void onAddItemClick(){
        showPictureialog(addPhotoAdapter.getCount());
    }

    @OnClick(R.id.add_listing_to_the_db_txt)
    public void onAddListingClick(){
        //Check all ediitext are empty or not
        float acresNumber = -1.0f;
        float priceNumber = -1.0f;
        Integer yearBuiltNumber = -1;


        String half_bath_min=halfBathTextView.getText().toString();
        String half_bath_max=halfBathTextView.getText().toString();
        String fireplaces_min=firePlaceTextView.getText().toString();
        String fireplaces_max=firePlaceTextView.getText().toString();
        String bedrooms_min=bedRoomTextView.getText().toString();
        String bedrooms_max=bedRoomTextView.getText().toString();
        String bonus_room_min=bonusroom_value.getText().toString();
        String bonus_room_max=bonusroom_value.getText().toString();
        String full_bath_min=fullBathTextView.getText().toString();
        String full_bath_max=fullBathTextView.getText().toString();


        int min_half_bath =0,max_half_bath =0,min_firplaces =0, max_firepalces=0,min_bedrooms =0,
                max_bedrooms =0,min_bouns =0,max_bouns =0,min_full_bath =0,max_full_bath =0;

        try{
            min_half_bath = Integer.parseInt( half_bath_min);
        }catch (Exception e){
            min_half_bath = 0;
        }
        try{
            max_half_bath = Integer.parseInt( half_bath_max);
        }catch (Exception e){
            max_half_bath = 0;
        }
        try{
            min_firplaces = Integer.parseInt( fireplaces_min);
        }catch (Exception e){
            min_firplaces = 0;
        }
        try{
            max_firepalces = Integer.parseInt( fireplaces_max);
        }catch (Exception e){
            max_firepalces = 0;
        }
        try{
            min_bedrooms = Integer.parseInt( bedrooms_min);
        }catch (Exception e){
            min_bedrooms= 0;
        }
        try{
            max_bedrooms = Integer.parseInt( bedrooms_max);
        }catch (Exception e){
            max_bedrooms= 0;
        }
        try{
            min_bouns = Integer.parseInt( bonus_room_min);
        }catch (Exception e){
            min_bouns= 0;
        }
        try{
            max_bouns = Integer.parseInt( bonus_room_max);
        }catch (Exception e){
            max_bouns= 0;
        }
        try{
            min_full_bath = Integer.parseInt( full_bath_min);
        }catch (Exception e){
            min_full_bath= 0;
        }
        try{
            max_full_bath = Integer.parseInt( full_bath_max);
        }catch (Exception e){
            max_full_bath= 0;
        }



        String zip = zipcodeEditText.getText().toString();
        String remark = remarkEditText.getText().toString();
        String acres = acresEditText.getText().toString();
        String price = priceEditText.getText().toString();
        String yearBuilt = yearBuiltEditText.getText().toString();
        String firePlace = firePlaceTextView.getText().toString();
        String fullBath = fullBathTextView.getText().toString();
        String halfBath = halfBathTextView.getText().toString();
        String bedRoom = bedRoomTextView.getText().toString();
        if (zip.matches(""))
            super.showToastMessage("Zip can't be blank.");
        else if (remark.matches(""))
            super.showToastMessage("Remark can't be blank.");
        else if (acres.matches(""))
            super.showToastMessage("Acres can't be blank.");
        else if (price.matches(""))
            super.showToastMessage("Price can't be blank.");
        else if (yearBuilt.matches(""))
            super.showToastMessage("Year Built can't be blank.");
        else {
            try {
                acresNumber = Float.parseFloat(acres);
            } catch (Exception e) {
                super.showToastMessage("Acres is not a number.");
                return;
            }
            try {
                yearBuiltNumber = Integer.parseInt(yearBuilt);
                if (!(yearBuiltNumber <= Calendar.getInstance().get(Calendar.YEAR))) {
                    super.showToastMessage("Year Built should be current and previous year.");
                    return;
                }
            } catch (Exception e) {
                super.showToastMessage("Year Built is not a number.");
                return;
            }

            try {
                priceNumber = Float.parseFloat(price);
            } catch (Exception e) {
                super.showToastMessage("Price is not a number.");
                return;
            }
            WeakHashMap<String, String> listingData = new WeakHashMap<>(11);
            //Require fields
           // listingData.put("listing[user_id]", sharedPreferences.getString("id", "null"));
            listingData.put(constants.LISTING_ZIP, zip);
            listingData.put(constants.LISTING_REMARKS, remark);
            listingData.put(constants.LISTING_HALFBATH, halfBath);
            listingData.put(constants.LISTING_FIREPLACES, firePlace);
            listingData.put(constants.LISTING_PRICENUMBER, "" + priceNumber);
            listingData.put(constants.LISTING_BEDROOMS, bedRoom);
            listingData.put(constants.LISTING_ACERS, "" + acresNumber);
            listingData.put(constants.LISTING_YEAR_BUILT, "" + yearBuiltNumber);
            listingData.put(constants.LISTING_FULL_BATH, fullBath);
            listingData.put(constants.LISTING_LON, String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)));
            listingData.put(constants.LISTING_LAT, String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));

            listingData.put(constants.LISTING_PHOTO,"");
            listingData.put(constants.LISTING_LAST_SOLD_PRICE,last_sold_price_edittext.getText().toString());
            listingData.put(constants.LISTING_RECOREDED_MORTAGE,recorded_mortgage_edittext.getText().toString());
            listingData.put(constants.LISTING_ASSESED_VALUE,assesed_value_edittext.getText().toString());
            listingData.put(constants.LISTING_CUREENT_TAXES,currentTaxes.getText().toString());
            listingData.put(constants.LISTING_HOME_OWNER_FEES, homeOwnerFees.getText().toString());
            listingData.put(constants.LISTING_HOME_OWNER_TOTAL_FEES, homeownerass.getText().toString());
            listingData.put(constants.LISTING_OTHER_FEES, otherfees1.getText().toString());
            listingData.put(constants.LISTING_PARKING, parkingnon.getText().toString());
            listingData.put(constants.LISTING_ESTIMATED_SQUARE_FEET,  estimatedsquare.getText().toString());
            listingData.put(constants.LISTING_LOT_SQFT, lotsqft.getText().toString());
            listingData.put(constants.LISTING_GARAGE, garagespaces.getText().toString());
            listingData.put(constants.LISTING_PARKING_SAPCE, parkingspaces.getText().toString());
            listingData.put(constants.LISTING_MONTHLY_TOTAL_FEES, monthlytotal.getText().toString());
            listingData.put(constants.LISTING_CONDITION_RATING, "");
            listingData.put(constants.LISTING_OPTIONAL_BEDROOMS, "");
           // listingData.put("listing[listing_class]", "");
           // listingData.put("listing[listing_class_type]", "");
            listingData.put("listing[thumbnails]","");
            listingData.put("listing[optional_bedrooms]","");
            listingData.put("filter[min_half_bath]",String.valueOf(min_half_bath));
            listingData.put("filter[max_half_bath]",String.valueOf(max_half_bath));
            listingData.put("filter[min_firplaces]",String.valueOf(min_firplaces));
            listingData.put("filter[max_firplaces]",String.valueOf(max_firepalces));
            listingData.put("filter[min_bedrooms]",String.valueOf(min_bedrooms));
            listingData.put("filter[max_bedrooms]",String.valueOf(max_bedrooms));
            listingData.put("filter[min_bouns]",String.valueOf(min_bouns));
            listingData.put("filter[max_bouns]",String.valueOf(max_bouns));
            listingData.put("filter[min_full_bath]",String.valueOf(min_full_bath));
            listingData.put("filter[max_full_bath]",String.valueOf(max_full_bath));
            this.addListingPresenter.initialize(listingData);
        }

    }
    @OnClick(R.id.listing_class_type_layout)
    public void onListingClassTypeClick(){
     // this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingClassTypeFragment.newInstance());
    }
    @OnClick(R.id.property_type_layout)
    public void onPropertyClick(){
       // this.replaceListener.CallReplaceFragment(R.id.conatainer_layout, PropertyTypeFragment.newInstance());
    }
    @OnClick(R.id.half_bath_minus)
    public void setHalf_bath_minus(){
        if(halfBathTextView.getText().toString().isEmpty()||halfBathTextView.getText().toString().equalsIgnoreCase("0")){
            halfBathTextView.setText("0");
        }
        else {
            halfBathTextView.setText(String.valueOf(Integer.parseInt(halfBathTextView.getText().toString()) - 1));
        }

       }
    @OnClick(R.id.half_bath_plus)
    public void setHalf_bath_plus(){
        if(halfBathTextView.getText().toString().isEmpty()||halfBathTextView.getText().toString().equalsIgnoreCase("0")){
            halfBathTextView.setText("1");
        }else {
            halfBathTextView.setText( String.valueOf(Integer.parseInt(halfBathTextView.getText().toString()) + 1));
        }
        Log.d(TAG, halfBathTextView.getText().toString());
    }
    @OnClick(R.id.fire_place_minus)
    public void setFire_place_minus(){
        if( firePlaceTextView.getText().toString().isEmpty()|| firePlaceTextView.getText().toString().equalsIgnoreCase("0")){
            firePlaceTextView.setText("0");
        }
        else {
            firePlaceTextView.setText(String.valueOf(Integer.parseInt( firePlaceTextView.getText().toString()) - 1));
        }

    }
    @OnClick(R.id.fire_place_plus)
    public void setFire_place_plus(){
        if( firePlaceTextView.getText().toString().isEmpty()|| firePlaceTextView.getText().toString().equalsIgnoreCase("0")){
            firePlaceTextView.setText("1");
        }
        else {
            firePlaceTextView.setText(String.valueOf(Integer.parseInt( firePlaceTextView.getText().toString()) + 1));
        }

    }
    @OnClick(R.id.bedroom_minus)
    public void setBedroom_minus(){
        if( bedRoomTextView.getText().toString().isEmpty()|| bedRoomTextView.getText().toString().equalsIgnoreCase("0")){
            bedRoomTextView.setText("0");
        }
        else {
            bedRoomTextView.setText(String.valueOf(Integer.parseInt( bedRoomTextView.getText().toString()) - 1));
        }

    }
    @OnClick(R.id.bedroom_plus)
    public void setBedroom_plus(){
        if( bedRoomTextView.getText().toString().isEmpty()||bedRoomTextView.getText().toString().equalsIgnoreCase("0")){
            bedRoomTextView.setText("1");
        }
        else {
            bedRoomTextView.setText(String.valueOf(Integer.parseInt( bedRoomTextView.getText().toString()) + 1));
        }

    }
    @OnClick(R.id.bonusroom_minus)
    public void setBonusroom_minus(){
        if( bonusroom_value.getText().toString().isEmpty()||bonusroom_value.getText().toString().equalsIgnoreCase("0")){
            bonusroom_value.setText("0");
        }
        else {
            bonusroom_value.setText(String.valueOf(Integer.parseInt( bonusroom_value.getText().toString()) - 1));
        }

    }
    @OnClick(R.id.bonusroom_plus)
    public void setBonusroom_plus(){
        if( bonusroom_value.getText().toString().isEmpty()||bonusroom_value.getText().toString().equalsIgnoreCase("0")){
            bonusroom_value.setText("1");
        }
        else {
            bonusroom_value.setText(String.valueOf(Integer.parseInt( bonusroom_value.getText().toString()) + 1));
        }

    }
    @OnClick(R.id.full_bath_minus)
    public void setFull_bath_minus(){
        if( fullBathTextView.getText().toString().isEmpty()||fullBathTextView.getText().toString().equalsIgnoreCase("0")){
            fullBathTextView.setText("0");
        }
        else {
            fullBathTextView.setText(String.valueOf(Integer.parseInt( fullBathTextView.getText().toString()) - 1));
        }

    }
    @OnClick(R.id.full_bath_plus)
    public void setFull_bath_plus(){
        if( fullBathTextView.getText().toString().isEmpty()||fullBathTextView.getText().toString().equalsIgnoreCase("0")){
            fullBathTextView.setText("1");
        }
        else {
            fullBathTextView.setText(String.valueOf(Integer.parseInt( fullBathTextView.getText().toString()) + 1));
        }

    }


    //dialog to select the Picture
    private void showPictureialog(final int  pos) {
        final Dialog dialog = new Dialog(getActivity());
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setTitle(null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photo_dailog);

        dialog.setCancelable(true);
        dialog.show();

        TextView cancel = (TextView)dialog.findViewById(R.id.cancel);
        TextView take_photo = (TextView)dialog.findViewById(R.id.take_photo);
        TextView photo_gallery = (TextView)dialog.findViewById(R.id.photo_gallery);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //create a new file
                File  file = new File(Environment.getExternalStorageDirectory(),"_"+System.currentTimeMillis()+".jpg");
                 file_uri= Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
                position = pos;
                startActivityForResult(cameraIntent, Constants.PICK_IMAGE_REQUEST_CAMERA);
                dialog.cancel();
            }
        });

        photo_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get image from library
                Intent galleryintent = new Intent();
                // Show only images, no videos or anything else
                galleryintent.setType("image/*");
                galleryintent.setAction(Intent.ACTION_GET_CONTENT);
                position = pos;
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(galleryintent, "Choose  Picture"), Constants.PICK_IMAGE_REQUEST_LIB);
                dialog.cancel();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PICK_IMAGE_REQUEST_LIB && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                String file_path= photoProcessing.getRealPathFromURI(uri,AddListing.this.getContext());
                onImageSuccess(position, file_path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(requestCode == Constants.PICK_IMAGE_REQUEST_CAMERA && resultCode == Activity.RESULT_OK ){
            try {
                onImageSuccess(position,
                        photoProcessing.compressImage(file_uri.getPath()));
                // delete the temp file from storage
                File fileTodelete=new File(file_uri.getPath());
                if(fileTodelete.exists()){
                    fileTodelete.delete();
                    Log.i("File", "deleted");
                }
                // null the value of file uri
                file_uri = null;
                position = -1;
            } catch (Exception e) {
                e.printStackTrace();
                file_uri = null;
                position = -1;
            }
        }
        else{
            file_uri = null;
            position = -1;
        }
    }
    public void onImageSuccess(int position ,String filepath){
        Log.d(TAG,filepath);
        PhotoPath.add(position, filepath);
        addPhotoAdapter.notifyDataSetChanged();
    }



    @Override
    public void showError(String message) {
    showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void viewAddListing(AddListingModel addListingModel) {
        Log.d(TAG, "The Added Listing  " + addListingModel.getId());
        //Initialize the Photo  Upload From Here
        for (String photoUrl :PhotoPath) {
            WeakHashMap<String,String> photo=new WeakHashMap<>(1);
            photo.put("photo",photoUrl);
            this.addListingPresenter.initialize(String.valueOf(addListingModel.getId()),photo,null);
        }
        super.showToastMessage("Your listing has been added to our database successfully.");
        
    }

    @Override
    public void viewAddListingPhoto(AddListingPhotoModel addListingPhotoModel) {
        Log.d(TAG, "The Photo Url" + addListingPhotoModel.getUrl());

    }
}


