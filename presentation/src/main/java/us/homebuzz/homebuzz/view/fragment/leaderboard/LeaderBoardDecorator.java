package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.WeakHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.BaseDecorator;

/**
 * Created by amit.singh on 10/20/2015.
 */
public abstract class LeaderBoardDecorator extends BaseDecorator implements LeaderBoardFilterFragment.LeaderBoardFilter {
    public LeaderBoardDecorator() {
    }

    @Bind(R.id.container_layout_tab)
    FrameLayout container_layout_tab;
    @Bind(R.id.tab_view)
    LinearLayout tab_fragment;



    @Override
    public void onLeftIconClick(Fragment leftFragment) {
        super.replaceFragment(R.id.container_layout_tab, leftFragment);
        //text_accuracy.setTextColor(getActivity().getColor(R.color.color_yellow));
        getActivity().getActionBar().setTitle("Leaderboard");

    }

    @Override
    public void onRightIconClick(Fragment rightFragment) {
        super.replaceFragment(R.id.container_layout_tab, rightFragment);
        //  text_accuracy.setTextColor(getActivity().getColor(R.color.color_white));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View base_Leader = inflater.inflate(R.layout.base_search, container, false);
        ButterKnife.bind(this, base_Leader);
        return base_Leader;

    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.leaderbord_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_filter:
             onFilterClick(LeaderBoardFilterFragment.newInstance());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onFilterClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());
    }

    protected abstract void OnFilterUpdate(WeakHashMap<String, String> filterData);

    @Override
    public void onUpdateClick(WeakHashMap<String, String> filterData) {
        this.OnFilterUpdate(filterData);
    }
}
