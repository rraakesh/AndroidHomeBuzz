package us.homebuzz.homebuzz.view.fragment.Notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.NearbyModule;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.adapter.notification.JustSoldAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy.ListingFragment;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;
import us.homebuzz.homebuzz.view.profile.friendsprofile.InviteFriendsFragment;

/**
 * Created by amit.singh on 11/19/2015.
 */
public class JustSoldListingFragment extends BaseFragment {

    private static final String ARGUMENT_KEY_USER_JUST_SOLD_MODEL = "UserJustSoldModel";


    public static JustSoldListingFragment newInstance(ArrayList<UserJustSoldModel> userJustSoldModel) {
        JustSoldListingFragment notificationFragment = new JustSoldListingFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL, userJustSoldModel);
        notificationFragment.setArguments(argumentsBundle);
        return notificationFragment;
    }

    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Inject
    Picasso picasso;

    private JustSoldAdapter justSoldAdapter;
    private UsersLayoutManager usersLayoutManager;
    private ArrayList<UserJustSoldModel> userJustListedModelArrayList;
    private UserNearbyModel userNearbyModel = new UserNearbyModel();;

    //ArrayList<UserJustSoldModel> mdata = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_base = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, list_base);
        if (this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL) != null)
            userJustListedModelArrayList = (ArrayList) this.getArguments().getParcelableArrayList(ARGUMENT_KEY_USER_JUST_SOLD_MODEL);
        return list_base;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        this.initialize();
        this.setupUI();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).nearbyComponent(new NearbyModule()).inject(this);

    }

    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.justSoldAdapter = new JustSoldAdapter(getActivity(), new ArrayList<UserJustSoldModel>(), picasso);
        this.justSoldAdapter.setOnItemClickListener(setOnItemClickListener);
        this.base_list.setAdapter(justSoldAdapter);
         if (userJustListedModelArrayList != null && userJustListedModelArrayList.size() > 0)
            this.justSoldAdapter.setUsersCollection(userJustListedModelArrayList);
    }

    private JustSoldAdapter.OnItemClickListener setOnItemClickListener = new JustSoldAdapter.OnItemClickListener() {
        @Override
        public void JustSoldItemClick(UserJustSoldModel userJustListedModel) {
            if (userJustListedModel != null) {
                userNearbyModel.setPrice(Double.valueOf(userJustListedModel.getPrice()));
                userNearbyModel.setBedrooms(userJustListedModel.getBedrooms());
                userNearbyModel.setRemarks(userJustListedModel.getRemarks());
                userNearbyModel.setBaths(userJustListedModel.getBaths());
                userNearbyModel.setStreet(userJustListedModel.getStreet() != null ? userJustListedModel.getStreet() : "");
                userNearbyModel.setCity(userJustListedModel.getCity() != null ? userJustListedModel.getCity() : "");
                userNearbyModel.setState(userJustListedModel.getState() != null ? userJustListedModel.getState() : "");
                userNearbyModel.setZip(userJustListedModel.getZip() != null ? userJustListedModel.getZip() : "");
                userNearbyModel.setYearBuilt(userJustListedModel.getYearBuilt() != null ? userJustListedModel.getYearBuilt() : 0);
                userNearbyModel.setAcres(Double.valueOf(userJustListedModel.getAcres()) != null ? Double.valueOf(userJustListedModel.getAcres()) : 0.0);
                userNearbyModel.setEstimatedSquareFeet(Double.valueOf(userJustListedModel.getEstimatedSquareFeet()) != null ? Double.valueOf(userJustListedModel.getEstimatedSquareFeet()) : 0.0);
                userNearbyModel.setLotSqftApprox( 0.0);
                userNearbyModel.setLastSoldPrice(0.0);
                userNearbyModel.setFireplaces( 0);
                userNearbyModel.setGarage(0.0);
                userNearbyModel.setLat(userJustListedModel.getLat());
                userNearbyModel.setLon(userJustListedModel.getLon());
                userNearbyModel.setFireplaces(userJustListedModel.getFireplaces());
                userNearbyModel.setOptionalBedrooms(userJustListedModel.getOptionalBedrooms());
                replaceListener.CallReplaceFragment(R.id.conatainer_layout, ListingFragment.newInstance(userNearbyModel, true));
            }
        }
    };

    @Override
    public void onDestroyView() {
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }

}
