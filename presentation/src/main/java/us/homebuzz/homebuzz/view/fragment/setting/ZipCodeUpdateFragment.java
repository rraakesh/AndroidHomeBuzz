package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.UpdateZipcodeModel;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;
import us.homebuzz.homebuzz.presenter.ZipCodeUpdatePresenter;
import us.homebuzz.homebuzz.view.ZipUpdateView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class ZipCodeUpdateFragment extends BaseFragment implements ZipUpdateView {
    private static final String TAG = ZipCodeUpdateFragment.class.getSimpleName();
    private static final String ARGUMENT_KEY_USER_UPDATE_ZIPCODE = "zip_update";

    public static ZipCodeUpdateFragment newInstance(ZipPurchaseModel zipPurchaseModel) {
        ZipCodeUpdateFragment zipCodeUpdate = new ZipCodeUpdateFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_UPDATE_ZIPCODE, zipPurchaseModel);
        zipCodeUpdate.setArguments(argumentsBundle);
        return zipCodeUpdate;
    }

    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.edZipCode)
    EditText edZipCode;
    @Bind(R.id.add_button)
    Button add_button;
    @Bind(R.id.txtZip)
    TextView txtZip;
    ZipPurchaseModel zipPurchaseModel;
    @Inject
    Constants constants;
    @Inject
    ZipCodeUpdatePresenter zipCodeUpdatePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View zip_update = inflater.inflate(R.layout.zip_code_add, container, false);
        ButterKnife.bind(this, zip_update);
        zipPurchaseModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_UPDATE_ZIPCODE);
        return zip_update;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Edit Zip");
        this.initialize();
        this.setUI();
    }

    private void setUI() {
        add_button.setText("Update");
        txtZip.setText("Zip Code");
        if (zipPurchaseModel.getZipCode() != null)
            edZipCode.setText(zipPurchaseModel.getZipCode());
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.zipCodeUpdatePresenter.setView(this);

    }

    @OnClick(R.id.add_button)
    public void OnButtonClick() {
        hideKeyboard();
        if (edZipCode.getText().toString().isEmpty()) {
            showToastMessage("Please enter the zip code");
        } else {
            WeakHashMap<String, String> data = new WeakHashMap<>(2);
            data.put(constants.UPDATE_ZIP, edZipCode.getText().toString());
            data.put(constants.SEARCH_ID, zipPurchaseModel.getId().toString());
            this.zipCodeUpdatePresenter.initialize(data);
        }
    }

    @Override
    public void renderAfterUpdateZip(UpdateZipcodeModel updateZipcodeModel) {
        showToastMessage("ZipCode Updated Successfully");
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.zipCodeUpdatePresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.zipCodeUpdatePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.zipCodeUpdatePresenter.destroy();
    }

    @Override
    public void showError(String message) {
        super.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}