package us.homebuzz.homebuzz.view.fragment.addlisting._Nov;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobeta.android.dslv.DragSortListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.PhotoProcessing;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;
import us.homebuzz.homebuzz.presenter.AddListingPresenter;
import us.homebuzz.homebuzz.view.AddListingView;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireCamera;
import us.homebuzz.homebuzz.view.RequireStorage;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.adapter.addnewlisting.AddPhotoAdapter;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.CheckInNearby;

/**
 * Created by amit.singh on 11/28/2015.
 */
public class AddListingStep2 extends BaseFragment implements AddListingView,PermissionBus{
    private static final String TAG = AddListingStep2.class.getSimpleName();
    private static final String ADD_LISTING_DATA = "AddListing";
    public static AddListingStep2 newInstance(AddListingData addListingData) {
        AddListingStep2 addListing = new AddListingStep2();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ADD_LISTING_DATA, addListingData);
        addListing.setArguments(argumentsBundle);
        return addListing;
    }
    AddListingData addListingData;
    @Bind(R.id.home_owner_fee)
    EditText home_owner_fee;
    @Bind(R.id.garage_spaces_edt)
    EditText garage_spaces_edt;
    @Bind(R.id.parking_not_edt)
    EditText parking_not_edt;
    @Bind(R.id.real_estate_image)
    ImageView real_estate_image;
    @Inject
    Constants constants;
    @Inject
    AddListingPresenter addListingPresenter;
    boolean requestEstiomatToggle;
    List<String> PhotoPath;
    AddPhotoAdapter addPhotoAdapter;
    private Uri file_uri;
    private int position;

    DragSortListView dragSortListView;
    @Inject
    PhotoProcessing photoProcessing;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    private RequireStorage requireStorage;
    private RequireCamera requireCamera;
    private boolean cameraPermission, storagePermission;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_listing_add_1, container, false);
        if (this.getArguments().getParcelable(ADD_LISTING_DATA) != null) {
            this.addListingData = this.getArguments().getParcelable(ADD_LISTING_DATA);
            Log.d(TAG, "addListingData Address" + addListingData.getAddress());
        }
        PhotoPath=new ArrayList<>();
        requestEstiomatToggle=true;
        addListingData.setRequestEstimate(requestEstiomatToggle);

        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.add__listing_btn)
    public void onAdd__listing_btnlick(){
        addListingData.setParkngNonGaragedSpaces(parking_not_edt.getText().toString());
        addListingData.setParkingSpacesTotal(garage_spaces_edt.getText().toString());
        addListingData.setHomeOwnerFees(home_owner_fee.getText().toString());

        WeakHashMap<String, String> listingData = new WeakHashMap<>(14);
        listingData.put(constants.LISTING_ZIP, addListingData.getZip());
        listingData.put(constants.LISTING_REMARKS, addListingData.getRemarks());
        listingData.put(constants.LISTING_HALFBATH, String.valueOf(addListingData.getBathshalf()));
        listingData.put(constants.LISTING_FIREPLACES, String.valueOf(addListingData.getFireplaces()));
        listingData.put(constants.LISTING_PRICENUMBER, "" + addListingData.getPrice());
        listingData.put(constants.LISTING_BEDROOMS, String.valueOf(addListingData.getBedrooms()));
        listingData.put(constants.LISTING_ACERS, "" + addListingData.getAcres());
        listingData.put(constants.LISTING_YEAR_BUILT, "" + addListingData.getYearBuilt());
        listingData.put(constants.LISTING_FULL_BATH, addListingData.getBathsfull());
        listingData.put(constants.LISTING_LON, String.valueOf(addListingData.getLon()));
        listingData.put(constants.LISTING_LAT, String.valueOf(addListingData.getLat()));
        listingData.put(constants.LISTING_ADDRESS, addListingData.getAddress());
        listingData.put(constants.LISTING_PRICELNUMBER, "" + addListingData.getParcelNum());
        //TODO REMOVE THE HARD CODING
        listingData.put(constants.LISTING_STATUS, "active");

        listingData.put(constants.LISTING_PARKING_SAPCE, addListingData.getParkngNonGaragedSpaces());
        listingData.put(constants.LISTING_GARAGE, addListingData.getParkingSpacesTotal());
        listingData.put(constants.LISTING_HOME_OWNER_FEES, addListingData.getHomeOwnerFees());
        listingData.put(constants.KEY_LISTING_CLASS_TYPE, addListingData.getListingClass());
        listingData.put(constants.KEY_LISTING_CLASS, addListingData.getListingClassType());
        listingData.put(constants.LISTING_FOR_SALE, (addListingData.getForSale())?"1":"0");
        listingData.put(constants.LISTING_REQUEST_ESTIMATE, (addListingData.getRequestEstimate())?"1":"0");
        this.addListingPresenter.initialize(listingData);
      //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, CheckInNearby.newInstance(36.1215,115.1739));
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).addListingComponent(new AddListingModule()).inject(this);
        this.addListingPresenter.setView(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add New Listing");
        this.initialize();
        if ((MainActivity) this.getActivity() instanceof RequireStorage)
            requireStorage = (RequireStorage) this.getActivity();
        if ((MainActivity) this.getActivity() instanceof RequireCamera)
            requireCamera = (RequireCamera) this.getActivity();


    }
    @Override
    public void onResume() {
        super.onResume();
        this.addListingPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.addListingPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.addListingPresenter.destroy();
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add New Listing");
    }
    @OnClick(R.id.btn_add_image_btn)
    public void addPhotoClick() {
        PhotoPath.clear();
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (requireStorage != null && requireCamera != null) {
                requireStorage.onStorageRequest();
                requireCamera.onCameraRequest();
            }
        } else {
            // Pre-Marshmallow
            StartPhotoDialog();
        }


    }
    private void StartPhotoDialog() {
        final Dialog d=new Dialog(this.getActivity(),android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        d.getWindow().setGravity(Gravity.CENTER_HORIZONTAL );
        //d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams params = d.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        d.setContentView(R.layout.new_listing_fragment);
        //d.setTitle("");
        dragSortListView=(DragSortListView)d.findViewById(R.id.addPhoto);
        addPhotoAdapter=new AddPhotoAdapter(this.getContext(),PhotoPath);
        View  footer=this.getActivity().getLayoutInflater().inflate(R.layout.newaddlisting_footer, dragSortListView, false);
        ( (DragSortListView)d.findViewById(R.id.addPhoto)).addFooterView(footer);
        RelativeLayout photo_layout=(RelativeLayout) d.findViewById(R.id.photo_layout);
        photo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureialog(addPhotoAdapter.getCount());
            }
        });
        TextView addPhotText=(TextView)d.findViewById(R.id.addPhotText);
        TextView Cancel=(TextView)d.findViewById(R.id.Cancel);

        final EditText edRemarks = (EditText) d.findViewById(R.id.edRemarks);
        addPhotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                d.dismiss();
            }
        });
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoPath.clear();
                d.dismiss();
            }
        });
        d.show();
        setupUI();

    }


    private void setupUI() {

        dragSortListView.setDragEnabled(true);
        dragSortListView.setAdapter(addPhotoAdapter);
        dragSortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showPictureialog(position - 1);
            }
        });

        dragSortListView.setRemoveListener(new DragSortListView.RemoveListener() {
            @Override
            public void remove(int i) {
                PhotoPath.remove(i);
                addPhotoAdapter.notifyDataSetChanged();
            }
        });

        dragSortListView.setDropListener(new DragSortListView.DropListener() {
            @Override
            public void drop(int from, int to) {
                if (from != to) {
                    Collections.swap(PhotoPath, from, to);
                    addPhotoAdapter.notifyDataSetChanged();
                }
            }
        });
      //  Log.d(TAG, "Lon" + String.valueOf(sharedPreferences.getFloat(constants._LON, 0.0f)) + "Lat" + String.valueOf(sharedPreferences.getFloat(constants._LAT, 0.0f)));
    }
    private void permisssionStatus() {
        if(storagePermission==true && cameraPermission==true)
            StartPhotoDialog();
    }
    @OnClick(R.id.real_estate_image)
    public void imageClick() {
        if (requestEstiomatToggle) {
            real_estate_image.setImageResource(R.drawable.toggle_off);
            requestEstiomatToggle=false;
        } else {
            real_estate_image.setImageResource(R.drawable.toggle_on);
            requestEstiomatToggle=true;
        }
        addListingData.setRequestEstimate(requestEstiomatToggle);
    }
    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        rl_progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void viewAddListingPhoto(AddListingPhotoModel addListingPhotoModel) {
        Log.d(TAG, "The Photo Url" + addListingPhotoModel.getUrl());

    }
    @Override
    public void viewAddListing(AddListingModel addListingModel) {
        Log.d(TAG, "The Added Listing  " + addListingModel.getId());
       /* if(addListingModel.getId()!=null && addListingModel.getId()!=0 ) {*/
            for (String photoUrl : PhotoPath) {
                WeakHashMap<String, String> photo = new WeakHashMap<>(1);
                photo.put("photo", photoUrl);
                this.addListingPresenter.initialize(String.valueOf(addListingModel.getId()), photo, null);
            }
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, CheckInNearby.newInstance(addListingModel.getLat(),addListingModel.getLon()));
            super.showToastMessage("Your listing has been added to our database successfully.");
        Log.d(TAG, " Listing LON "+addListingModel.getLat()+"LON"+addListingModel.getLon());

        Log.d(TAG, " Listing LON "+addListingModel.getLat()+"LON"+addListingModel.getLon());

        /*}else{
            super.showToastMessage("There Was an Error On Adding The Home!!.");
        }
*/
    }
    //dialog to select the Picture
    private void showPictureialog(final int  pos) {
        final Dialog dialog = new Dialog(getActivity());
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setTitle(null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photo_dailog);

        dialog.setCancelable(true);
        dialog.show();

        TextView cancel = (TextView)dialog.findViewById(R.id.cancel);
        TextView take_photo = (TextView)dialog.findViewById(R.id.take_photo);
        TextView photo_gallery = (TextView)dialog.findViewById(R.id.photo_gallery);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //create a new file
                File file = new File(Environment.getExternalStorageDirectory(),"_"+System.currentTimeMillis()+".jpg");
                file_uri= Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
                position = pos;
                startActivityForResult(cameraIntent, Constants.PICK_IMAGE_REQUEST_CAMERA);
                dialog.cancel();
            }
        });

        photo_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get image from library
                Intent galleryintent = new Intent();
                // Show only images, no videos or anything else
                galleryintent.setType("image/*");
                galleryintent.setAction(Intent.ACTION_GET_CONTENT);
                position = pos;
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(galleryintent, "Choose  Picture"), Constants.PICK_IMAGE_REQUEST_LIB);
                dialog.cancel();
            }
        });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PICK_IMAGE_REQUEST_LIB && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                String file_path= photoProcessing.getRealPathFromURI(uri,AddListingStep2.this.getContext());
                onImageSuccess(position, file_path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(requestCode == Constants.PICK_IMAGE_REQUEST_CAMERA && resultCode == Activity.RESULT_OK ){
            try {
                onImageSuccess(position,
                        photoProcessing.compressImage(file_uri.getPath()));
                // delete the temp file from storage
                File fileTodelete=new File(file_uri.getPath());
                if(fileTodelete.exists()){
                    fileTodelete.delete();
                    Log.i("File", "deleted");
                }
                // null the value of file uri
                file_uri = null;
                position = -1;
            } catch (Exception e) {
                e.printStackTrace();
                file_uri = null;
                position = -1;
            }
        }
        else{
            file_uri = null;
            position = -1;
        }
    }
    public void onImageSuccess(int position ,String filepath){
        Log.d(TAG,filepath);
        PhotoPath.add(position, filepath);
        addPhotoAdapter.notifyDataSetChanged();
    }
    //TODO BETTER NEGATIVE FEEDBACK HANDLING
    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {
        cameraPermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {
        storagePermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {

    }
}
