package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.SubscriptionModel;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public interface SubcriptionPlanView   extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  subscriptionModel The user that will be shown.
     */
    void viewSubPlan(SubscriptionModel subscriptionModel);
}
