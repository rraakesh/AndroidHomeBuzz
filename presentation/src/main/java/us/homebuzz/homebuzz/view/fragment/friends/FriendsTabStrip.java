package us.homebuzz.homebuzz.view.fragment.friends;

import android.support.v4.app.Fragment;

import us.homebuzz.homebuzz.view.fragment.NearBy.LowerTabStrip;

/**
 * Created by amit.singh on 10/28/2015.
 */
public interface FriendsTabStrip extends LowerTabStrip {
    void onSecondIconClick(Fragment SecondFragment);
    void onThirdIconClick(Fragment ThirdFragment);
}
