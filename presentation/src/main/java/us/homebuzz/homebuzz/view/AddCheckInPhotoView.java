package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.AddCheckInPhotoModel;

/**
 * Created by abhishek.singh on 12/2/2015.
 */
public interface AddCheckInPhotoView extends LoadDataView {
    void viewAddCheckInPhoto(AddCheckInPhotoModel addCheckInPhotoModel);
}
