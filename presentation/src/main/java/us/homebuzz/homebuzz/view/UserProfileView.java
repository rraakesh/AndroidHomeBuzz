package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.UserProfileModel;

/**
 * Created by amit.singh on 10/29/2015.
 */
public interface UserProfileView extends LoadDataView {
    /**
     * View a {@link } profile/details.
     *
     * @param  userProfileModel The user that will be shown.
     */
    void viewUserProfile(UserProfileModel userProfileModel);

}
