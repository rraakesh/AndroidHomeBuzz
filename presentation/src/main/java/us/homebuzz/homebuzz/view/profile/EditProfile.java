package us.homebuzz.homebuzz.view.profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.Utility.PhotoProcessing;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.EditProfileModelValues;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.presenter.UserEditProfilePresenter;
import us.homebuzz.homebuzz.view.PermissionBus;
import us.homebuzz.homebuzz.view.RequireCamera;
import us.homebuzz.homebuzz.view.RequireStorage;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/13/2015.
 */
public class EditProfile extends BaseFragment implements PermissionBus {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";
    private static final String ARGUMENT_KEY_USER_PROFILE = "Profile";

    public static EditProfile newInstance(boolean isProfileExist, UserProfileModel userProfileModel) {
        EditProfile userEditProfile = new EditProfile();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE, userProfileModel);
        argumentsBundle.putBoolean(ARGUMENT_KEY_IS_USER_PROFILE, isProfileExist);
        userEditProfile.setArguments(argumentsBundle);
        return userEditProfile;
    }

    private String PhotoPath = "";
    private Uri file_uri;
    @Inject
    PhotoProcessing photoProcessing;
    private boolean isProfileExist;
    @Bind(R.id.edFirstName)

    EditText edFirstName;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.edLastName)
    EditText edLastName;
    @Bind(R.id.edEmail)
    EditText edEmail;
    @Bind(R.id.edPhone)
    EditText edPhone;
    @Bind(R.id.edState)
    EditText edState;
    @Bind(R.id.edCity)
    EditText edCity;
    @Bind(R.id.btnCamera)
    ImageView btnCamera;
    @Bind(R.id.edStreetAddress)
    EditText edStreetAddress;
    @Bind(R.id.edZipCode)
    EditText edZipCode;
    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    private UserProfileModel userProfileModel;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Inject
    UserEditProfilePresenter editProfilePresenter;
    @Inject
    Picasso picasso;
    EditProfileModelValues values; private RequireStorage requireStorage;
    private RequireCamera requireCamera;
    private boolean cameraPermission, storagePermission;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.edit_profile_new, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE) != null)
            userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE);
        isProfileExist = this.getArguments().getBoolean(ARGUMENT_KEY_IS_USER_PROFILE);
        //System.out.println("Boolean Profile =" + isProfileExist + "UserProfile " + userProfileModel.getId());
        values = new EditProfileModelValues();
        ButterKnife.bind(this, list_nearby);
        return list_nearby;
    }

    private void setUpUI() {
        if(userProfileModel!=null) {
            edEmail.setText((userProfileModel.getEmail() != null && !(userProfileModel.getEmail().matches("null")) ? userProfileModel.getEmail() : ""));
            txtProfileName.setText((userProfileModel.getName() != null && !(userProfileModel.getName().matches("null")) ? userProfileModel.getName() : ""));
            edFirstName.setText((userProfileModel.getFirstName() != null && !(userProfileModel.getFirstName().matches("null")) ? userProfileModel.getFirstName() : ""));
            edLastName.setText((userProfileModel.getLastName() != null && !(userProfileModel.getLastName().matches("null")) ? userProfileModel.getLastName() : ""));
            edPhone.setText((userProfileModel.getPhone() != null && !(userProfileModel.getPhone().matches("null")) ? userProfileModel.getPhone() : ""));
            edState.setText((userProfileModel.getState() != null && !(userProfileModel.getState().matches("null"))) ? userProfileModel.getState().toString() : "");
            edCity.setText(userProfileModel.getCity() != null && (!(userProfileModel.getCity().matches("null"))) ? userProfileModel.getCity().toString() : "");
            edStreetAddress.setText(userProfileModel.getStreet() != null && (!(userProfileModel.getStreet().matches("null"))) ? userProfileModel.getStreet().toString() : "");
            edZipCode.setText(userProfileModel.getZipCode() != null && (!(userProfileModel.getZipCode().matches("null"))) ? userProfileModel.getZipCode().toString() : "");
            if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().matches("null"))
                picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Edit Profile");


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setUpUI();
        if ((MainActivity) this.getActivity() instanceof RequireStorage)
            requireStorage = (RequireStorage) this.getActivity();
        if ((MainActivity) this.getActivity() instanceof RequireCamera)
            requireCamera = (RequireCamera) this.getActivity();
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    @OnClick(R.id.btnEditProfileContinue)
    public void onBtnContinueClick() {
        if (!edFirstName.getText().toString().isEmpty() && !edEmail.getText().toString().isEmpty() && !edLastName.getText().toString().isEmpty() && !edPhone.getText().toString().isEmpty() && !edZipCode.getText().toString().isEmpty()) {
            values.setProfileId((userProfileModel.getProfileId() != 0) ? String.valueOf(userProfileModel.getProfileId()) : "0");
            values.setFirstName(edFirstName.getText().toString());
            values.setEmail(edEmail.getText().toString());
            values.setLastName(edLastName.getText().toString());
            values.setCity(edCity.getText().toString());
            values.setState(edState.getText().toString());
            values.setZipCode(edZipCode.getText().toString());
            values.setPhone(edPhone.getText().toString());
            values.setStreet(edStreetAddress.getText().toString());
            values.setProfilePic(PhotoPath);
            replaceListener.CallReplaceFragment(R.id.conatainer_layout, EditSocialMediaProfile.newInstance(isProfileExist, userProfileModel, values));
        } else {
            showToastMessage("Please fill All the Fields!!");
        }
    }

    @OnClick(R.id.btnCamera)
    public void showPictureialog(){
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (requireStorage != null && requireCamera != null) {
                requireStorage.onStorageRequest();
                requireCamera.onCameraRequest();
            }
        } else {
            // Pre-Marshmallow
            StartPhotoDialog();
        }

    }

    private void StartPhotoDialog() {
        final Dialog dialog = new Dialog(getActivity());
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setTitle(null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photo_dailog);

        dialog.setCancelable(true);
        dialog.show();

        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        TextView take_photo = (TextView) dialog.findViewById(R.id.take_photo);
        TextView photo_gallery = (TextView) dialog.findViewById(R.id.photo_gallery);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //create a new file
                File file = new File(Environment.getExternalStorageDirectory(), "_" + System.currentTimeMillis() + ".jpg");
                file_uri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
                startActivityForResult(cameraIntent, Constants.PICK_IMAGE_REQUEST_CAMERA);
                dialog.cancel();
            }
        });

        photo_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get image from library
                Intent galleryintent = new Intent();
                // Show only images, no videos or anything else
                galleryintent.setType("image/*");
                galleryintent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(galleryintent, "Choose  Picture"), Constants.PICK_IMAGE_REQUEST_LIB);
                dialog.cancel();
            }
        });
    }

    private void permisssionStatus() {
        if(storagePermission==true && cameraPermission==true)
            StartPhotoDialog();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PICK_IMAGE_REQUEST_LIB && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                String file_path = photoProcessing.getRealPathFromURI(uri, EditProfile.this.getContext());
                onImageSuccess(file_path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.PICK_IMAGE_REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            try {
                onImageSuccess(photoProcessing.compressImage(file_uri.getPath()));
                // delete the temp file from storage
                File fileTodelete = new File(file_uri.getPath());
               /* if(fileTodelete.exists()){
                    fileTodelete.delete();
                    Log.i("File", "deleted");
                }*/
                // null the value of file uri
                file_uri = null;
            } catch (Exception e) {
                e.printStackTrace();
                file_uri = null;
            }
        } else {
            file_uri = null;
        }
    }

    public void onImageSuccess(String filepath) {
        Log.e(TAG, filepath);
        PhotoPath = filepath;
        values.setProfilePic(PhotoPath);
        picasso.load(new File(PhotoPath)).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);

    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("My Profile");
        super.onDestroyView();

    }
    //TODO BETTER NEGATIVE FEEDBACK HANDLING
    @Override
    public void onCameraPermissionSuccess(boolean isSuccess) {
        cameraPermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onStoragePermissionSuccess(boolean isSuccess) {
        storagePermission = isSuccess;
        permisssionStatus();
    }

    @Override
    public void onContactPermissionSuccess(boolean isSuccess) {

    }

    @Override
    public void onLocationPermissionSuccess(boolean isSuccess) {

    }
}
