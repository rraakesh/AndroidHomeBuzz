package us.homebuzz.homebuzz.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.SearchPeopleModel;
import us.homebuzz.homebuzz.view.adapter.friends.FriendFollowAdapter;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by abhishek.singh on 12/17/2015.
 */
public class SearchPeopleAdapter extends RecyclerView.Adapter<SearchPeopleAdapter.UserViewHolder> {

    public static String TAG = FriendFollowAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        public void onSearchClicked(SearchPeopleModel searchPeopleModel);
    }

    private List<SearchPeopleModel> userModelCollection;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;

    public SearchPeopleAdapter(Context context, List<SearchPeopleModel> searchPeopleModel, Picasso picasso) {
        this.validateUsersCollection(searchPeopleModel);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.userModelCollection = searchPeopleModel;
        this.picasso = picasso;
        Log.d(TAG, "ADAPTER PICASSO" + picasso);
    }

    @Override
    public int getItemCount() {
        return (this.userModelCollection != null) ? this.userModelCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.search_people_adapter, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final SearchPeopleModel userModel = this.userModelCollection.get(position);
        holder.email.setText((userModel.getEmail() != null) ? userModel.getEmail() : "");
        holder.name.setText((userModel.getName() != null) ? userModel.getName() : "");
        Log.d(TAG, "name Name" + userModel.getName());
        Log.d(TAG, "name Name" + userModel.getEmail());
        Log.e(TAG, "name id" + userModel.getId());


        if (userModel.getProfilePic() != null) {
            if (userModel.getProfilePic() != null && !(userModel.getProfilePic().isEmpty()) && !(userModel.getProfilePic().matches("null"))) {
                picasso.load(String.valueOf(userModel.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(holder.profilePic);

            } else {
                holder.profilePic.setImagePlaceHolder(R.drawable.ic_launcher);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SearchPeopleAdapter.this.onItemClickListener != null) {
                    Log.e("click", "Friend list clicked");
                    onItemClickListener.onSearchClicked(userModel);
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<SearchPeopleModel> searchPeopleModel) {
        this.validateUsersCollection(userModelCollection);
        this.userModelCollection = (List<SearchPeopleModel>) searchPeopleModel;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<SearchPeopleModel> searchPeopleModel) {
        if (searchPeopleModel == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.email)
        TextView email;
        @Bind(R.id.profile_pic)
        AutoLoadImageView profilePic;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}