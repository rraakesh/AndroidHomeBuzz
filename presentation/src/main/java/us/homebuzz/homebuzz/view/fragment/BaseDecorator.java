package us.homebuzz.homebuzz.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.NearBy.LowerTabStrip;

/**
 * Created by amit.singh on 10/20/2015.
 */
public abstract class BaseDecorator extends BaseFragment implements LowerTabStrip{
    public BaseDecorator(){}

    @Bind(R.id.container_layout_tab)
    protected FrameLayout container_layout_tab;
    @Bind(R.id.tab_view)
   protected LinearLayout tab_fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ButterKnife.setDebug(true);
        View LowerTabFrameLayout = inflater.inflate(R.layout.lower_tab_framelayout, container, false);
        ButterKnife.bind(this, LowerTabFrameLayout);

//        tab_fragment.setAlpha(0.4f);
        return LowerTabFrameLayout;
    }
    public void onLeftIconClick(Fragment leftFragment){
        addFragment(R.id.container_layout_tab, leftFragment);

    }
    public abstract void setupLowerTab();
    public void onRightIconClick(Fragment rightFragment){
        addFragment(R.id.container_layout_tab,rightFragment);
    }
    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment The fragment to be added.
     */
    protected void addFragment(int containerViewId, Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        android.support.v4.app.FragmentManager manager = this.getChildFragmentManager();
        boolean fragmentPopped = manager
                .popBackStackImmediate(backStateName, 0);
        //  System.out.println("IS POped"+fragmentPopped);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            // fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(containerViewId, fragment, fragmentTag);
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            ft.addToBackStack(backStateName);
            ft.commit();

        }
    }
    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment The fragment to be Replaced.
     */
    protected  void replaceFragment(int containerViewId,Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        android.support.v4.app.FragmentManager manager = this.getChildFragmentManager();
        boolean fragmentPopped = manager
                .popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            // fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(containerViewId, fragment, fragmentTag);
            ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            ft.addToBackStack(backStateName);
            ft.commit();

        }
    }
}
