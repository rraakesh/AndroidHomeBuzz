package us.homebuzz.homebuzz.view.fragment.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.MessageModule;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.presenter.AllMessagesPresenter;
import us.homebuzz.homebuzz.view.AllMessageView;
import us.homebuzz.homebuzz.view.adapter.Message.AllMessageAdapter;
import us.homebuzz.homebuzz.view.adapter.UsersLayoutManager;
import us.homebuzz.homebuzz.view.fragment.BaseNestedFragment;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * Created by amit.singh on 10/22/2015.
 */
public class AllMessages extends BaseNestedFragment implements AllMessageView{
    private static final String TAG = AllMessages.class.getSimpleName();
    @Inject
    AllMessagesPresenter allMessagesPresenter;
    @Bind(R.id.base_list)
    RecyclerView base_list;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    private ReplaceFragment replaceFragmentAllMessages;
    private UsersLayoutManager usersLayoutManager;
    private AllMessageAdapter allMessageAdapter;
    @Inject
    Picasso picasso;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.base_recycler, container, false);
        ButterKnife.bind(this, list_nearby);
        setupUI();
        return list_nearby;
        //Test Commit
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        if(this.getParentFragment()instanceof Messages){
           this.replaceFragmentAllMessages=((Messages) this.getParentFragment()).replaceFragmentMessage;
        }

    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).messageComponent(new MessageModule()).inject(this);
        allMessagesPresenter.setView(this);
        allMessagesPresenter.initialize();
    }
    private void setupUI() {
        this.usersLayoutManager = new UsersLayoutManager(getActivity());
        this.base_list.setLayoutManager(usersLayoutManager);
        this.allMessageAdapter = new AllMessageAdapter(getActivity(), new ArrayList<UserAllMessagesModel>(),picasso);
        this.allMessageAdapter.setOnItemClickListener(onItemClickListener);
        this.base_list.setAdapter(allMessageAdapter);
    }
    @Override public void onResume() {
        super.onResume();
        this.allMessagesPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        this.allMessagesPresenter.pause();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        this.allMessagesPresenter.destroy();
    }
    @Override
    public void showError(String message) {
    showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading()  {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    private AllMessageAdapter.OnItemClickListener onItemClickListener =
            new AllMessageAdapter.OnItemClickListener() {
                @Override
                public void onMessItemClicked(UserAllMessagesModel userAllMessagesModel) {
                    if (AllMessages.this.allMessagesPresenter!= null && userAllMessagesModel!= null) {
                        AllMessages.this.allMessagesPresenter.onUserClicked(userAllMessagesModel);

                    }

                }


            };

    @Override
    public void renderUserAllMessageList(Collection<UserAllMessagesModel> userAllMessagesModels) {
        Log.d(TAG, "The All Messages List " + userAllMessagesModels.size());
        //TODO Set the ADAPTER OF RECYCLER VIEW HERE
        if (userAllMessagesModels != null) {
            this.allMessageAdapter.setUsersCollection(userAllMessagesModels);
        }
    }

    @Override
    public void viewUser(UserAllMessagesModel userAllMessagesModel) {
        Log.d(TAG, "The Clicked UserID " + userAllMessagesModel.getId());
        replaceFragmentAllMessages.CallReplaceFragment(R.id.conatainer_layout, ReplyMessageFragment.newInstance(userAllMessagesModel));
        }

}

