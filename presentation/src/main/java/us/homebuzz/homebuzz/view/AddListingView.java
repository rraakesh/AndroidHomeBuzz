package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;

/**
 * Created by amit.singh on 11/4/2015.
 */
public interface AddListingView  extends LoadDataView {
    /**
     * View a {@link } AddListing/details
     *
     * @param addListingModel The user that will be shown.
     */
    void viewAddListing(AddListingModel addListingModel);
    void viewAddListingPhoto(AddListingPhotoModel addListingPhotoModel);
}
