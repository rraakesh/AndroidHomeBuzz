package us.homebuzz.homebuzz.view.fragment.leaderboard;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.WeakHashMap;

import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.LeaderBoardModule;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.ReplaceFragment;

/**
 * Created by amit.singh on 10/20/2015.
 */
@SuppressWarnings("ALL")
public class LeaderBoard extends LeaderBoardDecorator implements View.OnClickListener {
    private static final String TAG = LeaderBoard.class.getSimpleName();

    public static LeaderBoard newInstance() {
        LeaderBoard leaderBoard = new LeaderBoard();
        return leaderBoard;
    }
    public interface LederBoardFilter{
        void performFilter(WeakHashMap<String,String>filterData);
    }


    public LeaderBoard() {
        super();
    }
    LederBoardFilter lederBoardFilter;
    protected ReplaceFragment replaceFragmentLarboard;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupLowerTab();
        super.replaceFragment(R.id.container_layout_tab, new EstimateFragment());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.left_icon:
                LinearLayout left_layout = (LinearLayout) tab_fragment.findViewById(R.id.left_icon);
                ((TextView) v.findViewById(R.id.text_accuracy)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                left_layout.setBackgroundColor(getActivity().getResources().getColor(R.color.leaderboard_back));
                LinearLayout parent = (LinearLayout) v.getParent();
                ((TextView) parent.findViewById(R.id.text_estimate)).setTextColor(getActivity().getResources().getColor(R.color.leaderboard_back));
                parent.findViewById(R.id.right_icon).setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

                super.replaceFragment(R.id.container_layout_tab, new EstimateFragment());
                break;
            case R.id.right_icon:
                ((TextView) v.findViewById(R.id.text_estimate)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
                LinearLayout right_layout = (LinearLayout) tab_fragment.findViewById(R.id.right_icon);
                right_layout.setBackgroundColor(getActivity().getResources().getColor(R.color.leaderboard_back));

                LinearLayout p1 = (LinearLayout) v.getParent();
                ((TextView) p1.findViewById(R.id.text_accuracy)).setTextColor(getActivity().getResources().getColor(R.color.leaderboard_back));
                p1.findViewById(R.id.left_icon).setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
                super.replaceFragment(R.id.container_layout_tab, new AccuracyFragment());

                break;
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
    //Todo Consider a Better Solution to Have Nested Fragment Access the Listener
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Leaderboard");

        this.replaceFragmentLarboard = replaceListener;

    }


    private void initialize() {
        this.getComponent(MainActivityComponent.class).leaderBoardComponent(new LeaderBoardModule()).inject(this);
    }

    @Override
    public void setupLowerTab() {
        LinearLayout tab_strip = (LinearLayout) this.getActivity().getLayoutInflater().inflate(R.layout.base_search_tab_layout, tab_fragment, false);
        tab_fragment.addView(tab_strip);
        ButterKnife.bind(super.getClass(), tab_strip);
        LinearLayout left_layout = (LinearLayout) tab_fragment.findViewById(R.id.left_icon);
        left_layout.setBackgroundColor(getActivity().getResources().getColor(R.color.leaderboard_back));
        LinearLayout right_layout = (LinearLayout) tab_fragment.findViewById(R.id.right_icon);
        ((TextView) left_layout.findViewById(R.id.text_accuracy)).setTextColor(getActivity().getResources().getColor(R.color.color_white));
        right_layout.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

       // ((ImageView) left_layout.findViewById(R.id.icon_accuracy)).setImageResource(R.drawable.list_yellow);
        ((TextView) right_layout.findViewById(R.id.text_estimate)).setTextColor(getActivity().getResources().getColor(R.color.leaderboard_back));
        //((ImageView) right_layout.findViewById(R.id.icon_estimate)).setImageResource(R.drawable.list_white);
        left_layout.setOnClickListener(this);
        right_layout.setOnClickListener(this);

    }

    @Override
    protected void OnFilterUpdate(WeakHashMap<String, String> filterData) {
        Log.d(TAG, "" + filterData.size());
        if((this.getChildFragmentManager().findFragmentById(R.id.container_layout_tab) instanceof AccuracyFragment)){
            lederBoardFilter=(AccuracyFragment)(this.getChildFragmentManager().findFragmentById(R.id.container_layout_tab));
        }else if((this.getChildFragmentManager().findFragmentById(R.id.container_layout_tab) instanceof EstimateFragment)){
            lederBoardFilter=(EstimateFragment)(this.getChildFragmentManager().findFragmentById(R.id.container_layout_tab));
        }
        Log.d(TAG, "Running Fragment" + (this.getChildFragmentManager().findFragmentById(R.id.container_layout_tab)));
        lederBoardFilter.performFilter(filterData);
    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }
}
