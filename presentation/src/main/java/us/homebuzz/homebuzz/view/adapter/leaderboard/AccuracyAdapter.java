package us.homebuzz.homebuzz.view.adapter.leaderboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by amit.singh on 10/20/2015.
 */
public class AccuracyAdapter  extends RecyclerView.Adapter<AccuracyAdapter.UserViewHolder> {
    public static String TAG = AccuracyAdapter.class.getClass().getSimpleName();
    public interface OnItemClickListener {
        void onAccuracyItemClicked(UserAccuracyModel userModel);
    }
    private Picasso picasso;
    private List<UserAccuracyModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private DecimalFormat accuracyFormatter;
    private OnItemClickListener onItemClickListener;

    public AccuracyAdapter(Context context, Collection<UserAccuracyModel> usersCollection,Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserAccuracyModel>) usersCollection;
        this.accuracyFormatter=new DecimalFormat("#.##");
        this.picasso=picasso;
    }

    @Override public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.leaderboard_row, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserAccuracyModel userModel = this.usersCollection.get(position);
        holder.number.setText(String.valueOf(position + 1));


        holder.leader_zips.setText(userModel.getEstimateZips().toString().replace("[", "").replace("]", "").replace("null",""));
        holder.leader_name.setText(userModel.getName());
        holder.leader_accuracy.setText(userModel.getEstimatesCount() + " Estimates,");
        holder.leader_estimate.setText(this.accuracyFormatter.format(userModel.getAccurate() * 100)+"%"+"Accuracy");
        if(userModel.getProfilePic()!=null && !(userModel.getProfilePic().isEmpty())&& !(userModel.getProfilePic().matches("null")) ) {
            System.out.println(TAG + "Pic" + userModel.getProfilePic() + "PIC" + picasso);
             picasso.load( userModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder.person_image);

        }else{
            //TODO Place A DEFAULT IMAGE
            holder.person_image.setImagePlaceHolder(R.drawable.ic_launcher);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (AccuracyAdapter.this.onItemClickListener != null) {
                    AccuracyAdapter.this.onItemClickListener.onAccuracyItemClicked(userModel);
                }
            }
        });
    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserAccuracyModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserAccuracyModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<UserAccuracyModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.leader_estimate)
        TextView leader_estimate;
        @Bind(R.id.number)
        TextView number;
        @Bind(R.id.leader_zips)
        TextView leader_zips;
        @Bind(R.id.leader_name)
        TextView leader_name;
        @Bind(R.id.leader_accuracy)
        TextView leader_accuracy;
        @Bind(R.id.leader_image)
        AutoLoadImageView person_image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
