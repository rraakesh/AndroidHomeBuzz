package us.homebuzz.homebuzz.view;

/**
 * Created by amit.singh on 12/11/2015.
 */
public interface PermissionBus {
    void onLocationPermissionSuccess(boolean isSuccess);
    void onContactPermissionSuccess(boolean isSuccess);
    void onStoragePermissionSuccess(boolean isSuccess);
    void onCameraPermissionSuccess(boolean isSuccess);
}
