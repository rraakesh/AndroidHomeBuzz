package us.homebuzz.homebuzz.view.fragment.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.BaseDecorator;

/**
 * Created by amit.singh on 10/22/2015.
 */
public abstract class MessagesDecorator extends BaseDecorator {

    public MessagesDecorator() {
    }

    @Bind(R.id.messsage_text_top)
    TextView message_top;
    @Bind(R.id.container_layout_tab)
    FrameLayout container_layout_tab;
    @Bind(R.id.tab_view)
    LinearLayout tab_fragment;

    @Override
    public void onLeftIconClick(Fragment leftFragment) {
        super.replaceFragment(R.id.container_layout_tab, leftFragment);
    }

    @Override
    public void onRightIconClick(Fragment rightFragment) {
        super.replaceFragment(R.id.container_layout_tab, rightFragment);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View base_Leader = inflater.inflate(R.layout.base_message, container, false);
        ButterKnife.bind(this, base_Leader);
        return base_Leader;
    }

    protected void setTopText(String message){
        message_top.setText(message);
    }
}
