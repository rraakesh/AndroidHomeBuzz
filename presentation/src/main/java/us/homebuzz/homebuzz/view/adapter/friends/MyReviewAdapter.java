package us.homebuzz.homebuzz.view.adapter.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.view.adapter.leaderboard.AccuracyAdapter;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;

/**
 * Created by abhishek.singh on 11/27/2015.
 */
public class MyReviewAdapter extends RecyclerView.Adapter<MyReviewAdapter.UserViewHolder> {
    public static String TAG = AccuracyAdapter.class.getClass().getSimpleName();

    public interface OnItemClickListener {
        void onPreviousEstimateAdapterItemClicked(UserPreviousEstimateModel userModel);
    }

    private List<UserPreviousEstimateModel> usersCollection;
    private final LayoutInflater layoutInflater;
    private DecimalFormat accuracyFormatter;
    private OnItemClickListener onItemClickListener;
    private Picasso picasso;

    public MyReviewAdapter(Context context, Collection<UserPreviousEstimateModel> usersCollection, Picasso picasso) {
        this.validateUsersCollection(usersCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersCollection = (List<UserPreviousEstimateModel>) usersCollection;
        this.accuracyFormatter = new DecimalFormat("#.##");
        this.picasso = picasso;
    }

    @Override
    public int getItemCount() {
        return (this.usersCollection != null) ? this.usersCollection.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.my_review_adapter, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserPreviousEstimateModel userModel = this.usersCollection.get(position);
        if (userModel.getListing() != null) {
            holder.txtStreet.setText(userModel.getListing().getStreet() != null ? userModel.getListing().getStreet() : "");
            holder.txtState.setText(userModel.getListing().getCity() != null ? userModel.getListing().getCity() : "");
            holder.txtState.append(", " + (userModel.getListing().getState() != null ? userModel.getListing().getState() : ""));
            holder.txtState.append(" " + (userModel.getListing().getZip() != null ? userModel.getListing().getZip() : ""));
            holder.txtPrice.setText("$ " +new DecimalFormat("#,###").format(userModel.getListing().getPrice().intValue()));
            holder.txtPrice.append(" " + (userModel.getListing().getBedrooms() + "BR" + " / " +userModel.getListing().getBaths()+ "BA"));
            if (userModel.getListing() != null && (userModel.getListing().getPhotos().size() > 0)) {
                System.out.println(TAG + "Pic" + userModel.getListing().getPhotos().get(0));
                picasso.load(String.valueOf(userModel.getListing().getPhotos().get(0))).resize(200, 200).centerCrop().placeholder(R.drawable.holder_home).error(R.drawable.error).into(holder.person_image);
                //  holder.person_image.setImageUrl(String.valueOf(userModel.getListing().getPhotos().get(0)));
            } else {

                holder.person_image.setImagePlaceHolder(R.drawable.holder_home);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyReviewAdapter.this.onItemClickListener != null) {
                    MyReviewAdapter.this.onItemClickListener.onPreviousEstimateAdapterItemClicked(userModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(Collection<UserPreviousEstimateModel> usersCollection) {
        this.validateUsersCollection(usersCollection);
        this.usersCollection = (List<UserPreviousEstimateModel>) usersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(Collection<UserPreviousEstimateModel> usersCollection) {
        if (usersCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.txtStreet)
        TextView txtStreet;
        @Bind(R.id.txtState)
        TextView txtState;
        @Bind(R.id.txtPrice)
        TextView txtPrice;
        @Bind(R.id.leader_image)
        AutoLoadImageView person_image;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
