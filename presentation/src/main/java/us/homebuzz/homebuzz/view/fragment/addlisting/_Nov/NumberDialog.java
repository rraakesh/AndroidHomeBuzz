package us.homebuzz.homebuzz.view.fragment.addlisting._Nov;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.NumberPicker;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.view.fragment.NearBy._Nov.ListingPhoto;

/**
 * Created by amit.singh on 11/30/2015.
 */
public class NumberDialog extends android.support.v4.app.DialogFragment {
    private static final String TAG = ListingPhoto.class.getSimpleName();
    private static final String KEY_INITIALIZE = "inti";

    public interface NumberDialoge {
        void onNumberDialogClick(int initialIndex, int value);
    }

    public static NumberDialog newInstance(int index) {
        NumberDialog listingPhoto = new NumberDialog();
        Bundle argumentsBundle = new Bundle();
        //argumentsBundle.putParcelable(ARGUMENT_KEY_USER_NEARBY_LIST_MODEL, userAccuracyModel);
        argumentsBundle.putInt(KEY_INITIALIZE, index);
        listingPhoto.setArguments(argumentsBundle);
        return listingPhoto;
    }

    Dialog dialog;
    @Inject
    Picasso picasso;
    NumberDialoge numberDialoge;
    @Bind(R.id.number_val)
    NumberPicker number_val;
    int indexInit, finalValue;
    @Bind(R.id.date_val)
    DatePicker data_val;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.number_dialog);
        final Drawable d = new ColorDrawable(Color.argb(140, 223, 222, 220));
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        float bottomMapPadding = 100 * getResources().getDisplayMetrics().density;
        params.width = WindowManager.LayoutParams.MATCH_PARENT/*-(int)bottomMapPadding*/;
        params.height = WindowManager.LayoutParams.MATCH_PARENT/*-(int)bottomMapPadding*/;
        params.y = (int) bottomMapPadding;
        dialog.setCanceledOnTouchOutside(true);
        // if (this.getArguments().getBundle() != null)
        this.indexInit = this.getArguments().getInt(KEY_INITIALIZE);
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        return dialog;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //this.initialize();
        setupUI();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (this.getParentFragment() instanceof AddListingStep1) {
            numberDialoge = (AddListingStep1) this.getParentFragment();
        }
    }

    @OnTouch(R.id.number_view)
    public boolean onOutsidetouch() {
        this.dismiss();
        return true;
    }

    private void setupUI() {
        number_val.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                finalValue = newVal;
            }
        });
        switch (indexInit) {
            case 4:
                number_val.setMinValue(0);// restricted number to minimum value i.e 1
                number_val.setMaxValue(5);// restricked number to maximum value i.e. 31
                number_val.setWrapSelectorWheel(true);
                break;
            default:
                number_val.setMinValue(0);// restricted number to minimum value i.e 1
                number_val.setMaxValue(10);// restricked number to maximum value i.e. 31
                number_val.setWrapSelectorWheel(true);
                break;
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        numberDialoge.onNumberDialogClick(indexInit, finalValue);
    }
}
