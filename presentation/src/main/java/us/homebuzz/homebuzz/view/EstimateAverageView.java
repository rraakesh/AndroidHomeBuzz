package us.homebuzz.homebuzz.view;

import us.homebuzz.homebuzz.model.EstimateAverageModel;

/**
 * Created by amit.singh on 12/12/2015.
 */
public interface EstimateAverageView  extends LoadDataView {
    /**
     * View a {@link } UnreadMessages
     *
     * @param estimateAverageModel Average Estimate
     */
    void viewGetEstimateAverage(EstimateAverageModel estimateAverageModel);
}
