package us.homebuzz.homebuzz.view.profile.friendsprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;
import us.homebuzz.homebuzz.view.fragment.searchPeople.SearchPeople;

/**
 * Created by abhishek.singh on 11/17/2015.
 */
public class ProfileFriends extends BaseFragment {
    private static final String ARGUMENT_KEY_USER_PROFILE = "Profile";
    private UserProfileModel userProfileModel;

    public static ProfileFriends newInstance(UserProfileModel userProfileModel) {
        ProfileFriends profileFriends = new ProfileFriends();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE, userProfileModel);
        profileFriends.setArguments(argumentsBundle);
        return profileFriends;
    }

    @Bind(R.id.txtFollowing)
    TextView txtFollowing;
    @Bind(R.id.txtFollowers)
    TextView txtFollowers;
    @Bind(R.id.txtSearchForPeople)
    TextView txtSearchForPeople;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View newFriends = inflater.inflate(R.layout.new_friends, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE) != null)
            userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE);
        ButterKnife.bind(ProfileFriends.this, newFriends);
        return newFriends;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Friends");
        setUpUI();

    }

    private void setUpUI() {
        txtFollowing.setText((userProfileModel.getFollowings() != null && (!userProfileModel.getFollowings().toString().matches("null")) ? "Following " + "(" + userProfileModel.getFollowings().toString() + ")" : "Following " + "(" + "0" + ")"));
        txtFollowers.setText((userProfileModel.getFollowers() != null && (!userProfileModel.getFollowers().toString().matches("null")) ? "Followers " + "(" + userProfileModel.getFollowers().toString() + ")" : "Followers " + "(" + "0" + ")"));
    }

    @OnClick(R.id.txtFollowing)
    public void btnFollowingClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, FriendsFollowingList.newInstance());
    }

    @OnClick(R.id.txtFollowers)
    public void btnFollowersClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, FriendsFollowersList.newInstance());

    }

    @OnClick(R.id.txtSearchForPeople)
    public void btnSearchPeople() {
        Fragment fragment = new SearchPeople();
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, fragment);
    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }
}
