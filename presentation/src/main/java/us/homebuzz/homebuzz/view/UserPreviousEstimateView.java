package us.homebuzz.homebuzz.view;

import java.util.Collection;

import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;

/**
 * Created by amit.singh on 10/27/2015.
 */
public interface UserPreviousEstimateView  extends LoadDataView{
    /**
     * Render a PreviousEstimate in the UI.
     *
     * @param user The {@link UserLoginModel} that will be shown.
     */
    void readerPreviousEstimate(Collection<UserPreviousEstimateModel> user);
    /**
     * View a {@link } profile/details.
     *
     * @param  userAccuracyModel The user that will be shown.
     */
    void viewDetails(UserPreviousEstimateModel userAccuracyModel);
}
