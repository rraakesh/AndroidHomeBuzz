package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.view.activity.LoginSignUp.Login;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class UserSettingsFragment extends BaseFragment {

    public static UserSettingsFragment newInstance() {
        UserSettingsFragment userSettingsFragment = new UserSettingsFragment();
        return userSettingsFragment;
    }

    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Constants constants;
    @Bind(R.id.user_logout)
    LinearLayout user_logout;
    @Bind(R.id.txtGeneral)
    TextView Genearl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View user_settings = inflater.inflate(R.layout.user_settings, container, false);
        ButterKnife.bind(this, user_settings);
        FacebookSdk.sdkInitialize(this.getActivity());
        return user_settings;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Settings");
        initialize();

    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    /**
     * Navigate to ProfileFragment
     */
    @OnClick(R.id.user_profiles)
    public void onProfileClick() {
        //Go to Profile Fragment
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, UserProfileFragment.newInstance());

    }

    @OnClick(R.id.txtGeneral)
    public void onGenral() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, AutoLogout.newInstance());

    }

    @OnClick(R.id.zip_code_purchase)
    public void onZipCodepurchaseClick() {
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, ZipCodePuchaseFragment1.newInstance());
    }

    /**
     * Navigate to LoginActivity After the User Is Successfully logout
     */
    @OnClick(R.id.user_logout)
    public void onLogoutClick() {
        //Todo Finish the Activity After the deleting the SharedPreference Login Token
        if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null) {
            LoginManager.getInstance().logOut();
            sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
            UserSettingsFragment.this.getActivity().finish();
            replaceListener.CallNavigateToActivity(new Login());

        } else {
            sharedPreferences.edit().putString(constants.KEY_EMAIL, "").putBoolean(constants.KEY_IS_LOGIN, false).putString(constants.KEY_TOKEN, "").commit();
            UserSettingsFragment.this.getActivity().finish();
            replaceListener.CallNavigateToActivity(new Login());
        }
      //  replaceListener.CallReplaceFragment(R.id.conatainer_layout, CheckInNearby.newInstance(42.725429,-73.797491));
    }


    @OnClick(R.id.user_bug_report)
    public void onUserBugClick() {
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//        alertDialog.setTitle("Homebuzz");
//        alertDialog.setMessage("Please shake your phone 3 times to report a feedback / bug");
//        alertDialog.setPositiveButton("ok", null);
//        alertDialog.show();

        Intent internetIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://homebuzzinc.zendesk.com/hc/en-us"));
        this.getContext().startActivity(internetIntent);
    }
    @Override
    public void onDestroyView() {
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("HomeBuzz");
        super.onDestroyView();

    }


}

