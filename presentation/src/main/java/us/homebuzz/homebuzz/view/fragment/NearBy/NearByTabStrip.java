package us.homebuzz.homebuzz.view.fragment.NearBy;

/**
 * Created by amit.singh on 10/5/2015.
 */
public interface NearByTabStrip extends LowerTabStrip {
    void onFilterIconClick(android.support.v4.app.DialogFragment dialogFragment);
}
