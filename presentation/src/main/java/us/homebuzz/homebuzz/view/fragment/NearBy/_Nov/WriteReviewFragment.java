package us.homebuzz.homebuzz.view.fragment.NearBy._Nov;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 11/26/2015.
 */
public class WriteReviewFragment extends BaseFragment {
    private String reason = "";
    public static final String KEY_WRITE_REVIEW_TEXT = "reason";

    public static WriteReviewFragment newInstance(String reason) {
        WriteReviewFragment WriteReviewFragment = new WriteReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_WRITE_REVIEW_TEXT, reason);
        WriteReviewFragment.setArguments(bundle);
        return WriteReviewFragment;
    }

    @Bind(R.id.btn_save)
    Button btn_save;
    @Bind(R.id.btn_cancel)
    Button btn_cancel;
    @Bind(R.id.edReview)
    EditText edReview;
    public static String review = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_nearby = inflater.inflate(R.layout.write_review, container, false);
        ButterKnife.bind(this, list_nearby);
        if (this.getArguments().getString(KEY_WRITE_REVIEW_TEXT) != null) {
            reason = this.getArguments().getString(KEY_WRITE_REVIEW_TEXT);
        }
        return list_nearby;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        setUI();
    }

    private void setUI() {
        edReview.setText(reason);
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }

    @OnClick(R.id.btn_save)
    public void btnSave() {
        review = edReview.getText().toString();
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_cancel)
    public void btnCancel() {
        getActivity().onBackPressed();
    }
}
