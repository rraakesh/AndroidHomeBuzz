package us.homebuzz.homebuzz.view.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.EditProfileModelValues;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 11/16/2015.
 */
public class EditBioProfile extends BaseFragment {
    private static final String ARGUMENT_KEY_IS_USER_PROFILE = "isProfile";
    private static final String ARGUMENT_KEY_USER_PROFILE1 = "isProfile1";
    private static final String ARGUMENT_KEY_USER_PROFILE = "Profile";
    private String photoPath="";

    public static EditBioProfile newInstance(boolean isProfileExist, UserProfileModel userProfileModel, EditProfileModelValues values) {
        EditBioProfile EditBioProfile = new EditBioProfile();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE, userProfileModel);
        argumentsBundle.putParcelable(ARGUMENT_KEY_USER_PROFILE1, values);
        argumentsBundle.putBoolean(ARGUMENT_KEY_IS_USER_PROFILE, isProfileExist);
        EditBioProfile.setArguments(argumentsBundle);
        return EditBioProfile;
    }

    @Bind(R.id.profilePic)
    CircleImageView profilePic;
    @Bind(R.id.txtProfileName)
    TextView txtProfileName;
    @Bind(R.id.edBio)
    EditText edBio;
    @Bind(R.id.edCollege)
    EditText edCollege;
    @Bind(R.id.edDegree)
    EditText edDegree;
    @Bind(R.id.edYear)
    EditText edYear;
    @Bind(R.id.Other_school)
    EditText Other_school;
    @Bind(R.id.otherDesignation)
    EditText otherDesignation;
    @Bind(R.id.btnEditProfileContinue)
    Button btnEditProfileContinue;
    private UserProfileModel userProfileModel;
    private EditProfileModelValues values;
    private boolean isProfileExist;
    @Inject
    Picasso picasso;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list_editSocialMedia = inflater.inflate(R.layout.edit_profile_bio, container, false);
        if (this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE) != null)
            userProfileModel = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE);
        values = this.getArguments().getParcelable(ARGUMENT_KEY_USER_PROFILE1);
        isProfileExist = this.getArguments().getBoolean(ARGUMENT_KEY_IS_USER_PROFILE);
        System.out.println("Boolean Profile =" + isProfileExist + "UserProfile " + userProfileModel.getId());
        System.out.println("Boolean Profile =" + isProfileExist + "values " + values.getName());
        ButterKnife.bind(this, list_editSocialMedia);
        return list_editSocialMedia;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Bio");
        this.initialize();
        setUpUI();
    }
    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
    }
    private void setUpUI() {
        txtProfileName.setText((userProfileModel.getName() != null && !(userProfileModel.getName().matches("null")) ? userProfileModel.getName() : ""));
        edBio.setText((userProfileModel.getBio() != null && !(userProfileModel.getBio().matches("null")) ? userProfileModel.getBio() : ""));
        edCollege.setText((userProfileModel.getCollege() != null && !(userProfileModel.getCollege().matches("null")) ? userProfileModel.getCollege() : ""));
        edDegree.setText((userProfileModel.getDegree() != null && !(userProfileModel.getDegree().matches("null")) ? userProfileModel.getDegree() : ""));
        edYear.setText((userProfileModel.getYrGraduated() != null && !(userProfileModel.getYrGraduated().toString().matches("null"))) ? userProfileModel.getYrGraduated().toString() : "");
        Other_school.setText((userProfileModel.getOtherSchool() != null && !(userProfileModel.getOtherSchool().toString().matches("null"))) ? userProfileModel.getOtherSchool().toString() : "");
        otherDesignation.setText((userProfileModel.getDesignations() != null && !(userProfileModel.getDesignations().toString().matches("null"))) ? userProfileModel.getDesignations().toString() : "");
        if (!values.getProfilePic().equalsIgnoreCase("")) {
            picasso.load(new File(values.getProfilePic())).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
        } else{
            if (userProfileModel.getProfilePic() != null && !userProfileModel.getProfilePic().matches("null")){
                picasso.load(userProfileModel.getProfilePic()).resize(200, 200).centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.error).into(profilePic);
            }else {

            }

        }
    }

    @OnClick(R.id.btnEditProfileContinue)
    public void onClickEditBioProfile() {
        values.setBio(edBio.getText().toString());
        values.setCollege(edCollege.getText().toString());
        values.setDegree(edDegree.getText().toString());
        if (!edYear.getText().toString().isEmpty())
            values.setYrGraduated(Integer.valueOf(edYear.getText().toString()));
        else values.setYrGraduated(0);
        values.setOtherSchool(Other_school.getText().toString());
        values.setDesignations(otherDesignation.getText().toString());
          Log.d(TAG, "College"+values.getCollege());
        replaceListener.CallReplaceFragment(R.id.conatainer_layout, EditAgentProfile.newInstance(isProfileExist, userProfileModel, values));
    }


}
