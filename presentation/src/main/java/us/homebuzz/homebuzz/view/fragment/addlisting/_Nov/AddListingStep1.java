package us.homebuzz.homebuzz.view.fragment.addlisting._Nov;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.AddListingModule;
import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;
import us.homebuzz.homebuzz.presenter.AddListingPresenter;
import us.homebuzz.homebuzz.view.AddListingView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by amit.singh on 11/28/2015.
 */
public class AddListingStep1 extends BaseFragment implements AddListingView, NumberDialog.NumberDialoge {
    private static final String TAG = AddListingStep1.class.getSimpleName();
    private static final String ADD_LISTING_DATA = "AddListing";

    public static AddListingStep1 newInstance(AddListingData addListingData) {
        AddListingStep1 addListing = new AddListingStep1();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putParcelable(ADD_LISTING_DATA, addListingData);
        addListing.setArguments(argumentsBundle);
        return addListing;
    }

    AddListingData addListingData;
    /*@Bind(R.id.address_txt)
    TextView address_txt;*/
    @Bind(R.id.Zip_txt)
    TextView Zip_txt;
    @Bind(R.id.tax_txt)
    TextView tax_txt;
    @Bind(R.id.address_txt)
    TextView address_txt;
    @Bind(R.id.price_edt)
    EditText price_edt;
    @Bind(R.id.firplaces_edt)
    EditText firplaces_edt;
    @Bind(R.id.half_bath_edt)
    EditText half_bath_edt;
    @Bind(R.id.year_edt)
    EditText year_edt;
    @Bind(R.id.sq_ft_edt)
    EditText sq_ft_edt;
    @Bind(R.id.bedrooms_edt)
    EditText bedrooms_edt;
    @Bind(R.id.full_bath_edt)
    EditText full_bath_edt;
    @Bind(R.id.acres_edt)
    EditText acres_edt;
    @Bind(R.id.lot_sq_edt)
    EditText lot_sq_edt;
    @Bind(R.id.tax_id_txt)
    TextView tax_id_txt;
    @Bind(R.id.forSale)
    ImageView forSale;
    @Inject
    AddListingPresenter addListingPresenter;
    @Inject
    Constants constants;
    @Bind(R.id.residential_edt)
    Spinner residential;
    @Bind(R.id.lots_land_edt)
    Spinner lots_land;
    private String addRemarks = "";
    List<String> property,lotarray;
    boolean toogleForSale;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_new_listing_ad, container, false);
        if (this.getArguments().getParcelable(ADD_LISTING_DATA) != null) {
            this.addListingData = this.getArguments().getParcelable(ADD_LISTING_DATA);
            Log.d(TAG, "addListingData Address" + addListingData.getAddress());
        }
        ButterKnife.bind(this, view);
        return view;
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).addListingComponent(new AddListingModule()).inject(this);
        this.addListingPresenter.setView(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add New Listing");
        this.initialize();
        this.setupUI();


    }

    private void setupUI() {
        if (addListingData != null) {
            Zip_txt.setText(addListingData.getCity()+","+addListingData.getState()+addListingData.getZip());
            address_txt.setText(addListingData.getStreet());
           // tax_txt.setText(addListingData.getParcelNum());
            acres_edt.setText(addListingData.getAcres());
            tax_id_txt.setText(addListingData.getParcelNum());
            Log.d(TAG,"PArcel NU"+addListingData.getParcelNum());
        }

         property = Arrays.asList(this.getResources().getStringArray(R.array.PropertyTypeArray));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getContext(),
                android.R.layout.simple_spinner_item, property);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        residential.setAdapter(dataAdapter);


        lotarray = Arrays.asList(this.getResources().getStringArray(R.array.listingClassTypeArray));
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this.getContext(),
                android.R.layout.simple_spinner_item, lotarray);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lots_land.setAdapter(dataAdapter1);

        forSale.setImageResource(R.drawable.toggle_on);
        addListingData.setForSale(true);
        toogleForSale=true;
    }

    @OnClick(R.id.forSale)
    public void imageClick() {
        if (toogleForSale) {
            forSale.setImageResource(R.drawable.toggle_off);
            toogleForSale=false;
        } else {
            forSale.setImageResource(R.drawable.toggle_on);
            toogleForSale=true;
        }
        addListingData.setForSale(toogleForSale);
    }
    @OnItemSelected(R.id.lots_land_edt)
    void onLotSelected(int position) {
        addListingData.setListingClass(lotarray.get(position).toLowerCase());
      //  Toast.makeText(this, "Selected position " + position + "!", Toast.LENGTH_SHORT).show();
    }
    @OnItemSelected(R.id.residential_edt)
    void onResSelected(int position) {
        addListingData.setListingClassType(property.get(position).toLowerCase());
        //  Toast.makeText(this, "Selected position " + position + "!", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.btn_remarks)
    public void addRemarksBtnClick() {
       final Dialog d = new Dialog(getActivity());
        d.getWindow().setGravity(Gravity.CENTER_HORIZONTAL );
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams params = d.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT/*-(int)bottomMapPadding*/;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        d.setContentView(R.layout.add_new_listing_remarks);
        d.setTitle("");
        Button btn_save = (Button) d.findViewById(R.id.btn_save);
        Button btn_cancel = (Button) d.findViewById(R.id.btn_cancel);

        final EditText edRemarks = (EditText) d.findViewById(R.id.edRemarks);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRemarks = edRemarks.getText().toString();
                d.dismiss();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    @OnClick(R.id.btn_continue)
    public void onContinueClick() {
        int yearbuild=-1;
        if(!(year_edt.getText().toString().isEmpty())){
            yearbuild= Integer.parseInt(year_edt.getText().toString());
        }
        addListingData.setYearBuilt(yearbuild);
        addListingData.setPrice(price_edt.getText().toString());
        addListingData.setBathshalf((half_bath_edt.getText().toString().isEmpty())?0:(Integer.parseInt(half_bath_edt.getText().toString())));
        addListingData.setBedrooms((bedrooms_edt.getText().toString().isEmpty())?0:Integer.parseInt(bedrooms_edt.getText().toString()));
        addListingData.setFireplaces((firplaces_edt.getText().toString().isEmpty())?0:Integer.parseInt(firplaces_edt.getText().toString()));
        addListingData.setBathsfull(full_bath_edt.getText().toString());
        addListingData.setAcres(acres_edt.getText().toString());
        addListingData.setLotSqftApprox(sq_ft_edt.getText().toString());
        addListingData.setEstimatedSquareFeet(lot_sq_edt.getText().toString());
        addListingData.setRemarks(addRemarks);

        if (addRemarks.matches("")) {
            super.showToastMessage("Please add remarks !!");
        } else if (addListingData.getZip().matches("")) {
            super.showToastMessage("Zip can't be blank.");
        } else if (String.valueOf(addListingData.getBathshalf().toString()).equalsIgnoreCase("0")) {
            super.showToastMessage("Half bath can't be blank.");
        } else if (String.valueOf(addListingData.getFireplaces().toString()).equalsIgnoreCase("0")) {
            super.showToastMessage("Fire Places can't be blank.");
        } else if (String.valueOf(addListingData.getBedrooms().toString()).equalsIgnoreCase("0")) {
            super.showToastMessage("BedRoom can't be blank.");
        } else if (String.valueOf(addListingData.getAcres().toString()).matches("")) {
            super.showToastMessage("Acres can't be blank.");
        } else if (String.valueOf(addListingData.getYearBuilt().toString()).equalsIgnoreCase("-1")) {
            super.showToastMessage("Year built can't be blank.");
        } else if (String.valueOf(addListingData.getBathsfull().toString()).matches("")) {
            super.showToastMessage("Bath Full bath can't be blank.");
        } else if (String.valueOf(addListingData.getPrice().toString()).matches("")) {
            super.showToastMessage("price can't be blank.");
        }else if(yearbuild > Calendar.getInstance().get(Calendar.YEAR)){
            super.showToastMessage("Year Built should be current and previous year.!");
        }
        else {
            //Go to Next Fragment From Here
            //replaceListener.CallReplaceFragment();
            Log.d(TAG, "Full Bath" + addListingData.getBathsfull());
            replaceListener.CallReplaceFragment(R.id.conatainer_layout,AddListingStep2.newInstance(addListingData));
          /*  WeakHashMap<String, String> listingData = new WeakHashMap<>(11);
            listingData.put(constants.LISTING_ZIP, addListingData.getZip());
            listingData.put(constants.LISTING_REMARKS, addListingData.getRemarks());
            listingData.put(constants.LISTING_HALFBATH, String.valueOf(addListingData.getBathshalf()));
            listingData.put(constants.LISTING_FIREPLACES, String.valueOf(addListingData.getFireplaces()));
            listingData.put(constants.LISTING_PRICENUMBER, "" + addListingData.getPrice());
            listingData.put(constants.LISTING_BEDROOMS, String.valueOf(addListingData.getBedrooms()));
            listingData.put(constants.LISTING_ACERS, "" + addListingData.getAcres());
            listingData.put(constants.LISTING_YEAR_BUILT, "" + addListingData.getYearBuilt());
            listingData.put(constants.LISTING_FULL_BATH, addListingData.getBathsfull());
            listingData.put(constants.LISTING_LON, String.valueOf(addListingData.getLon()));
            listingData.put(constants.LISTING_LAT, String.valueOf(addListingData.getLat()));
            this.addListingPresenter.initialize(listingData);*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.addListingPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.addListingPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.addListingPresenter.destroy();
        ((TextView)((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add New Listing");
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void viewAddListingPhoto(AddListingPhotoModel addListingPhotoModel) {
        Log.d(TAG, "The Photo Url" + addListingPhotoModel.getUrl());

    }

    @OnFocusChange(R.id.full_bath_edt)
    public void onFullBathfocus(boolean hasFocus) {
        if (hasFocus) {
            onDialogClick(NumberDialog.newInstance(1));
        } else {

        }
    }

    @OnFocusChange(R.id.half_bath_edt)
    public void onhHlf_bath_edtfocus(boolean hasFocus) {
        if (hasFocus) {
            onDialogClick(NumberDialog.newInstance(2));
        } else {

        }
    }

    @OnFocusChange(R.id.bedrooms_edt)
    public void onBedrooms_edtfocus(boolean hasFocus) {
        if (hasFocus) {
            onDialogClick(NumberDialog.newInstance(3));
        } else {

        }
    }

    @OnFocusChange(R.id.firplaces_edt)
    public void onFirplacesfocus(boolean hasFocus) {
        if (hasFocus) {
            onDialogClick(NumberDialog.newInstance(4));
        } else {

        }
    }

    private void onDialogClick(android.support.v4.app.DialogFragment dialogFragment) {
        if (dialogFragment instanceof android.support.v4.app.DialogFragment)
            dialogFragment.show(this.getChildFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName().toString());
    }

    @Override
    public void viewAddListing(AddListingModel addListingModel) {
        Log.d(TAG, "The Added Listing  " + addListingModel.getId());
        super.showToastMessage("Your listing has been added to our database successfully.");
    }

    @Override
    public void onNumberDialogClick(int InitialIndex, int value) {
        Log.d(TAG, "The Value Get From NumberDialog " + value);
        switch (InitialIndex) {
            case 1:
                full_bath_edt.setText(String.valueOf(value));
//                addListingData.setBathsfull(String.valueOf(value));
                break;
            case 2:
                half_bath_edt.setText(String.valueOf(value));
                break;
            case 3:
                bedrooms_edt.setText(String.valueOf(value));
                break;
            case 4:
                firplaces_edt.setText(String.valueOf(value));
                break;

        }
    }

}
