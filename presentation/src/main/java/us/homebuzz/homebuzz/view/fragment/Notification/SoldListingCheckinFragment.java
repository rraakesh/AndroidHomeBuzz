package us.homebuzz.homebuzz.view.fragment.Notification;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.LinearLayout;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.GpsLocation;
import us.homebuzz.homebuzz.view.component.AutoLoadImageView;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by rohitkumar on 4/12/15.
 */
public class SoldListingCheckinFragment extends BaseFragment  {
    private static final String TAG = SoldListingCheckinFragment.class.getSimpleName();
    @Bind(R.id.last_sold_price_txt)
    TextView lastsoldprice;
    @Bind(R.id.Last_sold_price)
    TextView LastSoldPrice;
    @Bind(R.id.remarks_txt)
    TextView remarks;
    @Bind(R.id.txtEstimate)
    TextView txtEstimate;
    @Bind(R.id.rate_text)
    TextView price;
    @Bind(R.id.bedsbaths)
    TextView bedsbaths;
    @Bind(R.id.zipcode)
    TextView Zipcode;
    @Bind(R.id.address)
    TextView Address;
    @Bind(R.id.lot_acres)
    TextView LotAcres;
    @Bind(R.id.lot_acr)
    TextView lot_acr;
    @Bind(R.id.fireplace)
    TextView Fireplace;
    @Bind(R.id.year_built_text)
    TextView year;
    @Bind(R.id.txtReview)
    TextView txtReview;
    @Bind(R.id.previous_condition_rating)
    RatingBar previous_condition_rating;
    @Bind(R.id.estimate)
    TextView Estimates;
    @Bind(R.id.park_sp)
    TextView park_sp;
    @Bind(R.id.image)
    AutoLoadImageView image;
    @Bind(R.id.Est_Sq_ft)
    TextView Est_Sq_ft;
    @Bind(R.id.park_space)
    TextView park_space;
    @Bind(R.id.fireplaces)
    TextView fireplaces;
    @Bind(R.id.Est_Sq_txt)
    TextView Est_Sq_txt;
    @Bind(R.id.estimate_layout)
    LinearLayout estimate_layout;
    @Bind(R.id.review_layout)
    LinearLayout review_layout;
    @Bind(R.id.lot_Est_Sq_ft)
    TextView lot_Est_Sq_ft;
    @Bind(R.id.lot_Est_Sq_txt)
    TextView lot_Est_Sq_txt;
    @Bind(R.id.photo)
    LinearLayout Photo;
    @Bind(R.id.city_txt)
    TextView city;
    @Bind(R.id.state_txt)
    TextView state;
    @Bind(R.id.zipcode_txt)
    TextView zip;

    @Bind(R.id.update_checkin)
    LinearLayout update_checkin;
    @Inject
    public GpsLocation gpsLocation;
    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    Picasso picasso;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View soldlisting = inflater.inflate(R.layout.check_in_estimate,container,false);
        return  soldlisting;
    }
}
