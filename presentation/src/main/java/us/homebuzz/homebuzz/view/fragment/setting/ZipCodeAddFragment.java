package us.homebuzz.homebuzz.view.fragment.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.WeakHashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import us.homebuzz.homebuzz.R;
import us.homebuzz.homebuzz.Utility.Constants;
import us.homebuzz.homebuzz.di.components.MainActivityComponent;
import us.homebuzz.homebuzz.di.modules.SettingModule;
import us.homebuzz.homebuzz.model.ZipAddModel;
import us.homebuzz.homebuzz.presenter.ZipCodeAddPresenter;
import us.homebuzz.homebuzz.view.ZipCodeAddView;
import us.homebuzz.homebuzz.view.activity.MainActivity;
import us.homebuzz.homebuzz.view.fragment.BaseFragment;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class ZipCodeAddFragment extends BaseFragment implements ZipCodeAddView {
    private static final String TAG = ZipCodeAddFragment.class.getSimpleName();

    public static ZipCodeAddFragment newInstance() {
        ZipCodeAddFragment zipCodeAdd = new ZipCodeAddFragment();
        return zipCodeAdd;
    }

    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.edZipCode)
    EditText edZipCode;
    @Bind(R.id.add_button)
    Button add_button;
    @Bind(R.id.txtZip)
    TextView txtZip;
    @Inject
    ZipCodeAddPresenter zipCodeAddPresenter;
    @Inject
    Constants constants;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View zip_add = inflater.inflate(R.layout.zip_code_add, container, false);
        ButterKnife.bind(this, zip_add);
        return zip_add;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((TextView) ((MainActivity) this.getActivity()).toolbar.findViewById(R.id.toolbar_title)).setText("Add Zip");
        this.initialize();
        setUI();
    }

    private void setUI() {
        add_button.setText("Add");
        txtZip.setText("Zip Code");
    }

    private void initialize() {
        this.getComponent(MainActivityComponent.class).settingComponent(new SettingModule()).inject(this);
        this.zipCodeAddPresenter.setView(this);
    }

    @OnClick(R.id.add_button)
    public void OnButtonClick() {
        hideKeyboard();
        if (edZipCode.getText().toString().isEmpty()) {
            showToastMessage("Please enter the zip code");
        } else {
            WeakHashMap<String, String> data = new WeakHashMap<>(1);
            data.put(constants.ADD_NEW_ZIP, edZipCode.getText().toString());
            zipCodeAddPresenter.initialize(data);
        }
    }

    @Override
    public void viewZipAdd(ZipAddModel zipAddModel) {
        showToastMessage("ZipCode Added Successfully");
        getActivity().onBackPressed();
    }

    @Override
    public void viewDetails(ZipAddModel ZipAddModel) {

    }

    @Override
    public void onResume() {
        super.onResume();
        this.zipCodeAddPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.zipCodeAddPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.zipCodeAddPresenter.destroy();
    }

    @Override
    public void showError(String message) {
        super.showToastMessage(message);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
