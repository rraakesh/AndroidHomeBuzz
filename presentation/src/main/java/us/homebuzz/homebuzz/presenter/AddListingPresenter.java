package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingPhoto;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetAddListing;
import us.homebuzz.homebuzz.domain.interactor.GetAddListingPhoto;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserAddListingDataMapper;
import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;
import us.homebuzz.homebuzz.view.AddListingView;

/**
 * Created by amit.singh on 11/5/2015.
 */
@PerActivity
public class AddListingPresenter implements Presenter {
    public static String TAG = UserRegPresenter.class.getClass().getSimpleName();
    /**
     * WeakHashMap  used to Register user
     */

    private AddListingView viewUserView;
    private GetAddListing getAddListingUserCase;
    private GetAddListingPhoto getAddListingPhotoUseCase;
    private final UserAddListingDataMapper userAddListingDataMapper;

    @Inject
    public AddListingPresenter(@Named("AddListing") GetAddListing getAddListingUserCase,@Named("AddListingPhoto") GetAddListingPhoto getAddListingPhotoUseCase,
                               UserAddListingDataMapper userAddListingDataMapper) {
        this.getAddListingUserCase = getAddListingUserCase;
        this.getAddListingPhotoUseCase = getAddListingPhotoUseCase;
        this.userAddListingDataMapper = userAddListingDataMapper;
    }

    public void setView(@NonNull AddListingView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getAddListingUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> userData) {
        //TODO FIND A BETTER WAY TO INJECT THIS VARIABLE TO DEPENDENCY
        getAddListingUserCase.setUserAddListing(userData);
        this.loadUserDetails();
    }
    public void initialize(String listing_id,WeakHashMap<String, String> multipartFiles,WeakHashMap<String, String> multipartData){
        getAddListingPhotoUseCase.setUserAddListingPhoto(listing_id,multipartFiles, multipartData);
        this.postPhoto();
    }

    /**
     * Adding the Photo with data Or without Data
     */
    private void postPhoto() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserPhoto();
    }


    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(AddListingDomain user) {
        final AddListingModel userModel = this.userAddListingDataMapper.transform(user);
        this.viewUserView.viewAddListing(userModel);
    }
    private void showUserAddedphotoInView(AddListingPhoto user) {
        final AddListingPhotoModel userModel = this.userAddListingDataMapper.transform(user);
        System.out.println("Add Listing photo Presenter"+user.getUrl()+"Model"+userModel.getUrl());
        this.viewUserView.viewAddListingPhoto(userModel);
    }
    private void getUserPhoto() {
        this.getAddListingPhotoUseCase.execute(new UserAddPhotoubscriber());
    }

    private void getUserDetails() {
        this.getAddListingUserCase.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<AddListingDomain> {

        @Override
        public void onCompleted() {
            AddListingPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            AddListingPresenter.this.hideViewLoading();
            AddListingPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            AddListingPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(AddListingDomain user) {
            AddListingPresenter.this.showUserDetailsInView(user);
        }
    }

    private final class UserAddPhotoubscriber extends DefaultSubscriber<AddListingPhoto> {

        @Override
        public void onCompleted() {
            AddListingPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            AddListingPresenter.this.hideViewLoading();
            AddListingPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            AddListingPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(AddListingPhoto user) {
            AddListingPresenter.this.showUserAddedphotoInView(user);
        }
    }
}