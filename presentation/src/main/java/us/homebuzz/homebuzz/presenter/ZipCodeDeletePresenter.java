package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.ZipCodeDeleteDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetZipDeleteInteractor;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.ZipDeleteMapper;
import us.homebuzz.homebuzz.model.ZipCodeDeleteModel;
import us.homebuzz.homebuzz.view.ZipCodeDeleteView;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipCodeDeletePresenter  extends DefaultSubscriber<ZipCodeDeleteModel> implements Presenter {
    public static String TAG = ZipCodeDeletePresenter.class.getClass().getSimpleName();

    private ZipCodeDeleteView viewUserView;
    private GetZipDeleteInteractor getZipDeleteInteractor;
    private final ZipDeleteMapper zipDeleteMapper;

    @Inject
    public ZipCodeDeletePresenter(@Named("DeleteZipCode") GetZipDeleteInteractor getSubscriptionInteractor,
                                  ZipDeleteMapper userModelDataMapper) {
        this.getZipDeleteInteractor = getSubscriptionInteractor;
        this.zipDeleteMapper = userModelDataMapper;
    }

    public void setView(@NonNull ZipCodeDeleteView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getZipDeleteInteractor.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> subPlanData) {
        getZipDeleteInteractor.setZipDelete(subPlanData);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(ZipCodeDeleteDomain zipCodeDeleteDomain) {
        final ZipCodeDeleteModel zipCodeDeleteModel = this.zipDeleteMapper.transform(zipCodeDeleteDomain);
        this.viewUserView.viewZipDelete(zipCodeDeleteModel);
    }

    private void getUserDetails() {
        this.getZipDeleteInteractor.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<ZipCodeDeleteDomain> {

        @Override
        public void onCompleted() {
            ZipCodeDeletePresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            ZipCodeDeletePresenter.this.hideViewLoading();
            ZipCodeDeletePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            ZipCodeDeletePresenter.this.showViewRetry();
        }

        @Override
        public void onNext(ZipCodeDeleteDomain user) {
            ZipCodeDeletePresenter.this.showUserDetailsInView(user);
        }
    }
}