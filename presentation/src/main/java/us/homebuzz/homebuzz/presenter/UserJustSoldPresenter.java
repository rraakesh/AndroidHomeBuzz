package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.listing.JustSoldDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetJustSoldListing;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserJustSoldDataMapper;
import us.homebuzz.homebuzz.model.UserJustSoldModel;
import us.homebuzz.homebuzz.view.UseJustSoldView;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class UserJustSoldPresenter extends DefaultSubscriber<List<UserJustSoldModel>> implements Presenter {
    public static String TAG = UserJustSoldPresenter.class.getClass().getSimpleName();
    private UseJustSoldView viewUserView;
    private final GetJustSoldListing getJustSoldListing;
    private final UserJustSoldDataMapper userJustSoldDataMapper;

    @Inject
    public UserJustSoldPresenter(@Named("justSoldListing") GetJustSoldListing getJustSoldListing,
                                 UserJustSoldDataMapper userJustSoldDataMapper) {
        this.getJustSoldListing = getJustSoldListing;
        this.userJustSoldDataMapper = userJustSoldDataMapper;
    }


    public void setView(@NonNull UseJustSoldView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getJustSoldListing.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String> justSoldData) {
        this.getJustSoldListing.setUserJustSold(justSoldData);
        this.loadUserAccuracy();
    }

    //
//    /**
//     * Loads user details.
//     * @param userAllMessagesModel
//     */
//    public void onUserClicked(UserAllMessagesModel userAllMessagesModel) {
//
//        this.viewUserView.viewUnreadMessages(userAllMessagesModel);
//    }
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(List<JustSoldDomain> allMessages) {
        final Collection<UserJustSoldModel> userJustSoldModelsList =
                this.userJustSoldDataMapper.transform(allMessages);
        this.viewUserView.viewJustSoldListing(userJustSoldModelsList);
    }

    private void getUserAllMessages() {
        this.getJustSoldListing.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<JustSoldDomain>> {

        @Override
        public void onCompleted() {
            UserJustSoldPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            UserJustSoldPresenter.this.hideViewLoading();
            UserJustSoldPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserJustSoldPresenter.this.showViewRetry();
        }


        @Override
        public void onNext(List<JustSoldDomain> allMessages) {
            UserJustSoldPresenter.this.showUserAllMessagesInView(allMessages);
        }
    }
}