package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetFollowUnfollow;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.FollowUnfollowMapper;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.view.FollowUnfollowView;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
@PerActivity
public class FollowUnfollowPresenter implements Presenter {
    public static String TAG = FollowUnfollowPresenter.class.getSimpleName();

    private FollowUnfollowView followUnfollowView;
    private GetFollowUnfollow getFollowUnfollow;
    private final FollowUnfollowMapper followUnfollowMapper;

    @Inject
    public FollowUnfollowPresenter(@Named("FollowUnfollow") GetFollowUnfollow getFollowUnfollow,
                                   FollowUnfollowMapper followUnfollowMapper) {
        this.getFollowUnfollow = getFollowUnfollow;
        this.followUnfollowMapper = followUnfollowMapper;
    }

    public void setView(@NonNull FollowUnfollowView view) {
        this.followUnfollowView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getFollowUnfollow.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(int id) {
        getFollowUnfollow.setUser_id(id);
        this.loadUserAccuracy();
    }


    /**
     * Loads user details.
     */
    public void onUserClicked(FollowUnfollowModel followUnfollowModel) {

        this.followUnfollowView.viewUser(followUnfollowModel);
    }

    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.setGetUserEstimate();
    }

    private void showViewLoading() {
        this.followUnfollowView.showLoading();
    }

    private void hideViewLoading() {
        this.followUnfollowView.hideLoading();
    }

    private void showViewRetry() {
        this.followUnfollowView.showRetry();
    }

    private void hideViewRetry() {
        this.followUnfollowView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.followUnfollowView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.followUnfollowView.showError(errorMessage);
    }

    private void followUnfollowView(FollowUnfollowData folllowUnfollow) {
        final FollowUnfollowModel folllowUnfollowModel = this.followUnfollowMapper.transform(folllowUnfollow);
        this.followUnfollowView.renderFollowUnfollow(folllowUnfollowModel);
    }

    private void setGetUserEstimate() {
        this.getFollowUnfollow.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<FollowUnfollowData> {

        @Override
        public void onCompleted() {
            FollowUnfollowPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            FollowUnfollowPresenter.this.hideViewLoading();
            FollowUnfollowPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            FollowUnfollowPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(FollowUnfollowData followUnfollowData) {
            FollowUnfollowPresenter.this.followUnfollowView(followUnfollowData);
        }
    }
}
