package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.friends.followed.FriendFollowedDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserFriendFollowedDataMapper;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.view.UserFrFriendsView;

/**
 * Created by amit.singh on 11/3/2015.
 */
@PerActivity
public class FriendsFollowedPresenter extends DefaultSubscriber<List<UserFriendFollowedModel>> implements Presenter {
    public static String TAG = FriendsFollowedPresenter.class.getClass().getSimpleName();
    private UserFrFriendsView viewUserView;
    private final UseCase getUsersFollowedUserCase;
    private final UserFriendFollowedDataMapper userFriendFollowedDataMapper;
    @Inject
    public FriendsFollowedPresenter(@Named("FriendsFollowedList") UseCase getUsersFollowedUserCase,
                                    UserFriendFollowedDataMapper userFriendFollowedDataMapper) {
        this.getUsersFollowedUserCase = getUsersFollowedUserCase;
        this.userFriendFollowedDataMapper = userFriendFollowedDataMapper;
    }


    public void setView(@NonNull UserFrFriendsView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUsersFollowedUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize() {
        this.loadUserFollowed();
    }

    /*/**
         * Loads user details.
         * @param UserFriendFollowedModel
         */
    public void onUserClicked(UserFriendFollowedModel userFriendFollowedModel) {

        this.viewUserView.viewUser(userFriendFollowedModel);
    }
    private void loadUserFollowed() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUseFoollowed();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(List<FriendFollowedDomain> friendFollowedDomains) {
        final Collection<UserFriendFollowedModel> userFriendFollowedModelCollection =
                this.userFriendFollowedDataMapper.transform(friendFollowedDomains);
        this.viewUserView.renderUserEstimateList(userFriendFollowedModelCollection);
    }

    private void getUseFoollowed() {
        this.getUsersFollowedUserCase.execute(new UserFollowedSubscriber());
    }

    private final class UserFollowedSubscriber extends DefaultSubscriber<List<FriendFollowedDomain>> {

        @Override public void onCompleted() {
            FriendsFollowedPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            FriendsFollowedPresenter.this.hideViewLoading();
            FriendsFollowedPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            FriendsFollowedPresenter.this.showViewRetry();
        }


        @Override public void onNext(List<FriendFollowedDomain> friendFollowedDomains) {
            FriendsFollowedPresenter.this.showUserAllMessagesInView(friendFollowedDomains);
        }
    }

}
