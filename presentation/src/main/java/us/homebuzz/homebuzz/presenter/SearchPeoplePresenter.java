package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.searchPeople.SearchPeopleDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetSearchPeople;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.SearchPeopleDataMapper;
import us.homebuzz.homebuzz.model.SearchPeopleModel;
import us.homebuzz.homebuzz.view.SearchPeopleView;

/**
 * Created by amit.singh on 12/8/2015.
 */
public class SearchPeoplePresenter  extends DefaultSubscriber<List<SearchPeopleModel>> implements Presenter{
    public static String TAG = SearchPeoplePresenter.class.getClass().getSimpleName();

    private SearchPeopleView viewUserView;
    private GetSearchPeople getUserProfile;
    private final SearchPeopleDataMapper userModelDataMapper;

    @Inject
    public SearchPeoplePresenter(@Named("PeopleSearch") GetSearchPeople getUserProfile,
                                 SearchPeopleDataMapper userModelDataMapper) {
        this.getUserProfile = getUserProfile;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull SearchPeopleView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUserProfile.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String> profiledata) {
        getUserProfile.setUserSearch(profiledata);
        this.loadUserDetails();
    }
    public void onUserClicked(SearchPeopleModel SearchPeopleModel ) {

        this.viewUserView.viewUser(SearchPeopleModel);
    }
    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(List<SearchPeopleDomain> user) {
        final Collection<SearchPeopleModel> userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.viewSearchPeople(userModel);
    }

    private void getUserDetails() {
        this.getUserProfile.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<List<SearchPeopleDomain> >{

        @Override public void onCompleted() {
            SearchPeoplePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            SearchPeoplePresenter.this.hideViewLoading();
            SearchPeoplePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            SearchPeoplePresenter.this.showViewRetry();
        }

        @Override public void onNext(List<SearchPeopleDomain> user) {
            SearchPeoplePresenter.this.showUserDetailsInView(user);
        }
    }
}


