package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.message.AllMessages;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserAllMessagesDataMapper;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.view.AllMessageView;

/**
 * Created by amit.singh on 10/22/2015.
 */
@PerActivity
public class AllMessagesPresenter extends DefaultSubscriber<List<UserAllMessagesModel>> implements Presenter {
    public static String TAG = AllMessagesPresenter.class.getClass().getSimpleName();
    private AllMessageView viewUserView;
    private final UseCase getUsersAllMessagesUserCase;
    private final UserAllMessagesDataMapper userAllMessagesDataMapper;
    @Inject
    public AllMessagesPresenter(@Named("AllMessagesList") UseCase getUsersAllMessagesUserCase,
                                UserAllMessagesDataMapper userAllMessagesDataMapper) {
        this.getUsersAllMessagesUserCase = getUsersAllMessagesUserCase;
        this.userAllMessagesDataMapper = userAllMessagesDataMapper;
    }


    public void setView(@NonNull AllMessageView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUsersAllMessagesUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize() {
        this.loadUserAccuracy();
    }

    /**
     * Loads user details.
     * @param userAllMessagesModel
     */
    public void onUserClicked(UserAllMessagesModel userAllMessagesModel) {

        this.viewUserView.viewUser(userAllMessagesModel);
    }
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(List<AllMessages> allMessages) {
        final Collection<UserAllMessagesModel> userNearbyModelList =
                this.userAllMessagesDataMapper.transform(allMessages);
        this.viewUserView.renderUserAllMessageList(userNearbyModelList);
    }

    private void getUserAllMessages() {
        this.getUsersAllMessagesUserCase.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<AllMessages>> {

        @Override public void onCompleted() {
            AllMessagesPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            AllMessagesPresenter.this.hideViewLoading();
            AllMessagesPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            AllMessagesPresenter.this.showViewRetry();
        }


        @Override public void onNext(List<AllMessages> allMessages) {
            AllMessagesPresenter.this.showUserAllMessagesInView(allMessages);
        }
    }

}
