package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.zipcode.UpdateZipCodeDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetUpdatedZipCode;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserUpdateZipCodeDataMapper;
import us.homebuzz.homebuzz.model.UpdateZipcodeModel;
import us.homebuzz.homebuzz.view.ZipUpdateView;

/**
 * Created by amit.singh on 12/15/2015.
 */
public class ZipCodeUpdatePresenter extends DefaultSubscriber<UpdateZipCodeDomain> implements Presenter{
    public static String TAG = ZipCodeUpdatePresenter.class.getClass().getSimpleName();
    private ZipUpdateView viewUserView;
    private final GetUpdatedZipCode getZipCodePurchaseUseCase;
    private final UserUpdateZipCodeDataMapper userZipPurchaseMapper;
    @Inject
    public ZipCodeUpdatePresenter(@Named("ZipCodeUpdate") GetUpdatedZipCode getZipCodePurchaseUseCase,
                                  UserUpdateZipCodeDataMapper userZipPurchaseMapper) {
        this.getZipCodePurchaseUseCase = getZipCodePurchaseUseCase;
        this.userZipPurchaseMapper = userZipPurchaseMapper;
    }


    public void setView(@NonNull ZipUpdateView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getZipCodePurchaseUseCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String>zipData) {
        this.getZipCodePurchaseUseCase.setUpdateZipCode(zipData);
        this.loadUserAccuracy();
    }
/*
    public void onUserClicked(UserAccuracyModel userAccuracyModel) {

        this.viewUserView.viewUser(userAccuracyModel);
    }*/
    /**
     * Loads user details.
     */
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAccuracy();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAccuracyInView(UpdateZipCodeDomain userAccuracyDomainList) {
        final UpdateZipcodeModel userNearbyModelList =
                this.userZipPurchaseMapper.transform(userAccuracyDomainList);
        this.viewUserView.renderAfterUpdateZip(userNearbyModelList);
    }

    private void getUserAccuracy() {
        this.getZipCodePurchaseUseCase.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<UpdateZipCodeDomain> {

        @Override public void onCompleted() {
            ZipCodeUpdatePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            ZipCodeUpdatePresenter.this.hideViewLoading();
            ZipCodeUpdatePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            ZipCodeUpdatePresenter.this.showViewRetry();
        }


        @Override public void onNext(UpdateZipCodeDomain userAccuracyDomainList) {
            ZipCodeUpdatePresenter.this.showUserAccuracyInView(userAccuracyDomainList);
        }
    }
}
