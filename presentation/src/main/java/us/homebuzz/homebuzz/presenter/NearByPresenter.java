package us.homebuzz.homebuzz.presenter;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.SortedMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.NearbyDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetNearbyListing;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserNearbyDataMapper;
import us.homebuzz.homebuzz.model.UserNearbyModel;
import us.homebuzz.homebuzz.view.UserNearbyView;

/**
 * Created by amit.singh on 10/13/2015.
 */
@PerActivity
public class NearByPresenter extends DefaultSubscriber<List<UserNearbyModel>>implements Presenter {
    public static String TAG = NearByPresenter.class.getClass().getSimpleName();
    private UserNearbyView viewUserView;
    private GetNearbyListing getUserNearbyCase;
    private final UserNearbyDataMapper userModelDataMapper;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    public NearByPresenter(@Named("nearbyListing") GetNearbyListing getUserNearbyCase,
                           UserNearbyDataMapper userModelDataMapper) {
        this.getUserNearbyCase = getUserNearbyCase;
        this.userModelDataMapper = userModelDataMapper;
    }


    public void setView(@NonNull UserNearbyView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUserNearbyCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(int user_id,SortedMap<String,String> nearbyData/*String lat,String lon,String radius*/) {
        Log.d(TAG,"Getting the lat"+nearbyData.size());
        //TODO FIND A BETTER WAY TO INJECT THIS VARIABLE TO DEPENDENCY OR MAKE A WRAPPER OF THIS
        getUserNearbyCase.setNearbyData(/*lat, lon, radius*/user_id,nearbyData);
        this.loadUserDetails();
    }
    public void onUserClicked(UserNearbyModel userNearbyModel) {

       // this.viewUserView.viewUser(userNearbyModel);
    }
    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(List<NearbyDomain> nearbyDomainList) {
        final Collection<UserNearbyModel> userNearbyModelList =
                this.userModelDataMapper.transform(nearbyDomainList);
        this.viewUserView.renderUserNearbyList(userNearbyModelList);
    }

    private void getUserDetails() {
        this.getUserNearbyCase.execute(new UserNarbySubscriber());
    }

    private final class UserNarbySubscriber extends DefaultSubscriber<List<NearbyDomain>> {

        @Override public void onCompleted() {
            NearByPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            NearByPresenter.this.hideViewLoading();
            NearByPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            NearByPresenter.this.showViewRetry();
        }

        @Override public void onNext(List<NearbyDomain> userNearbyDomainList) {
            NearByPresenter.this.showUserDetailsInView(userNearbyDomainList);
        }
    }
}
