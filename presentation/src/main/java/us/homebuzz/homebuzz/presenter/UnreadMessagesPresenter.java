package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.message.unread.UnreadMessageDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UnreadMessageDataMapper;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.view.UnreadMessageview;

/**
 * Created by amit.singh on 11/18/2015.
 */
@PerActivity
public class UnreadMessagesPresenter extends DefaultSubscriber<List<UserUnreadMessagesModel>> implements Presenter {
    public static String TAG = UnreadMessagesPresenter.class.getClass().getSimpleName();
    private UnreadMessageview viewUserView;
    private final UseCase getUnreadMessage;
    private final UnreadMessageDataMapper userUnreadMessageDataMapper;
    @Inject
    public UnreadMessagesPresenter(@Named("UnreadMessages") UseCase getUnreadMessage,
                                   UnreadMessageDataMapper userUnreadMessageDataMapper) {
        this.getUnreadMessage = getUnreadMessage;
        this.userUnreadMessageDataMapper = userUnreadMessageDataMapper;
    }


    public void setView(@NonNull UnreadMessageview view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUnreadMessage.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize() {
        this.loadUserAccuracy();
    }
//
//    /**
//     * Loads user details.
//     * @param userAllMessagesModel
//     */
//    public void onUserClicked(UserAllMessagesModel userAllMessagesModel) {
//
//        this.viewUserView.viewUnreadMessages(userAllMessagesModel);
//    }
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(List<UnreadMessageDomain> allMessages) {
        final Collection<UserUnreadMessagesModel> userNearbyModelList =
                this.userUnreadMessageDataMapper.transform(allMessages);
        this.viewUserView.viewUnreadMessages(userNearbyModelList);
    }

    private void getUserAllMessages() {
        this.getUnreadMessage.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<UnreadMessageDomain>> {

        @Override public void onCompleted() {
            UnreadMessagesPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            UnreadMessagesPresenter.this.hideViewLoading();
            UnreadMessagesPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UnreadMessagesPresenter.this.showViewRetry();
        }


        @Override public void onNext(List<UnreadMessageDomain> allMessages) {
            UnreadMessagesPresenter.this.showUserAllMessagesInView(allMessages);
        }
    }

}
