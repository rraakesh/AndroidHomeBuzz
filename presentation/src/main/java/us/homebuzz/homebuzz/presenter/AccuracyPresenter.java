package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.AccuracyLeaderboardDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderByAccuracy;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserAccuracyDataMapper;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.view.UserAccuracyView;

/**
 * Created by amit.singh on 10/21/2015.
 */
@PerActivity
public class AccuracyPresenter extends DefaultSubscriber<List<UserAccuracyModel>> implements Presenter{
    public static String TAG = AccuracyPresenter.class.getClass().getSimpleName();
    private UserAccuracyView viewUserView;
    private final GetLeaderByAccuracy getUsersAccuracyUserCase;
    private final UserAccuracyDataMapper userAccuracyDataMapper;
    @Inject
    public AccuracyPresenter(@Named("LeaderBoardAccuracyList") GetLeaderByAccuracy getUsersAccuracyUserCase,
                           UserAccuracyDataMapper userAccuracyDataMapper) {
        this.getUsersAccuracyUserCase = getUsersAccuracyUserCase;
        this.userAccuracyDataMapper = userAccuracyDataMapper;
    }


    public void setView(@NonNull UserAccuracyView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUsersAccuracyUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String> filterData) {
        this.getUsersAccuracyUserCase.setFilterData(filterData);
        this.loadUserAccuracy();
    }

    public void onUserClicked(UserAccuracyModel userAccuracyModel) {

        this.viewUserView.viewUser(userAccuracyModel);
    }
    /**
     * Loads user details.
     */
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAccuracy();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAccuracyInView(List<AccuracyLeaderboardDomain> userAccuracyDomainList) {
        final Collection<UserAccuracyModel> userNearbyModelList =
                this.userAccuracyDataMapper.transform(userAccuracyDomainList);
        this.viewUserView.renderUserAccuracyList(userNearbyModelList);
    }

    private void getUserAccuracy() {
        this.getUsersAccuracyUserCase.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<AccuracyLeaderboardDomain>> {

        @Override public void onCompleted() {
            AccuracyPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            AccuracyPresenter.this.hideViewLoading();
            AccuracyPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            AccuracyPresenter.this.showViewRetry();
        }


        @Override public void onNext(List<AccuracyLeaderboardDomain> userAccuracyDomainList) {
            AccuracyPresenter.this.showUserAccuracyInView(userAccuracyDomainList);
        }
    }

}
