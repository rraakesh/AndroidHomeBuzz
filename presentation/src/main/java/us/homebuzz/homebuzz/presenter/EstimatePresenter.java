package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.EstimatesDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderByEstimates;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserEstimateDataMapper;
import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.view.UserEstimatesView;

/**
 * Created by amit.singh on 10/21/2015.
 */
@PerActivity
public class EstimatePresenter extends DefaultSubscriber<List<UserEstimateModel>> implements Presenter {
    public static String TAG = EstimatePresenter.class.getClass().getSimpleName();
    private UserEstimatesView viewUserView;
    private final GetLeaderByEstimates getUserEstimateUserCase;
    private final UserEstimateDataMapper userEstimateDataMapper;

    @Inject
    public EstimatePresenter(@Named("LeaderBoardEstimateList") GetLeaderByEstimates getUserEstimateUserCase,
                             UserEstimateDataMapper userEstimateDataMapper) {
        this.getUserEstimateUserCase = getUserEstimateUserCase;
        this.userEstimateDataMapper = userEstimateDataMapper;
    }


    public void setView(@NonNull UserEstimatesView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getUserEstimateUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String>filterData) {
        this.getUserEstimateUserCase.setFilterData(filterData);
        this.loadUserAccuracy();
    }


    /**
     * Loads user details.
     */
    public void onUserClicked(UserEstimateModel userEstimateModel) {

        this.viewUserView.viewUser(userEstimateModel);
    }
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.setGetUserEstimate();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAccuracyInView(List<EstimatesDomain> userAccuracyDomainList) {
        final Collection<UserEstimateModel> userNearbyModelList =
                this.userEstimateDataMapper.transform(userAccuracyDomainList);
        this.viewUserView.renderUserEstimateList(userNearbyModelList);
    }

    private void setGetUserEstimate() {
        this.getUserEstimateUserCase.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<EstimatesDomain>> {

        @Override
        public void onCompleted() {
            EstimatePresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            EstimatePresenter.this.hideViewLoading();
            EstimatePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            EstimatePresenter.this.showViewRetry();
        }


        @Override
        public void onNext(List<EstimatesDomain> userAccuracyDomainList) {
            EstimatePresenter.this.showUserAccuracyInView(userAccuracyDomainList);
        }
    }
}
