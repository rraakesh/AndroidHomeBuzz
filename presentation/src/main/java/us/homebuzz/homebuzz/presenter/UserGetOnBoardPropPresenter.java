package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.addListing.SearchAddressDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetOnBoardProp;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.SearchAddressMapper;
import us.homebuzz.homebuzz.model.SearchAdressModel;
import us.homebuzz.homebuzz.view.GetUserPropView;

/**
 * Created by amit.singh on 11/28/2015.
 */
public class UserGetOnBoardPropPresenter extends DefaultSubscriber<SearchAdressModel> implements Presenter {
    public static String TAG = UserGetOnBoardPropPresenter.class.getClass().getSimpleName();
    private GetUserPropView viewUserView;
    private final GetOnBoardProp getOnBoardProp;
    private final SearchAddressMapper searchAddressMapper;

    @Inject
    public UserGetOnBoardPropPresenter(@Named("GetProp") GetOnBoardProp getOnBoardProp,
                                       SearchAddressMapper searchAddressMapper) {
        this.getOnBoardProp = getOnBoardProp;
        this.searchAddressMapper = searchAddressMapper;
    }


    public void setView(@NonNull GetUserPropView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getOnBoardProp.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> justSoldData) {
        this.getOnBoardProp.setUserSearch(justSoldData);
        this.loadUserAccuracy();
    }

    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(SearchAddressDomain allMessages) {
        final SearchAdressModel userJustSoldModelsList =
                this.searchAddressMapper.transform(allMessages);
        this.viewUserView.viewGetProp(userJustSoldModelsList);
    }

    private void getUserAllMessages() {
        this.getOnBoardProp.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<SearchAddressDomain> {

        @Override
        public void onCompleted() {
            UserGetOnBoardPropPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            UserGetOnBoardPropPresenter.this.hideViewLoading();
            UserGetOnBoardPropPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserGetOnBoardPropPresenter.this.showViewRetry();
        }


        @Override
        public void onNext(SearchAddressDomain searchAdressModel) {
            UserGetOnBoardPropPresenter.this.showUserAllMessagesInView(searchAdressModel);
        }
    }
}
