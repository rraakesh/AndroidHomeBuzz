package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.estimateAverage.EstimateAverageDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetAverageEstimate;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.EstimateAverageDataMapper;
import us.homebuzz.homebuzz.model.EstimateAverageModel;
import us.homebuzz.homebuzz.view.EstimateAverageView;

/**
 * Created by amit.singh on 12/12/2015.
 */
public class EstimateAveragePresenter extends DefaultSubscriber<EstimateAverageModel> implements Presenter {
    public static String TAG = AllMessagesPresenter.class.getClass().getSimpleName();
    private EstimateAverageView viewUserView;
    private final GetAverageEstimate getUsersAllMessagesUserCase;
    private final EstimateAverageDataMapper userAllMessagesDataMapper;

    @Inject
    public EstimateAveragePresenter(@Named("EstimateAverage") GetAverageEstimate getUsersAllMessagesUserCase,
                                    EstimateAverageDataMapper userAllMessagesDataMapper) {
        this.getUsersAllMessagesUserCase = getUsersAllMessagesUserCase;
        this.userAllMessagesDataMapper = userAllMessagesDataMapper;
    }


    public void setView(@NonNull EstimateAverageView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getUsersAllMessagesUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> estimateAverageData) {
        this.getUsersAllMessagesUserCase.setEstimateAverage(estimateAverageData);
        this.loadUserAccuracy();
    }

    /*/**
     * Loads user details.
     * @param estimateAverageModel
     */
   /* public void onUserClicked(UserAllMessagesModel userAllMessagesModel) {

        this.viewUserView.(userAllMessagesModel);
    }*/
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(EstimateAverageDomain allMessages) {
        final EstimateAverageModel userNearbyModelList =
                this.userAllMessagesDataMapper.transform(allMessages);
        this.viewUserView.viewGetEstimateAverage(userNearbyModelList);
    }

    private void getUserAllMessages() {
        this.getUsersAllMessagesUserCase.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<EstimateAverageDomain> {

        @Override
        public void onCompleted() {
            EstimateAveragePresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            EstimateAveragePresenter.this.hideViewLoading();
            EstimateAveragePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            EstimateAveragePresenter.this.showViewRetry();
        }


        @Override
        public void onNext(EstimateAverageDomain allMessages) {
            EstimateAveragePresenter.this.showUserAllMessagesInView(allMessages);
        }
    }

}
