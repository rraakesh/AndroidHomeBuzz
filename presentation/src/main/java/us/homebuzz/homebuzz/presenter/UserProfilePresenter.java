package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.profile.ProfileDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetUserProfile;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserProfileDataMapper;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.view.UserProfileView;

/**
 * Created by amit.singh on 10/29/2015.
 */
@PerActivity
public class UserProfilePresenter extends DefaultSubscriber<UserProfileModel> implements Presenter{
    public static String TAG = LoginPresenter.class.getClass().getSimpleName();

    private UserProfileView viewUserView;
    private GetUserProfile getUserProfile;
    private final UserProfileDataMapper userModelDataMapper;

    @Inject
    public UserProfilePresenter(@Named("UserProfile") GetUserProfile getUserProfile,
                                UserProfileDataMapper userModelDataMapper) {
        this.getUserProfile = getUserProfile;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull UserProfileView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUserProfile.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(int User_id) {
        //TODO FIND A BETTER WAY TO INJECT THIS VARIABLE TO DEPENDENCY
        getUserProfile.setUserId(User_id);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(ProfileDomain user) {
        final UserProfileModel userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.viewUserProfile(userModel);
    }

    private void getUserDetails() {
        this.getUserProfile.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<ProfileDomain> {

        @Override public void onCompleted() {
            UserProfilePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            UserProfilePresenter.this.hideViewLoading();
            UserProfilePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserProfilePresenter.this.showViewRetry();
        }

        @Override public void onNext(ProfileDomain user) {
            UserProfilePresenter.this.showUserDetailsInView(user);
        }
    }
}
