package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.listing.JustListedDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetJustListedListing;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserJustListedDataMapper;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;
import us.homebuzz.homebuzz.view.UserJustListedView;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class UserJustListedPresenter extends DefaultSubscriber<List<UserUnreadMessagesModel>> implements Presenter {
    public static String TAG = UnreadMessagesPresenter.class.getClass().getSimpleName();
    private UserJustListedView viewUserView;
    private final GetJustListedListing getJustListedListing;
    private final UserJustListedDataMapper userJustListedDataMapper;

    @Inject
    public UserJustListedPresenter(@Named("justListedListing") GetJustListedListing getJustListedListing,
                                   UserJustListedDataMapper userJustListedDataMapper) {
        this.getJustListedListing = getJustListedListing;
        this.userJustListedDataMapper = userJustListedDataMapper;
    }


    public void setView(@NonNull UserJustListedView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getJustListedListing.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String> justListed) {
        this.getJustListedListing.setUserJustListed(justListed);
        this.loadUserAccuracy();
    }

    //
//    /**
//     * Loads user details.
//     * @param userAllMessagesModel
//     */
//    public void onUserClicked(UserAllMessagesModel userAllMessagesModel) {
//
//        this.viewUserView.viewUnreadMessages(userAllMessagesModel);
//    }
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAllMessages();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAllMessagesInView(List<JustListedDomain> allMessages) {
        final Collection<UserJustListedModel> userNearbyModelList =
                this.userJustListedDataMapper.transform(allMessages);
        this.viewUserView.viewJustListedListing(userNearbyModelList);
    }

    private void getUserAllMessages() {
        this.getJustListedListing.execute(new UserAccuracySubscriber());
    }

    private final class UserAccuracySubscriber extends DefaultSubscriber<List<JustListedDomain>> {

        @Override
        public void onCompleted() {
            UserJustListedPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            UserJustListedPresenter.this.hideViewLoading();
            UserJustListedPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserJustListedPresenter.this.showViewRetry();
        }


        @Override
        public void onNext(List<JustListedDomain> allMessages) {
            UserJustListedPresenter.this.showUserAllMessagesInView(allMessages);
        }
    }
}