package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.signup.User_Signup_Domain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetUserSignUp;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserSignUpDataMapper;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.view.UserSignupView;

/**
 * Created by amit.singh on 10/31/2015.
 */
public class UserRegPresenter implements Presenter {
    public static String TAG = UserRegPresenter.class.getClass().getSimpleName();
    /**
     * WeakHashMap  used to Register user
     */

    private UserSignupView viewUserView;
    private GetUserSignUp getUserSignUpUserCase;
    private final UserSignUpDataMapper userModelDataMapper;

    @Inject
    public UserRegPresenter(@Named("userRegistration") GetUserSignUp getUserSignUpUserCase,
                            UserSignUpDataMapper userModelDataMapper) {
        this.getUserSignUpUserCase = getUserSignUpUserCase;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull UserSignupView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getUserSignUpUserCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> userData, boolean isFbLogin) {
        //TODO FIND A BETTER WAY TO INJECT THIS VARIABLE TO DEPENDENCY
        getUserSignUpUserCase.setUserRegDetails(userData,isFbLogin);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        //  this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(User_Signup_Domain user) {
        final UserLoginModel userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.renderAfterUserSignUp(userModel);
    }

    private void getUserDetails() {
        this.getUserSignUpUserCase.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<User_Signup_Domain> {

        @Override
        public void onCompleted() {
            UserRegPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            UserRegPresenter.this.hideViewLoading();
            UserRegPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserRegPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(User_Signup_Domain user) {
            UserRegPresenter.this.showUserDetailsInView(user);
        }
    }
}
