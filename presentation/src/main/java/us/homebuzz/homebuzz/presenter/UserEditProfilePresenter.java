package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.EditUserProfile;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserUpdateDataMapper;
import us.homebuzz.homebuzz.model.UserProfileModel;
import us.homebuzz.homebuzz.model.UserUpdateModel;
import us.homebuzz.homebuzz.view.UserUpdateProfileView;

/**
 * Created by amit.singh on 11/13/2015.
 */
@PerActivity
public class UserEditProfilePresenter extends DefaultSubscriber<UserProfileModel> implements Presenter {
    public static String TAG = UserEditProfilePresenter.class.getClass().getSimpleName();

    private UserUpdateProfileView viewUserView;
    private EditUserProfile getUserProfile;
    private final UserUpdateDataMapper userModelDataMapper;

    @Inject
    public UserEditProfilePresenter(@Named("UserEditProfile") EditUserProfile getUserProfile,
                                    UserUpdateDataMapper userModelDataMapper) {
        this.getUserProfile = getUserProfile;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull UserUpdateProfileView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getUserProfile.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> userProfilePic, WeakHashMap<String, String> userData) {
        getUserProfile.setUserData(userProfilePic, userData);
        this.loadUserDetails();
    }



    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(UpdateProfileDomain user) {
        final UserUpdateModel userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.viewUserProfile(userModel);
    }

    private void getUserDetails() {
        this.getUserProfile.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<UpdateProfileDomain> {

        @Override
        public void onCompleted() {
            UserEditProfilePresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            UserEditProfilePresenter.this.hideViewLoading();
            UserEditProfilePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UserEditProfilePresenter.this.showViewRetry();
        }

        @Override
        public void onNext(UpdateProfileDomain user) {
            UserEditProfilePresenter.this.showUserDetailsInView(user);
        }
    }
}
