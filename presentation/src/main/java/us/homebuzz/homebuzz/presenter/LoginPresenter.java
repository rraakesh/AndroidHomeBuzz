package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetUserDetailsLogin;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserLoginDataMapper;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.view.UserLoginView;

/**
 * Created by amit.singh on 10/7/2015.
 * <p>
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class LoginPresenter implements Presenter {
    public static String TAG = LoginPresenter.class.getClass().getSimpleName();
    /**
     * Username and Password used to retrieve user details after validation
     */

    private UserLoginView viewUserView;
    private GetUserDetailsLogin getUserDetailsUseCase;
    private final UserLoginDataMapper userModelDataMapper;

    @Inject
    public LoginPresenter(@Named("userDetailsLogin") GetUserDetailsLogin getUserDetailsUseCase,
                          UserLoginDataMapper userModelDataMapper) {
        this.getUserDetailsUseCase = getUserDetailsUseCase;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull UserLoginView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getUserDetailsUseCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    /*public void initialize(String username,String password,String device_token) {
        //TODO FIND A BETTER WAY TO INJECT THIS VARIABLE TO DEPENDENCY
        getUserDetailsUseCase.setUserLoginDetails(username,password,device_token);
        this.loadUserDetails();
    }*/
    public void initialize(WeakHashMap<String, String> loginCredential, boolean isFbLogin) {
        getUserDetailsUseCase.setUserLoginDetails(loginCredential, isFbLogin);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(User_ user) {
        final UserLoginModel userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.renderAfterUserLogin(userModel);
    }

    private void getUserDetails() {
        this.getUserDetailsUseCase.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<User_> {

        @Override
        public void onCompleted() {
            LoginPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            LoginPresenter.this.hideViewLoading();
            LoginPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            LoginPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(User_ user) {
            LoginPresenter.this.showUserDetailsInView(user);
        }
    }
}
