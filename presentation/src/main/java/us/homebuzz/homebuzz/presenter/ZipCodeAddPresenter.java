package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodeAddDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetZipCodeAdd;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.ZipCodeAddMapper;
import us.homebuzz.homebuzz.model.ProfilePicModel;
import us.homebuzz.homebuzz.model.ZipAddModel;
import us.homebuzz.homebuzz.view.ZipCodeAddView;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class ZipCodeAddPresenter extends DefaultSubscriber<ProfilePicModel> implements Presenter {
    public static String TAG = ZipCodeAddPresenter.class.getClass().getSimpleName();

    private ZipCodeAddView viewUserView;
    private GetZipCodeAdd getZipCodeAdd;
    private final ZipCodeAddMapper userModelDataMapper;

    @Inject
    public ZipCodeAddPresenter(@Named("ZipCodeAdd") GetZipCodeAdd getZipCodeAdd,
                               ZipCodeAddMapper userModelDataMapper) {
        this.getZipCodeAdd = getZipCodeAdd;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull ZipCodeAddView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getZipCodeAdd.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> zipAddData) {
        getZipCodeAdd.setZipAdd(zipAddData);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(ZipCodeAddDomain user) {
        final ZipAddModel zipAddModel = this.userModelDataMapper.transform(user);
        this.viewUserView.viewZipAdd(zipAddModel);
    }

    private void getUserDetails() {
        this.getZipCodeAdd.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<ZipCodeAddDomain> {

        @Override
        public void onCompleted() {
            ZipCodeAddPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            ZipCodeAddPresenter.this.hideViewLoading();
            ZipCodeAddPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            ZipCodeAddPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(ZipCodeAddDomain user) {
            ZipCodeAddPresenter.this.showUserDetailsInView(user);
        }
    }
}

