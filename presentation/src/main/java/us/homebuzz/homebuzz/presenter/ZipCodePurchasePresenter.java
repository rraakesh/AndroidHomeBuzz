package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodePurchaseDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.UseCase;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserZipPurchaseMapper;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;
import us.homebuzz.homebuzz.view.ZipPurchaseView;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipCodePurchasePresenter extends DefaultSubscriber<List<ZipPurchaseModel>> implements Presenter{
    public static String TAG = ZipCodePurchasePresenter.class.getClass().getSimpleName();
    private ZipPurchaseView viewUserView;
    private final UseCase getZipCodePurchaseUseCase;
    private final UserZipPurchaseMapper userZipPurchaseMapper;
    @Inject
    public ZipCodePurchasePresenter(@Named("ZipCodePurchase") UseCase getZipCodePurchaseUseCase,
                                    UserZipPurchaseMapper userZipPurchaseMapper) {
        this.getZipCodePurchaseUseCase = getZipCodePurchaseUseCase;
        this.userZipPurchaseMapper = userZipPurchaseMapper;
    }


    public void setView(@NonNull ZipPurchaseView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getZipCodePurchaseUseCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize() {
        this.loadUserAccuracy();
    }
    public void onUserClicked(ZipPurchaseModel ZipPurchaseModel) {

        this.viewUserView.viewUser(ZipPurchaseModel);
    }
    public void initializeDelete() {
        this.loadDelete();
    }
    /**
     * Loads user details.
     */
    private void loadUserAccuracy() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserAccuracy();
    }
    private void loadDelete() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getDeleteZip();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAccuracyInView(List<ZipCodePurchaseDomain> userAccuracyDomainList) {
        final Collection<ZipPurchaseModel> userNearbyModelList =
                this.userZipPurchaseMapper.transform(userAccuracyDomainList);
        this.viewUserView.viewZipCodePurchaseList(userNearbyModelList);
    }

    private void getUserAccuracy() {
        this.getZipCodePurchaseUseCase.execute(new UserAccuracySubscriber());
    }
    private void getDeleteZip() {
        this.getZipCodePurchaseUseCase.execute(new UserAccuracySubscriber());
    }
    private final class UserAccuracySubscriber extends DefaultSubscriber<List<ZipCodePurchaseDomain>> {

        @Override public void onCompleted() {
            ZipCodePurchasePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            ZipCodePurchasePresenter.this.hideViewLoading();
            ZipCodePurchasePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            ZipCodePurchasePresenter.this.showViewRetry();
        }


        @Override public void onNext(List<ZipCodePurchaseDomain> userAccuracyDomainList) {
            ZipCodePurchasePresenter.this.showUserAccuracyInView(userAccuracyDomainList);
        }
    }
}
