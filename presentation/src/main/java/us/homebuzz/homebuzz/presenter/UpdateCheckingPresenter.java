package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.UpdateChecking;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetUpdateChecking;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserUpdateCheckingMapper;
import us.homebuzz.homebuzz.model.UserUpdateCheckingModel;
import us.homebuzz.homebuzz.view.UpdateCheckingView;
import us.homebuzz.homebuzz.view.fragment.updateCheckin.UpdateCheckInEstimateFragment;

/**
 * Created by rohitkumar on 6/11/15.
 */
public class UpdateCheckingPresenter implements Presenter {
    private String TAG = UpdateCheckInEstimateFragment.class.getSimpleName();
    private UpdateCheckingView updateCheckinView;
    private GetUpdateChecking getUpdateCheckin;
    private final UserUpdateCheckingMapper userUpdateCheckinMapper;

    @Inject
    public UpdateCheckingPresenter(@Named("UpdateCheck_in") GetUpdateChecking getUpdateChecking, UserUpdateCheckingMapper userUpdateCheckinMapper) {
        this.getUpdateCheckin = getUpdateChecking;
        this.userUpdateCheckinMapper = userUpdateCheckinMapper;
    }

    public void SetView(@NonNull UpdateCheckingView view) {
        this.updateCheckinView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getUpdateCheckin.unsubscribe();
    }

    public void initialize(String listing_id, WeakHashMap<String, String> updateCheckin) {
        getUpdateCheckin.setUpdateChecking(listing_id, updateCheckin);
        this.loadUserDetails();
    }

    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUpdate();
    }

    private void showViewLoading() {
        this.updateCheckinView.showLoading();
    }

    private void hideViewLoading() {
        //this.updateCheckinView.hideLoading();
    }

    private void showViewRetry() {
        this.updateCheckinView.showRetry();
    }

    private void hideViewRetry() {
        this.updateCheckinView.hideRetry();
    }

    private void getUpdate() {
        this.getUpdateCheckin.execute(new UserUpdate());
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.updateCheckinView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.updateCheckinView.showError(errorMessage);
    }

    private void showUpdateCheckin(UpdateChecking updateCheckin) {
        final UserUpdateCheckingModel userUpdateCheckinModel = this.userUpdateCheckinMapper.transfrom(updateCheckin);
        this.updateCheckinView.renderAfterUpdateCheckin(userUpdateCheckinModel);
    }

    private final class UserUpdate extends DefaultSubscriber<UpdateChecking> {
        @Override
        public void onCompleted() {
            UpdateCheckingPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            UpdateCheckingPresenter.this.hideViewLoading();
            UpdateCheckingPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            UpdateCheckingPresenter.this.showViewRetry();
            e.printStackTrace();
        }

        @Override
        public void onNext(UpdateChecking updateChecking) {
            UpdateCheckingPresenter.this.showUpdateCheckin(updateChecking);
        }
    }
}
