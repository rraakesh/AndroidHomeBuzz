package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.profile.ProfilePicDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.EditProfilePic;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserProfilePicDataMapper;
import us.homebuzz.homebuzz.model.ProfilePicModel;
import us.homebuzz.homebuzz.view.ProfilePicUpdateView;

/**
 * Created by amit.singh on 12/3/2015.
 */
public class ProfilePicUpdatePresenter extends DefaultSubscriber<ProfilePicModel> implements Presenter{
    public static String TAG = UserProfilePicDataMapper.class.getClass().getSimpleName();

    private ProfilePicUpdateView viewUserView;
    private EditProfilePic getUserProfile;
    private final UserProfilePicDataMapper userModelDataMapper;

    @Inject
    public ProfilePicUpdatePresenter(@Named("ProfilePicUpdate") EditProfilePic getUserProfile,
                                     UserProfilePicDataMapper userModelDataMapper) {
        this.getUserProfile = getUserProfile;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull ProfilePicUpdateView view) {
        this.viewUserView = view;
    }

    @Override public void resume() {}

    @Override public void pause() {}

    @Override public void destroy() {
        this.getUserProfile.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String,String> profiledata) {
        getUserProfile.setUserProfileData(profiledata);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(ProfilePicDomain user) {
        final ProfilePicModel userModel = this.userModelDataMapper.transform(user);
        this.viewUserView.viewUpdateProfilePic(userModel);
    }

    private void getUserDetails() {
        this.getUserProfile.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<ProfilePicDomain> {

        @Override public void onCompleted() {
            ProfilePicUpdatePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            e.printStackTrace();
            ProfilePicUpdatePresenter.this.hideViewLoading();
            ProfilePicUpdatePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            ProfilePicUpdatePresenter.this.showViewRetry();
        }

        @Override public void onNext(ProfilePicDomain user) {
            ProfilePicUpdatePresenter.this.showUserDetailsInView(user);
        }
    }
}
