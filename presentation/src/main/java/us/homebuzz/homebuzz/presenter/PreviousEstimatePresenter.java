package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import us.homebuzz.homebuzz.domain.data.previousEstimate.PreviouseEstimate;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetLeaderPreviousEstimate;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserPreviousEstimateDataMapper;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;
import us.homebuzz.homebuzz.view.UserPreviousEstimateView;

/**
 * Created by amit.singh on 10/27/2015.
 */
public class PreviousEstimatePresenter extends DefaultSubscriber<List<UserPreviousEstimateModel>> implements Presenter {
    public static String TAG = PreviousEstimatePresenter.class.getClass().getSimpleName();
    private UserPreviousEstimateView viewUserView;
    private final GetLeaderPreviousEstimate getPreviousEstimateUseCase;
    private final UserPreviousEstimateDataMapper userPreviousEstimateDataMapper;

    @Inject @Singleton
    public PreviousEstimatePresenter(@Named("LeaderBoardPreviousEstimateList") GetLeaderPreviousEstimate getPreviousEstimateUseCase,
                                     UserPreviousEstimateDataMapper userPreviousEstimateDataMapper) {
        this.getPreviousEstimateUseCase = getPreviousEstimateUseCase;
        this.userPreviousEstimateDataMapper = userPreviousEstimateDataMapper;
    }


    public void setView(@NonNull UserPreviousEstimateView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getPreviousEstimateUseCase.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(int user_id) {
        this.getPreviousEstimateUseCase.setUser_id(String.valueOf(user_id));
        this.loadUserPreviousEstimate();
    }


    /**
     * Loads  details.
     */
    public void onUserClicked(UserPreviousEstimateModel userPreviousEstimateModel) {

        this.viewUserView.viewDetails(userPreviousEstimateModel);
    }
    private void loadUserPreviousEstimate() {
        this.hideViewRetry();
        this.showViewLoading();
        this.setGetUserPreviousEstimate();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserPreviousEstimateInView(List<PreviouseEstimate> userPreviouseEstimateList) {
        final Collection<UserPreviousEstimateModel> userNearbyModelList =
                this.userPreviousEstimateDataMapper.transform(userPreviouseEstimateList);
        this.viewUserView.readerPreviousEstimate(userNearbyModelList);
    }

    private void setGetUserPreviousEstimate() {
        this.getPreviousEstimateUseCase.execute(new UserPreviousEstimateSubscriber());
    }

    private final class UserPreviousEstimateSubscriber extends DefaultSubscriber<List<PreviouseEstimate>> {

        @Override
        public void onCompleted() {
            PreviousEstimatePresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            PreviousEstimatePresenter.this.hideViewLoading();
            PreviousEstimatePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            PreviousEstimatePresenter.this.showViewRetry();
        }


        @Override
        public void onNext(List<PreviouseEstimate> userAccuracyDomainList) {
            PreviousEstimatePresenter.this.showUserPreviousEstimateInView(userAccuracyDomainList);
        }
    }
}
