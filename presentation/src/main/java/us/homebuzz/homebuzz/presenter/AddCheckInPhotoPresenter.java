package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.AddCheckInPhotoData;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetAddCheckInPhoto;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserUpdateCheckingMapper;
import us.homebuzz.homebuzz.model.AddCheckInPhotoModel;
import us.homebuzz.homebuzz.view.AddCheckInPhotoView;

/**
 * Created by abhishek.singh on 12/2/2015.
 */
public class AddCheckInPhotoPresenter implements Presenter {
    public static String TAG = AddCheckInPhotoPresenter.class.getClass().getSimpleName();

    private AddCheckInPhotoView viewUserView;
    private GetAddCheckInPhoto getAddListingPhotoUseCase;
    private final UserUpdateCheckingMapper userAddListingDataMapper;

    @Inject
    public AddCheckInPhotoPresenter(@Named("AddCheckInPhoto") GetAddCheckInPhoto getAddListingPhotoUseCase,
                                    UserUpdateCheckingMapper userAddListingDataMapper) {
        this.getAddListingPhotoUseCase = getAddListingPhotoUseCase;
        this.userAddListingDataMapper = userAddListingDataMapper;
    }

    public void setView(@NonNull AddCheckInPhotoView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getAddListingPhotoUseCase.unsubscribe();
    }


    public void initialize(String listing_id, WeakHashMap<String, String> multipartFiles, WeakHashMap<String, String> multipartData) {
        getAddListingPhotoUseCase.setUserAddCheckInPhoto(listing_id, multipartFiles, multipartData);
        this.postPhoto();
    }

    /**
     * Adding the Photo with data Or without Data
     */
    private void postPhoto() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserPhoto();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserAddedphotoInView(AddCheckInPhotoData user) {
        final AddCheckInPhotoModel userModel = this.userAddListingDataMapper.transform(user);
        System.out.println("Add Listing photo Presenter" + user.getUrl() + "Model" + userModel.getUrl());
        this.viewUserView.viewAddCheckInPhoto(userModel);
    }

    private void getUserPhoto() {
        this.getAddListingPhotoUseCase.execute(new UserAddPhotoubscriber());
    }


    private final class UserAddPhotoubscriber extends DefaultSubscriber<AddCheckInPhotoData> {

        @Override
        public void onCompleted() {
            AddCheckInPhotoPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            AddCheckInPhotoPresenter.this.hideViewLoading();
            AddCheckInPhotoPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            AddCheckInPhotoPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(AddCheckInPhotoData user) {
            AddCheckInPhotoPresenter.this.showUserAddedphotoInView(user);
        }
    }
}