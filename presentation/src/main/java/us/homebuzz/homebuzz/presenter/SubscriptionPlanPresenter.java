package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.SubscriptionDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetSubscriptionInteractor;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.SubscriptionPlanMapper;
import us.homebuzz.homebuzz.model.SubscriptionModel;
import us.homebuzz.homebuzz.view.SubcriptionPlanView;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class SubscriptionPlanPresenter extends DefaultSubscriber<SubscriptionModel> implements Presenter {
    public static String TAG = SubscriptionPlanPresenter.class.getClass().getSimpleName();

    private SubcriptionPlanView viewUserView;
    private GetSubscriptionInteractor getSubscriptionInteractor;
    private final SubscriptionPlanMapper userModelDataMapper;

    @Inject
    public SubscriptionPlanPresenter(@Named("SubscriptionPlan") GetSubscriptionInteractor getSubscriptionInteractor,
                                     SubscriptionPlanMapper userModelDataMapper) {
        this.getSubscriptionInteractor = getSubscriptionInteractor;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull SubcriptionPlanView view) {
        this.viewUserView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getSubscriptionInteractor.unsubscribe();
    }

    /**
     * Initializes the presenter by start retrieving user details.
     */
    public void initialize(WeakHashMap<String, String> subPlanData) {
        getSubscriptionInteractor.setSubPlan(subPlanData);
        this.loadUserDetails();
    }

    /**
     * Loads user details.
     */
    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserDetails();
    }

    private void showViewLoading() {
        this.viewUserView.showLoading();
    }

    private void hideViewLoading() {
        this.viewUserView.hideLoading();
    }

    private void showViewRetry() {
        this.viewUserView.showRetry();
    }

    private void hideViewRetry() {
        this.viewUserView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewUserView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.viewUserView.showError(errorMessage);
    }

    private void showUserDetailsInView(SubscriptionDomain subscriptionDomain) {
        final SubscriptionModel subscriptionModel = this.userModelDataMapper.transform(subscriptionDomain);
        this.viewUserView.viewSubPlan(subscriptionModel);
    }

    private void getUserDetails() {
        this.getSubscriptionInteractor.execute(new UserDetailsSubscriber());
    }

    private final class UserDetailsSubscriber extends DefaultSubscriber<SubscriptionDomain> {

        @Override
        public void onCompleted() {
            SubscriptionPlanPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            SubscriptionPlanPresenter.this.hideViewLoading();
            SubscriptionPlanPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            SubscriptionPlanPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(SubscriptionDomain user) {
            SubscriptionPlanPresenter.this.showUserDetailsInView(user);
        }
    }
}

