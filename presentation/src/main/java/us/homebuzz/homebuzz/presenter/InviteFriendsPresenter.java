package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.profile.InviteFriendsDomain;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetInviteFriends;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserInviteFriendsMapper;
import us.homebuzz.homebuzz.model.InviteFreindsModel;
import us.homebuzz.homebuzz.view.profile.InviteFriendsView;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class InviteFriendsPresenter implements Presenter {
    public static  String TAG = InviteFriendsPresenter.class.getSimpleName();
    private InviteFriendsView inviteFriendsView;
    private GetInviteFriends getInviteFriends;
    private final UserInviteFriendsMapper userInviteFriendsMapper;

    @Inject
    public InviteFriendsPresenter(@Named("InviteFriends") GetInviteFriends getInviteFriends,
                                  UserInviteFriendsMapper userInviteFriendsMapper){
        this.getInviteFriends = getInviteFriends;
        this.userInviteFriendsMapper = userInviteFriendsMapper;
    }
    public void setView(@NonNull InviteFriendsView view){
        this.inviteFriendsView = view;
    }
    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getInviteFriends.unsubscribe();
    }
    public void initilize(WeakHashMap<String, String> sendInvitation){
        getInviteFriends.setInviteFriends(sendInvitation);
        this.loadUserDetails();
    }
    public void loadUserDetails(){
        this.hideViewRetry();
        this.showViewLoading();
        this.getfreindsInvite();
    }
    public void hideViewRetry(){
        this.inviteFriendsView.hideLoading();
    }
    public void showViewLoading(){
        this.inviteFriendsView.showRetry();
    }
    public void getfreindsInvite(){
        this.getInviteFriends.execute(new UserInviteFriends() );
    }
    public void showViewRetry(){
        this.inviteFriendsView.showRetry();
    }
    public void hideViewLoading(){
        this.inviteFriendsView.hideLoading();
    }
    public void showErrorMessage(ErrorBundle errorBundle){
        String errorMessage = ErrorMessageFactory.create(this.inviteFriendsView.getContext(),
                errorBundle.getException());
        this.inviteFriendsView.showError(errorMessage);

    }
    public void showInviteFriends(InviteFriendsDomain inviteFriendsDomain)   {
        final InviteFreindsModel inviteFreindsModel =  this.userInviteFriendsMapper.transform(inviteFriendsDomain);
        this.inviteFriendsView.renderAfterInviteFreinds(inviteFreindsModel);
    }
    private final class UserInviteFriends extends DefaultSubscriber<InviteFriendsDomain>{
        @Override
        public void onCompleted() {
          InviteFriendsPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            InviteFriendsPresenter.this.hideViewLoading();
            InviteFriendsPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            InviteFriendsPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(InviteFriendsDomain inviteFriendsDomain) {
            InviteFriendsPresenter.this.showInviteFriends(inviteFriendsDomain);
        }
    }

}
