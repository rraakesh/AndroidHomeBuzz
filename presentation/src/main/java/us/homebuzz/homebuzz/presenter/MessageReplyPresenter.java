package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.GetMessageReply;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserMessageReplyMapper;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.view.MessageReplyView;
import us.homebuzz.homebuzz.view.UserLoginView;

/**
 * Created by abhishek.singh on 11/5/2015.
 */
@PerActivity
public class MessageReplyPresenter implements Presenter {
    public static String TAG = MessageReplyPresenter.class.getSimpleName();

    private MessageReplyView messageReplyView;
    private GetMessageReply getMessageReply;
    private final UserMessageReplyMapper userMessageReplyMapper;

    @Inject
    public MessageReplyPresenter(@Named("ReplyMessages") GetMessageReply getMessageReply,
                                 UserMessageReplyMapper userMessageReplyMapper) {
        this.getMessageReply = getMessageReply;
        this.userMessageReplyMapper = userMessageReplyMapper;
    }

    public void setView(@NonNull MessageReplyView view) {
        this.messageReplyView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getMessageReply.unsubscribe();
    }

    public void initialize(WeakHashMap<String, String> replyMessageCredential) {
        getMessageReply.setMessageReply(replyMessageCredential);
        this.loadUserDetails();
    }

    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getMsgReply();
    }

    private void showViewLoading() {
        this.messageReplyView.showLoading();
    }

    private void hideViewLoading() {
        this.messageReplyView.hideLoading();
    }

    private void showViewRetry() {
        this.messageReplyView.showRetry();
    }

    private void hideViewRetry() {
        this.messageReplyView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.messageReplyView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.messageReplyView.showError(errorMessage);
    }

    private void showMessageReply(MessageReply messageReply) {
        final UserMessageReplyModel replyModel = this.userMessageReplyMapper.transform(messageReply);
        this.messageReplyView.renderAfterMessageReply(replyModel);
    }

    private void getMsgReply() {
        this.getMessageReply.execute(new UserMessageReply());
    }

    private final class UserMessageReply extends DefaultSubscriber<MessageReply> {

        @Override
        public void onCompleted() {
            MessageReplyPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            MessageReplyPresenter.this.hideViewLoading();
            MessageReplyPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            MessageReplyPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(MessageReply messageReply) {
            MessageReplyPresenter.this.showMessageReply(messageReply);
        }
    }
}
