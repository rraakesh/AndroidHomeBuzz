package us.homebuzz.homebuzz.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.exception.DefaultErrorBundle;
import us.homebuzz.homebuzz.domain.exception.ErrorBundle;
import us.homebuzz.homebuzz.domain.interactor.DefaultSubscriber;
import us.homebuzz.homebuzz.domain.interactor.MarkMessageRead;
import us.homebuzz.homebuzz.exception.ErrorMessageFactory;
import us.homebuzz.homebuzz.mapper.UserMessageReplyMapper;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.view.MessageReplyView;

/**
 * Created by amit.singh on 12/4/2015.
 */
public class MessageReadPrsenter    implements Presenter {
    public static String TAG = MessageReplyPresenter.class.getSimpleName();

    private MessageReplyView messageReplyView;
    private MarkMessageRead getMessageReply;
    private final UserMessageReplyMapper userMessageReplyMapper;

    @Inject
    public MessageReadPrsenter(@Named("MarkreadMessages") MarkMessageRead getMessageReply,
                                 UserMessageReplyMapper userMessageReplyMapper) {
        this.getMessageReply = getMessageReply;
        this.userMessageReplyMapper = userMessageReplyMapper;
    }

    public void setView(@NonNull MessageReplyView view) {
        this.messageReplyView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getMessageReply.unsubscribe();
    }

    public void initialize(WeakHashMap<String, String> replyMessageCredential) {
        getMessageReply.setMessageRead(replyMessageCredential);
        this.loadUserDetails();
    }

    private void loadUserDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getMsgReply();
    }

    private void showViewLoading() {
        this.messageReplyView.showLoading();
    }

    private void hideViewLoading() {
        this.messageReplyView.hideLoading();
    }

    private void showViewRetry() {
        this.messageReplyView.showRetry();
    }

    private void hideViewRetry() {
        this.messageReplyView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.messageReplyView.getContext(),
                errorBundle.getException());
        Log.d(TAG, "Error " + errorMessage);
        this.messageReplyView.showError(errorMessage);
    }

    private void showMessageReply(MessageReply messageReply) {
        final UserMessageReplyModel replyModel = this.userMessageReplyMapper.transform(messageReply);
        this.messageReplyView.renderAfterMessageReply(replyModel);
    }

    private void getMsgReply() {
        this.getMessageReply.execute(new UserMessageReply());
    }

    private final class UserMessageReply extends DefaultSubscriber<MessageReply> {

        @Override
        public void onCompleted() {
            MessageReadPrsenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            MessageReadPrsenter.this.hideViewLoading();
            MessageReadPrsenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            MessageReadPrsenter.this.showViewRetry();
        }

        @Override
        public void onNext(MessageReply messageReply) {
            MessageReadPrsenter.this.showMessageReply(messageReply);
        }
    }
}
