package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.ZipCodeDeleteDomain;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.ZipCodeDeleteModel;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipDeleteMapper {
    @Inject
    public ZipDeleteMapper() {
    }

    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @return {@link FollowUnfollowData}.
     */
    public ZipCodeDeleteModel transform(ZipCodeDeleteDomain ZipCodeDeleteDomain) {
        if (ZipCodeDeleteDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipCodeDeleteModel ZipCodeDeleteModel = new ZipCodeDeleteModel();
        ZipCodeDeleteModel.setDelete(ZipCodeDeleteDomain.getDelete());
        return ZipCodeDeleteModel;
    }
}