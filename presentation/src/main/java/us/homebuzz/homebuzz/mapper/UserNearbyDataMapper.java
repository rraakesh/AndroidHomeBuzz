package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.NearbyDomain;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.UserNearbyModel;

/**
 * Created by amit.singh on 10/15/2015.
 */

/**
 * Mapper class used to transform {@link User} (in the domain layer) to {@link UserModel} in the
 * presentation layer.
 */
@PerActivity
public class UserNearbyDataMapper {
    @Inject
    public UserNearbyDataMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param nearbyDomain
     * @return UserNearbyModel
     */
    public UserNearbyModel transform(NearbyDomain nearbyDomain) {
        if (nearbyDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserNearbyModel userNearbyModel = new UserNearbyModel();
        userNearbyModel.setId(nearbyDomain.getId());
        userNearbyModel.setForSale(nearbyDomain.getForSale());
        userNearbyModel.setPhotos(nearbyDomain.getPhotos());
        userNearbyModel.setZip(nearbyDomain.getZip());
        userNearbyModel.setAddress(nearbyDomain.getAddress());
        userNearbyModel.setUserId(nearbyDomain.getUserId());
        userNearbyModel.setAgentId(nearbyDomain.getAgentId());
        userNearbyModel.setBaths(nearbyDomain.getBaths());
        userNearbyModel.setBedrooms(nearbyDomain.getBedrooms());
        userNearbyModel.setCity(nearbyDomain.getCity());
        userNearbyModel.setLat(nearbyDomain.getLat());
        userNearbyModel.setLon(nearbyDomain.getLon());
        userNearbyModel.setLocationRating(nearbyDomain.getLocationRating());
        userNearbyModel.setConditionRating(nearbyDomain.getConditionRating().floatValue());
        userNearbyModel.setLastSoldPrice(nearbyDomain.getLastSoldPrice());
        userNearbyModel.setYearBuilt(nearbyDomain.getYearBuilt());
        userNearbyModel.setStatus(nearbyDomain.getStatus());
        userNearbyModel.setStreet(nearbyDomain.getStreet());
        userNearbyModel.setRemarks(nearbyDomain.getRemarks());
        userNearbyModel.setFavorited(nearbyDomain.getFavorited());
        userNearbyModel.setPrice(nearbyDomain.getPrice());
        userNearbyModel.setFireplaces(nearbyDomain.getFireplaces());
        userNearbyModel.setLotSqftApprox(nearbyDomain.getLotSqftApprox());
        userNearbyModel.setGarage(nearbyDomain.getGarage());
        userNearbyModel.setEstimates_count(nearbyDomain.getEstimates_count());
        userNearbyModel.setReviews_count(nearbyDomain.getReviews_count());
        userNearbyModel.setCheckin_count(nearbyDomain.getCheckin_count());
        userNearbyModel.setOptionalBedrooms(nearbyDomain.getOptionalBedrooms());
       // System.out.println("Square Feet Domain" + nearbyDomain.getFireplaces());
        userNearbyModel.setEstimatedSquareFeet(nearbyDomain.getEstimatedSquareFeet());
        userNearbyModel.setAcres(nearbyDomain.getAcres());
        userNearbyModel.setState(nearbyDomain.getState());
        if (nearbyDomain.getEstimates() != null && nearbyDomain.getEstimates().size() > 0) {
            ArrayList<UserNearbyModel.Estimate> data = new ArrayList<>();
            for (NearbyDomain.Estimate estimate : nearbyDomain.getEstimates()) {
              //  Log.e("Value in Model data ", "Near by Model");
                UserNearbyModel.Estimate estimateData = userNearbyModel.new Estimate();
                estimateData.setId(estimate.getId());
                estimateData.setUserId(estimate.getUserId());
                estimateData.setPrice(estimate.getPrice());
                estimateData.setLocationRating(estimate.getLocationRating());
                estimateData.setConditionRating(estimate.getConditionRating());
                estimateData.setImageUrl(estimate.getImageUrl());
                estimateData.setName(estimate.getName());
                estimateData.setReason(estimate.getReason());
//                Log.e("Value model  value", estimate.getPrice().toString());
                data.add(estimateData);
            }
            userNearbyModel.setEstimates(data);
        }
        //userNearbyModel.setIsEstimated(nearbyDomain.getIsEstimated());
        return userNearbyModel;
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param nearbyDomainCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserNearbyModel> transform(Collection<NearbyDomain> nearbyDomainCollection) {
        Collection<UserNearbyModel> userNearbyModelCollection;

        if (nearbyDomainCollection != null && !nearbyDomainCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (NearbyDomain nearbyDomain : nearbyDomainCollection) {
                userNearbyModelCollection.add(transform(nearbyDomain));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }

}


