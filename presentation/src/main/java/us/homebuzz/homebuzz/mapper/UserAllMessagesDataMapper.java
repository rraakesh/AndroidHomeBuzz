package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.message.AllMessages;
import us.homebuzz.homebuzz.model.UserAllMessagesModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/22/2015.
 */
@PerActivity
public class UserAllMessagesDataMapper {
    @Inject
    public UserAllMessagesDataMapper() {
    }
    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param allMessagesCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserAllMessagesModel> transform(Collection<AllMessages> allMessagesCollection) {
        Collection<UserAllMessagesModel> userNearbyModelCollection;

        if (allMessagesCollection != null && !allMessagesCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (AllMessages allMessages : allMessagesCollection) {
                userNearbyModelCollection.add(transform(allMessages));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }

    public UserAllMessagesModel transform(AllMessages allMessages) {
        if (allMessages == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserAllMessagesModel allMessagesModel = new UserAllMessagesModel();
        allMessagesModel.setId(allMessages.getId());
        allMessagesModel.setListingId(allMessages.getListingId());
        allMessagesModel.setStatus(allMessages.getStatus());
        allMessagesModel.setBody(allMessages.getBody());
        allMessagesModel.setCreatedAt(allMessages.getCreatedAt());
       // allMessagesModel.set(allMessages.getCreatedAt());
        if(allMessages.getSender()!=null) {
            UserAllMessagesModel.Sender sender = allMessagesModel.new Sender();
            sender.setId(allMessages.getSender().getId());
            sender.setEmail(allMessages.getSender().getEmail());
            sender.setName(allMessages.getSender().getName());
            sender.setProfilePic(allMessages.getSender().getProfile_pic());
//            System.out.println("Sender in Data" + sender.getId());
            allMessagesModel.setSender(sender);
        }
        if(allMessages.getRecipient()!=null) {
            UserAllMessagesModel.Recipient recipient = allMessagesModel.new Recipient();
            recipient.setId(allMessages.getRecipient().getId());
            recipient.setEmail(allMessages.getRecipient().getEmail());
            recipient.setName(allMessages.getRecipient().getName());
            allMessagesModel.setRecipient(recipient);
        }

        return allMessagesModel;
    }
}
