package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.addListing.SearchAddressDomain;
import us.homebuzz.homebuzz.model.SearchAdressModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 11/28/2015.
 */
@PerActivity
public class SearchAddressMapper {
    @Inject
    public SearchAddressMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param justSoldEntity Object to be transformed.
     * @return {@link UserModel}.
     */

    public SearchAdressModel transform(SearchAddressDomain justSoldEntity) {
        if (justSoldEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SearchAdressModel justSoldDomain = new SearchAdressModel();
        justSoldDomain.setId(justSoldEntity.getId());
        justSoldDomain.setForSale(justSoldEntity.getForSale());
        justSoldDomain.setPhotos(justSoldEntity.getPhotos());
        justSoldDomain.setZip(justSoldEntity.getZip());
        justSoldDomain.setAddress(justSoldEntity.getAddress());
        justSoldDomain.setUserId((Integer)justSoldEntity.getUserId());
        justSoldDomain.setAgentId(justSoldEntity.getAgentId());
        justSoldDomain.setBaths(justSoldEntity.getBaths());
        justSoldDomain.setBedrooms(justSoldEntity.getBedrooms());
        justSoldDomain.setCity(justSoldEntity.getCity());
        justSoldDomain.setLat(justSoldEntity.getLat());
        justSoldDomain.setLon(justSoldEntity.getLon());
        justSoldDomain.setLocationRating(justSoldEntity.getLocationRating());
        justSoldDomain.setConditionRating(justSoldEntity.getConditionRating());
        justSoldDomain.setLastSoldPrice(justSoldEntity.getLastSoldPrice());
        justSoldDomain.setYearBuilt(justSoldEntity.getYearBuilt());
        justSoldDomain.setStatus(justSoldEntity.getStatus());
        justSoldDomain.setRemarks(justSoldEntity.getRemarks());
        justSoldDomain.setPrice(justSoldEntity.getPrice());
        justSoldDomain.setEstimatedSquareFeet(justSoldEntity.getEstimatedSquareFeet());
        justSoldDomain.setAcres(justSoldEntity.getAcres());
        justSoldDomain.setState(justSoldEntity.getState());
        justSoldDomain.setStreet(justSoldEntity.getStreet());
        justSoldDomain.setParcelNum(justSoldEntity.getParcelNum());
         System.out.println("PArcel in Mapper"+justSoldEntity.getParcelNum());
        return justSoldDomain;
    }
}
