package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.profile.ProfileDomain;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.UserProfileModel;

/**
 * Created by amit.singh on 10/29/2015.
 */

/**
 * Mapper class used to transform {@link User} (in the domain layer) to {@link UserModel} in the
 * presentation layer.
 */
@PerActivity
public class UserProfileDataMapper {
    @Inject
    public UserProfileDataMapper() {
    }

    /**
     * TranForm  A ProfileEntity to ProfileDomain
     *
     * @param profileDomain
     * @return
     */
    public UserProfileModel transform(ProfileDomain profileDomain) {
        if (profileDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserProfileModel userProfileModel = new UserProfileModel();
        userProfileModel.setId(profileDomain.getId());


        userProfileModel.setEmail(profileDomain.getEmail());
        userProfileModel.setSignInCount(profileDomain.getSignInCount());
        userProfileModel.setCurrentSignInAt(profileDomain.getCurrentSignInAt());
        userProfileModel.setLastSignInAt(profileDomain.getLastSignInAt());
        userProfileModel.setConfirmedAt(String.valueOf(profileDomain.getConfirmedAt()));
        userProfileModel.setName(profileDomain.getName());
        userProfileModel.setZipCode(profileDomain.getZipCode());
        userProfileModel.setRole(profileDomain.getRole());
        userProfileModel.setFacebookId(String.valueOf(profileDomain.getFacebookId()));
        userProfileModel.setAccurate(profileDomain.getAccurate());
        userProfileModel.setPoints(profileDomain.getPoints());
        userProfileModel.setTeamId(String.valueOf(profileDomain.getTeamId()));
        userProfileModel.setOfficeId(String.valueOf(profileDomain.getOfficeId()));
        userProfileModel.setFirstName(profileDomain.getFirstName());
        userProfileModel.setLastName(profileDomain.getLastName());
        userProfileModel.setSubscriptionActive(String.valueOf(profileDomain.getSubscriptionActive()));
        userProfileModel.setConfirmed(profileDomain.getConfirmed());
        userProfileModel.setFollowed(profileDomain.getFollowed());
        userProfileModel.setSoldListings(profileDomain.getSoldListings());
        userProfileModel.setFollowers(profileDomain.getFollowers());
        userProfileModel.setFollowings(profileDomain.getFollowings());
        userProfileModel.setEstimatesCount(profileDomain.getEstimatesCount());
        userProfileModel.setCheckinCount((profileDomain.getCheckinCount()));
        userProfileModel.setReviewsCount((profileDomain.getReviewsCount()));
        userProfileModel.setPhotos(profileDomain.getPhotos());
        userProfileModel.setEducations(profileDomain.getEducations());


        userProfileModel.setUserId(profileDomain.getUserId());
        userProfileModel.setBio(profileDomain.getBio());
        userProfileModel.setDegree(profileDomain.getDegree());
        userProfileModel.setYrGraduated(profileDomain.getYrGraduated());
        userProfileModel.setOtherSchool(profileDomain.getOtherSchool());
        userProfileModel.setDesignations(profileDomain.getDesignations());
        userProfileModel.setPhone(profileDomain.getPhone());
        userProfileModel.setProfilePic(profileDomain.getProfilePic());
        userProfileModel.setAgentId(profileDomain.getAgentId());
        userProfileModel.setWebsite(profileDomain.getWebsite());
        userProfileModel.setTwitterUrl((String) profileDomain.getTwitterUrl());
        userProfileModel.setFacebookUrl((String) profileDomain.getFacebookUrl());
        userProfileModel.setPhone((String) profileDomain.getPhone());
        userProfileModel.setCity((String) profileDomain.getCity());
        userProfileModel.setState((String) profileDomain.getState());
        userProfileModel.setStreet((String) profileDomain.getStreet());
        userProfileModel.setLinkedIn((String) profileDomain.getLinkedIn());

        userProfileModel.setJobTitle((String) profileDomain.getJobTitle());
        userProfileModel.setCompany((String) profileDomain.getCompany());
        userProfileModel.setMlsName((String) profileDomain.getMlsName());
        userProfileModel.setNapNum((String) profileDomain.getNapNum());
        userProfileModel.setLicenseNum((String) profileDomain.getLicenseNum());
        userProfileModel.setProfileId( profileDomain.getProfileId());
        userProfileModel.setCollege( profileDomain.getCollege());
        return userProfileModel;
    }

}
