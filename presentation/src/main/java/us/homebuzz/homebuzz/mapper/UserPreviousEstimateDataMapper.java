package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.previousEstimate.PreviouseEstimate;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.UserPreviousEstimateModel;

/**
 * Created by amit.singh on 10/28/2015.
 */
@PerActivity
public class UserPreviousEstimateDataMapper {
    @Inject
    public UserPreviousEstimateDataMapper() {
    }

    /**
     * Transform a {@link UserAccuracyModel} into an {@link UserAccuracyModel}.
     *
     * @param previouseEstimate
     * @return UserNearbyModel
     */
    public UserPreviousEstimateModel transform(PreviouseEstimate previouseEstimate) {
        if (previouseEstimate == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserPreviousEstimateModel userPreviousEstimateModel = new UserPreviousEstimateModel();
        userPreviousEstimateModel.setId(previouseEstimate.getId());
        userPreviousEstimateModel.setListingId(previouseEstimate.getListingId());
        userPreviousEstimateModel.setLocationRating(previouseEstimate.getLocationRating());
        userPreviousEstimateModel.setConditionRating(previouseEstimate.getConditionRating());
        userPreviousEstimateModel.setPrice(previouseEstimate.getPrice());
        userPreviousEstimateModel.setReason(previouseEstimate.getReason());
        userPreviousEstimateModel.setPrice(previouseEstimate.getPrice());
       // System.out.println("Listing in Previous Estimate PRe" + previouseEstimate.getListing());
        if(previouseEstimate.getListing()!=null) {
         //   System.out.println("Listing in Previous Estimate"+previouseEstimate.getListing());
            UserPreviousEstimateModel.Listing listing=userPreviousEstimateModel.new Listing();
            listing.setId(previouseEstimate.getListing().getId());
            listing.setForSale(previouseEstimate.getListing().getForSale());
            listing.setPhotos(previouseEstimate.getListing().getPhotos());
            listing.setZip(previouseEstimate.getListing().getZip());
            listing.setAddress(previouseEstimate.getListing().getAddress());
            listing.setUserId(previouseEstimate.getListing().getUserId());
            listing.setAgentId(previouseEstimate.getListing().getAgentId());
            listing.setBaths(previouseEstimate.getListing().getBaths());
            listing.setBedrooms(previouseEstimate.getListing().getBedrooms());
            listing.setCity(previouseEstimate.getListing().getCity());
            listing.setLat(previouseEstimate.getListing().getLat());
            listing.setLon(previouseEstimate.getListing().getLon());
            listing.setLocationRating(previouseEstimate.getListing().getLocationRating());
            listing.setConditionRating(previouseEstimate.getListing().getConditionRating());
            listing.setLastSoldPrice(previouseEstimate.getListing().getLastSoldPrice());
            listing.setYearBuilt(previouseEstimate.getListing().getYearBuilt());
            listing.setStatus(previouseEstimate.getListing().getStatus());
            listing.setRemarks(previouseEstimate.getListing().getRemarks());
            listing.setFavorited(previouseEstimate.getListing().getFavorited());
            listing.setPrice(previouseEstimate.getListing().getPrice());
            listing.setBedrooms(previouseEstimate.getListing().getBedrooms());
            listing.setAcres(previouseEstimate.getListing().getAcres());
            listing.setEstimatedSquareFeet(previouseEstimate.getListing().getEstimatedSquareFeet());
            listing.setStreet(previouseEstimate.getListing().getStreet());
            listing.setState(previouseEstimate.getListing().getState());
            userPreviousEstimateModel.setListing(listing);
        }
     //   System.out.println("Listing in Previous Estimate"+userPreviousEstimateModel.getListing().getId());
        //System.out.println("Listing in Previous Estimate PRe USERS" + previouseEstimate.getUser());
        if(previouseEstimate.getUser()!=null) {
            UserPreviousEstimateModel.User user = userPreviousEstimateModel.new User();
            user.setEmail(previouseEstimate.getUser().getEmail());
            user.setId(previouseEstimate.getUser().getId());
            user.setProfilePic(previouseEstimate.getUser().getProfilePic());
            user.setAccurate(previouseEstimate.getUser().getAccurate());
            user.setEstimatesCount(previouseEstimate.getUser().getEstimatesCount());
            user.setAccurate(previouseEstimate.getUser().getAccurate());
            user.setName(previouseEstimate.getUser().getName());
            user.setYrGraduated(previouseEstimate.getUser().getYrGraduated());
            user.setRole(previouseEstimate.getUser().getRole());
            user.setPhone(previouseEstimate.getUser().getPhone());
            user.setEducations(previouseEstimate.getUser().getEducations());
            user.setBio(previouseEstimate.getUser().getBio());
            user.setZipCode(previouseEstimate.getUser().getZipCode());
            user.setFollowed(previouseEstimate.getUser().getFollowed());
            System.out.println("Listing in Previous Estimate F" + previouseEstimate.getUser().getFollowed());
            userPreviousEstimateModel.setUser(user);
          //  System.out.println("Listing in Previous Estimate  USERS" + userPreviousEstimateModel.getUser().getId());
        }
      //  System.out.println("Listing in Previous Estimate USer" + previouseEstimate.getUser().getId());
        return userPreviousEstimateModel;
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param previouseEstimateCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserPreviousEstimateModel> transform(Collection<PreviouseEstimate> previouseEstimateCollection) {
        Collection<UserPreviousEstimateModel> userPreviousEstimateModelCollection;

        if (previouseEstimateCollection != null && !previouseEstimateCollection.isEmpty()) {
            userPreviousEstimateModelCollection = new ArrayList<>();
            for (PreviouseEstimate nearbyDomain : previouseEstimateCollection) {
                userPreviousEstimateModelCollection.add(transform(nearbyDomain));
            }
        } else {
            userPreviousEstimateModelCollection = Collections.emptyList();
        }

        return userPreviousEstimateModelCollection;
    }
}
