package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.estimateAverage.EstimateAverageDomain;
import us.homebuzz.homebuzz.model.EstimateAverageModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 12/12/2015.
 */
public class EstimateAverageDataMapper {
    @Inject
    public EstimateAverageDataMapper() {
    }

    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @return {@link FollowUnfollowData}.
     */
    public EstimateAverageModel transform(EstimateAverageDomain estimateAverageDomain) {
        if (estimateAverageDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        EstimateAverageModel estimateAverageModel = new EstimateAverageModel();
        estimateAverageModel.setAccurate(estimateAverageDomain.getAccurate());
        estimateAverageModel.setImageUrl(estimateAverageDomain.getImageUrl());
        estimateAverageModel.setPrice(estimateAverageDomain.getPrice());
        estimateAverageModel.setLocationRating(estimateAverageDomain.getLocationRating());
        estimateAverageModel.setName(estimateAverageDomain.getName());
        return estimateAverageModel;
    }
}
