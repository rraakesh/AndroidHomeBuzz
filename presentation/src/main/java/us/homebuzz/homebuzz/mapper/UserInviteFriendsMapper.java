package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.data.entity.pojo.profile.InviteFriends;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.domain.data.profile.InviteFriendsDomain;
import us.homebuzz.homebuzz.model.InviteFreindsModel;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class UserInviteFriendsMapper {
    @Inject
    public UserInviteFriendsMapper(){

    }
    public InviteFreindsModel transform(InviteFriendsDomain inviteFriendsDomain) {
        if (inviteFriendsDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }

        InviteFreindsModel inviteFreindsModel =  new InviteFreindsModel();
        inviteFreindsModel.setMessage(inviteFreindsModel.getMessage());
        inviteFreindsModel.setPhone(inviteFreindsModel.getPhone());
        return inviteFreindsModel;
    }
}
