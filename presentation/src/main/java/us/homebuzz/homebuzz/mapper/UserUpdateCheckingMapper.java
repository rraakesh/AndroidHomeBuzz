package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.AddCheckInPhotoData;
import us.homebuzz.homebuzz.domain.data.UpdateChecking;
import us.homebuzz.homebuzz.model.AddCheckInPhotoModel;
import us.homebuzz.homebuzz.model.UserUpdateCheckingModel;

/**
 * Created by rohitkumar on 6/11/15.
 */
public class UserUpdateCheckingMapper {
    @Inject
    UserUpdateCheckingMapper() {

        /**
         * Transform a {@link User} into an {@link UserModel}.
         *
         * @return {@link UserUpdateCheckingModel}.
         */
    }

    public UserUpdateCheckingModel transfrom(UpdateChecking updateCheckinEntity) {
        if (updateCheckinEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserUpdateCheckingModel updateChecking = new UserUpdateCheckingModel();
        UserUpdateCheckingModel.Listing listing = updateChecking.new Listing();
        listing.setUserId(updateCheckinEntity.getListing().getUserId());
        listing.setId(updateCheckinEntity.getListing().getId());
        if (updateCheckinEntity.getListing().getEstimates() != null && updateCheckinEntity.getListing().getEstimates().size() > 0) {
            ArrayList<UserUpdateCheckingModel.Estimate> data = new ArrayList<>();
            for (UpdateChecking.Estimate estimate : updateCheckinEntity.getListing().getEstimates()) {
                UserUpdateCheckingModel.Estimate estimateData = updateChecking.new Estimate();
                estimateData.setId(estimate.getId());
                estimateData.setUserId(estimate.getUserId());
                estimateData.setListingId(estimate.getListingId());
                data.add(estimateData);
            }
            updateChecking.setListing(listing);
            updateChecking.getListing().setEstimates(data);
        }
        return updateChecking;
    }

    public AddCheckInPhotoModel transform(AddCheckInPhotoData addCheckInPhoto) {
        if (addCheckInPhoto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddCheckInPhotoModel addCheckInPhotoModel = new AddCheckInPhotoModel();
        addCheckInPhotoModel.setUrl(addCheckInPhoto.getUrl());
        System.out.println("Add photo Url" + addCheckInPhotoModel.getUrl());
        return addCheckInPhotoModel;
    }
}
