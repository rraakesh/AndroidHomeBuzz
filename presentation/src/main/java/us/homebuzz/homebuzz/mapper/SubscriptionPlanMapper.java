package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.SubscriptionDomain;
import us.homebuzz.homebuzz.model.SubscriptionModel;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class SubscriptionPlanMapper {
    @Inject
    public SubscriptionPlanMapper() {
    }

    public SubscriptionModel transform(SubscriptionDomain subscriptionDomain) {
        if (subscriptionDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SubscriptionModel subscriptionModel = new SubscriptionModel();
        subscriptionModel.setId(subscriptionDomain.getId());
        subscriptionModel.setEmail(subscriptionDomain.getEmail());
        subscriptionModel.setName(subscriptionDomain.getName());
        subscriptionModel.setZipCode(subscriptionDomain.getZipCode());
        subscriptionModel.setRole(subscriptionDomain.getRole());
        subscriptionModel.setFacebookId(subscriptionDomain.getFacebookId());
        subscriptionModel.setCreatedAt(subscriptionDomain.getCreatedAt());
        subscriptionModel.setUpdatedAt(subscriptionDomain.getUpdatedAt());
        subscriptionModel.setUpdatedAt(subscriptionDomain.getUpdatedAt());
        subscriptionModel.setEstimatesCount(subscriptionDomain.getEstimatesCount());
        subscriptionModel.setAccurate(subscriptionDomain.getAccurate());
        subscriptionModel.setSashId(subscriptionDomain.getSashId());
        subscriptionModel.setLevel(subscriptionDomain.getLevel());
        subscriptionModel.setVerificationCode(subscriptionDomain.getVerificationCode());
        subscriptionModel.setPhone(subscriptionDomain.getPhone());
        subscriptionModel.setEstimateZips(subscriptionDomain.getEstimateZips());
        subscriptionModel.setTeamId(subscriptionDomain.getTeamId());
        subscriptionModel.setTeamRequest(subscriptionDomain.getTeamRequest());
        subscriptionModel.setOfficeId(subscriptionDomain.getOfficeId());
        subscriptionModel.setOfficeRequest(subscriptionDomain.getOfficeRequest());
        subscriptionModel.setFirstName(subscriptionDomain.getFirstName());
        subscriptionModel.setLastName(subscriptionDomain.getLastName());
        subscriptionModel.setInvitationToken(subscriptionDomain.getInvitationToken());
        subscriptionModel.setInvitationCreatedAt(subscriptionDomain.getInvitationCreatedAt());
        subscriptionModel.setInvitationSentAt(subscriptionDomain.getInvitationSentAt());
        subscriptionModel.setInvitationAcceptedAt(subscriptionDomain.getInvitationAcceptedAt());
        subscriptionModel.setInvitationLimit(subscriptionDomain.getInvitationLimit());
        subscriptionModel.setInvitedById(subscriptionDomain.getInvitedById());
        subscriptionModel.setInvitedByType(subscriptionDomain.getInvitedByType());
        subscriptionModel.setInvitationsCount(subscriptionDomain.getInvitationsCount());
        subscriptionModel.setIapReceipt(subscriptionDomain.getIapReceipt());
        subscriptionModel.setSubscriptionActive(subscriptionDomain.getSubscriptionActive());
        subscriptionModel.setNotifyJustListed(subscriptionDomain.getNotifyJustListed());
        subscriptionModel.setNotifyJustSold(subscriptionDomain.getNotifyJustSold());
        subscriptionModel.setNotifyCheckins(subscriptionDomain.getNotifyCheckins());
        subscriptionModel.setNotifyOffers(subscriptionDomain.getNotifyOffers());
        subscriptionModel.setAndroidReceipt(subscriptionDomain.getAndroidReceipt());
        return subscriptionModel;
    }
}

