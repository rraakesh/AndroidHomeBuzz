package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.AccuracyLeaderboardDomain;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/21/2015.
 */
@PerActivity
public class UserAccuracyDataMapper {
    @Inject
    public UserAccuracyDataMapper() {
    }

    /**
     * Transform a {@link UserAccuracyModel} into an {@link UserAccuracyModel}.
     *
     * @param accuracyLeaderboardDomain
     * @return UserNearbyModel
     */
    public UserAccuracyModel transform(AccuracyLeaderboardDomain accuracyLeaderboardDomain) {
        if (accuracyLeaderboardDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserAccuracyModel userAccuracyModel = new UserAccuracyModel();
        userAccuracyModel.setId(accuracyLeaderboardDomain.getId());
        userAccuracyModel.setEstimatesCount(accuracyLeaderboardDomain.getEstimatesCount());
        userAccuracyModel.setLevel(accuracyLeaderboardDomain.getLevel());
        userAccuracyModel.setTeamId(accuracyLeaderboardDomain.getTeamId());
        userAccuracyModel.setEmail(accuracyLeaderboardDomain.getEmail());
        userAccuracyModel.setEstimateZips(accuracyLeaderboardDomain.getEstimateZips());
        userAccuracyModel.setRole(accuracyLeaderboardDomain.getRole());
        userAccuracyModel.setZipCode(accuracyLeaderboardDomain.getZipCode());
        userAccuracyModel.setEstimateZips(accuracyLeaderboardDomain.getEstimateZips());

        System.out.println("Profile in Presentation" + accuracyLeaderboardDomain.getEstimateZips());
        userAccuracyModel.setProfilePic(String.valueOf(accuracyLeaderboardDomain.getProfilePic()));
        userAccuracyModel.setAccurate(accuracyLeaderboardDomain.getAccurate());
        userAccuracyModel.setName(accuracyLeaderboardDomain.getName());
        userAccuracyModel.setPhone(String.valueOf(accuracyLeaderboardDomain.getPhone()));
        userAccuracyModel.setFacebookId(String.valueOf(accuracyLeaderboardDomain.getFacebookId()));
        userAccuracyModel.setFirstName(accuracyLeaderboardDomain.getFirstName());


        /*userAccuracyDomain.setId(accuracyEntity.getId());
        userAccuracyDomain.setId(accuracyEntity.getId());*/
        return userAccuracyModel;
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param nearbyDomainCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserAccuracyModel> transform(Collection<AccuracyLeaderboardDomain> nearbyDomainCollection) {
        Collection<UserAccuracyModel> userNearbyModelCollection;

        if (nearbyDomainCollection != null && !nearbyDomainCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (AccuracyLeaderboardDomain nearbyDomain : nearbyDomainCollection) {
                userNearbyModelCollection.add(transform(nearbyDomain));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }
}
