package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.message.MessageReply;
import us.homebuzz.homebuzz.model.UserMessageReplyModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by abhishek.singh on 11/5/2015.
 */

public class UserMessageReplyMapper {
    @Inject
    public UserMessageReplyMapper() {
    }

    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @return {@link UserMessageReplyModel}.
     */
    public UserMessageReplyModel transform(MessageReply msgReply) {
        if (msgReply == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserMessageReplyModel msgReplyModel = new UserMessageReplyModel();
        msgReplyModel.setBody(msgReply.getBody());
        msgReplyModel.setCreated_at(msgReply.getCreated_at());
        msgReplyModel.setId(msgReply.getId());
        msgReplyModel.setRecipient_id(msgReply.getRecipient_id());
        msgReplyModel.setSender_id(msgReply.getSender_id());
        msgReplyModel.setStatus(msgReply.getStatus());
        msgReplyModel.setUpdated_at(msgReply.getUpdated_at());
        return msgReplyModel;
    }
}