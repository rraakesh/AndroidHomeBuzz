package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.listing.JustListedDomain;
import us.homebuzz.homebuzz.model.UserJustListedModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 11/18/2015.
 */
@PerActivity
public class UserJustListedDataMapper {
    @Inject
    public UserJustListedDataMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param nearbyDomainCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserJustListedModel> transform(Collection<JustListedDomain> nearbyDomainCollection) {
        Collection<UserJustListedModel> userNearbyModelCollection;

        if (nearbyDomainCollection != null && !nearbyDomainCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (JustListedDomain nearbyDomain : nearbyDomainCollection) {
                userNearbyModelCollection.add(transform(nearbyDomain));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }

    public UserJustListedModel transform(JustListedDomain justSoldEntity) {
        if (justSoldEntity == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserJustListedModel justSoldDomain = new UserJustListedModel();
        justSoldDomain.setId(justSoldEntity.getId());
        justSoldDomain.setForSale(justSoldEntity.getForSale());
        justSoldDomain.setPhotos(justSoldEntity.getPhotos());
        justSoldDomain.setZip(justSoldEntity.getZip());
        justSoldDomain.setAddress(justSoldEntity.getAddress());
        justSoldDomain.setUserId((Double)justSoldEntity.getUserId());
        justSoldDomain.setAgentId(justSoldEntity.getAgentId());
        justSoldDomain.setBaths(justSoldEntity.getBaths());
        justSoldDomain.setBedrooms(justSoldEntity.getBedrooms());
        justSoldDomain.setCity(justSoldEntity.getCity());
        justSoldDomain.setLat(justSoldEntity.getLat());
        justSoldDomain.setLon(justSoldEntity.getLon());
        justSoldDomain.setLocationRating(justSoldEntity.getLocationRating());
        justSoldDomain.setConditionRating(justSoldEntity.getConditionRating());
        justSoldDomain.setLastSoldPrice(justSoldEntity.getLastSoldPrice());
        justSoldDomain.setYearBuilt(justSoldEntity.getYearBuilt());
        justSoldDomain.setStatus(justSoldEntity.getStatus());
        justSoldDomain.setRemarks(justSoldEntity.getRemarks());
        justSoldDomain.setPrice(justSoldEntity.getPrice());
        justSoldDomain.setEstimatedSquareFeet(justSoldEntity.getEstimatedSquareFeet());
        justSoldDomain.setAcres(justSoldEntity.getAcres());
        justSoldDomain.setState(justSoldEntity.getState());
        justSoldDomain.setStreet(justSoldEntity.getStreet());

        return justSoldDomain;
    }

}
