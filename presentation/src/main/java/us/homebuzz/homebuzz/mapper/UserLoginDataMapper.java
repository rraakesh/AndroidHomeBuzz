package us.homebuzz.homebuzz.mapper;

/**
 * Created by amit.singh on 10/7/2015.
 */

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.User_;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Mapper class used to transform {@link User} (in the domain layer) to {@link UserModel} in the
 * presentation layer.
 */
@PerActivity
public class UserLoginDataMapper {
    @Inject
    public UserLoginDataMapper() {}

    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @param user Object to be transformed.
     * @return {@link UserModel}.
     */
    public UserLoginModel transform(User_ user) {
        if (user == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserLoginModel userModel = new UserLoginModel();
        userModel.setEmail(user.getEmail());
        userModel.setId(user.getId());
        userModel.setProfilePic(user.getProfilePic());
        userModel.setToken(user.getToken());
        userModel.setAccurate(user.getAccurate());
        userModel.setEstimatesCount(user.getEstimatesCount());
        userModel.setAccurate(user.getAccurate());
        userModel.setEstimateZips(user.getEstimateZips());
        userModel.setName(user.getName());
        userModel.setLevel(user.getLevel());
        userModel.setRole(user.getRole());
        userModel.setTeamId(user.getTeamId());
        userModel.setTeamId(user.getTeamId());
        userModel.setRole(user.getRole());
        userModel.setZipCode(user.getZipCode());
        return userModel;
    }

}
