package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.message.unread.UnreadMessageDomain;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.UserUnreadMessagesModel;

/**
 * Created by amit.singh on 11/18/2015.
 */
@PerActivity
public class UnreadMessageDataMapper {
    @Inject
    public UnreadMessageDataMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param allMessagesCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserUnreadMessagesModel> transform(Collection<UnreadMessageDomain> allMessagesCollection) {
        Collection<UserUnreadMessagesModel> userNearbyModelCollection;

        if (allMessagesCollection != null && !allMessagesCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (UnreadMessageDomain allMessages : allMessagesCollection) {
                userNearbyModelCollection.add(transform(allMessages));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }

    public UserUnreadMessagesModel transform(UnreadMessageDomain allMessages) {
        if (allMessages == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserUnreadMessagesModel allMessagesModel = new UserUnreadMessagesModel();
        allMessagesModel.setId(allMessages.getId());
        allMessagesModel.setListingId(allMessages.getListingId());
        allMessagesModel.setStatus(allMessages.getStatus());
        allMessagesModel.setBody(allMessages.getBody());
        allMessagesModel.setCreatedAt(allMessages.getCreatedAt());
//        System.out.println("get Created id" + allMessages.getBody());
//        System.out.println("For All Messages Sender in Data MApper" + allMessages.getSender());


        if (allMessages.getSender() != null) {
            UserUnreadMessagesModel.Sender sender = allMessagesModel.new Sender();
            sender.setId(allMessages.getSender().getId());
            sender.setEmail(allMessages.getSender().getEmail());
            sender.setName(allMessages.getSender().getName());
            sender.setProfile_pic(allMessages.getSender().getProfile_pic());
//            System.out.println("Sender in Data" + sender.getId());
            allMessagesModel.setSender(sender);
        }
        if (allMessages.getRecipient() != null) {
            UserUnreadMessagesModel.Recipient recipient = allMessagesModel.new Recipient();
            recipient.setId(allMessages.getRecipient().getId());
            recipient.setEmail(allMessages.getRecipient().getEmail());
            recipient.setName(allMessages.getRecipient().getName());
            allMessagesModel.setRecipient(recipient);
        }

        return allMessagesModel;
    }
}
