package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.profile.ProfilePicDomain;
import us.homebuzz.homebuzz.model.ProfilePicModel;

/**
 * Created by amit.singh on 12/3/2015.
 */
@PerActivity
public class UserProfilePicDataMapper {
    @Inject
    public UserProfilePicDataMapper() {
    }
    /**
     * TranForm  A ProfileEntity to ProfileDomain
     *
     * @param profilePicDomain
     * @return
     */
    public ProfilePicModel transform(ProfilePicDomain profilePicDomain) {
        if (profilePicDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ProfilePicModel profilePicModel=new ProfilePicModel();
        profilePicModel.setImageUrl(profilePicDomain.getImageUrl());
        return profilePicModel;
    }
}
