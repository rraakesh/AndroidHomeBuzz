package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.searchPeople.SearchPeopleDomain;
import us.homebuzz.homebuzz.model.SearchPeopleModel;

/**
 * Created by amit.singh on 12/8/2015.
 */
@PerActivity
public class SearchPeopleDataMapper {
    @Inject
    public SearchPeopleDataMapper() {
    }


    public Collection<SearchPeopleModel> transform(Collection<SearchPeopleDomain> searchPeopleDomain) {
        Collection<SearchPeopleModel> userNearbyModelCollection;

        if (searchPeopleDomain != null && !searchPeopleDomain.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (SearchPeopleDomain searchDomain : searchPeopleDomain) {
                userNearbyModelCollection.add(transform(searchDomain));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }

        return userNearbyModelCollection;
    }



    public SearchPeopleModel transform(SearchPeopleDomain searchPeopleDomain) {
        if (searchPeopleDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SearchPeopleModel searchPeopleModel=new SearchPeopleModel();
        // searchPeopleModel.setImageUrl(searchPeopleDomain.getImageUrl());
        searchPeopleModel.setAccurate(searchPeopleDomain.getAccurate());
        searchPeopleModel.setProfilePic(searchPeopleDomain.getProfilePic());
        searchPeopleModel.setEmail(searchPeopleDomain.getEmail());
        searchPeopleModel.setName(searchPeopleDomain.getName());
        searchPeopleModel.setEstimateZips(searchPeopleDomain.getEstimateZips());
        searchPeopleModel.setEstimatesCount(searchPeopleDomain.getEstimatesCount());
        searchPeopleModel.setFirstName(searchPeopleDomain.getFirstName());
        searchPeopleModel.setZipCode(searchPeopleDomain.getZipCode());
        searchPeopleModel.setLastName(searchPeopleDomain.getLastName());
        searchPeopleModel.setId(searchPeopleDomain.getId());
        searchPeopleModel.setPhone(searchPeopleDomain.getPhone());
        searchPeopleModel.setRole(searchPeopleDomain.getRole());
        //  searchPeopleModel.setInvitaion(searchPeopleDomain.getEstimateZips());
        return searchPeopleModel;
    }



}
