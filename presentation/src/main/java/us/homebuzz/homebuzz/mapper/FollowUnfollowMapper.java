package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.FollowUnfollowData;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.model.FollowUnfollowModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by abhishek.singh on 11/20/2015.
 */
public class FollowUnfollowMapper {
    @Inject
    public FollowUnfollowMapper() {
    }

    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @return {@link FollowUnfollowData}.
     */
    public FollowUnfollowModel transform(FollowUnfollowData followUnfollowData) {
        if (followUnfollowData == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        FollowUnfollowModel followUnfollowModel = new FollowUnfollowModel();
        followUnfollowModel.setFollowed(followUnfollowData.getFollowed());
        return followUnfollowModel;
    }
}