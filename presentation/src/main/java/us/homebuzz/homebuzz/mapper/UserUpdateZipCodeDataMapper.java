package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.zipcode.UpdateZipCodeDomain;
import us.homebuzz.homebuzz.model.UpdateZipcodeModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 12/15/2015.
 */
@PerActivity
public class UserUpdateZipCodeDataMapper {
    @Inject
    public UserUpdateZipCodeDataMapper() {
    }
    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param allMessagesCollection Object to be transformed.
     * @return {@link UserModel}.
     */

    /**
     * TranForm  A zipPurchaseDomain to ZipPurchaseModel
     *
     * @param zipPurchaseDomain
     * @return
     */
    public UpdateZipcodeModel transform(UpdateZipCodeDomain zipPurchaseDomain) {
        if (zipPurchaseDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UpdateZipcodeModel zipPurchaseModel = new UpdateZipcodeModel();
        zipPurchaseModel.setId(zipPurchaseDomain.getId());
        zipPurchaseModel.setUserId(zipPurchaseDomain.getUserId());
        zipPurchaseModel.setUpdatedAt(zipPurchaseDomain.getUpdatedAt());
        zipPurchaseModel.setExpiredAt(zipPurchaseDomain.getExpiredAt());
        zipPurchaseModel.setZipCode(zipPurchaseDomain.getZipCode());
        return zipPurchaseModel;
    }

}
