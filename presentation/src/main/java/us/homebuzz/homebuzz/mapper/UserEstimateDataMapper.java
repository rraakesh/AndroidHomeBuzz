package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.EstimatesDomain;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.model.UserEstimateModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/21/2015.
 */
@PerActivity
public class UserEstimateDataMapper {
    @Inject
    public UserEstimateDataMapper() {
    }

    /**
     * Transform a {@link UserAccuracyModel} into an {@link UserAccuracyModel}.
     *
     * @param estimatesDomain
     * @return UserNearbyModel
     */
    public UserEstimateModel transform(EstimatesDomain estimatesDomain) {
        if (estimatesDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserEstimateModel userEstimateModel = new UserEstimateModel();
        userEstimateModel.setId(estimatesDomain.getId());
        userEstimateModel.setEstimatesCount(estimatesDomain.getEstimatesCount());
        userEstimateModel.setLevel(estimatesDomain.getLevel());
        userEstimateModel.setTeamId(estimatesDomain.getTeamId());
        userEstimateModel.setEmail(estimatesDomain.getEmail());
        userEstimateModel.setEstimateZips(estimatesDomain.getEstimateZips());
        userEstimateModel.setRole(estimatesDomain.getRole());
        userEstimateModel.setZipCode(estimatesDomain.getZipCode());
        userEstimateModel.setProfilePic(estimatesDomain.getProfilePic());
        userEstimateModel.setAccurate(estimatesDomain.getAccurate());
        userEstimateModel.setName(estimatesDomain.getName());
        userEstimateModel.setPhone(estimatesDomain.getPhone());
        userEstimateModel.setFirstName(estimatesDomain.getFirstName());

        /*userAccuracyDomain.setId(accuracyEntity.getId());
        userAccuracyDomain.setId(accuracyEntity.getId());*/
        return userEstimateModel;
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param estimatesDomainsCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserEstimateModel> transform(Collection<EstimatesDomain> estimatesDomainsCollection) {
        Collection<UserEstimateModel> userEstimateModelsCollection;

        if (estimatesDomainsCollection != null && !estimatesDomainsCollection.isEmpty()) {
            userEstimateModelsCollection = new ArrayList<>();
            for (EstimatesDomain nearbyDomain : estimatesDomainsCollection) {
                userEstimateModelsCollection.add(transform(nearbyDomain));
            }
        } else {
            userEstimateModelsCollection = Collections.emptyList();
        }

        return userEstimateModelsCollection;
    }
}
