package us.homebuzz.homebuzz.mapper;

/**
 * Created by amit.singh on 11/3/2015.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.friends.followed.FriendFollowedDomain;
import us.homebuzz.homebuzz.model.UserAccuracyModel;
import us.homebuzz.homebuzz.model.UserFriendFollowedModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Mapper class used to transform {@link User} (in the domain layer) to {@link UserModel} in the
 * presentation layer.
 */
@PerActivity
public class UserFriendFollowedDataMapper {
    @Inject
    public UserFriendFollowedDataMapper() {}

    /**
     * Transform a {@link UserAccuracyModel} into an {@link UserAccuracyModel}.
     *
     * @param friendFollowedDomain
     * @return UserFriendFollowedModel
     */
    public UserFriendFollowedModel transform(FriendFollowedDomain friendFollowedDomain) {
        if (friendFollowedDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }

        UserFriendFollowedModel userFriendFollowedModel = new UserFriendFollowedModel();
        userFriendFollowedModel.setId(friendFollowedDomain.getId());
        userFriendFollowedModel.setEmail(friendFollowedDomain.getEmail());
        userFriendFollowedModel.setName(friendFollowedDomain.getName());
        userFriendFollowedModel.setZipCode(friendFollowedDomain.getZipCode());
        userFriendFollowedModel.setRole(friendFollowedDomain.getRole());
        userFriendFollowedModel.setEstimatesCount(friendFollowedDomain.getEstimatesCount());
        userFriendFollowedModel.setAccurate(friendFollowedDomain.getAccurate());
        userFriendFollowedModel.setProfilePic(friendFollowedDomain.getProfilePic());
        userFriendFollowedModel.setFollowed(friendFollowedDomain.getFollowed());
        userFriendFollowedModel.setFirstName(friendFollowedDomain.getFirstName());
        userFriendFollowedModel.setLastName(friendFollowedDomain.getLastName());

        // friendFollowedDomain.setAccurate(FriendFollowedDomainEntity.getAccurate());

        return userFriendFollowedModel;
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param FriendFollowedDomainCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<UserFriendFollowedModel> transform(Collection<FriendFollowedDomain> FriendFollowedDomainCollection) {
        Collection<UserFriendFollowedModel> userFriendFollowedModelCollection;

        if (FriendFollowedDomainCollection != null && !FriendFollowedDomainCollection.isEmpty()) {
            userFriendFollowedModelCollection = new ArrayList<>();
            for (FriendFollowedDomain friendFollowedDomain : FriendFollowedDomainCollection) {
                userFriendFollowedModelCollection.add(transform(friendFollowedDomain));
            }
        } else {
            userFriendFollowedModelCollection = Collections.emptyList();
        }

        return userFriendFollowedModelCollection;
    }
}
