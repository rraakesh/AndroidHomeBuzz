package us.homebuzz.homebuzz.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodePurchaseDomain;
import us.homebuzz.homebuzz.model.UserModel;
import us.homebuzz.homebuzz.model.ZipPurchaseModel;

/**
 * Created by amit.singh on 11/27/2015.
 */
@PerActivity
public class UserZipPurchaseMapper {
    @Inject
    public UserZipPurchaseMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param allMessagesCollection Object to be transformed.
     * @return {@link UserModel}.
     */
    public Collection<ZipPurchaseModel> transform(Collection<ZipCodePurchaseDomain> allMessagesCollection) {
        Collection<ZipPurchaseModel> userNearbyModelCollection;

        if (allMessagesCollection != null && !allMessagesCollection.isEmpty()) {
            userNearbyModelCollection = new ArrayList<>();
            for (ZipCodePurchaseDomain allMessages : allMessagesCollection) {
                userNearbyModelCollection.add(transform(allMessages));
            }
        } else {
            userNearbyModelCollection = Collections.emptyList();
        }
        return userNearbyModelCollection;
    }

    /**
     * TranForm  A zipPurchaseDomain to ZipPurchaseModel
     *
     * @param zipPurchaseDomain
     * @return
     */
    public ZipPurchaseModel transform(ZipCodePurchaseDomain zipPurchaseDomain) {
        if (zipPurchaseDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipPurchaseModel zipPurchaseModel = new ZipPurchaseModel();
        zipPurchaseModel.setId(zipPurchaseDomain.getId());
        zipPurchaseModel.setUserId(zipPurchaseDomain.getUserId());
        zipPurchaseModel.setExpired(zipPurchaseDomain.getExpired());
        zipPurchaseModel.setExpiredAt(zipPurchaseDomain.getExpiredAt());
        zipPurchaseModel.setZipCode(zipPurchaseDomain.getZipCode());
        return zipPurchaseModel;
    }

}
