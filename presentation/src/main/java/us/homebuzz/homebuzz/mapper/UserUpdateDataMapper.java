package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.profile.UpdateProfileDomain;
import us.homebuzz.homebuzz.model.UserUpdateModel;

/**
 * Created by amit.singh on 11/14/2015.
 */
@PerActivity
public class UserUpdateDataMapper {

    @Inject
    public UserUpdateDataMapper() {
    }

    /**
     * TranForm  A ProfileEntity to ProfileDomain
     *
     * @param updateProfileDomain
     * @return
     */
    public UserUpdateModel transform(UpdateProfileDomain updateProfileDomain) {
        if (updateProfileDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserUpdateModel userModel = new UserUpdateModel();
        userModel.setId(updateProfileDomain.getUser().getId());
        userModel.setEmail(updateProfileDomain.getUser().getEmail());
        userModel.setId(updateProfileDomain.getUser().getId());
        userModel.setProfilePic(updateProfileDomain.getUser().getProfilePic());
        userModel.setToken(updateProfileDomain.getToken());
        userModel.setAccurate(updateProfileDomain.getUser().getAccurate());
        userModel.setEstimatesCount(updateProfileDomain.getUser().getEstimatesCount());
        userModel.setAccurate(updateProfileDomain.getUser().getAccurate());
        userModel.setEstimateZips(updateProfileDomain.getUser().getEstimateZips());
        userModel.setName(updateProfileDomain.getUser().getName());
        userModel.setLevel(updateProfileDomain.getUser().getLevel());
        userModel.setRole(updateProfileDomain.getUser().getRole());
        userModel.setTeamId(updateProfileDomain.getUser().getTeamId());
        userModel.setTeamId(updateProfileDomain.getUser().getTeamId());
        userModel.setRole(updateProfileDomain.getUser().getRole());
        userModel.setZipCode(updateProfileDomain.getUser().getZipCode());
        return userModel;
    }


}
