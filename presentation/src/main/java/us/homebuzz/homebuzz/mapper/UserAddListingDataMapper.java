package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingDomain;
import us.homebuzz.homebuzz.domain.data.addListing.AddListingPhoto;
import us.homebuzz.homebuzz.model.AddListingModel;
import us.homebuzz.homebuzz.model.AddListingPhotoModel;

/**
 * Created by amit.singh on 11/5/2015.
 */
@PerActivity
public class UserAddListingDataMapper {
    @Inject
    public UserAddListingDataMapper() {
    }

    /**
     * Transform a {@link us.homebuzz.homebuzz.di.modules.NearbyModule} into an {@link us.homebuzz.homebuzz.model.UserNearbyModel}.
     *
     * @param addListingDomain
     * @return UserNearbyModel
     */
    public AddListingModel transform(AddListingDomain addListingDomain) {
        if (addListingDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddListingModel userNearbyModel = new AddListingModel();
        userNearbyModel.setId(addListingDomain.getId());
        userNearbyModel.setForSale(addListingDomain.getForSale());
        userNearbyModel.setPhotos(addListingDomain.getPhotos());
        userNearbyModel.setZip(addListingDomain.getZip());
        userNearbyModel.setAddress(addListingDomain.getAddress());
        userNearbyModel.setUserId(addListingDomain.getUserId());
        userNearbyModel.setAgentId(addListingDomain.getAgentId());
        userNearbyModel.setBaths(addListingDomain.getBaths());
        userNearbyModel.setBedrooms(addListingDomain.getBedrooms());
        userNearbyModel.setCity(addListingDomain.getCity());
        System.out.println("Lat Lon"+addListingDomain.getLat()+"LON"+addListingDomain.getLon());
        userNearbyModel.setLat(addListingDomain.getLat());
        userNearbyModel.setLon(addListingDomain.getLon());
        userNearbyModel.setLocationRating(addListingDomain.getLocationRating());
        userNearbyModel.setLastSoldPrice(addListingDomain.getLastSoldPrice());
        userNearbyModel.setYearBuilt(addListingDomain.getYearBuilt());
        userNearbyModel.setStatus(addListingDomain.getStatus());
        userNearbyModel.setRemarks(addListingDomain.getRemarks());
        userNearbyModel.setFavorited(addListingDomain.getFavorited());
        userNearbyModel.setPrice(addListingDomain.getPrice());
        userNearbyModel.setEstimatedSquareFeet(addListingDomain.getEstimatedSquareFeet());
        userNearbyModel.setAcres(addListingDomain.getAcres());
        userNearbyModel.setState(addListingDomain.getState());
        return userNearbyModel;
    }
    public AddListingPhotoModel transform( AddListingPhoto addListingPhoto) {
        if (addListingPhoto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        AddListingPhotoModel addListingPhotoModel = new AddListingPhotoModel();
        addListingPhotoModel.setUrl(addListingPhoto.getUrl());
        System.out.println("Add photo Url"+addListingPhotoModel.getUrl());
        return addListingPhotoModel;
    }
}
