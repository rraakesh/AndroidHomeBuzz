package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.domain.data.zipcode.ZipCodeAddDomain;
import us.homebuzz.homebuzz.model.ZipAddModel;

/**
 * Created by abhishek.singh on 12/15/2015.
 */
public class ZipCodeAddMapper {
    @Inject
    public ZipCodeAddMapper() {
    }

    public ZipAddModel transform(ZipCodeAddDomain zipCodeAddDomain) {
        if (zipCodeAddDomain == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ZipAddModel zipAddModel = new ZipAddModel();
        zipAddModel.setId(zipCodeAddDomain.getId());
        zipAddModel.setUserId(zipCodeAddDomain.getUserId());
        zipAddModel.setZipCode(zipCodeAddDomain.getZipCode());
        zipAddModel.setExpiredAt(zipCodeAddDomain.getExpiredAt());
        zipAddModel.setCreatedAt(zipCodeAddDomain.getCreatedAt());
        zipAddModel.setUpdatedAt(zipCodeAddDomain.getUpdatedAt());
        return zipAddModel;
    }
}
