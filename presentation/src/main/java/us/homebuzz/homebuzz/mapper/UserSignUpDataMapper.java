package us.homebuzz.homebuzz.mapper;

import javax.inject.Inject;

import us.homebuzz.homebuzz.di.PerActivity;
import us.homebuzz.homebuzz.domain.data.User;
import us.homebuzz.homebuzz.domain.data.signup.User_Signup_Domain;
import us.homebuzz.homebuzz.model.UserLoginModel;
import us.homebuzz.homebuzz.model.UserModel;

/**
 * Created by amit.singh on 10/31/2015.
 */
@PerActivity
public class UserSignUpDataMapper {
    @Inject
    public UserSignUpDataMapper() {
    }
    /**
     * Transform a {@link User} into an {@link UserModel}.
     *
     * @param user Object to be transformed.
     * @return {@link UserModel}.
     */
    public UserLoginModel transform(User_Signup_Domain user) {
        if (user == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        UserLoginModel userModel = new UserLoginModel();
        if(user.getUser()!=null) {
            userModel.setEmail(user.getUser().getEmail());
            userModel.setId((user.getUser().getId()));
            userModel.setProfilePic(user.getUser().getProfilePic());
            userModel.setToken(user.getToken());
            userModel.setAccurate(user.getUser().getAccurate());
            //userModel.setEstimatesCount((user.getUser().getEstimatesCount());
            userModel.setAccurate(user.getUser().getAccurate());
            //  userModel.setEstimateZips((String)(user.getUser().getEstimateZips()));
            userModel.setName((String) user.getUser().getName());
            userModel.setLevel(user.getUser().getLevel());
            userModel.setRole(user.getUser().getRole());
            userModel.setTeamId(user.getUser().getTeamId());
            userModel.setTeamId(user.getUser().getTeamId());
            userModel.setRole(user.getUser().getRole());
            userModel.setZipCode(user.getUser().getZipCode());
        }
        userModel.setErrors(user.getErrors());
        return userModel;
    }

}
