package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amit.singh on 10/29/2015.
 */
public class UserProfileModel implements Parcelable {
    private Integer userId;

    private Integer id;
    private String email;
    private Integer signInCount;
    private String currentSignInAt;
    private String lastSignInAt;
    private String confirmedAt;
    private String name;
    private String zipCode;
    private String role;
    private String facebookId;
    private Double accurate;
    private Integer points;
    private String teamId;
    private String officeId;
    private String firstName;
    private String lastName;
    private String subscriptionActive;
    private Boolean confirmed;
    private Boolean followed;
    private Integer soldListings;
    private Integer followers;
    private Integer followings;
    private Integer photos;
    private Integer estimatesCount;
    private Integer checkinCount;
    private Integer reviewsCount;

    private String educations;

    private String bio;

    private String degree;

    private Integer yrGraduated;

    private String college;

    private String otherSchool;

    private String designations;

    private String linkedIn;

    private String website;

    private String profilePic;

    private String verified;

    private Integer agentId;

    private String createdAt;

    private String updatedAt;

    private String phone;

    private String mlsName;

    private String licenseNum;

    private String napNum;

    private String street;

    private String city;

    private String state;

    private String jobTitle;

    private String company;

    private String facebookUrl;

    private String twitterUrl;
    private Integer profile_id;

    /**
     * @return The userId
     */
    public Integer getProfileId() {
        return profile_id;
    }

    /**
     * @param profile_id The user_id
     */
    public void setProfileId(Integer profile_id) {
        this.profile_id = profile_id;
    }

    public Integer getSignInCount() {
        return signInCount;
    }

    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    public String getLastSignInAt() {
        return lastSignInAt;
    }

    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

    public String getConfirmedAt() {
        return confirmedAt;
    }

    public void setConfirmedAt(String confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Double getAccurate() {
        return accurate;
    }

    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSubscriptionActive() {
        return subscriptionActive;
    }

    public void setSubscriptionActive(String subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Boolean getFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public Integer getSoldListings() {
        return soldListings;
    }

    public void setSoldListings(Integer soldListings) {
        this.soldListings = soldListings;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getFollowings() {
        return followings;
    }

    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    public Integer getPhotos() {
        return photos;
    }

    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    public Integer getCheckinCount() {
        return checkinCount;
    }

    public void setCheckinCount(Integer checkinCount) {
        this.checkinCount = checkinCount;
    }

    public Integer getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(Integer reviewsCount) {
        this.reviewsCount = reviewsCount;
    }


    public String getEducations() {
        return educations;
    }

    public void setEducations(String educations) {
        this.educations = educations;
    }


    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * @return The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     * @param degree The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     * @return The yrGraduated
     */
    public Integer getYrGraduated() {
        return yrGraduated;
    }

    /**
     * @param yrGraduated The yr_graduated
     */
    public void setYrGraduated(Integer yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    /**
     * @return The college
     */
    public String getCollege() {
        return college;
    }

    /**
     * @param college The college
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     * @return The otherSchool
     */
    public String getOtherSchool() {
        return otherSchool;
    }

    /**
     * @param otherSchool The other_school
     */
    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    /**
     * @return The designations
     */
    public String getDesignations() {
        return designations;
    }

    /**
     * @param designations The designations
     */
    public void setDesignations(String designations) {
        this.designations = designations;
    }

    /**
     * @return The linkedIn
     */
    public String getLinkedIn() {
        return linkedIn;
    }

    /**
     * @param linkedIn The linked_in
     */
    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     * @param profilePic The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     * @return The verified
     */
    public Object getVerified() {
        return verified;
    }

    /**
     * @param verified The verified
     */
    public void setVerified(String verified) {
        this.verified = verified;
    }

    /**
     * @return The agentId
     */
    public Integer getAgentId() {
        return agentId;
    }

    /**
     * @param agentId The agent_id
     */
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The mlsName
     */
    public String getMlsName() {
        return mlsName;
    }

    /**
     * @param mlsName The mls_name
     */
    public void setMlsName(String mlsName) {
        this.mlsName = mlsName;
    }

    /**
     * @return The licenseNum
     */
    public String getLicenseNum() {
        return licenseNum;
    }

    /**
     * @param licenseNum The license_num
     */
    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    /**
     * @return The napNum
     */
    public String getNapNum() {
        return napNum;
    }

    /**
     * @param napNum The nap_num
     */
    public void setNapNum(String napNum) {
        this.napNum = napNum;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The jobTitle
     */
    public Object getJobTitle() {
        return jobTitle;
    }

    /**
     * @param jobTitle The job_title
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * @return The company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company The company
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return The facebookUrl
     */
    public String getFacebookUrl() {
        return facebookUrl;
    }

    /**
     * @param facebookUrl The facebook_url
     */
    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    /**
     * @return The twitterUrl
     */
    public String getTwitterUrl() {
        return twitterUrl;
    }

    /**
     * @param twitterUrl The twitter_url
     */
    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public UserProfileModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.userId);
        dest.writeValue(this.id);
        dest.writeString(this.email);
        dest.writeValue(this.signInCount);
        dest.writeString(this.currentSignInAt);
        dest.writeString(this.lastSignInAt);
        dest.writeString(this.confirmedAt);
        dest.writeString(this.name);
        dest.writeString(this.zipCode);
        dest.writeString(this.role);
        dest.writeString(this.facebookId);
        dest.writeValue(this.accurate);
        dest.writeValue(this.points);
        dest.writeString(this.teamId);
        dest.writeString(this.officeId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.subscriptionActive);
        dest.writeValue(this.confirmed);
        dest.writeValue(this.followed);
        dest.writeValue(this.soldListings);
        dest.writeValue(this.followers);
        dest.writeValue(this.followings);
        dest.writeValue(this.photos);
        dest.writeValue(this.estimatesCount);
        dest.writeValue(this.checkinCount);
        dest.writeValue(this.reviewsCount);
        dest.writeString(this.educations);
        dest.writeString(this.bio);
        dest.writeString(this.degree);
        dest.writeValue(this.yrGraduated);
        dest.writeString(this.college);
        dest.writeString(this.otherSchool);
        dest.writeString(this.designations);
        dest.writeString(this.linkedIn);
        dest.writeString(this.website);
        dest.writeString(this.profilePic);
        dest.writeString(this.verified);
        dest.writeValue(this.agentId);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.phone);
        dest.writeString(this.mlsName);
        dest.writeString(this.licenseNum);
        dest.writeString(this.napNum);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.jobTitle);
        dest.writeString(this.company);
        dest.writeString(this.facebookUrl);
        dest.writeString(this.twitterUrl);
    }

    protected UserProfileModel(Parcel in) {
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.signInCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.currentSignInAt = in.readString();
        this.lastSignInAt = in.readString();
        this.confirmedAt = in.readString();
        this.name = in.readString();
        this.zipCode = in.readString();
        this.role = in.readString();
        this.facebookId = in.readString();
        this.accurate = (Double) in.readValue(Double.class.getClassLoader());
        this.points = (Integer) in.readValue(Integer.class.getClassLoader());
        this.teamId = in.readString();
        this.officeId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.subscriptionActive = in.readString();
        this.confirmed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.followed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.soldListings = (Integer) in.readValue(Integer.class.getClassLoader());
        this.followers = (Integer) in.readValue(Integer.class.getClassLoader());
        this.followings = (Integer) in.readValue(Integer.class.getClassLoader());
        this.photos = (Integer) in.readValue(Integer.class.getClassLoader());
        this.estimatesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.checkinCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.reviewsCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.educations = in.readString();
        this.bio = in.readString();
        this.degree = in.readString();
        this.yrGraduated = (Integer) in.readValue(Integer.class.getClassLoader());
        this.college = in.readString();
        this.otherSchool = in.readString();
        this.designations = in.readString();
        this.linkedIn = in.readString();
        this.website = in.readString();
        this.profilePic = in.readString();
        this.verified = in.readString();
        this.agentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.phone = in.readString();
        this.mlsName = in.readString();
        this.licenseNum = in.readString();
        this.napNum = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.jobTitle = in.readString();
        this.company = in.readString();
        this.facebookUrl = in.readString();
        this.twitterUrl = in.readString();
    }

    public static final Creator<UserProfileModel> CREATOR = new Creator<UserProfileModel>() {
        public UserProfileModel createFromParcel(Parcel source) {
            return new UserProfileModel(source);
        }

        public UserProfileModel[] newArray(int size) {
            return new UserProfileModel[size];
        }
    };
}
