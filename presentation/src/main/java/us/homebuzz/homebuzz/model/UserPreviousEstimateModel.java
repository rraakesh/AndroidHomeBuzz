package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 10/27/2015.
 */
public class UserPreviousEstimateModel implements Parcelable {

    private Integer id;

    private Integer userId;

    private Integer listingId;

    private Double locationRating;

    private Double conditionRating;

    private Double price;

    private String reason;

    private Object accurate;

    private String createdAt;

    private String updatedAt;

    private User user;

    private Listing listing;

    private Integer bedrooms;


    /**
     *
     * @return
     * The bedrooms
     */
    public Integer getBedrooms() {
        return bedrooms;
    }

    /**
     *
     * @param bedrooms
     * The bedrooms
     */
    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }


    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The listingId
     */
    public Integer getListingId() {
        return listingId;
    }

    /**
     *
     * @param listingId
     * The listing_id
     */
    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    /**
     *
     * @return
     * The locationRating
     */
    public Double getLocationRating() {
        return locationRating;
    }

    /**
     *
     * @param locationRating
     * The location_rating
     */
    public void setLocationRating(Double locationRating) {
        this.locationRating = locationRating;
    }

    /**
     *
     * @return
     * The conditionRating
     */
    public Double getConditionRating() {
        return conditionRating;
    }

    /**
     *
     * @param conditionRating
     * The condition_rating
     */
    public void setConditionRating(Double conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     *
     * @return
     * The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }


    /**
     *
     * @return
     * The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Object getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Object accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The listing
     */
    public Listing getListing() {
        return listing;
    }

    /**
     *
     * @param listing
     * The listing
     */
    public void setListing(Listing listing) {
        this.listing = listing;
    }
    public class Estimate {

        private Integer listingId;

        private Integer id;

        private Integer userId;

        private Double locationRating;

        private Double conditionRating;

        private Double price;

        private String reason;

        private Object accurate;

        private String createdAt;

        private String updatedAt;

        private Integer bedrooms;

        private List<Object> imageUrl = new ArrayList<Object>();

        /**
         * @return The listingId
         */
        public Integer getListingId() {
            return listingId;
        }

        /**
         * @param listingId The listing_id
         */
        public void setListingId(Integer listingId) {
            this.listingId = listingId;
        }

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The bedrooms
         */
        public Integer getBedrooms() {
            return bedrooms;
        }

        /**
         * @param bedrooms The bedrooms
         */
        public void setBedrooms(Integer bedrooms) {
            this.bedrooms = bedrooms;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The user_id
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The locationRating
         */
        public Double getLocationRating() {
            return locationRating;
        }

        /**
         * @param locationRating The location_rating
         */
        public void setLocationRating(Double locationRating) {
            this.locationRating = locationRating;
        }

        /**
         * @return The conditionRating
         */
        public Double getConditionRating() {
            return conditionRating;
        }

        /**
         * @param conditionRating The condition_rating
         */
        public void setConditionRating(Double conditionRating) {
            this.conditionRating = conditionRating;
        }

        /**
         * @return The price
         */
        public Double getPrice() {
            return price;
        }

        /**
         * @param price The price
         */
        public void setPrice(Double price) {
            this.price = price;
        }

        /**
         * @return The reason
         */
        public String getReason() {
            return reason;
        }

        /**
         * @param reason The reason
         */
        public void setReason(String reason) {
            this.reason = reason;
        }

        /**
         * @return The accurate
         */
        public Object getAccurate() {
            return accurate;
        }

        /**
         * @param accurate The accurate
         */
        public void setAccurate(Object accurate) {
            this.accurate = accurate;
        }

        /**
         * @return The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         * @param updatedAt The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         * @return The imageUrl
         */
        public List<Object> getImageUrl() {
            return imageUrl;
        }

        /**
         * @param imageUrl The image_url
         */
        public void setImageUrl(List<Object> imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
    public class Listing {

        private Integer id;

        private Object user;

        private String mlsNum;

        private String listingClass;

        private String listingClassType;

        private String status;

        private Boolean forSale;

        private Double lat;

        private Double lon;

        private String address;

        private String zip;

        private List<Object> photos = new ArrayList<Object>();

        private List<Object> thumbnails = new ArrayList<Object>();

        private Double locationRating;

        private Double conditionRating;

        private Integer bathshalf;

        private Integer fireplaces;

        private Integer bedrooms;

        private Object optionalBedrooms;

        private Integer baths;

        private Double acres;

        private Double price;

        private String listOnMarket;

        private Double lastSoldPrice;

        private Integer yearBuilt;

        private Double recordedMortgage;

        private Double assesedValue;

        private Double currentTaxes;

        private String remarks;

        private Double homeOwnerFees;

        private Double homeOwnerTotalFees;

        private Object otherFees;

        private Object melloRoosFee;

        private Double bathsfull;

        private Object parkngNonGaragedSpaces;

        private Double estimatedSquareFeet;

        private Double lotSqftApprox;

        private Double garage;

        private String agentPhone;

        private Object parkingSpacesTotal;

        private Object monthlyTotalFees;

        private String createdAt;

        private String updatedAt;

        private String street;

        private String city;

        private String state;

        private String agentId;

        private String tourLink;

        private String mlsName;

        private String sysId;

        private Object userId;

        private Object upgrade1;

        private Object upgrade2;

        private Object upgrade3;

        private Object upgrade4;

        private Object upgrade1Pic;

        private Object upgrade2Pic;

        private Object upgrade3Pic;

        private Object upgrade4Pic;

        private Object greenFeature1;

        private Object greenFeature2;

        private Object greenFeature3;

        private Object greenFeature4;

        private Object greenFeature1Pic;

        private Object greenFeature2Pic;

        private Object greenFeature3Pic;

        private Object greenFeature4Pic;

        private Boolean favorited;

        private List<Object> previousReviews = new ArrayList<Object>();

        private List<us.homebuzz.homebuzz.domain.data.previousEstimate.Estimate> estimates = new ArrayList<us.homebuzz.homebuzz.domain.data.previousEstimate.Estimate>();

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The user
         */
        public Object getUser() {
            return user;
        }

        /**
         *
         * @param user
         * The user
         */
        public void setUser(Object user) {
            this.user = user;
        }

        /**
         *
         * @return
         * The mlsNum
         */
        public String getMlsNum() {
            return mlsNum;
        }

        /**
         *
         * @param mlsNum
         * The mls_num
         */
        public void setMlsNum(String mlsNum) {
            this.mlsNum = mlsNum;
        }

        /**
         *
         * @return
         * The listingClass
         */
        public String getListingClass() {
            return listingClass;
        }

        /**
         *
         * @param listingClass
         * The listing_class
         */
        public void setListingClass(String listingClass) {
            this.listingClass = listingClass;
        }

        /**
         *
         * @return
         * The listingClassType
         */
        public String getListingClassType() {
            return listingClassType;
        }

        /**
         *
         * @param listingClassType
         * The listing_class_type
         */
        public void setListingClassType(String listingClassType) {
            this.listingClassType = listingClassType;
        }

        /**
         *
         * @return
         * The status
         */
        public String getStatus() {
            return status;
        }

        /**
         *
         * @param status
         * The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         *
         * @return
         * The forSale
         */
        public Boolean getForSale() {
            return forSale;
        }

        /**
         *
         * @param forSale
         * The for_sale
         */
        public void setForSale(Boolean forSale) {
            this.forSale = forSale;
        }

        /**
         *
         * @return
         * The lat
         */
        public Double getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(Double lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The lon
         */
        public Double getLon() {
            return lon;
        }

        /**
         *
         * @param lon
         * The lon
         */
        public void setLon(Double lon) {
            this.lon = lon;
        }

        /**
         *
         * @return
         * The address
         */
        public String getAddress() {
            return address;
        }

        /**
         *
         * @param address
         * The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         *
         * @return
         * The zip
         */
        public String getZip() {
            return zip;
        }

        /**
         *
         * @param zip
         * The zip
         */
        public void setZip(String zip) {
            this.zip = zip;
        }

        /**
         *
         * @return
         * The photos
         */
        public List<Object> getPhotos() {
            return photos;
        }

        /**
         *
         * @param photos
         * The photos
         */
        public void setPhotos(List<Object> photos) {
            this.photos = photos;
        }

        /**
         *
         * @return
         * The thumbnails
         */
        public List<Object> getThumbnails() {
            return thumbnails;
        }

        /**
         *
         * @param thumbnails
         * The thumbnails
         */
        public void setThumbnails(List<Object> thumbnails) {
            this.thumbnails = thumbnails;
        }

        /**
         *
         * @return
         * The locationRating
         */
        public Double getLocationRating() {
            return locationRating;
        }

        /**
         *
         * @param locationRating
         * The location_rating
         */
        public void setLocationRating(Double locationRating) {
            this.locationRating = locationRating;
        }

        /**
         *
         * @return
         * The conditionRating
         */
        public Double getConditionRating() {
            return conditionRating;
        }

        /**
         *
         * @param conditionRating
         * The condition_rating
         */
        public void setConditionRating(Double conditionRating) {
            this.conditionRating = conditionRating;
        }

        /**
         *
         * @return
         * The bathshalf
         */
        public Integer getBathshalf() {
            return bathshalf;
        }

        /**
         *
         * @param bathshalf
         * The bathshalf
         */
        public void setBathshalf(Integer bathshalf) {
            this.bathshalf = bathshalf;
        }

        /**
         *
         * @return
         * The fireplaces
         */
        public Integer getFireplaces() {
            return fireplaces;
        }

        /**
         *
         * @param fireplaces
         * The fireplaces
         */
        public void setFireplaces(Integer fireplaces) {
            this.fireplaces = fireplaces;
        }

        /**
         *
         * @return
         * The bedrooms
         */
        public Integer getBedrooms() {
            return bedrooms;
        }

        /**
         *
         * @param bedrooms
         * The bedrooms
         */
        public void setBedrooms(Integer bedrooms) {
            this.bedrooms = bedrooms;
        }

        /**
         *
         * @return
         * The optionalBedrooms
         */
        public Object getOptionalBedrooms() {
            return optionalBedrooms;
        }

        /**
         *
         * @param optionalBedrooms
         * The optional_bedrooms
         */
        public void setOptionalBedrooms(Object optionalBedrooms) {
            this.optionalBedrooms = optionalBedrooms;
        }

        /**
         *
         * @return
         * The baths
         */
        public Integer getBaths() {
            return baths;
        }

        /**
         *
         * @param baths
         * The baths
         */
        public void setBaths(Integer baths) {
            this.baths = baths;
        }

        /**
         *
         * @return
         * The acres
         */
        public Double getAcres() {
            return acres;
        }

        /**
         *
         * @param acres
         * The acres
         */
        public void setAcres(Double acres) {
            this.acres = acres;
        }

        /**
         *
         * @return
         * The price
         */
        public Double getPrice() {
            return price;
        }

        /**
         *
         * @param price
         * The price
         */
        public void setPrice(Double price) {
            this.price = price;
        }

        /**
         *
         * @return
         * The listOnMarket
         */
        public String getListOnMarket() {
            return listOnMarket;
        }

        /**
         *
         * @param listOnMarket
         * The list_on_market
         */
        public void setListOnMarket(String listOnMarket) {
            this.listOnMarket = listOnMarket;
        }

        /**
         *
         * @return
         * The lastSoldPrice
         */
        public Double getLastSoldPrice() {
            return lastSoldPrice;
        }

        /**
         *
         * @param lastSoldPrice
         * The last_sold_price
         */
        public void setLastSoldPrice(Double lastSoldPrice) {
            this.lastSoldPrice = lastSoldPrice;
        }

        /**
         *
         * @return
         * The yearBuilt
         */
        public Integer getYearBuilt() {
            return yearBuilt;
        }

        /**
         *
         * @param yearBuilt
         * The year_built
         */
        public void setYearBuilt(Integer yearBuilt) {
            this.yearBuilt = yearBuilt;
        }

        /**
         *
         * @return
         * The recordedMortgage
         */
        public Double getRecordedMortgage() {
            return recordedMortgage;
        }

        /**
         *
         * @param recordedMortgage
         * The recorded_mortgage
         */
        public void setRecordedMortgage(Double recordedMortgage) {
            this.recordedMortgage = recordedMortgage;
        }

        /**
         *
         * @return
         * The assesedValue
         */
        public Double getAssesedValue() {
            return assesedValue;
        }

        /**
         *
         * @param assesedValue
         * The assesed_value
         */
        public void setAssesedValue(Double assesedValue) {
            this.assesedValue = assesedValue;
        }

        /**
         *
         * @return
         * The currentTaxes
         */
        public Double getCurrentTaxes() {
            return currentTaxes;
        }

        /**
         *
         * @param currentTaxes
         * The current_taxes
         */
        public void setCurrentTaxes(Double currentTaxes) {
            this.currentTaxes = currentTaxes;
        }

        /**
         *
         * @return
         * The remarks
         */
        public String getRemarks() {
            return remarks;
        }

        /**
         *
         * @param remarks
         * The remarks
         */
        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        /**
         *
         * @return
         * The homeOwnerFees
         */
        public Double getHomeOwnerFees() {
            return homeOwnerFees;
        }

        /**
         *
         * @param homeOwnerFees
         * The home_owner_fees
         */
        public void setHomeOwnerFees(Double homeOwnerFees) {
            this.homeOwnerFees = homeOwnerFees;
        }

        /**
         *
         * @return
         * The homeOwnerTotalFees
         */
        public Double getHomeOwnerTotalFees() {
            return homeOwnerTotalFees;
        }

        /**
         *
         * @param homeOwnerTotalFees
         * The home_owner_total_fees
         */
        public void setHomeOwnerTotalFees(Double homeOwnerTotalFees) {
            this.homeOwnerTotalFees = homeOwnerTotalFees;
        }

        /**
         *
         * @return
         * The otherFees
         */
        public Object getOtherFees() {
            return otherFees;
        }

        /**
         *
         * @param otherFees
         * The other_fees
         */
        public void setOtherFees(Object otherFees) {
            this.otherFees = otherFees;
        }

        /**
         *
         * @return
         * The melloRoosFee
         */
        public Object getMelloRoosFee() {
            return melloRoosFee;
        }

        /**
         *
         * @param melloRoosFee
         * The mello_roos_fee
         */
        public void setMelloRoosFee(Object melloRoosFee) {
            this.melloRoosFee = melloRoosFee;
        }

        /**
         *
         * @return
         * The bathsfull
         */
        public Double getBathsfull() {
            return bathsfull;
        }

        /**
         *
         * @param bathsfull
         * The bathsfull
         */
        public void setBathsfull(Double bathsfull) {
            this.bathsfull = bathsfull;
        }

        /**
         *
         * @return
         * The parkngNonGaragedSpaces
         */
        public Object getParkngNonGaragedSpaces() {
            return parkngNonGaragedSpaces;
        }

        /**
         *
         * @param parkngNonGaragedSpaces
         * The parkng_non_garaged_spaces
         */
        public void setParkngNonGaragedSpaces(Object parkngNonGaragedSpaces) {
            this.parkngNonGaragedSpaces = parkngNonGaragedSpaces;
        }

        /**
         *
         * @return
         * The estimatedSquareFeet
         */
        public Double getEstimatedSquareFeet() {
            return estimatedSquareFeet;
        }

        /**
         *
         * @param estimatedSquareFeet
         * The estimated_square_feet
         */
        public void setEstimatedSquareFeet(Double estimatedSquareFeet) {
            this.estimatedSquareFeet = estimatedSquareFeet;
        }

        /**
         *
         * @return
         * The lotSqftApprox
         */
        public Double getLotSqftApprox() {
            return lotSqftApprox;
        }

        /**
         *
         * @param lotSqftApprox
         * The lot_sqft_approx
         */
        public void setLotSqftApprox(Double lotSqftApprox) {
            this.lotSqftApprox = lotSqftApprox;
        }

        /**
         *
         * @return
         * The garage
         */
        public Double getGarage() {
            return garage;
        }

        /**
         *
         * @param garage
         * The garage
         */
        public void setGarage(Double garage) {
            this.garage = garage;
        }

        /**
         *
         * @return
         * The agentPhone
         */
        public String getAgentPhone() {
            return agentPhone;
        }

        /**
         *
         * @param agentPhone
         * The agent_phone
         */
        public void setAgentPhone(String agentPhone) {
            this.agentPhone = agentPhone;
        }

        /**
         *
         * @return
         * The parkingSpacesTotal
         */
        public Object getParkingSpacesTotal() {
            return parkingSpacesTotal;
        }

        /**
         *
         * @param parkingSpacesTotal
         * The parking_spaces_total
         */
        public void setParkingSpacesTotal(Object parkingSpacesTotal) {
            this.parkingSpacesTotal = parkingSpacesTotal;
        }

        /**
         *
         * @return
         * The monthlyTotalFees
         */
        public Object getMonthlyTotalFees() {
            return monthlyTotalFees;
        }

        /**
         *
         * @param monthlyTotalFees
         * The monthly_total_fees
         */
        public void setMonthlyTotalFees(Object monthlyTotalFees) {
            this.monthlyTotalFees = monthlyTotalFees;
        }

        /**
         *
         * @return
         * The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         * The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         *
         * @return
         * The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         *
         * @param updatedAt
         * The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         *
         * @return
         * The street
         */
        public String getStreet() {
            return street;
        }

        /**
         *
         * @param street
         * The street
         */
        public void setStreet(String street) {
            this.street = street;
        }

        /**
         *
         * @return
         * The city
         */
        public String getCity() {
            return city;
        }

        /**
         *
         * @param city
         * The city
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         *
         * @return
         * The state
         */
        public String getState() {
            return state;
        }

        /**
         *
         * @param state
         * The state
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         *
         * @return
         * The agentId
         */
        public String getAgentId() {
            return agentId;
        }

        /**
         *
         * @param agentId
         * The agent_id
         */
        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }

        /**
         *
         * @return
         * The tourLink
         */
        public String getTourLink() {
            return tourLink;
        }

        /**
         *
         * @param tourLink
         * The tour_link
         */
        public void setTourLink(String tourLink) {
            this.tourLink = tourLink;
        }

        /**
         *
         * @return
         * The mlsName
         */
        public String getMlsName() {
            return mlsName;
        }

        /**
         *
         * @param mlsName
         * The mls_name
         */
        public void setMlsName(String mlsName) {
            this.mlsName = mlsName;
        }

        /**
         *
         * @return
         * The sysId
         */
        public String getSysId() {
            return sysId;
        }

        /**
         *
         * @param sysId
         * The sys_id
         */
        public void setSysId(String sysId) {
            this.sysId = sysId;
        }

        /**
         *
         * @return
         * The userId
         */
        public Object getUserId() {
            return userId;
        }

        /**
         *
         * @param userId
         * The user_id
         */
        public void setUserId(Object userId) {
            this.userId = userId;
        }

        /**
         *
         * @return
         * The upgrade1
         */
        public Object getUpgrade1() {
            return upgrade1;
        }

        /**
         *
         * @param upgrade1
         * The upgrade1
         */
        public void setUpgrade1(Object upgrade1) {
            this.upgrade1 = upgrade1;
        }

        /**
         *
         * @return
         * The upgrade2
         */
        public Object getUpgrade2() {
            return upgrade2;
        }

        /**
         *
         * @param upgrade2
         * The upgrade2
         */
        public void setUpgrade2(Object upgrade2) {
            this.upgrade2 = upgrade2;
        }

        /**
         *
         * @return
         * The upgrade3
         */
        public Object getUpgrade3() {
            return upgrade3;
        }

        /**
         *
         * @param upgrade3
         * The upgrade3
         */
        public void setUpgrade3(Object upgrade3) {
            this.upgrade3 = upgrade3;
        }

        /**
         *
         * @return
         * The upgrade4
         */
        public Object getUpgrade4() {
            return upgrade4;
        }

        /**
         *
         * @param upgrade4
         * The upgrade4
         */
        public void setUpgrade4(Object upgrade4) {
            this.upgrade4 = upgrade4;
        }

        /**
         *
         * @return
         * The upgrade1Pic
         */
        public Object getUpgrade1Pic() {
            return upgrade1Pic;
        }

        /**
         *
         * @param upgrade1Pic
         * The upgrade1_pic
         */
        public void setUpgrade1Pic(Object upgrade1Pic) {
            this.upgrade1Pic = upgrade1Pic;
        }

        /**
         *
         * @return
         * The upgrade2Pic
         */
        public Object getUpgrade2Pic() {
            return upgrade2Pic;
        }

        /**
         *
         * @param upgrade2Pic
         * The upgrade2_pic
         */
        public void setUpgrade2Pic(Object upgrade2Pic) {
            this.upgrade2Pic = upgrade2Pic;
        }

        /**
         *
         * @return
         * The upgrade3Pic
         */
        public Object getUpgrade3Pic() {
            return upgrade3Pic;
        }

        /**
         *
         * @param upgrade3Pic
         * The upgrade3_pic
         */
        public void setUpgrade3Pic(Object upgrade3Pic) {
            this.upgrade3Pic = upgrade3Pic;
        }

        /**
         *
         * @return
         * The upgrade4Pic
         */
        public Object getUpgrade4Pic() {
            return upgrade4Pic;
        }

        /**
         *
         * @param upgrade4Pic
         * The upgrade4_pic
         */
        public void setUpgrade4Pic(Object upgrade4Pic) {
            this.upgrade4Pic = upgrade4Pic;
        }

        /**
         *
         * @return
         * The greenFeature1
         */
        public Object getGreenFeature1() {
            return greenFeature1;
        }

        /**
         *
         * @param greenFeature1
         * The green_feature1
         */
        public void setGreenFeature1(Object greenFeature1) {
            this.greenFeature1 = greenFeature1;
        }

        /**
         *
         * @return
         * The greenFeature2
         */
        public Object getGreenFeature2() {
            return greenFeature2;
        }

        /**
         *
         * @param greenFeature2
         * The green_feature2
         */
        public void setGreenFeature2(Object greenFeature2) {
            this.greenFeature2 = greenFeature2;
        }

        /**
         *
         * @return
         * The greenFeature3
         */
        public Object getGreenFeature3() {
            return greenFeature3;
        }

        /**
         *
         * @param greenFeature3
         * The green_feature3
         */
        public void setGreenFeature3(Object greenFeature3) {
            this.greenFeature3 = greenFeature3;
        }

        /**
         *
         * @return
         * The greenFeature4
         */
        public Object getGreenFeature4() {
            return greenFeature4;
        }

        /**
         *
         * @param greenFeature4
         * The green_feature4
         */
        public void setGreenFeature4(Object greenFeature4) {
            this.greenFeature4 = greenFeature4;
        }

        /**
         *
         * @return
         * The greenFeature1Pic
         */
        public Object getGreenFeature1Pic() {
            return greenFeature1Pic;
        }

        /**
         *
         * @param greenFeature1Pic
         * The green_feature1_pic
         */
        public void setGreenFeature1Pic(Object greenFeature1Pic) {
            this.greenFeature1Pic = greenFeature1Pic;
        }

        /**
         *
         * @return
         * The greenFeature2Pic
         */
        public Object getGreenFeature2Pic() {
            return greenFeature2Pic;
        }

        /**
         *
         * @param greenFeature2Pic
         * The green_feature2_pic
         */
        public void setGreenFeature2Pic(Object greenFeature2Pic) {
            this.greenFeature2Pic = greenFeature2Pic;
        }

        /**
         *
         * @return
         * The greenFeature3Pic
         */
        public Object getGreenFeature3Pic() {
            return greenFeature3Pic;
        }

        /**
         *
         * @param greenFeature3Pic
         * The green_feature3_pic
         */
        public void setGreenFeature3Pic(Object greenFeature3Pic) {
            this.greenFeature3Pic = greenFeature3Pic;
        }

        /**
         *
         * @return
         * The greenFeature4Pic
         */
        public Object getGreenFeature4Pic() {
            return greenFeature4Pic;
        }

        /**
         *
         * @param greenFeature4Pic
         * The green_feature4_pic
         */
        public void setGreenFeature4Pic(Object greenFeature4Pic) {
            this.greenFeature4Pic = greenFeature4Pic;
        }

        /**
         *
         * @return
         * The favorited
         */
        public Boolean getFavorited() {
            return favorited;
        }

        /**
         *
         * @param favorited
         * The favorited
         */
        public void setFavorited(Boolean favorited) {
            this.favorited = favorited;
        }

        /**
         *
         * @return
         * The previousReviews
         */
        public List<Object> getPreviousReviews() {
            return previousReviews;
        }

        /**
         *
         * @param previousReviews
         * The previous_reviews
         */
        public void setPreviousReviews(List<Object> previousReviews) {
            this.previousReviews = previousReviews;
        }

        /**
         *
         * @return
         * The estimates
         */
        public List<us.homebuzz.homebuzz.domain.data.previousEstimate.Estimate> getEstimates() {
            return estimates;
        }

        /**
         *
         * @param estimates
         * The estimates
         */
        public void setEstimates(List<us.homebuzz.homebuzz.domain.data.previousEstimate.Estimate> estimates) {
            this.estimates = estimates;
        }
    }
    public class User {

        private Integer id;

        private String email;

        private Integer signInCount;

        private String currentSignInAt;

        private String lastSignInAt;

        private Object confirmedAt;

        private String name;

        private String zipCode;

        private String role;

        private Object facebookId;

        private Integer estimatesCount;

        private Double accurate;

        private Boolean confirmed;

        private Boolean followed;

        private String bio;

        private String degree;

        private Integer yrGraduated;

        private String college;

        private String otherSchool;

        private String designations;

        private String linkedIn;

        private String website;

        private String profilePic;

        private Object verified;

        private Integer agentId;

        private String phone;

        private String educations;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The email
         */
        public String getEmail() {
            return email;
        }

        /**
         *
         * @param email
         * The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         *
         * @return
         * The signInCount
         */
        public Integer getSignInCount() {
            return signInCount;
        }

        /**
         *
         * @param signInCount
         * The sign_in_count
         */
        public void setSignInCount(Integer signInCount) {
            this.signInCount = signInCount;
        }

        /**
         *
         * @return
         * The currentSignInAt
         */
        public String getCurrentSignInAt() {
            return currentSignInAt;
        }

        /**
         *
         * @param currentSignInAt
         * The current_sign_in_at
         */
        public void setCurrentSignInAt(String currentSignInAt) {
            this.currentSignInAt = currentSignInAt;
        }

        /**
         *
         * @return
         * The lastSignInAt
         */
        public String getLastSignInAt() {
            return lastSignInAt;
        }

        /**
         *
         * @param lastSignInAt
         * The last_sign_in_at
         */
        public void setLastSignInAt(String lastSignInAt) {
            this.lastSignInAt = lastSignInAt;
        }

  /*  *//**
         *
         * @return
         * The currentSignInIp
         *//*
    public CurrentSignInIp getCurrentSignInIp() {
        return currentSignInIp;
    }

    *//**
         *
         * @param currentSignInIp
         * The current_sign_in_ip
         *//*
    public void setCurrentSignInIp(CurrentSignInIp currentSignInIp) {
        this.currentSignInIp = currentSignInIp;
    }

    *//**
         *
         * @return
         * The lastSignInIp
         *//*
    public LastSignInIp getLastSignInIp() {
        return lastSignInIp;
    }

    *//**
         *
         * @param lastSignInIp
         * The last_sign_in_ip
         *//*
    public void setLastSignInIp(LastSignInIp lastSignInIp) {
        this.lastSignInIp = lastSignInIp;
    }*/

        /**
         *
         * @return
         * The confirmedAt
         */
        public Object getConfirmedAt() {
            return confirmedAt;
        }

        /**
         *
         * @param confirmedAt
         * The confirmed_at
         */
        public void setConfirmedAt(Object confirmedAt) {
            this.confirmedAt = confirmedAt;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         * The zipCode
         */
        public String getZipCode() {
            return zipCode;
        }

        /**
         *
         * @param zipCode
         * The zip_code
         */
        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        /**
         *
         * @return
         * The role
         */
        public String getRole() {
            return role;
        }

        /**
         *
         * @param role
         * The role
         */
        public void setRole(String role) {
            this.role = role;
        }

        /**
         *
         * @return
         * The facebookId
         */
        public Object getFacebookId() {
            return facebookId;
        }

        /**
         *
         * @param facebookId
         * The facebook_id
         */
        public void setFacebookId(Object facebookId) {
            this.facebookId = facebookId;
        }

        /**
         *
         * @return
         * The estimatesCount
         */
        public Integer getEstimatesCount() {
            return estimatesCount;
        }

        /**
         *
         * @param estimatesCount
         * The estimates_count
         */
        public void setEstimatesCount(Integer estimatesCount) {
            this.estimatesCount = estimatesCount;
        }

        /**
         *
         * @return
         * The accurate
         */
        public Double getAccurate() {
            return accurate;
        }

        /**
         *
         * @param accurate
         * The accurate
         */
        public void setAccurate(Double accurate) {
            this.accurate = accurate;
        }

        /**
         *
         * @return
         * The confirmed
         */
        public Boolean getConfirmed() {
            return confirmed;
        }

        /**
         *
         * @param confirmed
         * The confirmed
         */
        public void setConfirmed(Boolean confirmed) {
            this.confirmed = confirmed;
        }

        /**
         *
         * @return
         * The followed
         */
        public Boolean getFollowed() {
            return followed;
        }

        /**
         *
         * @param followed
         * The followed
         */
        public void setFollowed(Boolean followed) {
            this.followed = followed;
        }

        /**
         *
         * @return
         * The bio
         */
        public String getBio() {
            return bio;
        }

        /**
         *
         * @param bio
         * The bio
         */
        public void setBio(String bio) {
            this.bio = bio;
        }

        /**
         *
         * @return
         * The degree
         */
        public String getDegree() {
            return degree;
        }

        /**
         *
         * @param degree
         * The degree
         */
        public void setDegree(String degree) {
            this.degree = degree;
        }

        /**
         *
         * @return
         * The yrGraduated
         */
        public Integer getYrGraduated() {
            return yrGraduated;
        }

        /**
         *
         * @param yrGraduated
         * The yr_graduated
         */
        public void setYrGraduated(Integer yrGraduated) {
            this.yrGraduated = yrGraduated;
        }

        /**
         *
         * @return
         * The college
         */
        public String getCollege() {
            return college;
        }

        /**
         *
         * @param college
         * The college
         */
        public void setCollege(String college) {
            this.college = college;
        }

        /**
         *
         * @return
         * The otherSchool
         */
        public String getOtherSchool() {
            return otherSchool;
        }

        /**
         *
         * @param otherSchool
         * The other_school
         */
        public void setOtherSchool(String otherSchool) {
            this.otherSchool = otherSchool;
        }

        /**
         *
         * @return
         * The designations
         */
        public String getDesignations() {
            return designations;
        }

        /**
         *
         * @param designations
         * The designations
         */
        public void setDesignations(String designations) {
            this.designations = designations;
        }

        /**
         *
         * @return
         * The linkedIn
         */
        public String getLinkedIn() {
            return linkedIn;
        }

        /**
         *
         * @param linkedIn
         * The linked_in
         */
        public void setLinkedIn(String linkedIn) {
            this.linkedIn = linkedIn;
        }

        /**
         *
         * @return
         * The website
         */
        public String getWebsite() {
            return website;
        }

        /**
         *
         * @param website
         * The website
         */
        public void setWebsite(String website) {
            this.website = website;
        }

        /**
         *
         * @return
         * The profilePic
         */
        public String getProfilePic() {
            return profilePic;
        }

        /**
         *
         * @param profilePic
         * The profile_pic
         */
        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        /**
         *
         * @return
         * The verified
         */
        public Object getVerified() {
            return verified;
        }

        /**
         *
         * @param verified
         * The verified
         */
        public void setVerified(Object verified) {
            this.verified = verified;
        }

        /**
         *
         * @return
         * The agentId
         */
        public Integer getAgentId() {
            return agentId;
        }

        /**
         *
         * @param agentId
         * The agent_id
         */
        public void setAgentId(Integer agentId) {
            this.agentId = agentId;
        }

        /**
         *
         * @return
         * The phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         *
         * @param phone
         * The phone
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         *
         * @return
         * The educations
         */
        public String getEducations() {
            return educations;
        }

        /**
         *
         * @param educations
         * The educations
         */
        public void setEducations(String educations) {
            this.educations = educations;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.userId);
        dest.writeValue(this.listingId);
        dest.writeValue(this.locationRating);
        dest.writeValue(this.conditionRating);
        dest.writeValue(this.price);
        dest.writeString(this.reason);
        dest.writeString((String) this.accurate);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(String.valueOf(this.user));
        dest.writeString(String.valueOf( this.listing));
    }

    public UserPreviousEstimateModel() {
    }

    private UserPreviousEstimateModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.listingId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.locationRating = (Double) in.readValue(Double.class.getClassLoader());
        this.conditionRating = (Double) in.readValue(Double.class.getClassLoader());
        this.price = (Double) in.readValue(Double.class.getClassLoader());
        this.reason = in.readString();
        this.accurate = in.readParcelable(Object.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.listing = in.readParcelable(Listing.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserPreviousEstimateModel> CREATOR = new Parcelable.Creator<UserPreviousEstimateModel>() {
        public UserPreviousEstimateModel createFromParcel(Parcel source) {
            return new UserPreviousEstimateModel(source);
        }

        public UserPreviousEstimateModel[] newArray(int size) {
            return new UserPreviousEstimateModel[size];
        }
    };
}
