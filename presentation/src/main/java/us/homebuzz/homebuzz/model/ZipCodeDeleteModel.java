package us.homebuzz.homebuzz.model;

/**
 * Created by abhishek.singh on 12/16/2015.
 */
public class ZipCodeDeleteModel {
    private Boolean delete;

    /**
     * @return The delete
     */
    public Boolean getDelete() {
        return delete;
    }

    /**
     * @param delete The delete
     */
    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

}
