package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class UserJustSoldModel implements Parcelable {
    private Double lat;

    private Double lon;

    private Integer id;

    private Integer userId;

    private String mlsNum;

    private String listingClass;

    private String listingClassType;

    private String status;

    private Boolean forSale;

    private String address;

    private String zip;

    private Double locationRating;

    private Double conditionRating;

    private Integer bathshalf;

    private Integer fireplaces;

    private Integer bedrooms;

    private Integer optionalBedrooms;

    private Integer baths;

    private String acres;

    private Integer price;

    private String listOnMarket;

    private String lastSoldPrice;

    private Integer yearBuilt;

    private String recordedMortgage;

    private String assesedValue;

    private String currentTaxes;

    private String remarks;

    private String homeOwnerFees;

    private String homeOwnerTotalFees;

    private String otherFees;

    private String melloRoosFee;

    private String bathsfull;

    private String parkngNonGaragedSpaces;

    private String estimatedSquareFeet;

    private String lotSqftApprox;

    private String garage;

    private String parkingSpacesTotal;

    private String monthlyTotalFees;

    private String createdAt;

    private String updatedAt;

    private String agentPhone;

    private String street;

    private String city;
    private String state;

    private String agentId;

    private String tourLink;

    private String mlsName;

    private Object sysId;

    private Object upgrade1;

    private Object upgrade2;

    private Object upgrade3;

    private Object upgrade4;

    private Object greenFeature1;

    private Object greenFeature2;

    private Object greenFeature3;

    private Object greenFeature4;

    private Object upgrade1Pic;

    private Object upgrade2Pic;

    private Object upgrade3Pic;

    private Object upgrade4Pic;

    private Object greenFeature1Pic;

    private Object greenFeature2Pic;

    private Object greenFeature3Pic;

    private Object greenFeature4Pic;

    private List<String> photos = new ArrayList<String>();

    private List<String> thumbnails = new ArrayList<String>();

    private String parcelNum;

    private Object avmLow;

    private Object avmHigh;

    private Object soldDate;

    private Object distance;

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The mlsNum
     */
    public String getMlsNum() {
        return mlsNum;
    }

    /**
     * @param mlsNum The mls_num
     */
    public void setMlsNum(String mlsNum) {
        this.mlsNum = mlsNum;
    }

    /**
     * @return The listingClass
     */
    public String getListingClass() {
        return listingClass;
    }

    /**
     * @param listingClass The listing_class
     */
    public void setListingClass(String listingClass) {
        this.listingClass = listingClass;
    }

    /**
     * @return The listingClassType
     */
    public String getListingClassType() {
        return listingClassType;
    }

    /**
     * @param listingClassType The listing_class_type
     */
    public void setListingClassType(String listingClassType) {
        this.listingClassType = listingClassType;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The forSale
     */
    public Boolean getForSale() {
        return forSale;
    }

    /**
     * @param forSale The for_sale
     */
    public void setForSale(Boolean forSale) {
        this.forSale = forSale;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip The zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return The locationRating
     */
    public Double getLocationRating() {
        return locationRating;
    }

    /**
     * @param locationRating The location_rating
     */
    public void setLocationRating(Double locationRating) {
        this.locationRating = locationRating;
    }

    /**
     * @return The conditionRating
     */
    public Double getConditionRating() {
        return conditionRating;
    }

    /**
     * @param conditionRating The condition_rating
     */
    public void setConditionRating(Double conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     * @return The bathshalf
     */
    public Integer getBathshalf() {
        return bathshalf;
    }

    /**
     * @param bathshalf The bathshalf
     */
    public void setBathshalf(Integer bathshalf) {
        this.bathshalf = bathshalf;
    }

    /**
     * @return The fireplaces
     */
    public Integer getFireplaces() {
        return fireplaces;
    }

    /**
     * @param fireplaces The fireplaces
     */
    public void setFireplaces(Integer fireplaces) {
        this.fireplaces = fireplaces;
    }

    /**
     * @return The bedrooms
     */
    public Integer getBedrooms() {
        return bedrooms;
    }

    /**
     * @param bedrooms The bedrooms
     */
    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    /**
     * @return The optionalBedrooms
     */
    public Integer getOptionalBedrooms() {
        return optionalBedrooms;
    }

    /**
     * @param optionalBedrooms The optional_bedrooms
     */
    public void setOptionalBedrooms(Integer optionalBedrooms) {
        this.optionalBedrooms = optionalBedrooms;
    }

    /**
     * @return The baths
     */
    public Integer getBaths() {
        return baths;
    }

    /**
     * @param baths The baths
     */
    public void setBaths(Integer baths) {
        this.baths = baths;
    }

    /**
     * @return The acres
     */
    public String getAcres() {
        return acres;
    }

    /**
     * @param acres The acres
     */
    public void setAcres(String acres) {
        this.acres = acres;
    }

    /**
     * @return The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return The listOnMarket
     */
    public String getListOnMarket() {
        return listOnMarket;
    }

    /**
     * @param listOnMarket The list_on_market
     */
    public void setListOnMarket(String listOnMarket) {
        this.listOnMarket = listOnMarket;
    }

    /**
     * @return The lastSoldPrice
     */
    public String getLastSoldPrice() {
        return lastSoldPrice;
    }

    /**
     * @param lastSoldPrice The last_sold_price
     */
    public void setLastSoldPrice(String lastSoldPrice) {
        this.lastSoldPrice = lastSoldPrice;
    }

    /**
     * @return The yearBuilt
     */
    public Integer getYearBuilt() {
        return yearBuilt;
    }

    /**
     * @param yearBuilt The year_built
     */
    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    /**
     * @return The recordedMortgage
     */
    public String getRecordedMortgage() {
        return recordedMortgage;
    }

    /**
     * @param recordedMortgage The recorded_mortgage
     */
    public void setRecordedMortgage(String recordedMortgage) {
        this.recordedMortgage = recordedMortgage;
    }

    /**
     * @return The assesedValue
     */
    public String getAssesedValue() {
        return assesedValue;
    }

    /**
     * @param assesedValue The assesed_value
     */
    public void setAssesedValue(String assesedValue) {
        this.assesedValue = assesedValue;
    }

    /**
     * @return The currentTaxes
     */
    public String getCurrentTaxes() {
        return currentTaxes;
    }

    /**
     * @param currentTaxes The current_taxes
     */
    public void setCurrentTaxes(String currentTaxes) {
        this.currentTaxes = currentTaxes;
    }

    /**
     * @return The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks The remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return The homeOwnerFees
     */
    public String getHomeOwnerFees() {
        return homeOwnerFees;
    }

    /**
     * @param homeOwnerFees The home_owner_fees
     */
    public void setHomeOwnerFees(String homeOwnerFees) {
        this.homeOwnerFees = homeOwnerFees;
    }

    /**
     * @return The homeOwnerTotalFees
     */
    public String getHomeOwnerTotalFees() {
        return homeOwnerTotalFees;
    }

    /**
     * @param homeOwnerTotalFees The home_owner_total_fees
     */
    public void setHomeOwnerTotalFees(String homeOwnerTotalFees) {
        this.homeOwnerTotalFees = homeOwnerTotalFees;
    }

    /**
     * @return The otherFees
     */
    public String getOtherFees() {
        return otherFees;
    }

    /**
     * @param otherFees The other_fees
     */
    public void setOtherFees(String otherFees) {
        this.otherFees = otherFees;
    }

    /**
     * @return The melloRoosFee
     */
    public String getMelloRoosFee() {
        return melloRoosFee;
    }

    /**
     * @param melloRoosFee The mello_roos_fee
     */
    public void setMelloRoosFee(String melloRoosFee) {
        this.melloRoosFee = melloRoosFee;
    }

    /**
     * @return The bathsfull
     */
    public String getBathsfull() {
        return bathsfull;
    }

    /**
     * @param bathsfull The bathsfull
     */
    public void setBathsfull(String bathsfull) {
        this.bathsfull = bathsfull;
    }

    /**
     * @return The parkngNonGaragedSpaces
     */
    public String getParkngNonGaragedSpaces() {
        return parkngNonGaragedSpaces;
    }

    /**
     * @param parkngNonGaragedSpaces The parkng_non_garaged_spaces
     */
    public void setParkngNonGaragedSpaces(String parkngNonGaragedSpaces) {
        this.parkngNonGaragedSpaces = parkngNonGaragedSpaces;
    }

    /**
     * @return The estimatedSquareFeet
     */
    public String getEstimatedSquareFeet() {
        return estimatedSquareFeet;
    }

    /**
     * @param estimatedSquareFeet The estimated_square_feet
     */
    public void setEstimatedSquareFeet(String estimatedSquareFeet) {
        this.estimatedSquareFeet = estimatedSquareFeet;
    }

    /**
     * @return The lotSqftApprox
     */
    public String getLotSqftApprox() {
        return lotSqftApprox;
    }

    /**
     * @param lotSqftApprox The lot_sqft_approx
     */
    public void setLotSqftApprox(String lotSqftApprox) {
        this.lotSqftApprox = lotSqftApprox;
    }

    /**
     * @return The garage
     */
    public String getGarage() {
        return garage;
    }

    /**
     * @param garage The garage
     */
    public void setGarage(String garage) {
        this.garage = garage;
    }

    /**
     * @return The parkingSpacesTotal
     */
    public String getParkingSpacesTotal() {
        return parkingSpacesTotal;
    }

    /**
     * @param parkingSpacesTotal The parking_spaces_total
     */
    public void setParkingSpacesTotal(String parkingSpacesTotal) {
        this.parkingSpacesTotal = parkingSpacesTotal;
    }

    /**
     * @return The monthlyTotalFees
     */
    public String getMonthlyTotalFees() {
        return monthlyTotalFees;
    }

    /**
     * @param monthlyTotalFees The monthly_total_fees
     */
    public void setMonthlyTotalFees(String monthlyTotalFees) {
        this.monthlyTotalFees = monthlyTotalFees;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The agentPhone
     */
    public String getAgentPhone() {
        return agentPhone;
    }

    /**
     * @param agentPhone The agent_phone
     */
    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId The agent_id
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return The tourLink
     */
    public String getTourLink() {
        return tourLink;
    }

    /**
     * @param tourLink The tour_link
     */
    public void setTourLink(String tourLink) {
        this.tourLink = tourLink;
    }

    /**
     * @return The mlsName
     */
    public String getMlsName() {
        return mlsName;
    }

    /**
     * @param mlsName The mls_name
     */
    public void setMlsName(String mlsName) {
        this.mlsName = mlsName;
    }

    /**
     * @return The sysId
     */
    public Object getSysId() {
        return sysId;
    }

    /**
     * @param sysId The sys_id
     */
    public void setSysId(Object sysId) {
        this.sysId = sysId;
    }

    /**
     * @return The upgrade1
     */
    public Object getUpgrade1() {
        return upgrade1;
    }

    /**
     * @param upgrade1 The upgrade1
     */
    public void setUpgrade1(Object upgrade1) {
        this.upgrade1 = upgrade1;
    }

    /**
     * @return The upgrade2
     */
    public Object getUpgrade2() {
        return upgrade2;
    }

    /**
     * @param upgrade2 The upgrade2
     */
    public void setUpgrade2(Object upgrade2) {
        this.upgrade2 = upgrade2;
    }

    /**
     * @return The upgrade3
     */
    public Object getUpgrade3() {
        return upgrade3;
    }

    /**
     * @param upgrade3 The upgrade3
     */
    public void setUpgrade3(Object upgrade3) {
        this.upgrade3 = upgrade3;
    }

    /**
     * @return The upgrade4
     */
    public Object getUpgrade4() {
        return upgrade4;
    }

    /**
     * @param upgrade4 The upgrade4
     */
    public void setUpgrade4(Object upgrade4) {
        this.upgrade4 = upgrade4;
    }

    /**
     * @return The greenFeature1
     */
    public Object getGreenFeature1() {
        return greenFeature1;
    }

    /**
     * @param greenFeature1 The green_feature1
     */
    public void setGreenFeature1(Object greenFeature1) {
        this.greenFeature1 = greenFeature1;
    }

    /**
     * @return The greenFeature2
     */
    public Object getGreenFeature2() {
        return greenFeature2;
    }

    /**
     * @param greenFeature2 The green_feature2
     */
    public void setGreenFeature2(Object greenFeature2) {
        this.greenFeature2 = greenFeature2;
    }

    /**
     * @return The greenFeature3
     */
    public Object getGreenFeature3() {
        return greenFeature3;
    }

    /**
     * @param greenFeature3 The green_feature3
     */
    public void setGreenFeature3(Object greenFeature3) {
        this.greenFeature3 = greenFeature3;
    }

    /**
     * @return The greenFeature4
     */
    public Object getGreenFeature4() {
        return greenFeature4;
    }

    /**
     * @param greenFeature4 The green_feature4
     */
    public void setGreenFeature4(Object greenFeature4) {
        this.greenFeature4 = greenFeature4;
    }

    /**
     * @return The upgrade1Pic
     */
    public Object getUpgrade1Pic() {
        return upgrade1Pic;
    }

    /**
     * @param upgrade1Pic The upgrade1_pic
     */
    public void setUpgrade1Pic(Object upgrade1Pic) {
        this.upgrade1Pic = upgrade1Pic;
    }

    /**
     * @return The upgrade2Pic
     */
    public Object getUpgrade2Pic() {
        return upgrade2Pic;
    }

    /**
     * @param upgrade2Pic The upgrade2_pic
     */
    public void setUpgrade2Pic(Object upgrade2Pic) {
        this.upgrade2Pic = upgrade2Pic;
    }

    /**
     * @return The upgrade3Pic
     */
    public Object getUpgrade3Pic() {
        return upgrade3Pic;
    }

    /**
     * @param upgrade3Pic The upgrade3_pic
     */
    public void setUpgrade3Pic(Object upgrade3Pic) {
        this.upgrade3Pic = upgrade3Pic;
    }

    /**
     * @return The upgrade4Pic
     */
    public Object getUpgrade4Pic() {
        return upgrade4Pic;
    }

    /**
     * @param upgrade4Pic The upgrade4_pic
     */
    public void setUpgrade4Pic(Object upgrade4Pic) {
        this.upgrade4Pic = upgrade4Pic;
    }

    /**
     * @return The greenFeature1Pic
     */
    public Object getGreenFeature1Pic() {
        return greenFeature1Pic;
    }

    /**
     * @param greenFeature1Pic The green_feature1_pic
     */
    public void setGreenFeature1Pic(Object greenFeature1Pic) {
        this.greenFeature1Pic = greenFeature1Pic;
    }

    /**
     * @return The greenFeature2Pic
     */
    public Object getGreenFeature2Pic() {
        return greenFeature2Pic;
    }

    /**
     * @param greenFeature2Pic The green_feature2_pic
     */
    public void setGreenFeature2Pic(Object greenFeature2Pic) {
        this.greenFeature2Pic = greenFeature2Pic;
    }

    /**
     * @return The greenFeature3Pic
     */
    public Object getGreenFeature3Pic() {
        return greenFeature3Pic;
    }

    /**
     * @param greenFeature3Pic The green_feature3_pic
     */
    public void setGreenFeature3Pic(Object greenFeature3Pic) {
        this.greenFeature3Pic = greenFeature3Pic;
    }

    /**
     * @return The greenFeature4Pic
     */
    public Object getGreenFeature4Pic() {
        return greenFeature4Pic;
    }

    /**
     * @param greenFeature4Pic The green_feature4_pic
     */
    public void setGreenFeature4Pic(Object greenFeature4Pic) {
        this.greenFeature4Pic = greenFeature4Pic;
    }

    /**
     * @return The photos
     */
    public List<String> getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    /**
     * @return The thumbnails
     */
    public List<String> getThumbnails() {
        return thumbnails;
    }

    /**
     * @param thumbnails The thumbnails
     */
    public void setThumbnails(List<String> thumbnails) {
        this.thumbnails = thumbnails;
    }

    /**
     * @return The parcelNum
     */
    public String getParcelNum() {
        return parcelNum;
    }

    /**
     * @param parcelNum The parcel_num
     */
    public void setParcelNum(String parcelNum) {
        this.parcelNum = parcelNum;
    }

    /**
     * @return The avmLow
     */
    public Object getAvmLow() {
        return avmLow;
    }

    /**
     * @param avmLow The avm_low
     */
    public void setAvmLow(Object avmLow) {
        this.avmLow = avmLow;
    }

    /**
     * @return The avmHigh
     */
    public Object getAvmHigh() {
        return avmHigh;
    }

    /**
     * @param avmHigh The avm_high
     */
    public void setAvmHigh(Object avmHigh) {
        this.avmHigh = avmHigh;
    }

    /**
     * @return The soldDate
     */
    public Object getSoldDate() {
        return soldDate;
    }

    /**
     * @param soldDate The sold_date
     */
    public void setSoldDate(Object soldDate) {
        this.soldDate = soldDate;
    }

    /**
     * @return The distance
     */
    public Object getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Object distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.lat);
        dest.writeValue(this.lon);
        dest.writeValue(this.id);
        dest.writeValue(this.userId);
        dest.writeString(this.mlsNum);
        dest.writeString(this.listingClass);
        dest.writeString(this.listingClassType);
        dest.writeString(this.status);
        dest.writeValue(this.forSale);
        dest.writeString(this.address);
        dest.writeString(this.zip);
        dest.writeValue(this.locationRating);
        dest.writeValue(this.conditionRating);
        dest.writeValue(this.bathshalf);
        dest.writeValue(this.fireplaces);
        dest.writeValue(this.bedrooms);
        dest.writeValue(this.optionalBedrooms);
        dest.writeValue(this.baths);
        dest.writeString(this.acres);
        dest.writeValue(this.price);
        dest.writeString(this.listOnMarket);
        dest.writeString(this.lastSoldPrice);
        dest.writeValue(this.yearBuilt);
        dest.writeString(this.recordedMortgage);
        dest.writeString(this.assesedValue);
        dest.writeString(this.currentTaxes);
        dest.writeString(this.remarks);
        dest.writeString(this.homeOwnerFees);
        dest.writeString(this.homeOwnerTotalFees);
        dest.writeString(this.otherFees);
        dest.writeString(this.melloRoosFee);
        dest.writeString(this.bathsfull);
        dest.writeString(this.parkngNonGaragedSpaces);
        dest.writeString(this.estimatedSquareFeet);
        dest.writeString(this.lotSqftApprox);
        dest.writeString(this.garage);
        dest.writeString(this.parkingSpacesTotal);
        dest.writeString(this.monthlyTotalFees);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.agentPhone);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.agentId);
        dest.writeString(this.tourLink);
        dest.writeString(this.mlsName);
        /* dest.writeParcelable(this.sysId, flags);
       dest.writeParcelable(this.upgrade1, flags);
        dest.writeParcelable(this.upgrade2, flags);
        dest.writeParcelable(this.upgrade3, flags);
        dest.writeParcelable(this.upgrade4, flags);
        dest.writeParcelable(this.greenFeature1, flags);
        dest.writeParcelable(this.greenFeature2, flags);
        dest.writeParcelable(this.greenFeature3, flags);
        dest.writeParcelable(this.greenFeature4, flags);
        dest.writeParcelable(this.upgrade1Pic, flags);
        dest.writeParcelable(this.upgrade2Pic, flags);
        dest.writeParcelable(this.upgrade3Pic, flags);
        dest.writeParcelable(this.upgrade4Pic, flags);
        dest.writeParcelable(this.greenFeature1Pic, flags);
        dest.writeParcelable(this.greenFeature2Pic, flags);
        dest.writeParcelable(this.greenFeature3Pic, flags);
        dest.writeParcelable(this.greenFeature4Pic, flags);*/
        dest.writeStringList(this.photos);
        dest.writeStringList(this.thumbnails);
        dest.writeString(this.parcelNum);
     /*   dest.writeParcelable(this.avmLow, flags);
        dest.writeParcelable(this.avmHigh, flags);
        dest.writeParcelable(this.soldDate, flags);
        dest.writeParcelable(this.distance, flags);*/
    }

    public UserJustSoldModel() {
    }

    protected UserJustSoldModel(Parcel in) {
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lon = (Double) in.readValue(Double.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mlsNum = in.readString();
        this.listingClass = in.readString();
        this.listingClassType = in.readString();
        this.status = in.readString();
        this.forSale = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.address = in.readString();
        this.zip = in.readString();
        this.locationRating = (Double) in.readValue(Double.class.getClassLoader());
        this.conditionRating = (Double) in.readValue(Double.class.getClassLoader());
        this.bathshalf = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fireplaces = (Integer) in.readValue(Integer.class.getClassLoader());
        this.bedrooms = (Integer) in.readValue(Integer.class.getClassLoader());
        this.optionalBedrooms = (Integer) in.readValue(Integer.class.getClassLoader());
        this.baths = (Integer) in.readValue(Integer.class.getClassLoader());
        this.acres = in.readString();
        this.price = (Integer) in.readValue(Integer.class.getClassLoader());
        this.listOnMarket = in.readString();
        this.lastSoldPrice = in.readString();
        this.yearBuilt = (Integer) in.readValue(Integer.class.getClassLoader());
        this.recordedMortgage = in.readString();
        this.assesedValue = in.readString();
        this.currentTaxes = in.readString();
        this.remarks = in.readString();
        this.homeOwnerFees = in.readString();
        this.homeOwnerTotalFees = in.readString();
        this.otherFees = in.readString();
        this.melloRoosFee = in.readString();
        this.bathsfull = in.readString();
        this.parkngNonGaragedSpaces = in.readString();
        this.estimatedSquareFeet = in.readString();
        this.lotSqftApprox = in.readString();
        this.garage = in.readString();
        this.parkingSpacesTotal = in.readString();
        this.monthlyTotalFees = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.agentPhone = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.agentId = in.readString();
        this.tourLink = in.readString();
        this.mlsName = in.readString();
        /*this.sysId = in.readParcelable(Object.class.getClassLoader());
        this.upgrade1 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade2 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade3 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade4 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature1 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature2 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature3 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature4 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade1Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade2Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade3Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade4Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature1Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature2Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature3Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature4Pic = in.readParcelable(Object.class.getClassLoader());*/
        this.photos = in.createStringArrayList();
        this.thumbnails = in.createStringArrayList();
        this.parcelNum = in.readString();
      /*  this.avmLow = in.readParcelable(Object.class.getClassLoader());
        this.avmHigh = in.readParcelable(Object.class.getClassLoader());
        this.soldDate = in.readParcelable(Object.class.getClassLoader());
        this.distance = in.readParcelable(Object.class.getClassLoader());*/
    }

    public static final Parcelable.Creator<UserJustSoldModel> CREATOR = new Parcelable.Creator<UserJustSoldModel>() {
        public UserJustSoldModel createFromParcel(Parcel source) {
            return new UserJustSoldModel(source);
        }

        public UserJustSoldModel[] newArray(int size) {
            return new UserJustSoldModel[size];
        }
    };
}
