package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import us.homebuzz.homebuzz.domain.data.User;

/**
 * Created by amit.singh on 10/15/2015.
 */
public class UserNearbyModel implements Parcelable {

    private Integer id;

    private User user;

    private Object mlsNum;

    private Object listingClass;

    private Object listingClassType;

    private String status;

    private Boolean forSale;

    private Double lat;

    private Double lon;

    private String address;

    private String zip;

    private List<String> photos = new ArrayList<String>();

    private List<String> thumbnails = new ArrayList<String>();

    private Double locationRating;

    private Float conditionRating;

    private Integer bathshalf;

    private Integer fireplaces;

    private Integer bedrooms;

    private Object optionalBedrooms;

    private Integer baths;

    private Double acres;

    private Double price;

    private String listOnMarket;

    private Object lastSoldPrice;

    private Integer yearBuilt;

    private Object recordedMortgage;

    private Object assesedValue;

    private Object currentTaxes;

    private String remarks;

    private Object homeOwnerFees;

    private Object homeOwnerTotalFees;

    private Object otherFees;

    private Object melloRoosFee;

    private Object bathsfull;

    private Object parkngNonGaragedSpaces;

    private Object estimatedSquareFeet;

    private Object lotSqftApprox;

    private Object garage;

    private Object agentPhone;


    private Object parkingSpacesTotal;

    private Object monthlyTotalFees;

    private String createdAt;

    private String updatedAt;

    private String street;

    private String city;

    private String state;

    private String agentId;

    private String tourLink;


    private Object mlsName;

    private Object sysId;

    private Integer userId;

    private Object upgrade1;

    private Object upgrade2;

    private Object upgrade3;

    private Object upgrade4;

    private Object upgrade1Pic;

    private Object upgrade2Pic;

    private Object upgrade3Pic;

    private Object upgrade4Pic;

    private Object greenFeature1;
    private Object greenFeature2;

    private Object greenFeature3;

    private Object greenFeature4;

    private Object greenFeature1Pic;

    private Object greenFeature2Pic;

    private Object greenFeature3Pic;

    private Object greenFeature4Pic;

    private Boolean favorited;

    boolean isEstimated;
    private String estimates_count;

    private String reviews_count;
    private String checkin_count;

    public String getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(String reviews_count) {
        this.reviews_count = reviews_count;
    }

    public String getEstimates_count() {
        return estimates_count;
    }

    public void setEstimates_count(String estimates_count) {
        this.estimates_count = estimates_count;
    }
    public String getCheckin_count() {
        return checkin_count;
    }

    public void setCheckin_count(String checkin_count) {
        this.checkin_count = checkin_count;
    }

    private List<Estimate> estimates = new ArrayList<Estimate>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The mlsNum
     */
    public Object getMlsNum() {
        return mlsNum;
    }

    /**
     * @param mlsNum The mls_num
     */
    public void setMlsNum(Object mlsNum) {
        this.mlsNum = mlsNum;
    }

    /**
     * @return The listingClass
     */
    public Object getListingClass() {
        return listingClass;
    }

    /**
     * @param listingClass The listing_class
     */
    public void setListingClass(Object listingClass) {
        this.listingClass = listingClass;
    }

    /**
     * @return The listingClassType
     */
    public Object getListingClassType() {
        return listingClassType;
    }

    /**
     * @param listingClassType The listing_class_type
     */
    public void setListingClassType(Object listingClassType) {
        this.listingClassType = listingClassType;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The forSale
     */
    public Boolean getForSale() {
        return forSale;
    }

    /**
     * @param forSale The for_sale
     */
    public void setForSale(Boolean forSale) {
        this.forSale = forSale;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip The zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return The photos
     */
    public List<String> getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    /**
     * @return The thumbnails
     */
    public List<String> getThumbnails() {
        return thumbnails;
    }

    /**
     * @param thumbnails The thumbnails
     */
    public void setThumbnails(List<String> thumbnails) {
        this.thumbnails = thumbnails;
    }

    /**
     * @return The locationRating
     */
    public Double getLocationRating() {
        return locationRating;
    }

    /**
     * @param locationRating The location_rating
     */
    public void setLocationRating(Double locationRating) {
        this.locationRating = locationRating;
    }

    /**
     * @return The conditionRating
     */
    public Float getConditionRating() {
        return conditionRating;
    }

    /**
     * @param conditionRating The condition_rating
     */
    public void setConditionRating(Float conditionRating) {
        this.conditionRating = conditionRating;
    }

    /**
     * @return The bathshalf
     */
    public Integer getBathshalf() {
        return bathshalf;
    }

    /**
     * @param bathshalf The bathshalf
     */
    public void setBathshalf(Integer bathshalf) {
        this.bathshalf = bathshalf;
    }

    /**
     * @return The fireplaces
     */
    public Integer getFireplaces() {
        return fireplaces;
    }

    /**
     * @param fireplaces The fireplaces
     */
    public void setFireplaces(Integer fireplaces) {
        this.fireplaces = fireplaces;
    }

    /**
     * @return The bedrooms
     */
    public Integer getBedrooms() {
        return bedrooms;
    }

    /**
     * @param bedrooms The bedrooms
     */
    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;

    }

    /**
     * @return The optionalBedrooms
     */
    public Object getOptionalBedrooms() {
        return optionalBedrooms;
    }

    /**
     * @param optionalBedrooms The optional_bedrooms
     */
    public void setOptionalBedrooms(Object optionalBedrooms) {
        this.optionalBedrooms = optionalBedrooms;
    }

    /**
     * @return The baths
     */
    public Integer getBaths() {
        return baths;
    }

    /**
     * @param baths The baths
     */
    public void setBaths(Integer baths) {
        this.baths = baths;
    }

    /**
     * @return The acres
     */
    public Double getAcres() {
        return acres;
    }

    /**
     * @param acres The acres
     */
    public void setAcres(Double acres) {
        this.acres = acres;
    }

    /**
     * @return The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return The listOnMarket
     */
    public String getListOnMarket() {
        return listOnMarket;
    }

    /**
     * @param listOnMarket The list_on_market
     */
    public void setListOnMarket(String listOnMarket) {
        this.listOnMarket = listOnMarket;
    }

    /**
     * @return The lastSoldPrice
     */
    public Object getLastSoldPrice() {
        return lastSoldPrice;
    }

    /**
     * @param lastSoldPrice The last_sold_price
     */
    public void setLastSoldPrice(Object lastSoldPrice) {
        this.lastSoldPrice = lastSoldPrice;
    }

    /**
     * @return The yearBuilt
     */
    public Integer getYearBuilt() {
        return yearBuilt;
    }

    /**
     * @param yearBuilt The year_built
     */
    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    /**
     * @return The recordedMortgage
     */
    public Object getRecordedMortgage() {
        return recordedMortgage;
    }

    /**
     * @param recordedMortgage The recorded_mortgage
     */
    public void setRecordedMortgage(Object recordedMortgage) {
        this.recordedMortgage = recordedMortgage;
    }

    /**
     * @return The assesedValue
     */
    public Object getAssesedValue() {
        return assesedValue;
    }

    /**
     * @param assesedValue The assesed_value
     */
    public void setAssesedValue(Object assesedValue) {
        this.assesedValue = assesedValue;
    }

    /**
     * @return The currentTaxes
     */
    public Object getCurrentTaxes() {
        return currentTaxes;
    }

    /**
     * @param currentTaxes The current_taxes
     */
    public void setCurrentTaxes(Object currentTaxes) {
        this.currentTaxes = currentTaxes;
    }

    /**
     * @return The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks The remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return The homeOwnerFees
     */
    public Object getHomeOwnerFees() {
        return homeOwnerFees;
    }

    /**
     * @param homeOwnerFees The home_owner_fees
     */
    public void setHomeOwnerFees(Object homeOwnerFees) {
        this.homeOwnerFees = homeOwnerFees;
    }

    /**
     * @return The homeOwnerTotalFees
     */
    public Object getHomeOwnerTotalFees() {
        return homeOwnerTotalFees;
    }

    /**
     * @param homeOwnerTotalFees The home_owner_total_fees
     */
    public void setHomeOwnerTotalFees(Object homeOwnerTotalFees) {
        this.homeOwnerTotalFees = homeOwnerTotalFees;
    }

    /**
     * @return The otherFees
     */
    public Object getOtherFees() {
        return otherFees;
    }

    /**
     * @param otherFees The other_fees
     */
    public void setOtherFees(Object otherFees) {
        this.otherFees = otherFees;
    }

    /**
     * @return The melloRoosFee
     */
    public Object getMelloRoosFee() {
        return melloRoosFee;
    }

    /**
     * @param melloRoosFee The mello_roos_fee
     */
    public void setMelloRoosFee(Object melloRoosFee) {
        this.melloRoosFee = melloRoosFee;
    }

    /**
     * @return The bathsfull
     */
    public Object getBathsfull() {
        return bathsfull;
    }

    /**
     * @param bathsfull The bathsfull
     */
    public void setBathsfull(Object bathsfull) {
        this.bathsfull = bathsfull;
    }

    /**
     * @return The parkngNonGaragedSpaces
     */
    public Object getParkngNonGaragedSpaces() {
        return parkngNonGaragedSpaces;
    }

    /**
     * @param parkngNonGaragedSpaces The parkng_non_garaged_spaces
     */
    public void setParkngNonGaragedSpaces(Object parkngNonGaragedSpaces) {
        this.parkngNonGaragedSpaces = parkngNonGaragedSpaces;
    }

    /**
     * @return The estimatedSquareFeet
     */
    public Object getEstimatedSquareFeet() {
        return estimatedSquareFeet;
    }

    /**
     * @param estimatedSquareFeet The estimated_square_feet
     */
    public void setEstimatedSquareFeet(Object estimatedSquareFeet) {
        this.estimatedSquareFeet = estimatedSquareFeet;
    }

    /**
     * @return The lotSqftApprox
     */
    public Object getLotSqftApprox() {
        return lotSqftApprox;
    }

    /**
     * @param lotSqftApprox The lot_sqft_approx
     */
    public void setLotSqftApprox(Object lotSqftApprox) {
        this.lotSqftApprox = lotSqftApprox;
    }

    /**
     * @return The garage
     */
    public Object getGarage() {
        return garage;
    }

    /**
     * @param garage The garage
     */
    public void setGarage(Object garage) {
        this.garage = garage;
    }

    /**
     * @return The agentPhone
     */
    public Object getAgentPhone() {
        return agentPhone;
    }

    /**
     * @param agentPhone The agent_phone
     */
    public void setAgentPhone(Object agentPhone) {
        this.agentPhone = agentPhone;
    }

    /**
     * @return The parkingSpacesTotal
     */
    public Object getParkingSpacesTotal() {
        return parkingSpacesTotal;
    }

    /**
     * @param parkingSpacesTotal The parking_spaces_total
     */
    public void setParkingSpacesTotal(Object parkingSpacesTotal) {
        this.parkingSpacesTotal = parkingSpacesTotal;
    }

    /**
     * @return The monthlyTotalFees
     */
    public Object getMonthlyTotalFees() {
        return monthlyTotalFees;
    }

    /**
     * @param monthlyTotalFees The monthly_total_fees
     */
    public void setMonthlyTotalFees(Object monthlyTotalFees) {
        this.monthlyTotalFees = monthlyTotalFees;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId The agent_id
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return The tourLink
     */
    public String getTourLink() {
        return tourLink;
    }

    /**
     * @param tourLink The tour_link
     */
    public void setTourLink(String tourLink) {
        this.tourLink = tourLink;
    }

    /**
     * @return The mlsName
     */
    public Object getMlsName() {
        return mlsName;
    }

    /**
     * @param mlsName The mls_name
     */
    public void setMlsName(Object mlsName) {
        this.mlsName = mlsName;
    }

    /**
     * @return The sysId
     */
    public Object getSysId() {
        return sysId;
    }

    /**
     * @param sysId The sys_id
     */
    public void setSysId(Object sysId) {
        this.sysId = sysId;
    }

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The upgrade1
     */
    public Object getUpgrade1() {
        return upgrade1;
    }

    /**
     * @param upgrade1 The upgrade1
     */
    public void setUpgrade1(Object upgrade1) {
        this.upgrade1 = upgrade1;
    }

    /**
     * @return The upgrade2
     */
    public Object getUpgrade2() {
        return upgrade2;
    }

    /**
     * @param upgrade2 The upgrade2
     */
    public void setUpgrade2(Object upgrade2) {
        this.upgrade2 = upgrade2;
    }

    /**
     * @return The upgrade3
     */
    public Object getUpgrade3() {
        return upgrade3;
    }

    /**
     * @param upgrade3 The upgrade3
     */
    public void setUpgrade3(Object upgrade3) {
        this.upgrade3 = upgrade3;
    }

    /**
     * @return The upgrade4
     */
    public Object getUpgrade4() {
        return upgrade4;
    }

    /**
     * @param upgrade4 The upgrade4
     */
    public void setUpgrade4(Object upgrade4) {
        this.upgrade4 = upgrade4;
    }

    /**
     * @return The upgrade1Pic
     */
    public Object getUpgrade1Pic() {
        return upgrade1Pic;
    }

    /**
     * @param upgrade1Pic The upgrade1_pic
     */
    public void setUpgrade1Pic(Object upgrade1Pic) {
        this.upgrade1Pic = upgrade1Pic;
    }

    /**
     * @return The upgrade2Pic
     */
    public Object getUpgrade2Pic() {
        return upgrade2Pic;
    }

    /**
     * @param upgrade2Pic The upgrade2_pic
     */
    public void setUpgrade2Pic(Object upgrade2Pic) {
        this.upgrade2Pic = upgrade2Pic;
    }

    /**
     * @return The upgrade3Pic
     */
    public Object getUpgrade3Pic() {
        return upgrade3Pic;
    }

    /**
     * @param upgrade3Pic The upgrade3_pic
     */
    public void setUpgrade3Pic(Object upgrade3Pic) {
        this.upgrade3Pic = upgrade3Pic;
    }

    /**
     * @return The upgrade4Pic
     */
    public Object getUpgrade4Pic() {
        return upgrade4Pic;
    }

    /**
     * @param upgrade4Pic The upgrade4_pic
     */
    public void setUpgrade4Pic(Object upgrade4Pic) {
        this.upgrade4Pic = upgrade4Pic;
    }

    /**
     * @return The greenFeature1
     */
    public Object getGreenFeature1() {
        return greenFeature1;
    }

    /**
     * @param greenFeature1 The green_feature1
     */
    public void setGreenFeature1(Object greenFeature1) {
        this.greenFeature1 = greenFeature1;
    }

    /**
     * @return The greenFeature2
     */
    public Object getGreenFeature2() {
        return greenFeature2;
    }

    /**
     * @param greenFeature2 The green_feature2
     */
    public void setGreenFeature2(Object greenFeature2) {
        this.greenFeature2 = greenFeature2;
    }

    /**
     * @return The greenFeature3
     */
    public Object getGreenFeature3() {
        return greenFeature3;
    }

    /**
     * @param greenFeature3 The green_feature3
     */
    public void setGreenFeature3(Object greenFeature3) {
        this.greenFeature3 = greenFeature3;
    }

    /**
     * @return The greenFeature4
     */
    public Object getGreenFeature4() {
        return greenFeature4;
    }

    /**
     * @param greenFeature4 The green_feature4
     */
    public void setGreenFeature4(Object greenFeature4) {
        this.greenFeature4 = greenFeature4;
    }

    /**
     * @return The greenFeature1Pic
     */
    public Object getGreenFeature1Pic() {
        return greenFeature1Pic;
    }

    /**
     * @param greenFeature1Pic The green_feature1_pic
     */
    public void setGreenFeature1Pic(Object greenFeature1Pic) {
        this.greenFeature1Pic = greenFeature1Pic;
    }

    /**
     * @return The greenFeature2Pic
     */
    public Object getGreenFeature2Pic() {
        return greenFeature2Pic;
    }

    /**
     * @param greenFeature2Pic The green_feature2_pic
     */
    public void setGreenFeature2Pic(Object greenFeature2Pic) {
        this.greenFeature2Pic = greenFeature2Pic;
    }

    /**
     * @return The greenFeature3Pic
     */
    public Object getGreenFeature3Pic() {
        return greenFeature3Pic;
    }

    /**
     * @param greenFeature3Pic The green_feature3_pic
     */
    public void setGreenFeature3Pic(Object greenFeature3Pic) {
        this.greenFeature3Pic = greenFeature3Pic;
    }

    /**
     * @return The greenFeature4Pic
     */
    public Object getGreenFeature4Pic() {
        return greenFeature4Pic;
    }

    /**
     * @param greenFeature4Pic The green_feature4_pic
     */
    public void setGreenFeature4Pic(Object greenFeature4Pic) {
        this.greenFeature4Pic = greenFeature4Pic;
    }

    /**
     * @return The favorited
     */
    public Boolean getFavorited() {
        return favorited;
    }

    /**
     * @param favorited The favorited
     */
    public void setFavorited(Boolean favorited) {
        this.favorited = favorited;
    }

    public void setIsEstimated(boolean isEstimated) {
        this.isEstimated = isEstimated;
    }

    public boolean getIsEstimated() {
        return this.isEstimated;
    }


  /*  *//**
     *
     * @return
     * The previousReviews
     *//*
    public List<PreviousReview> getPreviousReviews() {
        return previousReviews;
    }

    */

    /**
     *
     *//*
    public void setPreviousReviews(List<PreviousReview> previousReviews) {
        this.previousReviews = previousReviews;
    }

    /**
     *
     * @return
     * The estimates
     */
    public List<Estimate> getEstimates() {
        return estimates;
    }

    /**
     * @param estimates The estimates
     */
    public void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable((Parcelable) this.user, flags);
        dest.writeParcelable((Parcelable) this.mlsNum, flags);
        dest.writeParcelable((Parcelable) this.listingClass, flags);
        dest.writeParcelable((Parcelable) this.listingClassType, flags);
        dest.writeString(this.status);
        dest.writeValue(this.forSale);
        dest.writeValue(this.lat);
        dest.writeValue(this.lon);
        dest.writeString(this.address);
        dest.writeString(this.zip);
        dest.writeStringList(this.photos);
        dest.writeStringList(this.thumbnails);
        dest.writeValue(this.locationRating);
        dest.writeValue(this.conditionRating);
        dest.writeValue(this.bathshalf);
        dest.writeValue((this.fireplaces != null) ? this.fireplaces : 0);
        dest.writeValue(this.bedrooms);

        //dest.writeParcelable((Parcelable) this.optionalBedrooms, flags);
        dest.writeValue(this.optionalBedrooms);
        dest.writeValue(this.baths);
        dest.writeValue(this.acres);
        dest.writeValue(this.price);
        dest.writeString(this.listOnMarket);
        dest.writeDouble((this.lastSoldPrice != null) ? (Double) this.lastSoldPrice : 0.0);
        dest.writeValue(this.yearBuilt);
        dest.writeParcelable((Parcelable) this.recordedMortgage, flags);
        dest.writeParcelable((Parcelable) this.assesedValue, flags);
        dest.writeParcelable((Parcelable) this.currentTaxes, flags);
        dest.writeString(this.remarks);
        dest.writeParcelable((Parcelable) this.homeOwnerFees, flags);
        dest.writeParcelable((Parcelable) this.homeOwnerTotalFees, flags);
        dest.writeParcelable((Parcelable) this.otherFees, flags);
        dest.writeParcelable((Parcelable) this.melloRoosFee, flags);
        dest.writeParcelable((Parcelable) this.bathsfull, flags);
        dest.writeParcelable((Parcelable) this.parkngNonGaragedSpaces, flags);
        //dest.writeValue((Double.valueOf((Double) this.estimatedSquareFeet)));
        dest.writeDouble((this.lotSqftApprox != null) ? (Double) this.lotSqftApprox : 0.0);
        //dest.writeParcelable((Parcelable) this.garage, flags);
        dest.writeParcelable((Parcelable) this.agentPhone, flags);
        dest.writeParcelable((Parcelable) this.parkingSpacesTotal, flags);
        dest.writeParcelable((Parcelable) this.monthlyTotalFees, flags);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.agentId);
        dest.writeString(this.tourLink);
        dest.writeParcelable((Parcelable) this.mlsName, flags);
        dest.writeParcelable((Parcelable) this.sysId, flags);
        dest.writeValue(this.userId);
        dest.writeParcelable((Parcelable) this.upgrade1, flags);
        dest.writeParcelable((Parcelable) this.upgrade2, flags);
        dest.writeParcelable((Parcelable) this.upgrade3, flags);
        dest.writeParcelable((Parcelable) this.upgrade4, flags);
        dest.writeParcelable((Parcelable) this.upgrade1Pic, flags);
        dest.writeParcelable((Parcelable) this.upgrade2Pic, flags);
        dest.writeParcelable((Parcelable) this.upgrade3Pic, flags);
        dest.writeParcelable((Parcelable) this.upgrade4Pic, flags);
        dest.writeParcelable((Parcelable) this.greenFeature1, flags);
        dest.writeParcelable((Parcelable) this.greenFeature2, flags);
        dest.writeParcelable((Parcelable) this.greenFeature3, flags);
        dest.writeParcelable((Parcelable) this.greenFeature4, flags);
        dest.writeParcelable((Parcelable) this.greenFeature1Pic, flags);
        dest.writeParcelable((Parcelable) this.greenFeature2Pic, flags);
        dest.writeParcelable((Parcelable) this.greenFeature3Pic, flags);
        dest.writeParcelable((Parcelable) this.greenFeature4Pic, flags);
        dest.writeValue(this.favorited);
        dest.writeByte(isEstimated ? (byte) 1 : (byte) 0);
        dest.writeString(this.estimates_count);
        dest.writeString(this.reviews_count);
        //dest.writeList((this.estimates!=null)?this.estimates:null);
    }

    public UserNearbyModel() {
    }

    protected UserNearbyModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.user = in.readParcelable(User.class.getClassLoader());
        this.mlsNum = in.readParcelable(Object.class.getClassLoader());
        this.listingClass = in.readParcelable(Object.class.getClassLoader());
        this.listingClassType = in.readParcelable(Object.class.getClassLoader());
        this.status = in.readString();
        this.forSale = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lon = (Double) in.readValue(Double.class.getClassLoader());
        this.address = in.readString();
        this.zip = in.readString();
        this.photos = in.createStringArrayList();
        this.thumbnails = in.createStringArrayList();
        this.locationRating = (Double) in.readValue(Integer.class.getClassLoader());
        this.conditionRating = (float) in.readValue(Integer.class.getClassLoader());
        this.bathshalf = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fireplaces = (Integer) in.readValue(Integer.class.getClassLoader());
        this.bedrooms = (Integer) in.readValue(Integer.class.getClassLoader());
        this.optionalBedrooms = in.readParcelable(Object.class.getClassLoader());
        this.baths = (Integer) in.readValue(Integer.class.getClassLoader());
        this.acres = (Double) in.readValue(Double.class.getClassLoader());
        this.price = (Double) in.readValue(Double.class.getClassLoader());
        this.listOnMarket = in.readString();
        this.lastSoldPrice = in.readParcelable(Object.class.getClassLoader());
        this.yearBuilt = (Integer) in.readValue(Integer.class.getClassLoader());
        this.recordedMortgage = in.readParcelable(Object.class.getClassLoader());
        this.assesedValue = in.readParcelable(Object.class.getClassLoader());
        this.currentTaxes = in.readParcelable(Object.class.getClassLoader());
        this.remarks = in.readString();
        this.homeOwnerFees = in.readParcelable(Object.class.getClassLoader());
        this.homeOwnerTotalFees = in.readParcelable(Object.class.getClassLoader());
        this.otherFees = in.readParcelable(Object.class.getClassLoader());
        this.melloRoosFee = in.readParcelable(Object.class.getClassLoader());
        this.bathsfull = in.readParcelable(Object.class.getClassLoader());
        this.parkngNonGaragedSpaces = in.readParcelable(Object.class.getClassLoader());
        this.estimatedSquareFeet = in.readParcelable(Object.class.getClassLoader());
        this.lotSqftApprox = in.readParcelable(Double.class.getClassLoader());
        this.garage = in.readParcelable(Object.class.getClassLoader());
        this.agentPhone = in.readParcelable(Object.class.getClassLoader());
        this.parkingSpacesTotal = in.readParcelable(Object.class.getClassLoader());
        this.monthlyTotalFees = in.readParcelable(Object.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.agentId = in.readString();
        this.tourLink = in.readString();
        this.mlsName = in.readParcelable(Object.class.getClassLoader());
        this.sysId = in.readParcelable(Object.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.upgrade1 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade2 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade3 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade4 = in.readParcelable(Object.class.getClassLoader());
        this.upgrade1Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade2Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade3Pic = in.readParcelable(Object.class.getClassLoader());
        this.upgrade4Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature1 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature2 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature3 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature4 = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature1Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature2Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature3Pic = in.readParcelable(Object.class.getClassLoader());
        this.greenFeature4Pic = in.readParcelable(Object.class.getClassLoader());
        this.favorited = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.isEstimated = in.readByte() != 0;
        this.estimates = in.readArrayList(UserNearbyModel.Estimate.class.getClassLoader());
        this.estimates_count = in.readString();
        this.reviews_count = in.readString();

    }

    public static final Parcelable.Creator<UserNearbyModel> CREATOR = new Parcelable.Creator<UserNearbyModel>() {
        public UserNearbyModel createFromParcel(Parcel source) {
            return new UserNearbyModel(source);
        }

        public UserNearbyModel[] newArray(int size) {
            return new UserNearbyModel[size];
        }
    };

    public class Estimate {

        private Integer listingId;

        private Integer id;

        private Integer userId;

        private Double locationRating;

        private Double conditionRating;

        private Double price;

        private String reason;

        private Object accurate;

        private String createdAt;

        private String updatedAt;

        private List<String> imageUrl = new ArrayList<String>();
        private String name;
        private String image_url;

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The listingId
         */
        public Integer getListingId() {
            return listingId;
        }

        /**
         * @param listingId The listing_id
         */
        public void setListingId(Integer listingId) {
            this.listingId = listingId;
        }

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The user_id
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The locationRating
         */
        public Double getLocationRating() {
            return locationRating;
        }

        /**
         * @param locationRating The location_rating
         */
        public void setLocationRating(Double locationRating) {
            this.locationRating = locationRating;
        }

        /**
         * @return The conditionRating
         */
        public Double getConditionRating() {
            return conditionRating;
        }

        /**
         * @param conditionRating The condition_rating
         */
        public void setConditionRating(Double conditionRating) {
            this.conditionRating = conditionRating;
        }

        /**
         * @return The price
         */
        public Double getPrice() {
            return price;
        }

        /**
         * @param price The price
         */
        public void setPrice(Double price) {
            this.price = price;
        }

        /**
         * @return The reason
         */
        public String getReason() {
            return reason;
        }

        /**
         * @param reason The reason
         */
        public void setReason(String reason) {
            this.reason = reason;
        }

        /**
         * @return The accurate
         */
        public Object getAccurate() {
            return accurate;
        }

        /**
         * @param accurate The accurate
         */
        public void setAccurate(Object accurate) {
            this.accurate = accurate;
        }

        /**
         * @return The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The created_at
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         * @param updatedAt The updated_at
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         * @return The imageUrl
         */
        public List<String> getImageUrl() {
            return imageUrl;
        }

        /**
         * @param imageUrl The image_url
         */
        public void setImageUrl(List<String> imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
}


