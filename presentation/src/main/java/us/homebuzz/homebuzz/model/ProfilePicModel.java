package us.homebuzz.homebuzz.model;

/**
 * Created by amit.singh on 12/3/2015.
 */
public class ProfilePicModel {
    private String imageUrl;

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
