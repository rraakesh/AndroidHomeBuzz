package us.homebuzz.homebuzz.model;

/**
 * Created by abhishek.singh on 12/2/2015.
 */
public class AddCheckInPhotoModel {

    private String url;

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
