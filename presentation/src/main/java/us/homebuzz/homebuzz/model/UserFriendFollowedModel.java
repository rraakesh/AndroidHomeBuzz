package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amit.singh on 11/3/2015.
 */
public class UserFriendFollowedModel implements Parcelable {

    private Integer id;

    private String email;

    private Integer signInCount;

    private String currentSignInAt;

    private String lastSignInAt;
/*
    private CurrentSignInIp currentSignInIp;

    private LastSignInIp lastSignInIp;*/

    private Object confirmedAt;

    private String name;

    private String zipCode;

    private String role;

    private String facebookId;

    private Integer estimatesCount;

    private Double accurate;

    private Object firstName;

    private Object lastName;

    private Object subscriptionActive;

    private Boolean confirmed;

    private Boolean followed;

    private String bio;

    private String degree;

    private Object yrGraduated;

    private String college;

    private String otherSchool;

    private String designations;

    private String linkedIn;

    private String website;

    private String profilePic;

    private Object verified;

    private Integer agentId;

    private String phone;

    private String mlsName;

    private String licenseNum;

    private String napNum;

    private String street;

    private String city;

    private String state;

    private Object jobTitle;

    private Object company;
    private String facebookUrl;

    private String twitterUrl;

    private String educations;
    private String senderProfilePic;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The signInCount
     */
    public Integer getSignInCount() {
        return signInCount;
    }

    /**
     *
     * @param signInCount
     * The sign_in_count
     */
    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    /**
     *
     * @return
     * The currentSignInAt
     */
    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    /**
     *
     * @param currentSignInAt
     * The current_sign_in_at
     */
    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    /**
     *
     * @return
     * The lastSignInAt
     */
    public String getLastSignInAt() {
        return lastSignInAt;
    }

    /**
     *
     * @param lastSignInAt
     * The last_sign_in_at
     */
    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

   /* *//**
     *
     * @return
     * The currentSignInIp
     *//*
    public CurrentSignInIp getCurrentSignInIp() {
        return currentSignInIp;
    }

    *//**
     *
     * @param currentSignInIp
     * The current_sign_in_ip
     *//*
    public void setCurrentSignInIp(CurrentSignInIp currentSignInIp) {
        this.currentSignInIp = currentSignInIp;
    }

    *//**
     *
     * @return
     * The lastSignInIp
     *//*
    public LastSignInIp getLastSignInIp() {
        return lastSignInIp;
    }

    *//**
     *
     * @param lastSignInIp
     * The last_sign_in_ip
     *//*
    public void setLastSignInIp(LastSignInIp lastSignInIp) {
        this.lastSignInIp = lastSignInIp;
    }*/

    /**
     *
     * @return
     * The confirmedAt
     */
    public Object getConfirmedAt() {
        return confirmedAt;
    }

    /**
     *
     * @param confirmedAt
     * The confirmed_at
     */
    public void setConfirmedAt(Object confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public String getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The firstName
     */
    public Object getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public Object getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The subscriptionActive
     */
    public Object getSubscriptionActive() {
        return subscriptionActive;
    }

    /**
     *
     * @param subscriptionActive
     * The subscription_active
     */
    public void setSubscriptionActive(Object subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    /**
     *
     * @return
     * The confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     *
     * @param confirmed
     * The confirmed
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     *
     * @return
     * The followed
     */
    public Boolean getFollowed() {
        return followed;
    }

    /**
     *
     * @param followed
     * The followed
     */
    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    /**
     *
     * @return
     * The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     *
     * @param bio
     * The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     *
     * @return
     * The degree
     */
    public String getDegree() {
        return degree;
    }

    /**
     *
     * @param degree
     * The degree
     */
    public void setDegree(String degree) {
        this.degree = degree;
    }

    /**
     *
     * @return
     * The yrGraduated
     */
    public Object getYrGraduated() {
        return yrGraduated;
    }

    /**
     *
     * @param yrGraduated
     * The yr_graduated
     */
    public void setYrGraduated(Object yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    /**
     *
     * @return
     * The college
     */
    public String getCollege() {
        return college;
    }

    /**
     *
     * @param college
     * The college
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     *
     * @return
     * The otherSchool
     */
    public String getOtherSchool() {
        return otherSchool;
    }

    /**
     *
     * @param otherSchool
     * The other_school
     */
    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    /**
     *
     * @return
     * The designations
     */
    public String getDesignations() {
        return designations;
    }

    /**
     *
     * @param designations
     * The designations
     */
    public void setDesignations(String designations) {
        this.designations = designations;
    }

    /**
     *
     * @return
     * The linkedIn
     */
    public String getLinkedIn() {
        return linkedIn;
    }

    /**
     *
     * @param linkedIn
     * The linked_in
     */
    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The verified
     */
    public Object getVerified() {
        return verified;
    }

    /**
     *
     * @param verified
     * The verified
     */
    public void setVerified(Object verified) {
        this.verified = verified;
    }

    /**
     *
     * @return
     * The agentId
     */
    public Integer getAgentId() {
        return agentId;
    }

    /**
     *
     * @param agentId
     * The agent_id
     */
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The mlsName
     */
    public String getMlsName() {
        return mlsName;
    }

    /**
     *
     * @param mlsName
     * The mls_name
     */
    public void setMlsName(String mlsName) {
        this.mlsName = mlsName;
    }

    /**
     *
     * @return
     * The licenseNum
     */
    public String getLicenseNum() {
        return licenseNum;
    }

    /**
     *
     * @param licenseNum
     * The license_num
     */
    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    /**
     *
     * @return
     * The napNum
     */
    public String getNapNum() {
        return napNum;
    }

    /**
     *
     * @param napNum
     * The nap_num
     */
    public void setNapNum(String napNum) {
        this.napNum = napNum;
    }

    /**
     *
     * @return
     * The street
     */
    public String getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The jobTitle
     */
    public Object getJobTitle() {
        return jobTitle;
    }

    /**
     *
     * @param jobTitle
     * The job_title
     */
    public void setJobTitle(Object jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     *
     * @return
     * The company
     */
    public Object getCompany() {
        return company;
    }

    /**
     *
     * @param company
     * The company
     */
    public void setCompany(Object company) {
        this.company = company;
    }

    /**
     *
     * @return
     * The facebookUrl
     */
    public String getFacebookUrl() {
        return facebookUrl;
    }

    /**
     *
     * @param facebookUrl
     * The facebook_url
     */
    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    /**
     *
     * @return
     * The twitterUrl
     */
    public String getTwitterUrl() {
        return twitterUrl;
    }

    /**
     *
     * @param senderProfilePic
     * The twitter_url
     */
    public void setSenderProfilePic(String senderProfilePic) {
        this.senderProfilePic = senderProfilePic;
    }
    /**
     *
     * @return
     * The educations
     */
    public String getSenderProfilePic() {
        return senderProfilePic;
    }


    /**
     *
     * @return
     * The educations
     */
    public String getEducations() {
        return educations;
    }

    /**
     *
     * @param educations
     * The educations
     */
    public void setEducations(String educations) {
        this.educations = educations;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.email);
        dest.writeValue(this.signInCount);
        dest.writeString(this.currentSignInAt);
        dest.writeString(this.lastSignInAt);
        dest.writeParcelable((Parcelable)this.confirmedAt, flags);
        dest.writeString(this.name);
        dest.writeString(this.zipCode);
        dest.writeString(this.role);
        dest.writeString(this.facebookId);
        dest.writeValue(this.estimatesCount);
        dest.writeValue(this.accurate);
        dest.writeParcelable((Parcelable)this.firstName, flags);
        dest.writeParcelable((Parcelable)this.lastName, flags);
        dest.writeParcelable((Parcelable)this.subscriptionActive, flags);
        dest.writeValue(this.confirmed);
        dest.writeValue(this.followed);
        dest.writeString(this.bio);
        dest.writeString(this.degree);
        dest.writeParcelable((Parcelable)this.yrGraduated, flags);
        dest.writeString(this.college);
        dest.writeString(this.otherSchool);
        dest.writeString(this.designations);
        dest.writeString(this.linkedIn);
        dest.writeString(this.website);
        dest.writeString(this.profilePic);
        dest.writeParcelable((Parcelable)this.verified, flags);
        dest.writeValue(this.agentId);
        dest.writeString(this.phone);
        dest.writeString(this.mlsName);
        dest.writeString(this.licenseNum);
        dest.writeString(this.napNum);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeParcelable((Parcelable) this.jobTitle, flags);
        dest.writeParcelable((Parcelable)this.company, flags);
        dest.writeString(this.facebookUrl);
        dest.writeString(this.twitterUrl);
        dest.writeString(this.educations);
        dest.writeString(this.senderProfilePic);
    }

    public UserFriendFollowedModel() {
    }

    protected UserFriendFollowedModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.signInCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.currentSignInAt = in.readString();
        this.lastSignInAt = in.readString();
        this.confirmedAt = in.readParcelable(Object.class.getClassLoader());
        this.name = in.readString();
        this.zipCode = in.readString();
        this.role = in.readString();
        this.facebookId = in.readString();
        this.estimatesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.accurate = (Double) in.readValue(Double.class.getClassLoader());
        this.firstName = in.readParcelable(Object.class.getClassLoader());
        this.lastName = in.readParcelable(Object.class.getClassLoader());
        this.subscriptionActive = in.readParcelable(Object.class.getClassLoader());
        this.confirmed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.followed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.bio = in.readString();
        this.degree = in.readString();
        this.yrGraduated = in.readParcelable(Object.class.getClassLoader());
        this.college = in.readString();
        this.otherSchool = in.readString();
        this.designations = in.readString();
        this.linkedIn = in.readString();
        this.website = in.readString();
        this.profilePic = in.readString();
        this.verified = in.readParcelable(Object.class.getClassLoader());
        this.agentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.phone = in.readString();
        this.mlsName = in.readString();
        this.licenseNum = in.readString();
        this.napNum = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.jobTitle = in.readParcelable(Object.class.getClassLoader());
        this.company = in.readParcelable(Object.class.getClassLoader());
        this.facebookUrl = in.readString();
        this.twitterUrl = in.readString();
        this.educations = in.readString();
        this.senderProfilePic = in.readString();
    }

    public static final Parcelable.Creator<UserFriendFollowedModel> CREATOR = new Parcelable.Creator<UserFriendFollowedModel>() {
        public UserFriendFollowedModel createFromParcel(Parcel source) {
            return new UserFriendFollowedModel(source);
        }

        public UserFriendFollowedModel[] newArray(int size) {
            return new UserFriendFollowedModel[size];
        }
    };
}
