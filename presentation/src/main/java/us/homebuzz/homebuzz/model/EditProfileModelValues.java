package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abhishek.singh on 11/16/2015.
 */
public class EditProfileModelValues implements Parcelable {
    private String email;
    private Integer signInCount;
    private String confirmedAt;
    private String name;
    private String zipCode;
    private String role;
    private String facebookId;
    private Double accurate;
    private Integer points;
    private String teamId;
    private String officeId;
    private String firstName;
    private String lastName;
    private String subscriptionActive;
    private Boolean confirmed;
    private Boolean followed;
    private Integer soldListings;
    private Integer followers;
    private Integer followings;
    private Integer photos;
    private Integer estimatesCount;
    private String educations;

    private String bio;

    private String degree;

    private Integer yrGraduated;

    private String college;

    private String otherSchool;

    private String designations;

    private String linkedIn;

    private String website;

    private String profilePic;

    private String verified;

    private Integer agentId;

    private String createdAt;

    private String updatedAt;

    private String phone;

    private String mlsName;

    private String licenseNum;

    private String napNum;

    private String street;

    private String city;

    private String state;

    private String jobTitle;

    private String company;

    private String facebookUrl;

    private String twitterUrl;
    private String profile_id;
    /**
     * @return The userId
     */
    public String getProfileId() {
        return profile_id;
    }

    /**
     * @param profile_id The user_id
     */
    public void setProfileId(String profile_id) {
        this.profile_id = profile_id;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSignInCount() {
        return signInCount;
    }

    public void setSignInCount(Integer signInCount) {
        this.signInCount = signInCount;
    }

    public String getConfirmedAt() {
        return confirmedAt;
    }

    public void setConfirmedAt(String confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Double getAccurate() {
        return accurate;
    }

    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSubscriptionActive() {
        return subscriptionActive;
    }

    public void setSubscriptionActive(String subscriptionActive) {
        this.subscriptionActive = subscriptionActive;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Boolean getFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public Integer getSoldListings() {
        return soldListings;
    }

    public void setSoldListings(Integer soldListings) {
        this.soldListings = soldListings;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getFollowings() {
        return followings;
    }

    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    public Integer getPhotos() {
        return photos;
    }

    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    public String getEducations() {
        return educations;
    }

    public void setEducations(String educations) {
        this.educations = educations;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Integer getYrGraduated() {
        return yrGraduated;
    }

    public void setYrGraduated(Integer yrGraduated) {
        this.yrGraduated = yrGraduated;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getOtherSchool() {
        return otherSchool;
    }

    public void setOtherSchool(String otherSchool) {
        this.otherSchool = otherSchool;
    }

    public String getDesignations() {
        return designations;
    }

    public void setDesignations(String designations) {
        this.designations = designations;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMlsName() {
        return mlsName;
    }

    public void setMlsName(String mlsName) {
        this.mlsName = mlsName;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getNapNum() {
        return napNum;
    }

    public void setNapNum(String napNum) {
        this.napNum = napNum;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeValue(this.signInCount);
        dest.writeString(this.confirmedAt);
        dest.writeString(this.name);
        dest.writeString(this.zipCode);
        dest.writeString(this.role);
        dest.writeString(this.facebookId);
        dest.writeValue(this.accurate);
        dest.writeValue(this.points);
        dest.writeString(this.teamId);
        dest.writeString(this.officeId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.subscriptionActive);
        dest.writeValue(this.confirmed);
        dest.writeValue(this.followed);
        dest.writeValue(this.soldListings);
        dest.writeValue(this.followers);
        dest.writeValue(this.followings);
        dest.writeValue(this.photos);
        dest.writeValue(this.estimatesCount);
        dest.writeString(this.educations);
        dest.writeString(this.bio);
        dest.writeString(this.degree);
        dest.writeValue(this.yrGraduated);
        dest.writeString(this.college);
        dest.writeString(this.otherSchool);
        dest.writeString(this.designations);
        dest.writeString(this.linkedIn);
        dest.writeString(this.website);
        dest.writeString(this.profilePic);
        dest.writeString(this.verified);
        dest.writeValue(this.agentId);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.phone);
        dest.writeString(this.mlsName);
        dest.writeString(this.licenseNum);
        dest.writeString(this.napNum);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.jobTitle);
        dest.writeString(this.company);
        dest.writeString(this.facebookUrl);
        dest.writeString(this.twitterUrl);
    }

    public EditProfileModelValues() {
    }

    protected EditProfileModelValues(Parcel in) {
        this.email = in.readString();
        this.signInCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.confirmedAt = in.readString();
        this.name = in.readString();
        this.zipCode = in.readString();
        this.role = in.readString();
        this.facebookId = in.readString();
        this.accurate = (Double) in.readValue(Double.class.getClassLoader());
        this.points = (Integer) in.readValue(Integer.class.getClassLoader());
        this.teamId = in.readString();
        this.officeId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.subscriptionActive = in.readString();
        this.confirmed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.followed = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.soldListings = (Integer) in.readValue(Integer.class.getClassLoader());
        this.followers = (Integer) in.readValue(Integer.class.getClassLoader());
        this.followings = (Integer) in.readValue(Integer.class.getClassLoader());
        this.photos = (Integer) in.readValue(Integer.class.getClassLoader());
        this.estimatesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.educations = in.readString();
        this.bio = in.readString();
        this.degree = in.readString();
        this.yrGraduated = (Integer) in.readValue(Integer.class.getClassLoader());
        this.college = in.readString();
        this.otherSchool = in.readString();
        this.designations = in.readString();
        this.linkedIn = in.readString();
        this.website = in.readString();
        this.profilePic = in.readString();
        this.verified = in.readString();
        this.agentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.phone = in.readString();
        this.mlsName = in.readString();
        this.licenseNum = in.readString();
        this.napNum = in.readString();
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.jobTitle = in.readString();
        this.company = in.readString();
        this.facebookUrl = in.readString();
        this.twitterUrl = in.readString();
    }

    public static final Parcelable.Creator<EditProfileModelValues> CREATOR = new Parcelable.Creator<EditProfileModelValues>() {
        public EditProfileModelValues createFromParcel(Parcel source) {
            return new EditProfileModelValues(source);
        }

        public EditProfileModelValues[] newArray(int size) {
            return new EditProfileModelValues[size];
        }
    };
}
