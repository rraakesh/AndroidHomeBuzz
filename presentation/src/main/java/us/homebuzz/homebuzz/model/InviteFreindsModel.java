package us.homebuzz.homebuzz.model;

/**
 * Created by rohitkumar on 2/12/15.
 */
public class InviteFreindsModel {
    String message;
    String phone;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
