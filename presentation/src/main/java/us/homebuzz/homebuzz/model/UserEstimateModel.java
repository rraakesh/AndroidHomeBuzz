package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.singh on 10/21/2015.
 */
public class UserEstimateModel implements Parcelable {

    private String zipCode;

    private Integer id;

    private String email;

    private String name;
    private String firstName;

    private String role;

    private Object facebookId;

    private String createdAt;

    private String updatedAt;

    private Integer estimatesCount;

    private Double accurate;

    private Integer sashId;

    private Integer level;

    private Object verificationCode;

    private Object phone;

    private List<String> estimateZips = new ArrayList<String>();

    private Object teamId;

    private Object teamRequest;

    private Object profilePic;

    private Object officeId;

    private Object officeRequest;

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The facebookId
     */
    public Object getFacebookId() {
        return facebookId;
    }

    /**
     *
     * @param facebookId
     * The facebook_id
     */
    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The estimatesCount
     */
    public Integer getEstimatesCount() {
        return estimatesCount;
    }

    /**
     *
     * @param estimatesCount
     * The estimates_count
     */
    public void setEstimatesCount(Integer estimatesCount) {
        this.estimatesCount = estimatesCount;
    }

    /**
     *
     * @return
     * The accurate
     */
    public Double getAccurate() {
        return accurate;
    }

    /**
     *
     * @param accurate
     * The accurate
     */
    public void setAccurate(Double accurate) {
        this.accurate = accurate;
    }

    /**
     *
     * @return
     * The sashId
     */
    public Integer getSashId() {
        return sashId;
    }

    /**
     *
     * @param sashId
     * The sash_id
     */
    public void setSashId(Integer sashId) {
        this.sashId = sashId;
    }

    /**
     *
     * @return
     * The level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     *
     * @param level
     * The level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     *
     * @return
     * The verificationCode
     */
    public Object getVerificationCode() {
        return verificationCode;
    }

    /**
     *
     * @param verificationCode
     * The verification_code
     */
    public void setVerificationCode(Object verificationCode) {
        this.verificationCode = verificationCode;
    }

    /**
     *
     * @return
     * The phone
     */
    public Object getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(Object phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The estimateZips
     */
    public List<String> getEstimateZips() {
        return estimateZips;
    }

    /**
     *
     * @param estimateZips
     * The estimate_zips
     */
    public void setEstimateZips(List<String> estimateZips) {
        this.estimateZips = estimateZips;
    }

    /**
     *
     * @return
     * The teamId
     */
    public Object getTeamId() {
        return teamId;
    }

    /**
     *
     * @param teamId
     * The team_id
     */
    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    /**
     *
     * @return
     * The teamRequest
     */
    public Object getTeamRequest() {
        return teamRequest;
    }

    /**
     *
     * @param teamRequest
     * The team_request
     */
    public void setTeamRequest(Object teamRequest) {
        this.teamRequest = teamRequest;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public Object getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The officeId
     */
    public Object getOfficeId() {
        return officeId;
    }

    /**
     *
     * @param officeId
     * The office_id
     */
    public void setOfficeId(Object officeId) {
        this.officeId = officeId;
    }

    /**
     *
     * @return
     * The officeRequest
     */
    public Object getOfficeRequest() {
        return officeRequest;
    }

    /**
     *
     * @param officeRequest
     * The office_request
     */
    public void setOfficeRequest(Object officeRequest) {
        this.officeRequest = officeRequest;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.zipCode);
        dest.writeValue(this.id);
        dest.writeString(this.email);
        dest.writeString(this.name);
        dest.writeString(this.role);
        dest.writeString(String.valueOf(this.facebookId));
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeValue(this.estimatesCount);
        dest.writeValue(this.accurate);
        dest.writeValue(this.sashId);
        dest.writeValue(this.level);
        dest.writeString(String.valueOf(this.verificationCode));
        dest.writeString(String.valueOf(this.phone));
        dest.writeString(String.valueOf(this.estimateZips));
        dest.writeString(String.valueOf(this.teamId));
        dest.writeString(String.valueOf(this.teamRequest));
        dest.writeString(String.valueOf(this.profilePic));
        dest.writeString(String.valueOf(this.officeId));
        dest.writeString(String.valueOf(this.officeRequest));
    }

    public UserEstimateModel() {
    }

    protected UserEstimateModel(Parcel in) {
        this.zipCode = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.name = in.readString();
        this.role = in.readString();
        this.facebookId = in.readParcelable(Object.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.estimatesCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.accurate = (Double) in.readValue(Double.class.getClassLoader());
        this.sashId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.level = (Integer) in.readValue(Integer.class.getClassLoader());
        this.verificationCode = in.readParcelable(Object.class.getClassLoader());
        this.phone = in.readParcelable(Object.class.getClassLoader());
        this.estimateZips = in.createStringArrayList();
        this.teamId = in.readParcelable(Object.class.getClassLoader());
        this.teamRequest = in.readParcelable(Object.class.getClassLoader());
        this.profilePic = in.readParcelable(Object.class.getClassLoader());
        this.officeId = in.readParcelable(Object.class.getClassLoader());
        this.officeRequest = in.readParcelable(Object.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserEstimateModel> CREATOR = new Parcelable.Creator<UserEstimateModel>() {
        public UserEstimateModel createFromParcel(Parcel source) {
            return new UserEstimateModel(source);
        }

        public UserEstimateModel[] newArray(int size) {
            return new UserEstimateModel[size];
        }
    };
}
