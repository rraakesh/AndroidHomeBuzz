package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amit.singh on 11/18/2015.
 */
public class UserUnreadMessagesModel  implements Parcelable {

    private Integer id;

    private String body;

    private Integer listingId;

    private String status;

    private String createdAt;

    private String updatedAt;

    private Sender sender;

    private Recipient recipient;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The listingId
     */
    public Integer getListingId() {
        return listingId;
    }

    /**
     * @param listingId The listing_id
     */
    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The sender
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * @param sender The sender
     */
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    /**
     * @return The recipient
     */
    public Recipient getRecipient() {
        return recipient;
    }

    /**
     * @param recipient The recipient
     */
    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }


    public class Recipient {

        private Integer id;

        private String email;

        private String name;

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }
    }

    public class Sender {

        private Integer id;

        private String email;

        private String name;

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        private String profile_pic;

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.body);
        dest.writeValue(this.listingId);
        dest.writeString(this.status);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString((String.valueOf(this.sender)));
        dest.writeString((String.valueOf(this.recipient)));
    }

    public UserUnreadMessagesModel() {
    }

    private UserUnreadMessagesModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.body = in.readString();
        this.listingId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.sender = in.readParcelable(Sender.class.getClassLoader());
        this.recipient = in.readParcelable(Recipient.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserUnreadMessagesModel> CREATOR = new Parcelable.Creator<UserUnreadMessagesModel>() {
        public UserUnreadMessagesModel createFromParcel(Parcel source) {
            return new UserUnreadMessagesModel(source);
        }

        public UserUnreadMessagesModel[] newArray(int size) {
            return new UserUnreadMessagesModel[size];
        }
    };
}
