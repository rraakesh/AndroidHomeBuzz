package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amit.singh on 11/27/2015.
 */
public class ZipPurchaseModel implements Parcelable {

    private Integer id;

    private Integer userId;

    private String zipCode;

    private Boolean expired;

    private String expiredAt;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     * The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     * The expired
     */
    public Boolean getExpired() {
        return expired;
    }

    /**
     *
     * @param expired
     * The expired
     */
    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    /**
     *
     * @return
     * The expiredAt
     */
    public String getExpiredAt() {
        return expiredAt;
    }

    /**
     *
     * @param expiredAt
     * The expired_at
     */
    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.userId);
        dest.writeString(this.zipCode);
        dest.writeValue(this.expired);
        dest.writeString(this.expiredAt);
    }

    public ZipPurchaseModel() {
    }

    protected ZipPurchaseModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.zipCode = in.readString();
        this.expired = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.expiredAt = in.readString();
    }

    public static final Parcelable.Creator<ZipPurchaseModel> CREATOR = new Parcelable.Creator<ZipPurchaseModel>() {
        public ZipPurchaseModel createFromParcel(Parcel source) {
            return new ZipPurchaseModel(source);
        }

        public ZipPurchaseModel[] newArray(int size) {
            return new ZipPurchaseModel[size];
        }
    };
}
