/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package us.homebuzz.homebuzz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class that represents a user in the presentation layer.
 */
public class UserModel implements Parcelable {

  private final int userId;

  public UserModel(int userId) {
    this.userId = userId;
  }

  private String coverUrl;
  private String fullName;
  private String email;
  private String description;
  private int followers;

  public int getUserId() {
    return userId;
  }

  public String getCoverUrl() {
    return coverUrl;
  }

  public void setCoverUrl(String coverUrl) {
    this.coverUrl = coverUrl;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getFollowers() {
    return followers;
  }

  public void setFollowers(int followers) {
    this.followers = followers;
  }

  @Override public String toString() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append("***** User Model Details *****\n");
    stringBuilder.append("id=" + this.getUserId() + "\n");
    stringBuilder.append("cover url=" + this.getCoverUrl() + "\n");
    stringBuilder.append("fullname=" + this.getFullName() + "\n");
    stringBuilder.append("email=" + this.getEmail() + "\n");
    stringBuilder.append("description=" + this.getDescription() + "\n");
    stringBuilder.append("followers=" + this.getFollowers() + "\n");
    stringBuilder.append("*******************************");

    return stringBuilder.toString();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.userId);
    dest.writeString(this.coverUrl);
    dest.writeString(this.fullName);
    dest.writeString(this.email);
    dest.writeString(this.description);
    dest.writeInt(this.followers);
  }

  protected UserModel(Parcel in) {
    this.userId = in.readInt();
    this.coverUrl = in.readString();
    this.fullName = in.readString();
    this.email = in.readString();
    this.description = in.readString();
    this.followers = in.readInt();
  }

  public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
    public UserModel createFromParcel(Parcel source) {
      return new UserModel(source);
    }

    public UserModel[] newArray(int size) {
      return new UserModel[size];
    }
  };
}
